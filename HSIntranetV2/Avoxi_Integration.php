<?php
	// ---------------------------------------------------------------------------------------------------------
	//                                      DEFINITION OF NEED IT PARAMETERS
	// ---------------------------------------------------------------------------------------------------------
	// date = 2014-02-02
	// queues = Time_Clock
	// key = DetailsDO.AgentSessions

	$date=( $_GET['date'] );
	$queues=( $_GET['queues'] );
	$key=( $_GET['key'] );
	$url = 'http://10.0.0.10/SmartQueue/QmStats/jsonStatsApi.do';
	$parameters = 'queues='.$queues.'&from='.$date.'.00:00:00&to='.$date.'.23:59:59&block='.$key;

	// ---------------------------------------------------------------------------------------------------------
	//                                             CALL TO THE API
	// ---------------------------------------------------------------------------------------------------------
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
	curl_setopt($ch, CURLOPT_USERPWD, "robot:robot");
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_VERBOSE, 0);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
	$data = curl_exec($ch);
	$curl_errno = curl_errno($ch);
	$curl_error = curl_error($ch);
	echo $data;
	curl_close($ch);
?>
