﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class ASItoDGSmappingSportsModel
    {
        internal static DataTable GetSportsMapping(string lookup)
        {
            string[,] spParameters = {
                                         {"@prmLookup", lookup},
                                     };

            string storedProcedureName = "uSp_Intranet_GetSportsMapping";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_DGS_AddOns, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetLeagues()
        {
            string[,] spParameters = {
                                     };

            string storedProcedureName = "GetLeagues";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_DGSDATA, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetGameTypes()
        {
            string[,] spParameters = {
                                     };

            string storedProcedureName = "GameType_GetList";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_DGSDATA, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable SaveSportMapping(string id, string sportType, string sportSubType, string idLeague, string league, string idGameType, string idSport)
        {
            string[,] spParameters = {
                                         {"@prmId", id},
                                         {"@prmSportType", sportType},
                                         {"@prmSportSubType", sportSubType},
                                         {"@prmIdLeague", idLeague},
                                         {"@prmLeague", league},
                                         {"@prmIdGameType", idGameType},
                                         {"@prmIdSport", idSport}
                                     };

            string storedProcedureName = "uSp_Intranet_SaveSportMapping";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_DGS_AddOns, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetIdSport()
        {
            string[,] spParameters = {
                                     };

            string storedProcedureName = "Sport_GetList";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_DGSDATA, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}