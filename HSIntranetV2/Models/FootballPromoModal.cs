﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class FootballPromoModal
    {
        internal static System.Data.DataTable GetResult(string customerID, string freebetAmount)
        {
            string[,] spParameters = {
                                        { "@prmCustomerID", customerID },
                                        { "@prmFreebetAmount", freebetAmount },
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSp_Local_FootballPromo", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}