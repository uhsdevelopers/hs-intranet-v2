﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Security.Authentication;
using System.Text;

namespace Signup_Services.Models
{

    public class EmailData
    {
        public bool Result { get; set; }
        public string Message { get; set; }
        public string Excepcion { get; set; }
        public string ExData { get; set; }
    }

    public class EmailSender : IDisposable
    {



        #region Properties

        private SmtpCredential _credential;
        private Email _email;

        private string _errorMessage;
        private SmtpClient _smtpClient;
        private bool _isSmtpClientActive;
        private SmtpClient _mSmtpServer;
        /// <summary>
        /// Gets or sets the Receiver's Email address.
        /// </summary>
        public string Receiver
        {
            get
            {
                return _email.receiver;
            }
            set
            {
                _email.receiver = value.ToLower();
            }
        }

        /// <summary>
        /// Gets or sets the Senders's Email address.
        /// </summary>
        public string Sender
        {
            get
            {
                return _credential.User;
            }
            set
            {
                _credential.User = value.ToLower();
                _email.Sender = _credential.User;
                _isSmtpClientActive = false;
            }
        }

        /// <summary>
        /// Gets or sets the Senders's password.
        /// </summary>
        public string senderPassword
        {
            get
            {
                return this._credential.Password;
            }
            set
            {
                this._credential.Password = value;
                this._email.senderPassword = this._credential.Password;
                this._isSmtpClientActive = false;
            }
        }

        /// <summary>
        /// Gets or sets the email subject.
        /// </summary>
        public string subject
        {
            get
            {
                return this._email.subject;
            }
            set
            {
                this._email.subject = value;
            }
        }

        /// <summary>
        /// Gets or sets the SMTP client.
        /// </summary>
        public string smtpHost
        {
            get
            {
                return this._credential.Host;
            } //get
            set
            {
                this._credential.Host = value;
                this._isSmtpClientActive = false;
            } //set
        }

        /// <summary>
        /// Gets the error message after any action.
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this._errorMessage;
            }
        }

        #endregion Properties
        private void Init()
        {
            this._credential = new SmtpCredential();
            this._email = new Email();

        }

        public EmailSender(SmtpCredential pCredential)
        {
            this._credential = pCredential;
            this._isSmtpClientActive = false;
        }


        /// <summary>
        /// Sets the credentials used to authenticate against the SMTP Client.
        /// </summary>
        /// <param name="pHost"></param>
        /// <param name="pSender">Sender's email address that will be used to send emails and to authenticate againts the SMTP client.</param>
        /// <param name="pSenderPassword">Sender's password required to authenticate againts the SMTP client.</param>
        /// <returns>True on success, false on error (use the property _ErrorMessage to see the error message)</returns>
        public bool SetCredentials(string pHost, string pSender, string pSenderPassword)
        {
            _errorMessage = "";
            var lResult = false;

            if (pHost.Length == 0)
            {
                this._errorMessage = "Missing SMTP host.";
            }
            else
            {
                if (pSender.Length == 0)
                {
                    this._errorMessage = "Missing SMTP user.";
                }
                else
                {
                    if (this._smtpClient == null)
                    {
                        this._smtpClient = new SmtpClient();
                    } //if

                    if (pSender == "support@wagerweb.com" || pSender == "registration@drbookie.com")
                    {
                        _smtpClient.EnableSsl = true;
                        _smtpClient.Port = 465;

                    }

                    _smtpClient.Host = pHost;
                    this._smtpClient.Credentials = new NetworkCredential(pSender, pSenderPassword);         //Authenticate sender
                    _isSmtpClientActive = true;
                    lResult = true;
                }
            }
            return lResult;
        }

        private bool SetDefaultCredentials()
        {
            return _isSmtpClientActive || SetCredentials(smtpHost, Sender, senderPassword);
        }

        public bool Send(Email pEmail)
        {
            _errorMessage = string.Empty;
            var lResult = false;

            if (SetDefaultCredentials())
            {

                var mail = new MailAddress(Sender);
                var lEmailMessage = new MailMessage(Sender, pEmail.receiver, pEmail.subject, pEmail.body)
                {

                    DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
                    BodyEncoding = Encoding.UTF8,
                    HeadersEncoding = Encoding.UTF8,
                    SubjectEncoding = Encoding.UTF8,
                    IsBodyHtml = pEmail.isHtml,
                    Sender = mail,

                };
                //lEmailMessage.Priority = MailPriority.High;

                try
                {
                    //_smtpClient.EnableSsl = true;
                    _smtpClient.Send(lEmailMessage);
                    lResult = true;
                } //try
                catch (Exception lEmailException)
                {

                    _errorMessage = lEmailException.Data.ToString();
                    lResult = false;
                }   //catch		
            }

            return lResult;

        }




        public EmailData Senddata(Email pEmail)
        {
            _errorMessage = string.Empty;
            var lResult = new EmailData { Result = false, Message = string.Empty };

            if (SetDefaultCredentials())
            {

                var mail = new MailAddress(Sender);
                var lEmailMessage = new MailMessage(Sender, pEmail.receiver, pEmail.subject, pEmail.body)
                {

                    DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
                    BodyEncoding = Encoding.UTF8,
                    HeadersEncoding = Encoding.UTF8,
                    SubjectEncoding = Encoding.UTF8,
                    IsBodyHtml = pEmail.isHtml,
                    Sender = mail,


                };
                //lEmailMessage.Priority = MailPriority.High;

                try
                {
                    //_smtpClient.EnableSsl = true;

                    _smtpClient.Send(lEmailMessage);
                    lResult.Result = true;
                } //try
                catch (Exception lEmailException)
                {
                    _errorMessage = lEmailException.InnerException.ToString();
                    lResult.Message = _errorMessage;
                    lResult.Result = false;
                }   //catch		
            }

            return lResult;

        }



        /// <summary>
        /// Dispose the smtp client.
        /// </summary>
        public void Dispose()
        {
            if (this._smtpClient != null)
            {
                this._smtpClient.Dispose();
            }
        }




        public void MainSendMail(string name, string email, string message, string htmltoSent, string pstrFrom)
        {


            int lResult;



            lResult = InitSmtpServer();

            SendEmailWw(email, "The H Sportsbook: New Player", htmltoSent, pstrFrom);
            // SendEmail("MIKE@ORANGEGROVEENTERTAINMENT.COM", "The H Sportsbook: New Player", HtmltoMike);

        }
        static void smtpClient_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            var state = e.UserState;
            //"Done"
        }

        public EmailData SendEmailWw(string pstrTo, string pstrSubject, string pstrBody, string pstrFrom)
        {



            //Result = InitSmtpServer();
            string password = ConfigurationManager.AppSettings["email_CSD_PasswordWW"];
            string ccCSD = ConfigurationManager.AppSettings["WW_email_CSD"];
            string CCIT = ConfigurationManager.AppSettings["WW_email_IT"];
            SmtpClient _mSmtpServer = new SmtpClient("mail.runbox.com", 587)
            {
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("customercare@wagerweb.com", password),
                Port = 587
            };


            _errorMessage = string.Empty;
            var lResult = new EmailData { Result = false, Message = string.Empty };

            var lEmail = new MailMessage("Customer Care<customercare@wagerweb.com>", pstrTo, pstrSubject, pstrBody)
            {
                IsBodyHtml = true
            };
            MailAddress copyWW = new MailAddress(ccCSD);
            lEmail.CC.Add(copyWW);
            MailAddress copy = new MailAddress(CCIT);
            lEmail.CC.Add(copy);
            try
            {
                string userState = "test message1";
                _mSmtpServer.SendCompleted += new SendCompletedEventHandler(smtpClient_SendCompleted);
                _mSmtpServer.SendAsync(lEmail, userState);
                lResult.Result = true;
            }
            catch (Exception)
            {

                lResult = SendEmailWwRetry(pstrTo, pstrSubject, pstrBody, pstrFrom);



            }

            return lResult;
        }

        public EmailData SendEmailRunBox(string pstrTo, string pstrSubject, string pstrBody, string pstrFrom, string senderPWD)
        {


            string password = senderPWD;

            var Host = ConfigurationManager.AppSettings["email_SmtpClientWW"];
            var Port = ConfigurationManager.AppSettings["email_SmtpPortWW"];
            // 587 port
            SmtpClient smtpClient = new SmtpClient()
            {
                EnableSsl = true,
                Host = Host,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(pstrFrom, password),
                Port = 587
            };




            _errorMessage = string.Empty;
            //var lResult = new EmailData { sent = false, Results = string.Empty };
            var lResult = new EmailData { Result = false, Message = string.Empty };

            //var Bcc = new MailAddress("hugo.leiva@heritagesports.com");



            var message = new MailMessage("Customer Care<" + pstrFrom + ">", pstrTo, pstrSubject, pstrBody)
            {
                IsBodyHtml = true

            };
            //message.CC.Add(Bcc);

            //var errorData ="";

            try
            {
                smtpClient.SendCompleted += new SendCompletedEventHandler(smtpClient_SendCompleted);
                smtpClient.Send(message);
                lResult.Result = true;
                lResult.Message = "Email sent";
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Console.WriteLine("Delivery failed - retrying in 5 seconds.");
                        System.Threading.Thread.Sleep(5000);
                        smtpClient.Send(message);
                    }
                    else
                    {
                        lResult.Result = false;
                        lResult.Message = ex.ToString();
                        lResult.Excepcion = "Failed to deliver message to {0}" + ex.InnerExceptions[i].FailedRecipient;
                    }
                }
            }
            catch (AuthenticationException ex)
            {
                lResult.Result = false;
                lResult.Message = ex.Message;
                String innerMessage = (ex.InnerException != null)
                      ? ex.InnerException.Message
                      : "";

                lResult.Excepcion = ex.ToString();
            }
            catch (Exception ex)
            {
                lResult.Result = false;
                lResult.Message = ex.Message;
                String innerMessage = (ex.InnerException != null)
                      ? ex.InnerException.Message
                      : "";
                lResult.Excepcion = ex.ToString();

            };


            return lResult;
        }

        public EmailData SendEmailWwRetry(string pstrTo, string pstrSubject, string pstrBody, string pstrFrom)
        {
            int Result;



            Result = InitSmtpServer();


            _errorMessage = string.Empty;
            var lResult = new EmailData { Result = false, Message = string.Empty };

            var lEmail = new MailMessage("Customer Care<customercare@wagerweb.com>", pstrTo, pstrSubject, pstrBody)
            {
                IsBodyHtml = true
            };
            try
            {
                _mSmtpServer.Send(lEmail);
                lResult.Result = true;
            }
            catch (Exception ex)
            {
                lResult = SendEmailWw(pstrTo, pstrSubject, pstrBody, pstrFrom);

                Signup.SendError("hugo.leiva@heritagesports.com", pstrBody, ex.Message.ToString());



                _errorMessage = ex.Message.ToString();

                lResult.Message = _errorMessage;


                lResult.Result = false;
            }

            return lResult;
        }


        private int InitSmtpServer()
        {
            string password = ConfigurationManager.AppSettings["email_CSD_PasswordWW"];



            try
            {
                //_mSmtpServer = new SmtpClient("mail.runbox.com");
                //new ArrayList();
                //_mSmtpServer.EnableSsl = true;
                //var lSmtpUser = new NetworkCredential("customercare%wagerweb.com", password);
                //_mSmtpServer.UseDefaultCredentials = false;
                //_mSmtpServer.Credentials = lSmtpUser;

                SmtpClient _mSmtpServer = new SmtpClient("mail.runbox.com", 587)
                {
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential("customercare@wagerweb.com", password)
               ,
                    Port = 587
                };
            }
            catch (Exception ex)
            {

                return 9;
            }
            return 0;
        }


    }


}



