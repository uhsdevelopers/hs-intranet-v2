﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class MarchContest
    {


        internal static DataTable AddCustomerSelection(string customerId, string contestId, string teamId, string intValue, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@CustomerID", customerId }, { "@ContestID", contestId }, { "@TeamID", teamId }, { "@IntValue", intValue } };

            var cDal = new _BLL.cDAL(_BLL.ConnStringMarchContest, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable ContestCloseStatus(string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { };

            var cDal = new _BLL.cDAL(_BLL.connString_Contest, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable DeleteSelection(string customerId, string contestId, string teamId, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@CustomerID", customerId }, { "@ContestID", contestId }, { "@TeamID", teamId } };

            var cDal = new _BLL.cDAL(_BLL.ConnStringMarchContest, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetCustomerList(int sportbookid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { };

            string connstring = sportbookid == 1 ? _BLL.ConnStringMarchContest : _BLL.ConnStringMarchContestWw;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetallTeams(int sportbookid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { };

            string connstring = sportbookid == 1 ? _BLL.ConnStringMarchContest : _BLL.ConnStringMarchContestWw;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }




        internal static DataTable GetNFLGames(string week, int sportbookid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "weekID", week.ToString() }, { "SportbookID", sportbookid.ToString() } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable AddNflWeek(string idcontest, string week, string closeDateTime, int sportbookid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "idcontest", idcontest }, { "name", week.ToString() }, { "closeDateTime", closeDateTime }, { "SportbookID", sportbookid.ToString() } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetContestEventList(string idcontest, int sportbookid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "idcontest", idcontest }, { "SportbookID", sportbookid.ToString() } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }



        internal static DataTable GetContestinEvents(string idcontest, int sportbookid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "idcontest", idcontest } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable AddLinesSpread(string idcontest, string type, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "idcontest", idcontest }, { "type", type } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable AddLinesTotals(string idcontest, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "idcontest", idcontest } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable AddParticipantinEvent(string idcontest, int sportbookid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "idcontest", idcontest } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetTeamsList(string League, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "League", League } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetSurvivorWeekList(string ContestID, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "ContestID", ContestID } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetSurvivorGamesByWeek(string ContestID, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "weekid", ContestID } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetContestList(string idcontest, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "idcontest", idcontest } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable SurvivorAddweek(string idcontest, string weekid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "WeekId", weekid }, { "ContestID", idcontest } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable SurvivorUpdateStatus(string weekid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "WeekId", weekid } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable InsertEvent(string Team1ID, string Team2ID, string closeDateTime, string week, string type, string idcontest, int sportbookid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "Team1ID", Team1ID }, { "Team2ID", Team2ID }, { "WagerCutoff", closeDateTime }, { "Week", week }, { "Type", type }, { "SportbookID", sportbookid.ToString() }, { "ContestId", idcontest } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable InsertTotal(string IdEvent, string type, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "eventId", IdEvent }, { "Type", type } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable CloneEvents(string ParentContestId, string SourceContestId, string sportbookid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "ParentContestId", ParentContestId }, { "SourceContestId", SourceContestId }, { "SportbookID", sportbookid } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable DynamicLines(string ContestId, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "ContestID", ContestId } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }



        internal static DataTable AddEvent_Cursor(string idcontest, string week, string closeDateTime, int sportbookid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "weekID", week.ToString() }, { "dateID", closeDateTime }, { "SportbookID", sportbookid.ToString() }, { "ContestId", idcontest } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }



        internal static DataTable AddContestinEvents(string idcontest, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "idContest", idcontest } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable AddParticipantinEvent(string idcontest, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "idcontest", idcontest } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetParticipantinEvent(string idcontest, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "idcontest", idcontest } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable AddLines(string idcontest, string type, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "idcontest", idcontest }, { "type", type } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable AddTotalLines(string idcontest, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "idcontest", idcontest } };

            string connstring = _BLL.connString_Contest;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable UpdateTeams(int GameID, string Team1ID, string Team2ID, string RecId1, string RecId2, int sportbookid, string storedProcedureName)
        {
            string[,] spParameters = { { "pContestGameID", GameID.ToString() }, { "Team1ID", Team1ID.ToString() }, { "Team2ID", Team2ID.ToString() }, { "RecId1", RecId1.ToString() }, { "RecId2", RecId2.ToString() } };

            //string[,] spParameters = { };

            string connstring = sportbookid == 1 ? _BLL.ConnStringMarchContest : _BLL.ConnStringMarchContestWw;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }




        internal static DataTable GetAllCustomerList(int sportbookid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { };

            string connstring = sportbookid == 1 ? _BLL.ConnStringMarchContest : _BLL.ConnStringMarchContestWw;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }



        internal static DataTable GetAllGames(int sportbookid, string contestID, string showGradedFlag, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@ContestID", contestID }, { "ShowGradedFlag", showGradedFlag } };


            string connstring = sportbookid == 1 ? _BLL.ConnStringMarchContest : _BLL.ConnStringMarchContestWw;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GradeOneGame(int sportbookid, string contestID, string contestGameID, string winnerID, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@ContestID", contestID }, { "@contestGameID", contestGameID }, { "WinnerID", winnerID } };


            string connstring = sportbookid == 1 ? _BLL.ConnStringMarchContest : _BLL.ConnStringMarchContestWw;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetCustomerBracketTeams(int sportbookid, string roundNumber, string customerID, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@RoundNumber", roundNumber }, { "@CustomerID", customerID } };


            string connstring = sportbookid == 1 ? _BLL.ConnStringMarchContest : _BLL.ConnStringMarchContestWw;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetAllCustomerPoints(int sportbookid, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { };

            string connstring = sportbookid == 1 ? _BLL.ConnStringMarchContest : _BLL.ConnStringMarchContestWw;

            var cDal = new _BLL.cDAL(connstring, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

    }
}