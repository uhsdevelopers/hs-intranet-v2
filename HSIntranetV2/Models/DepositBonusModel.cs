﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class DepositBonusModel
    {



        internal static DataTable GetReport(string storedProcedureName, string dateStart, string dateEnd, string customerid, string initPercentage, string endPercentage, string cProfile)
        {
            string[,] spParameters = { { "@date1", dateStart }, { "@date2 ", dateEnd }, { "@customerID", customerid }, { "@initPercentage", initPercentage }, { "@endPercentage", endPercentage }, { "@Profile", cProfile } };


            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetBookPlayerCounter(string storedProcedureName, string week, string dateStart, string dateEnd, int company = 1)
        {
            string[,] spAsiParameters = { { "@week", week }, { "@StartDate", dateStart }, { "@EndDate", dateEnd } };

            var spParameters = spAsiParameters;

            var connString = _BLL.connString_ASIDb;
            if (company == 2)
            {
                connString = _BLL.connString_DGS_AddOns;
                string[,] spDgsParameters = { { "@prmWeek", week }, { "@prmStartDate", dateStart }, { "@prmEndDate", dateEnd } };
                spParameters = spDgsParameters;
            }

            var cDal = new _BLL.cDAL(connString, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetBookPhoneCounter(string storedProcedureName, string week, string dateStart, string dateEnd, string Agent = "99W_MASTER")
        {

            var connString = _BLL.connString_DGS_AddOns;

            string[,] spDgsParameters = { { "@prmWeek", week }, { "@prmStartDate", dateStart }, { "@prmEndDate", dateEnd }, { "@prmAgent", Agent } };
            var spParameters = spDgsParameters;


            var cDal = new _BLL.cDAL(connString, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }



        internal static DataTable GetAnalysisDepositAndBonusReport(string storedProcedureName, string dateStart, string dateEnd, string customerid, string businessUnit, string store, string cProfile, int timeout = 120)
        {
            string[,] spParameters = { { "@date1", dateStart }, { "@date2 ", dateEnd }, { "@customerID", customerid }, { "@BusinessUnit", businessUnit }, { "@store", store }, { "@Profile", cProfile } };


            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters, timeout);


            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetWagerCountReport(string storedProcedureName, string dateStart, string customerid, string dateEnd, string quantity)
        {
            string[,] spParameters = { { "@CustomerID", customerid }, { "@Begin", dateStart }, { "@End", dateEnd }, { "@count", quantity } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetDeletedWagers(string storedProcedureName, string dateStart, string customerid, string dateEnd, string clerk)
        {
            string[,] spParameters = { { "@CustomerID", customerid }, { "@DateFrom", dateStart }, { "@DateTo", dateEnd }, { "@Clerk", clerk } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetDeletedWagersDetails(string storedProcedureName, string id)
        {
            string[,] spParameters = { { "@TicketNumber", id } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetBet105Customer(string storedProcedureName, string betCustomer, string hsCustomer)
        {
            string[,] spParameters = { { "@BetCustomerid", betCustomer }, { "@HSCustomerid", hsCustomer } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetBet105Email(string storedProcedureName, string betCustomer)
        {
            string[,] spParameters = { { "@BetEmail", betCustomer } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable ActivateCustomer(string storedProcedureName, string customer, string login)
        {
            string[,] spParameters = { { "@Customerid", customer }, { "@Clerk", login } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetCashBoost(string storedProcedureName, string customer)
        {
            string[,] spParameters = { { "@BetCustomerid", customer } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetSimilarAccount(string storedProcedureName, string customer)
        {
            string[,] spParameters = { { "@CustomeriD", customer } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetWeekCountReport(string storedProcedureName, string dateStart, string dateEnd, string company)
        {
            string[,] spParameters = { { "@date1", dateStart }, { "@date2", dateEnd }, { "@MasterAgent", company } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }
        internal static DataTable GetWeekDailyCountReport(string storedProcedureName, string dateStart, string dateEnd, string company)
        {
            string[,] spParameters = { { "@date1", dateStart }, { "@date2", dateEnd }, { "@MasterAgent", company } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetCustomerByCountry(string storedProcedureName, string country)
        {
            string[,] spParameters = { { "@Country", country } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetCustomerInformation(string storedProcedureName, string customerid)
        {
            string[,] spParameters = { { "@Customerid", customerid } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable UpdateCustomer(string storedProcedureName, string customerid, string IsNewWeb)
        {
            string[,] spParameters = { { "@CustomerID", customerid }, { "@MarkedForNewWebSite", IsNewWeb } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetBitCoinWallet(string storedProcedureName, string startDate, string enddate, string status, string SportsbookID, string customerid)
        {
            string[,] spParameters = { { "@DateStart", startDate }, { "@DateEnd", enddate }, { "@Status", status }, { "@CustomerID", customerid }, { "@SportsbookID", SportsbookID } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }



        internal static DataTable UpdateBitCoinWallet(string storedProcedureName, string id, string userInCharge, string status)
        {
            string[,] spParameters = { { "@ID", id }, { "@Status", status }, { "@UserInCharge", userInCharge } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetOpenWagers(string storedProcedureName, string dateStart, string customerid, string dateEnd, string site, bool byDate)
        {
            string[,] spParameters = { { "@CustomerID", customerid }, { "@Begin", dateStart }, { "@End", dateEnd }, { "@site", site }, { "@90days", byDate.ToString() } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetOpenSports(string storedProcedureName)
        {
            string[,] spParameters = { };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetScores(string storedProcedureName, string sport = "")
        {
            string[,] spParameters = { };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetSOScores(string storedProcedureName, string sport = "")
        {
            string[,] spParameters = { };

            var cDal = new _BLL.cDAL(_BLL.connString_telephony, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetCustomerScores(string storedProcedureName, string customerid)
        {
            string[,] spParameters = { { "@customerID", customerid } };

            var cDal = new _BLL.cDAL(_BLL.connString_telephony, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable DeleteCustomerScores(string storedProcedureName, string customerid, string rotnum)
        {
            string[,] spParameters = { { "@customerID", customerid }, { "@RotNUM", rotnum } };

            var cDal = new _BLL.cDAL(_BLL.connString_telephony, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetNegativeBalances(string storedProcedureName, string customerid, string isCredit, string siteId)
        {


            string[,] spParameters = { { "@CustomerID", customerid }, { "@Credit", isCredit }, { "@site", siteId } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetSportSubtype(string storedProcedureName, string sport)
        {
            string[,] spParameters = {
                //{ "@SportType", sport }
                { "@prmSportType", sport }

            };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }

        internal static DataTable GetAllSports(string storedProcedureName)
        {

            string[,] spParameters = { };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }


        internal static DataTable GetAverageBalances(string storedProcedureName, string masterAgent, string startDate, string endDate, string valueFrom, string valueTo, string customerid = null, string sportSubtype = null)
        {

            if (string.IsNullOrEmpty(sportSubtype))
            {
                sportSubtype = null;
            }

            if (string.IsNullOrEmpty(customerid))
            {
                customerid = null;
            }

            string[,] spParameters = { { "@MasterAgent", masterAgent }, { "@StartDate", startDate }, { "@EndDate", endDate }, { "@ValueFrom", valueFrom }, { "@ValueTo", valueTo }, { "@customerid", customerid }, { "@SportSubtype", sportSubtype } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetLiveBets(string storedProcedureName, string dateStart, string dateEnd, string ishs)
        {



            string[,] spParameters = { { "@DateStart", dateStart }, { "@DateEnd", dateEnd }, { "@isHS", ishs } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetDGSLiveBets(string storedProcedureName, string dateStart, string dateEnd, string ishs)
        {



            string[,] spParameters = { { "@DateStart", dateStart }, { "@DateEnd", dateEnd }, { "@isHS", ishs } };

            var cDal = new _BLL.cDAL(_BLL.connString_DGSDATA, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetDepositByMethod(string storedProcedureName, string customerid, string method, string siteId, string dateStart, string dateEnd)
        {


            string[,] spParameters = { { "@CustomerID", customerid }, { "@method", method }, { "@site", siteId }, { "@Begin", dateStart }, { "@End", dateEnd } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetLiveBettingReport(string storedProcedureName, string tw, string customerid, string siteId, string dateStart, string dateEnd)
        {




            string[,] spParameters = { { "@TW", tw }, { "@Date1", dateStart }, { "@Date2", dateEnd }, { "@CustomerID", customerid } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetPayoutByMethod(string storedProcedureName, string customerid, string method, string siteId, string dateStart, string dateEnd)
        {


            string[,] spParameters = { { "@CustomerID", customerid }, { "@method", method }, { "@site", siteId }, { "@Begin", dateStart }, { "@End", dateEnd } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetFakeName(string storedProcedureName, string wasused, string country)
        {


            string[,] spParameters = { { "@wasused", wasused }, { "@country", country } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetSundayParlay(string storedProcedureName, string date, string endDate, string minAmount, string Picks)
        {


            string[,] spParameters = { { "@StartDate", date }, { "@EndDate", endDate }, { "@MinAmount", minAmount }, { "@TotalPicks", Picks } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }




        internal static bool General_BlockCC(string storedProcedureName, string MerchantID, string CustomerID, string User)
        {


            string[,] spParameters = { { "@MerchantID", MerchantID }, { "@CustomerID", CustomerID }, { "@User ", User } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            bool updated;

            try
            {
                _DAL.ExecuteNonQuery(cDal);
                updated = true;
            }
            catch (System.Exception)
            {

                updated = true;
            }

            return updated;

        }


        internal static DataTable GetNewManualAccount(string storedProcedureName, string startDate)
        {


            string[,] spParameters = { { "@dateFrom", startDate } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable UpdateFakeName(string storedProcedureName, string id, string wasused)
        {
            string[,] spParameters = { { "@ID", id }, { "@wasused", wasused } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable UpdateDBCustomerToken(string storedProcedureName, string id, string token)
        {
            string[,] spParameters = { { "@CustomerID", id }, { "@Token", token } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }




        internal static DataTable GetRotationName(string storedProcedureName, string rotnum, string status, string dateStart, string dateEnd)
        {


            string[,] spParameters = { { "@rotnum", rotnum }, { "@status", status }, { "@Begin", dateStart }, { "@End", dateEnd } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetCallOfWeekRequestbyWeek(string storedProcedureName, string dateStart, string StatusID, string Evaluator)
        {

            string[,] spParameters = { { "@date", dateStart }, { "@StatusID", StatusID }, { "@Evaluator", Evaluator } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetCallOfWeekRequestbyClerk(string storedProcedureName, string Clerk, string Evaluator)
        {

            string[,] spParameters = { { "@Clerk", Clerk }, { "@Evaluator", Evaluator } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetCallOfWeekDetails(string storedProcedureName, string id)
        {

            string[,] spParameters = { { "@CallID", id } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetCallOfWeekvotes(string storedProcedureName, string dateStart)
        {

            string[,] spParameters = { { "@date", dateStart } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable AddCallReview(string storedProcedureName, string Clerk, string callid, string vote, string comment)
        {

            string[,] spParameters = { { "@CallID", callid }, { "@EvaluationBy", Clerk }, { "@Vote", vote }, { "@Comment", comment } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable AddCallOfWeekRequest(string storedProcedureName, string clerk, string session, string dateStart, string extension, string description)
        {

            string[,] spParameters = { { "@clerk", clerk }, { "@session", session }, { "@date", dateStart }, { "@extension", extension }, { "@Description", description } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable UpdateCallOfWeekRequest(string storedProcedureName, string id, string session, string dateStart, string extension, string description)
        {

            string[,] spParameters = { { "@id", id }, { "@session", session }, { "@date", dateStart }, { "@extension", extension }, { "@Description", description } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable UpdateCallOfWeekStatus(string storedProcedureName, string id, string status)
        {

            string[,] spParameters = { { "@id", id }, { "@Status", status } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


    }
}