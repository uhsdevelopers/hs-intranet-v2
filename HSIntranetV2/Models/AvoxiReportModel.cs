﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class AvoxiReportModel
    {
        internal static DataTable GetAvoxiReport(string dateFrom, string dateTo, string agentID, string department)
        {
            string[,] spParameters = {
                                        { "@SartDate", dateFrom },
                                        { "@EndDate", dateTo },
                                        { "@Agent", agentID },
                                        {"@department",department}
                                     };

            string storedProcedureName = "[sp_GetAgentReport]";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.ConnectionString_CallCenter, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable GetAgents()
        {
            string[,] spParameters = {

                                     };

            string storedProcedureName = "[sp_GetAgents]";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.ConnectionString_CallCenter, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}

