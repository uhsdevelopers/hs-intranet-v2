﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class NoActionGames
    {

        internal static DataTable GetGamesList(string storedProcedureName, string isfullGame = "0")
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@IsfullGame", isfullGame } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable UpdateAllGames(string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable UpdateGames(string gamenun, bool isgame, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@pGameNumber", gamenun }, { "@isGame", isgame.ToString() } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

    }
}