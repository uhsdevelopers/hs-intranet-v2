﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class ShortcutModel
    {

        #region Customer Maintenance

        internal static DataTable GetCustomerMaintenance(string agentId, string customerId)
        {
            string[,] spParameters = { { "@prmAgentID", agentId }, { "@prmCustomerID", customerId } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.ConnStringRegal, "uSpPHN_Mobile_GetCustMaintenance", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable setCustomerMaintenance(string agentID, string customerID, string password, string creditLimit, string TempCreditAdj, string TempCreditAdjExpDate, string Active)
        {
            string[,] spParameters = {
                                        { "@prmAgentID", agentID },
                                        { "@prmCustomerID", customerID },
                                        { "@prmPassword", password },
                                        { "@prmTempCreditAdj", TempCreditAdj },
                                        { "@prmTempCreditAdjExpDate", TempCreditAdjExpDate },
                                        { "@prmActive", Active },
                                        { "@prmCreditLimit", creditLimit }
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.ConnStringRegal, "uSpPHN_Mobile_SetCustMaintenance", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable setCustomerDeposit(string agentID, string customerID, string TranCode, string TranType, string Amount, string Description, /*string DailyFigureDate,*/ string isFreePlay)
        {
            string[,] spParameters = {
                                        { "@EnteredBy", agentID },
                                        { "@CustomerID", customerID },
                                        { "@TranCode", TranCode },
                                        { "@TranType", TranType },
                                        { "@Amount", Amount },
                                        { "@Description", Description },
                                        /*{ "@DailyFigureDate", DailyFigureDate },*/
                                        { "@DocumentNumber", "" }

                                     };

            string spName = "[spBetSvcInsertTransaction_HS]";

            if (isFreePlay == "1") spName = "[spBetSvcInsertFreePlay_HS]";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.ConnStringRegal, spName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        #endregion


    }
}