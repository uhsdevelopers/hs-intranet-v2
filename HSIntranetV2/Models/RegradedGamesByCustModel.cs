﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class RegradedGamesByCustModel
    {
        public string customerID { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }

        internal static DataTable GetResult(RegradedGamesByCustModel cRegraded)
        {
            string[,] spParameters = {  { "@prmCustomerID", cRegraded.customerID },
                                        { "@prmStartDate", cRegraded.startDate },
                                        { "@prmEndDate", cRegraded.endDate }
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_IntranetDB, "HS_Intranet_RegradedGamesByCust", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }


}