﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class WagerWebLTVReportModel
    {
        internal static DataTable GetLTVReport(string dateFrom, string dateTo, string agentID, string CustomerID)
        {
            string[,] spParameters = {
                                        { "@StartDate ", dateFrom },
                                        { "@EndDate ", dateTo },
                                        { "@Affiliate ", agentID },
                                        { "@CustomerID ",CustomerID }
                                     };


            string storedProcedureName = "uSp_WWIntranet_GetDepositAndWithDrawBalance";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetCashBackReport(string dateFrom, string dateTo)
        {
            string[,] spParameters = {
                                        { "@RequestDateFrom", dateFrom },
                                        { "@RequestDateTO ", dateTo }
                                     };


            const string storedProcedureName = "spIntra_DBGetWWCashBackGeneral";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetCashBackReportByCustomer(string customerid, string dateFrom, string dateTo)
        {
            string[,] spParameters = {
                                         {"@customerid",customerid},
                                        { "@RequestDateFrom", dateFrom },
                                        { "@RequestDateTO ", dateTo }
                                     };


            const string storedProcedureName = "spIntra_DBGetWWCashBack";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetAvailableCashBackByCustomer(string customerid)
        {
            string[,] spParameters = {
                                         {"@FromCustomerID",customerid}
                                     };


            const string storedProcedureName = "[uSp_WWCashBack_GetCalculationV2]";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetReferAFriendList(string dateFrom, string dateTo, string Referred = "")
        {
            string[,] spParameters = {
                {"@Referrer",Referred},
                { "@dateFrom", dateFrom },
                { "@dateTO ", dateTo }
            };


            const string storedProcedureName = "[spIntra_GetReferaFriend]";

            var cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetWagerWebCustReport(string dateFrom, string dateTo, string Customerid)
        {
            string[,] spParameters = {
                {"@CustomerID",Customerid},
                { "@startDate", dateFrom },
                { "@enddate", dateTo }
            };


            const string storedProcedureName = "uSp_Local_GetWagerWebCustReport";

            var cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable UpdateReferAFriendList(string id, string customerid, string wascontacted, string Contactedby, string CallStatus, string comments)
        {
            string[,] spParameters = {
                {"@ID",id},
                { "@CustomerID", customerid },
                { "@Contacted", wascontacted },
                { "@contactedBy", Contactedby },
                { "@callStatus", CallStatus },
                { "@comments", comments }
            };


            const string storedProcedureName = "UpdateReferFriend";

            var cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable UpdateReferAFriendTx(string id, string granted)
        {
            string[,] spParameters = {
                {"@ID",id},
                { "@Bonus", granted }
            };


            const string storedProcedureName = "UpdateReferTX";

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }

        internal static DataTable AddBtcPayment(string customerId, string btcAddress, string btcAmount, string btcHash, double usdAmount,
            string currency, double rate, string status, string comments, string btcFromAddress, string walletId, string sportbookid)
        {
            string[,] spParameters = {
                                        { "@CustomerID", customerId },
                                        { "@BTCAddress", btcAddress },
                                        { "@BTCAmount", btcAmount },
                                        { "@BTCTXHash", btcHash },
                                        { "@Amount", usdAmount.ToString() },
                                        { "@CurrencyCode", currency },
                                        { "@ExchangeRate", rate.ToString() },
                                        { "@Status", status },
                                           { "@Comments", comments },
                                           { "@BTCFromAddress", btcFromAddress },
                                           { "@WalletID", walletId }
                                     };


            const string storedProcedureName = "uSp_BTC_CreatePayment";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetBtcPayment(string customerId, string startDate, string endDate, string sportbookid)
        {
            string[,] spParameters = {
                                        { "@CustomerID", customerId },
                                        { "@DateStart", startDate },
                                        { "@DateEnd", endDate }
                                     };


            const string storedProcedureName = "uSp_BTC_GetPayment";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.ConnStringDevBetsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }



        internal static DataTable GetBTCtransactions(string dateFrom, string dateTo, string filterConfirmed)
        {
            string[,] spParameters = {
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmDateTo", dateTo },
                                        { "@prmFilterConfirmed", filterConfirmed }
                                     };


            const string storedProcedureName = "uSp_BTC_GetBTCtransactions";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}