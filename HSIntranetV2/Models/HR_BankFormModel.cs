﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class HR_BankFormModel
    {

        internal static DataTable InsertForm(string ID, string IDType, string FullName, string LastName1, string LastName2, string ExpirationIDDate, string CivilStatus, string HomeAddress, string Location1, string Location2, string Location3, string BirthCountryName, string BirthPlaceName, string Citizenship, string SpouseID, string SpouseName, string ChildrenCount, string PhoneHome, string CellPhone, string Email, string storedProcedureName)
        {
            string[,] spParameters = { { "@ID ", ID }, { "@IDType ", IDType }, { "@FullName ", FullName }, { "@LastName1 ", LastName1 }, { "@LastName2 ", LastName2 }, { "@ExpirationIDDate ", ExpirationIDDate }, { "@CivilStatus ", CivilStatus }, { "@HomeAddress ", HomeAddress }, { "@Location1 ", Location1 }, { "@Location2 ", Location2 }, { "@Location3", Location3 }, { "@BirthCountryName ", BirthCountryName }, { "@BirthPlaceName ", BirthPlaceName }, { "@Citizenship ", Citizenship }, { "@SpouseID   ", SpouseID }, { "@SpouseName ", SpouseName }, { "@ChildrenCount ", ChildrenCount }, { "@PhoneHome  ", PhoneHome }, { "@CellPhone  ", CellPhone }, { "@Email  ", Email } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_HR_BankForm, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;

        }
        internal static DataTable GetForm(string storedProcedureName)
        {
            string[,] spParameters = { };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_HR_BankForm, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;

        }

    }
}