﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class RolloverModal
    {
        internal static System.Data.DataTable GetResult(string customerID, string startDate, string endDate)
        {
            string[,] spParameters = {
                                        { "@prmCustomerID", customerID },
                                        { "@prmStartDate", startDate },
                                        { "@prmEndDate", endDate }
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSp_Intranet_GetRollover", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}