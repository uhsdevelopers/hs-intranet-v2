﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace HSIntranetV2.Models
{
    public class _DAL
    {
        internal static DataTable getDataTable(_BLL.cDAL cDAL)
        {
            var dtResult = new DataTable();

            var conn = new SqlConnection(cDAL.connString);

            string queryString = cDAL.spName;

            var cmd = new SqlCommand(queryString, conn)
            {
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = 300
            };

            if (cDAL.spParameters != null)
            {
                // add parameter to command
                for (var i = 0; i < cDAL.spParameters.Length / 2; i++)
                {
                    string prmName = cDAL.spParameters[i, 0];
                    string prmValue = cDAL.spParameters[i, 1];

                    cmd.Parameters.AddWithValue(prmName, prmValue);
                }
            }

            var da = new SqlDataAdapter(cmd);

            try
            {
                conn.Open();

                da.Fill(dtResult);

                if (cDAL.logAction != null && cDAL.logAction.Action != null)
                    logChange(cDAL);

            }

            catch (SqlException sqlEx)
            {
                var dc = new DataColumn("ErrorNumber", typeof(Int32));
                dtResult.Columns.Add(dc);

                dc = new DataColumn("ErrorMessage", typeof(String));
                dtResult.Columns.Add(dc);

                DataRow dr = dtResult.NewRow();

                dr[0] = 0;
                dr[1] = sqlEx.Message;

                dtResult.Rows.Add(dr); //this will add the row at the end of the datatable
            }

            catch (Exception e)
            {
                var dc = new DataColumn("ErrorNumber", typeof(Int32));
                dtResult.Columns.Add(dc);

                dc = new DataColumn("ErrorMessage", typeof(String));
                dtResult.Columns.Add(dc);

                DataRow dr = dtResult.NewRow();

                dr[0] = 999;
                dr[1] = e.Message;

                dtResult.Rows.Add(dr); //this will add the row at the end of the datatable

            }
            finally
            {
                conn.Close();
            }

            return dtResult;

        }



        internal static DataTable getDataTableQstring(_BLL.cDAL cDAL)
        {
            DataTable dtResult = new DataTable();

            SqlConnection conn = new SqlConnection(cDAL.connString);

            string queryString = cDAL.spName;

            SqlCommand cmd = new SqlCommand(queryString, conn);

            cmd.CommandTimeout = 300;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            try
            {
                conn.Open();

                da.Fill(dtResult);

            }
            catch (Exception e)
            {

            }
            finally
            {
                conn.Close();
            }

            return dtResult;

        }

        internal static DataSet getDataSet(_BLL.cDAL cDAL)
        {
            DataSet dsResult = new DataSet();

            SqlConnection conn = new SqlConnection(cDAL.connString);

            string queryString = cDAL.spName;

            SqlCommand cmd = new SqlCommand(queryString, conn);

            cmd.CommandTimeout = 300;

            cmd.CommandType = CommandType.StoredProcedure;

            // add parameter to command
            for (int i = 0; i < cDAL.spParameters.Length / 2; i++)
            {
                string prmName = cDAL.spParameters[i, 0];
                string prmValue = cDAL.spParameters[i, 1];

                cmd.Parameters.AddWithValue(prmName, prmValue);
            }

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            try
            {
                conn.Open();

                da.Fill(dsResult);

            }
            catch (Exception e)
            {

            }
            finally
            {
                conn.Close();
            }

            return dsResult;

        }

        private static void logChange(_BLL.cDAL cDAL)
        {

            string prmDetails = cDAL.spName + " ";

            // create string from parameters
            for (int i = 0; i < cDAL.spParameters.Length / 2; i++)
            {
                if (i != 0) prmDetails += ", ";
                prmDetails += cDAL.spParameters[i, 0] + " = " + cDAL.spParameters[i, 1];
            }

            string[,] spParameters = {
                                         { "@prmUser", cDAL.logAction.User },
                                         { "@prmAction", cDAL.logAction.Action },
                                         { "@prmDetails", prmDetails}
                                     };


            _BLL.cDAL cDAL2 = new _BLL.cDAL(_BLL.Host_dbConnString, "uSpHS_Intranet_InsertLog", spParameters);

            _DAL.ExecuteNonQuery(cDAL2);

        }

        internal static void ExecuteNonQuery(_BLL.cDAL cDAL)
        {

            SqlConnection conn = new SqlConnection(cDAL.connString);

            string queryString = cDAL.spName;

            SqlCommand cmd = new SqlCommand(queryString, conn);

            cmd.CommandTimeout = 300;

            cmd.CommandType = CommandType.StoredProcedure;

            // add parameter to command
            for (int i = 0; i < cDAL.spParameters.Length / 2; i++)
            {
                string prmName = cDAL.spParameters[i, 0];
                string prmValue = cDAL.spParameters[i, 1];

                cmd.Parameters.AddWithValue(prmName, prmValue);
            }

            try
            {
                conn.Open();

                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {

            }
            finally
            {
                conn.Close();
            }


        }
    }
}