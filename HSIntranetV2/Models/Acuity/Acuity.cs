﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace HSIntranetV2.Models.Acuity
{
    public class Acuity
    {
        public class AcuityRequest
        {
            public string Merchant { get; set; }
            public string Password { get; set; }

            public string reference { get; set; }

            public string usernumber { get; set; }
            public string email { get; set; }

            public string redirect_url { get; set; }

            public string country { get; set; }
            public string language { get; set; }
            public string selected_service { get; set; }
        }

        public class AcuityResponse
        {
            public string status { get; set; }
            public string Description { get; set; }

            public string ReferenceId { get; set; }

            public string VerificationSource { get; set; }
        }

        public class AcuityPersonal
        {
            public string country { get; set; }
            public string email { get; set; }
            public string name { get; set; }

            public string error { get; set; }
            public string errormessage { get; set; }

            public string SignupRequest { get; set; }

        }




        public class AcuityServiceResponse
        {
            public int status { get; set; }
            public string description { get; set; }
            public string reference_id { get; set; }
            public string verification_source { get; set; }
        }

        internal static bool AddSelfieVerification(string customerid, string InitialRequest, string ReferenceId, string URL, string status, string Requestedby)
        {
            var added = false;
            var result = 0;


            string[] AcuityReference = ReferenceId.Split('-');

            try
            {
                var dtResult = AddSelfieVer(customerid, InitialRequest, AcuityReference[1], URL, status, Requestedby);

                if (dtResult != null)
                {
                    try
                    {
                        var rows = _BLL.ConvertDataTableToListL(dtResult);

                        Dictionary<string, object> item = rows[0];

                        result = (int)item["Added"];

                    }
                    catch (System.Exception ec)
                    {
                        result = -1;

                    }
                }
            }
            catch (System.Exception ex)
            {

                result = -2;

            }

            if (result == 1)
            {
                added = true;
            }

            return added;
        }




        internal static AcuityServiceResponse GetelfieListBycustomer(string customerid, string Datestart = "", string DateEnd = "")
        {
            var ServiceResponse = new AcuityServiceResponse();
            var result = "";
            try
            {
                var dtResult = GetelfieVer(customerid, Datestart, DateEnd);

                if (dtResult != null)
                {
                    try
                    {
                        var rows = _BLL.ConvertDataTableToListL(dtResult);

                        Dictionary<string, object> item = rows[0];

                        ServiceResponse.reference_id = (string)item["referenceid"];
                        ServiceResponse.verification_source = (string)item["url"];
                        ServiceResponse.description = (string)item["InitialRequest"];
                        ServiceResponse.status = Convert.ToInt32(item["status"]);

                    }
                    catch (System.Exception ec)
                    {
                        result = "-1";

                    }
                }
            }
            catch (System.Exception ex)
            {

                result = "-2";

            }


            return ServiceResponse;
        }

        internal static DataTable AddSelfieVer(string customerid, string InitialRequest, string ReferenceId, string URL, string status, string Requestedby)
        {
            string storedProcedureName = "uSpInsertSelfieVerification";
            string[,] spParameters = {
                { "@CustomerID", customerid }, { "@InitialRequest", InitialRequest }, { "@ReferenceID", ReferenceId },{ "@URL", URL }, { "@status", status }, {"@Requestedby",Requestedby}
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetelfieVer(string customerid, string Datestart = "", string DateEnd = "")
        {
            string storedProcedureName = "uSpGetSelfieVerification";
            string[,] spParameters = {
                { "@CustomerID", customerid }, { "@dateStart", Datestart }, { "@dateEnd", DateEnd }
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetelfieList(string Datestart = "", string DateEnd = "", string customerid = "")
        {
            string storedProcedureName = "uSpGetSelfieVerificationList";
            string[,] spParameters = {
                 { "@dateStart", Datestart }, { "@dateEnd", DateEnd },{ "@CustomerID", customerid }
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


        internal static DataTable GetCallbackResult(string AcuityId, string status = "0")
        {
            string storedProcedureName = "[uSpGetCallbackResponse]";
            string[,] spParameters = {
                { "@AcuityReferenceID", AcuityId }, { "@status", status }
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }




        internal static AcuityPersonal GetCustomerInfo(string customerid)
        {
            //string storedProcedureName = "spBetSvcGetCustomerInfo";

            string storedProcedureName = "uSp_Inet_GetCustomerPInformation2";


            var personal = new AcuityPersonal
            {
                error = "0",
                errormessage = "success"
            };


            try
            {
                var dtResult = BonusSettingsModel.GetCustomerInfo(customerid, 0, storedProcedureName);

                if (dtResult != null)
                {
                    try
                    {
                        var rows = _BLL.ConvertDataTableToListL(dtResult);

                        Dictionary<string, object> item = rows[0];

                        personal.country = (string)item["Country"];
                        personal.email = (string)item["EMail"];
                        personal.name = (string)item["NameFirst"];
                        personal.SignupRequest = item["requestid"].ToString();
                    }
                    catch (System.Exception ec)
                    {
                        personal.error = "-1";
                        personal.errormessage = ec.Message;
                    }
                }
            }
            catch (System.Exception ex)
            {

                personal.error = "-2";
                personal.errormessage = ex.Message;
            }



            return personal;
        }

        internal static async Task<string> LiveCheckVerification(AcuityRequest RequestToAcuity)
        {
            var client = new HttpClient();

            var serviceurl = System.Configuration.ConfigurationManager.AppSettings["AcuityTecURL"];
            var responseBody = "";

            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                   | SecurityProtocolType.Tls11
                                                   | SecurityProtocolType.Tls12
                                                   | SecurityProtocolType.Ssl3;

            var request = new HttpRequestMessage(HttpMethod.Post, serviceurl + "/photoIdOnlineVerification");

            var content = new MultipartFormDataContent();
            content.Add(new StringContent(RequestToAcuity.Merchant), "merchant_id");
            content.Add(new StringContent(RequestToAcuity.Password), "password");
            content.Add(new StringContent(RequestToAcuity.reference), "reference");
            content.Add(new StringContent(RequestToAcuity.usernumber), "usernumber");
            content.Add(new StringContent(RequestToAcuity.email), "email");
            content.Add(new StringContent(RequestToAcuity.redirect_url), "redirect_url");
            content.Add(new StringContent(RequestToAcuity.country), "country");
            content.Add(new StringContent(RequestToAcuity.language), "language");
            content.Add(new StringContent(RequestToAcuity.selected_service), "selected_service");

            try
            {
                request.Content = content;
                var response = await client.SendAsync(request);

                try
                {
                    response.EnsureSuccessStatusCode();
                    responseBody = await response.Content.ReadAsStringAsync();
                }
                catch (HttpRequestException ex)
                {
                    responseBody = ex.Message;
                }
                catch (Exception ex)
                {
                    responseBody = ex.Message;
                }
            }
            catch (Exception e)
            {
                responseBody = e.Message;

            }



            return responseBody;

        }


        internal static bool deactivateCustomer(string customerid = "")
        {
            string storedProcedureName = "uSpInactiveCustomer";
            string[,] spParameters = {
               { "@CustomerID", customerid }
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            var added = false;

            try
            {
                var rows = _BLL.ConvertDataTableToListL(dtResult);

                Dictionary<string, object> item = rows[0];

                var result = (bool)item["Error"];

                if (result)
                {
                    added = true;
                }

            }
            catch (System.Exception ec)
            {
                added = false;
            }

            return added;
        }

    }
}