﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class GOBetModel
    {
        internal static DataTable ValidateGOBCustomer(string CustomerID)
        {
            string[,] spParameters = {
                                        { "@CustomerID", CustomerID }
                                     };


            string storedProcedureName = "Hs_GetOutOfYourBet_Phone_SP1";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.ConnectionString_GOB_db, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable CancelGOBBet(string CustomerID)
        {
            string[,] spParameters = {
                                        { "@CustomerID", CustomerID }
                                     };


            string storedProcedureName = "Hs_GetOutOfYourBet_Phone_SP2";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.ConnectionString_GOB_db, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetGobBet(string customerID)
        {


            string[,] spParameters = {
                                         { "@GameNum", "1" },
                                        { "@PeriodNumber", "0" },
                                        { "@WagerType", "s" },
                                        { "@limit", "250" },
                                        { "@CustomerID", customerID }
                                     };


            const string storedProcedureName = "uSp_Local_GetOutOfYourBetEligibleWagers_v2";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }



        internal static DataTable Hs_GetOutOfYourBet(string customerID, string ticketnumber, string wagernumber, string gamenum)
        {


            string[,] spParameters = {
                                     { "@CustomerID", customerID },
                                         { "@TicketNumber", ticketnumber },
                                        { "@WagerNumber", wagernumber }


                                     };


            const string storedProcedureName = "Hs_GetOutOfYourBet_Phone_SPHL";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.ConnectionString_GOB_db, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


        internal static DataTable CheckIfCustomerParticipatedAlready(string customerID, string gamenumber)
        {


            string[,] spParameters = {
                                     { "@CustomerID", customerID },
                                        { "@GameNum", gamenumber },

                                     };


            const string storedProcedureName = "spCheckIfCustomerParticipatedAlready";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


    }
}