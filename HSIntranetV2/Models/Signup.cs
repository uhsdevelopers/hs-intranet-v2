﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;



namespace Signup_Services.Models
{
    public class Signup
    {
        #region "Signup "

        public class PersonalData
        {
            private string _callClerk;
            private DateTime _callDate;
            private int _addressDirectionId;
            private string _streetNameEntry;
            private int _addressStreetTypeId;
            private string _addressBuilding;

            private int _addressBuildingTypeId;
            public int VigDiscountPercent { get; set; }
            public char PointBuyingFlag { get; set; }
            public char EnforceAccumWagerLimitFlag { get; set; }
            public int ParlayMaxBet { get; set; }
            public int ContestMaxBet { get; set; }
            public char EnableRifFlag { get; set; }
            public char StaticLineFlag { get; set; }
            public char InstantActionFlag { get; set; }

            //Data we get
            public string Source { get; set; }
            public string Fname { get; set; }
            public string Lname { get; set; }
            [DefaultValue(1)]
            public int AddressNumber { get; set; }
            public string HashPassword { get; set; }

            public int AddressDirectionId
            {
                get { return _addressDirectionId; }
                set
                {
                    _addressDirectionId = value;
                    _addressDirectionId = 1;
                }
            }

            public string StreetNameEntry
            {
                get { return _streetNameEntry; }
                set { _streetNameEntry = String.Empty; }
            }

            public int AddressStreetTypeId
            {
                get { return _addressStreetTypeId; }
                set { _addressStreetTypeId = 1; }
            }

            public string AddressBuilding
            {
                get { return _addressBuilding; }
                set { _addressBuilding = String.Empty; }
            }

            public int AddressBuildingTypeId
            {
                get { return _addressBuildingTypeId; }
                set { _addressBuildingTypeId = 1; }
            }

            public string Country { get; set; }
            public string Zip { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public string CustomerIp { get; set; }
            public string Address { get; set; }
            public string HomePhone { get; set; }
            public string BusinessPhone { get; set; }
            public string Fax { get; set; }
            public string LeadSourceId { get; set; }
            public int AboutUs { get; set; }
            public string ParlayName { get; set; }
            public string Currency
            { get; set; }

            public string Language
            { get; set; }

            public DateTime Brithdate { get; set; }
            public bool LegalAge { get; set; }
            public bool ReadRules { get; set; }
            public bool PromoEmails { get; set; }
            public bool IsValidcountry { get; set; }


            //Data we provide
            public int SportBookId { get; set; }
            public string RequestId { get; set; }
            public int RequestStatusId { get; set; }
            public string PreCustomerId { get; set; }
            public string CustomerId { get; set; }
            public string SuAgentId { get; set; }
            public int ErrorId { get; set; }
            public int CodeId { get; set; }
            public int ScoreId { get; set; }
            public int RewardId { get; set; }
            public string SuClickId { get; set; }

            public string SuListId { get; set; }
            public string SuListIdNumber { get; set; }
            public string SuPromoter { get; set; }


            public string SuGrade { get; set; }

            //assign
            [DefaultValue(false)]
            public string Store { get; set; }

            public string InetTarget { get; set; }

            public string CommentsCustomer { get; set; }
            public string CommentsTw { get; set; }
            public char InternetAccess { get; set; }
            public char MailingList { get; set; }
            public string Comments { get; set; }
            public string CommentsForCustomer { get; set; }
            public string CommentsForTw { get; set; }
            public float PossibleDeposit { get; set; }
            public char DepositMethodId { get; set; }
            public int CashBonus { get; set; }
            public int FreePlay { get; set; }
            public string WuName { get; set; }

            public string WuLocation { get; set; }

            public DateTime OpenDateTime { get; set; }
            public string OpenedBy { get; set; }

            public DateTime Birthday { get; set; }
            public string SpecificReferralId { get; set; }
            public DateTime CallDate
            {
                get { return _callDate; }
                set { _callDate = DateTime.Now; }
            }

            public string CallClerk
            {
                get { return _callClerk; }
                set { _callClerk = String.Empty; }
            }

            public Int16 PromotionId { get; set; }
            public bool OkToCall { get; set; }
            public string Email2 { get; set; }
            public string Notes { get; set; }
            public bool UnsubscribeEmail { get; set; }
            public bool OkToCall2 { get; set; }
            public bool RulesPolicy { get; set; }
            public char PromotEmails { get; set; }
            public string ReferralAcct { get; set; }
            public string NextCustomerId { get; set; }


        }

        public class SignupMinRequestData
        {
            public string CustomerIp { get; set; }
            public string Fname { get; set; }
            public string Lname { get; set; }
            public string Email { get; set; }
            public string Country { get; set; }
            public string HomePhone { get; set; }
            public string Password { get; set; }

            public string CLK { get; set; }

            public int CodeId { get; set; }
            public int ScoreId { get; set; }
            public int ErrorId { get; set; }

            public int SportsBookId { get; set; }
            public int Promoter { get; set; }
        }

        public class SignupRequestData
        {
            public string CustomerIp { get; set; }
            public string Fname { get; set; }
            public string Lname { get; set; }
            public string Address { get; set; }

            public string Zip { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Country { get; set; }
            public string Email { get; set; }


            public string HomePhone { get; set; }
            public string Password { get; set; }
            public int Promoter { get; set; }
            public string PromoCode { get; set; }
            public DateTime Birthday { get; set; }
            public int CodeId { get; set; }
            public int ScoreId { get; set; }
            public int ErrorId { get; set; }

            public string Address2 { get; set; }
            public string Province { get; set; }
            public string PostalCode { get; set; }
            public string HomePhone2 { get; set; }
            public string IdType { get; set; }
            public string IdValue { get; set; }
            public string Gender { get; set; }
            public string Dob { get; set; }
            public int RewardProgram { get; set; }
            public int SportsBookId { get; set; }
            public string AffiliateId { get; set; }
            public string ClickId { get; set; }
            public string ReferredBy { get; set; }
            public int CaptureId { get; set; }
            public int UseAcuity { get; set; }
            public int UseHash { get; set; }

            public int legalAge { get; set; }
            public int readRules { get; set; }
            public int promoEmails { get; set; }

            public string Currency { get; set; }

            public string Language { get; set; }
        }

        public class SignupRequestDatav3
        {
            public string CustomerIp { get; set; }
            public string Fname { get; set; }
            public string Lname { get; set; }
            public string Address { get; set; }

            public string Zip { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Country { get; set; }
            public string Email { get; set; }

            public string HomePhone { get; set; }
            public string Password { get; set; }
            public int Promoter { get; set; }
            public string PromoCode { get; set; }
            public DateTime Birthday { get; set; }
            public int CodeId { get; set; }
            public int ScoreId { get; set; }
            public int ErrorId { get; set; }
            public string Address2 { get; set; }
            public int RewardProgram { get; set; }
            public int SportsBookId { get; set; }
            public string AffiliateId { get; set; }
            public string ClickId { get; set; }
            public string ReferredBy { get; set; }
            public int CaptureId { get; set; }
            public int UseAcuity { get; set; }
            public int UseHash { get; set; }

        }


        public class SignupCaptureRequestData
        {
            public string CustomerIp { get; set; }
            public string Fname { get; set; }
            public string Lname { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public int Promoter { get; set; }
            public int AboutUs { get; set; }
            public string PromoCode { get; set; }
            public int SportsBookId { get; set; }
            public string CustomerId { get; set; }
            public string RequestId { get; set; }
            public int RequestStatusId { get; set; }
            public string SignupRequestId { get; set; }
            public string ErrorId { get; set; }
        }



        public class CustomerRegistration
        {
            public SignupRequestData RequestData { get; set; }
            public string Username { get; set; }
            public string UserNumber { get; set; }
            public string RegistrationDate { get; set; }
            public string IpAddress { get; set; }
            public string RegDeviceId { get; set; }
            public string BonusCode;
            public string BonusSubmissionDate;
            public string BonusAmount;
            public string Website;
            public string HowDidYouHear;
            public string AffiliateId;
            public SignupRequestData CustomerInformation;

        }

        public class CustomerRegistrationResponse
        {
            public int Status { get; set; }
            public string Description { get; set; }
            public float Score { get; set; }
            public string Id { get; set; }                    //use this only for CustomerRegistration()'s Method  
            public string reference_id { get; set; }         // use this for MakeCall() and UpdateCall() Methods  

        }

        public class CustomerRegistrationResponsev2
        {
            public int Status { get; set; }
            public string Description { get; set; }
            public float Score { get; set; }
            public string Id { get; set; }                    //use this only for CustomerRegistration()'s Method  
            public string reference_id { get; set; }         // use this for MakeCall() and UpdateCall() Methods  

        }



        // to make call function
        public class MakeCallParameters
        {
            public string UserName { get; set; }
            public string UserNumber { get; set; }
            public string TransId { get; set; }
            public string Phone { get; set; }
            public string Language { get; set; }
            public string Code { get; set; }
            public int type { get; set; } = 2;

        }



        public class UpdateCallParameters
        {
            public string ReferenceId;
            public string Status;
            public string CodeEntered;
            public string SportsBookId { get; set; }

        }

        public class VerifyParameters
        {
            public string ReferenceId;
            public string Status;
            public string CustomerId;


        }

        // to make call function
        public class SignupRequestResponse
        {
            public string isValid { get; set; }
            public string failReason { get; set; }
            public string customerid { get; set; }
            public string AgentId { get; set; }
            public string Promoter { get; set; }
            public int RequestStatusId { get; set; }
            public string RequestId { get; set; }
            public string ListId { get; set; }
            public string Grade { get; set; }
        }


        public class EmailRequestResponse
        {
            public bool isValid { get; set; }
            public string failReason { get; set; }
        }
        public class Settings
        {
            public string CommentsCustomer { get; set; }
            public string CommentsTw { get; set; }
            public string AgentId { get; set; }
            public string Store { get; set; }
            public string NextCustomerId { get; set; }
            public string SportsBookId { get; set; }
            public string Promoter { get; set; }
            public string Grade { get; set; }
            public int RequestStatusId { get; set; }
            public string Prefix { get; set; }
            public string InetTarget { get; set; }

            public string UseHash { get; set; }

        }

        public class ServiceCaptureResponse
        {

            public ErrorCode.ErrorCodes ErrorCode = Models.ErrorCode.ErrorCodes.Ok;


            public string Description = Models.ErrorCode.ErrorCodes.Ok.ToString();

            public string RequestId
            { get; set; }
            public string RequestStatusId
            { get; set; }
            public string SignupRequestId
            { get; set; }



        }

        public class ServiceResponse
        {
            public int Errorcode
            { get; set; }
            public string CustomerId
            { get; set; }
            public string RequestId
            { get; set; }
            public string EmailSent
            { get; set; }
            public string Description
            { get; set; }
            public float ScoreId
            { get; set; }
            public int CallCode
            { get; set; }

        }


        public class PasswordErrors
        {
            public int Errorcode
            { get; set; }
            public string Description
            { get; set; }

        }

        public class PasswordServiceResponse
        {
            public int Errorcode
            { get; set; }
            public List<PasswordErrors> Errors { get; set; }
            public string Description
            { get; set; }
        }


        public class ValidCountry
        {
            public bool Isvalid
            { get; set; }
            public string Country
            { get; set; }

        }

        public class CountryCodes
        {
            public string Country
            { get; set; }
            public string Code
            { get; set; }
        }
        public class ActivatedCustomer
        {
            public int Isvalid
            { get; set; }
            public string CustomerId
            { get; set; }

        }

        public class ValidateCustomer
        {
            public string key
            { get; set; }
            public string CustomerId
            { get; set; }

        }


        public static bool IsValidAge(string value, int Limit)
        {
            bool isValid = false;
            DateTime bday = DateTime.Parse(value.ToString());
            DateTime today = DateTime.Today;
            int age = today.Year - bday.Year;
            if (bday > today.AddYears(-age))
            {

                isValid = true;
            }
            if (age > Limit)
            {
                isValid = true;
            }
            return isValid;
        }

        public class CustomerKey
        {

            public string Token
            { get; set; }

        }







        public static int GenerateCode()
        {
            var random = new Random();
            return random.Next(1001, 9999);
        }




        public static double IpAddressToNumber(string paddress)
        {
            double num = 0;

            if (String.IsNullOrEmpty(paddress))
            {
                return 0;
            }
            try
            {
                var arrDec = paddress.Split('.');
                int i;
                for (i = arrDec.Length - 1; i >= 0; i = i - 1)
                {
                    num += ((Int32.Parse(arrDec[i]) % 256) * Math.Pow(256, (3 - i)));
                }
                return num;
            }
            catch (Exception)
            {

                return 0;
            }

        }

        public static bool IsValidIp(string ip)
        {
            var isvalid = false;
            var parts = ip.Split('.');
            if (parts.Length < 4)
            {
                // not a IPv4 string in X.X.X.X format
                isvalid = false;
            }
            else
            {
                foreach (var part in parts)
                {
                    byte checkPart;
                    if (!Byte.TryParse(part, out checkPart))
                    {
                        // not a valid IPv4 string in X.X.X.X format
                        isvalid = false;
                        return isvalid;
                    }
                }
                // it is a valid IPv4 string in X.X.X.X format
                isvalid = true;
            }


            return isvalid;
        }











        public static CustomerRegistrationResponse ValidateCustomerRegistration(CustomerRegistration customer)
        {
            return null;
        }




        public static bool IsValid()
        {
            return true;
        }


        #endregion "Signup"


        #region "CountryCode"

        public static string GetCountryCode(string country = "")
        {
            const string jsonCountryCode = @"[
										{Country:'AD',Code:376},{Country:'AE',Code:971},{Country:'AF',Code:93},{Country:'AG',Code:1268},{Country:'AI',Code:1264},{Country:'AL',Code:355},
										{Country:'AM',Code:374},{Country:'AN',Code:599},{Country:'AO',Code:244},{Country:'AQ',Code:672},{Country:'AR',Code:54},{Country:'AS',Code:1684},
										{Country:'AT',Code:43},{Country:'AU',Code:61},{Country:'AW',Code:297},{Country:'AZ',Code:994},{Country:'BA',Code:387},{Country:'BB',Code:1246},
										{Country:'BD',Code:880},{Country:'BE',Code:32},{Country:'BF',Code:226},{Country:'BG',Code:359},{Country:'BH',Code:973},{Country:'BI',Code:257},
										{Country:'BJ',Code:229},{Country:'BL',Code:590},{Country:'BM',Code:1441},{Country:'BN',Code:673},{Country:'BO',Code:591},{Country:'BR',Code:55},
										{Country:'BS',Code:1242},{Country:'BT',Code:975},{Country:'BW',Code:267},{Country:'BY',Code:375},{Country:'BZ',Code:501},{Country:'CA',Code:1},
										{Country:'CC',Code:61},{Country:'CD',Code:243},{Country:'CF',Code:236},{Country:'CG',Code:242},{Country:'CH',Code:41},{Country:'CI',Code:225},
										{Country:'CK',Code:682},{Country:'CL',Code:56},{Country:'CM',Code:237},{Country:'CN',Code:86},{Country:'CO',Code:57},{Country:'CR',Code:506},
										{Country:'CU',Code:53},{Country:'CV',Code:238},{Country:'CX',Code:61},{Country:'CY',Code:357},{Country:'CZ',Code:420},{Country:'DE',Code:49},
										{Country:'DJ',Code:253},{Country:'DK',Code:45},{Country:'DM',Code:1767},{Country:'DO',Code:1809},{Country:'DZ',Code:213},{Country:'EC',Code:593},
										{Country:'EE',Code:372},{Country:'EG',Code:20},{Country:'ER',Code:291},{Country:'ES',Code:34},{Country:'ET',Code:251},{Country:'FI',Code:358},
										{Country:'FJ',Code:679},{Country:'FK',Code:500},{Country:'FM',Code:691},{Country:'FO',Code:298},{Country:'FR',Code:33},{Country:'GA',Code:241},
										{Country:'GB',Code:44},{Country:'GD',Code:1473},{Country:'GE',Code:995},{Country:'GH',Code:233},{Country:'GI',Code:350},{Country:'GL',Code:299},
										{Country:'GM',Code:220},{Country:'GN',Code:224},{Country:'GQ',Code:240},{Country:'GR',Code:30},{Country:'GT',Code:502},{Country:'GU',Code:1671},
										{Country:'GW',Code:245},{Country:'GY',Code:592},{Country:'HK',Code:852},{Country:'HN',Code:504},{Country:'HR',Code:385},{Country:'HT',Code:509},
										{Country:'HU',Code:36},{Country:'ID',Code:62},{Country:'IE',Code:353},{Country:'IL',Code:972},{Country:'IM',Code:44},{Country:'IN',Code:91},
										{Country:'IQ',Code:964},{Country:'IR',Code:98},{Country:'IS',Code:354},{Country:'IT',Code:39},{Country:'JM',Code:1876},{Country:'JO',Code:962},
										{Country:'JP',Code:81},{Country:'KE',Code:254},{Country:'KG',Code:996},{Country:'KH',Code:855},{Country:'KI',Code:686},{Country:'KM',Code:269},
										{Country:'KN',Code:1869},{Country:'KP',Code:850},{Country:'KR',Code:82},{Country:'KW',Code:965},{Country:'KY',Code:1345},{Country:'KZ',Code:7},
										{Country:'LA',Code:856},{Country:'LB',Code:961},{Country:'LC',Code:1758},{Country:'LI',Code:423},{Country:'LK',Code:94},{Country:'LR',Code:231},
										{Country:'LS',Code:266},{Country:'LT',Code:370},{Country:'LU',Code:352},{Country:'LV',Code:371},{Country:'LY',Code:218},{Country:'MA',Code:212},
										{Country:'MC',Code:377},{Country:'MD',Code:373},{Country:'ME',Code:382},{Country:'MF',Code:1599},{Country:'MG',Code:261},{Country:'MH',Code:692},
										{Country:'MK',Code:389},{Country:'ML',Code:223},{Country:'MM',Code:95},{Country:'MN',Code:976},{Country:'MO',Code:853},{Country:'MP',Code:1670},
										{Country:'MR',Code:222},{Country:'MS',Code:1664},{Country:'MT',Code:356},{Country:'MU',Code:230},{Country:'MV',Code:960},{Country:'MW',Code:265},
										{Country:'MX',Code:52},{Country:'MY',Code:60},{Country:'MZ',Code:258},{Country:'NA',Code:264},{Country:'NC',Code:687},{Country:'NE',Code:227},
										{Country:'NG',Code:234},{Country:'NI',Code:505},{Country:'NL',Code:31},{Country:'NO',Code:47},{Country:'NP',Code:977},{Country:'NR',Code:674},
										{Country:'NU',Code:683},{Country:'NZ',Code:64},{Country:'OM',Code:968},{Country:'PA',Code:507},{Country:'PE',Code:51},{Country:'PF',Code:689},
										{Country:'PG',Code:675},{Country:'PH',Code:63},{Country:'PK',Code:92},{Country:'PL',Code:48},{Country:'PM',Code:508},{Country:'PN',Code:870},
										{Country:'PR',Code:1},{Country:'PT',Code:351},{Country:'PW',Code:680},{Country:'PY',Code:595},{Country:'QA',Code:974},{Country:'RO',Code:40},
										{Country:'RS',Code:381},{Country:'RU',Code:7},{Country:'RW',Code:250},{Country:'SA',Code:966},{Country:'SB',Code:677},{Country:'SC',Code:248},
										{Country:'SD',Code:249},{Country:'SE',Code:46},{Country:'SG',Code:65},{Country:'SH',Code:290},{Country:'SI',Code:386},{Country:'SK',Code:421},
										{Country:'SL',Code:232},{Country:'SM',Code:378},{Country:'SN',Code:221},{Country:'SO',Code:252},{Country:'SR',Code:597},{Country:'ST',Code:239},
										{Country:'SV',Code:503},{Country:'SY',Code:963},{Country:'SZ',Code:268},{Country:'TC',Code:1649},{Country:'TD',Code:235},{Country:'TG',Code:228},
										{Country:'TH',Code:66},{Country:'TJ',Code:992},{Country:'TK',Code:690},{Country:'TL',Code:670},{Country:'TM',Code:993},{Country:'TN',Code:216},
										{Country:'TO',Code:676},{Country:'TR',Code:90},{Country:'TT',Code:1868},{Country:'TV',Code:506},{Country:'TW',Code:886},{Country:'TZ',Code:255},
										{Country:'UA',Code:380},{Country:'UG',Code:256},{Country:'US',Code:1},{Country:'UY',Code:598},{Country:'UZ',Code:998},{Country:'VA',Code:39},
										{Country:'VC',Code:1784},{Country:'VE',Code:58},{Country:'VG',Code:1284},{Country:'VI',Code:1340},{Country:'VN',Code:84},{Country:'VU',Code:678},
										{Country:'WF',Code:681},{Country:'WS',Code:685},{Country:'YE',Code:967},{Country:'YT',Code:262},{Country:'ZA',Code:27},{Country:'ZM',Code:260},
										{Country:'ZW',Code:263}
									   ]";
            var countryCode = JsonConvert.DeserializeObject<List<CountryCodes>>(jsonCountryCode);

            var codeArray = countryCode.FirstOrDefault(x => x.Country == country);
            return codeArray == null ? "" : codeArray.Code;
        }

        #endregion "CountryCode"




        #region "EMAIL SENDER"

        public static SmtpCredential GetEmailCredential(string emailSource)
        {
            SmtpCredential credential;
            switch (emailSource)
            {
                case EmailSource.CustomerService:
                    credential = new SmtpCredential
                    {
                        Host = ConfigurationManager.AppSettings["email_SmtpClient"],
                        User = ConfigurationManager.AppSettings["email_CSD"],
                        Password = ConfigurationManager.AppSettings["email_CSD_Password"]
                    };
                    break;
                case EmailSource.Intranet:
                    credential = new SmtpCredential
                    {
                        Host = ConfigurationManager.AppSettings["email_SmtpClient"],
                        User = ConfigurationManager.AppSettings["email_INT"],
                        Password = ConfigurationManager.AppSettings["email_INT_Password"]

                    };
                    break;
                case EmailSource.ClientServices:
                    credential = new SmtpCredential
                    {
                        Host = ConfigurationManager.AppSettings["email_SmtpClient"],
                        User = ConfigurationManager.AppSettings["email_CSV"],
                        Password = ConfigurationManager.AppSettings["email_CSV_Password"]
                    };
                    break;
                case EmailSource.WageringMenu:
                    credential = new SmtpCredential
                    {
                        Host = ConfigurationManager.AppSettings["email_SmtpClient"],
                        User = ConfigurationManager.AppSettings["email_WAG"],
                        Password = ConfigurationManager.AppSettings["email_WAG_Password"]
                    };
                    break;

                case EmailSource.WagerWeb:
                    credential = new SmtpCredential
                    {
                        Host = ConfigurationManager.AppSettings["email_SmtpClientWW"],
                        User = ConfigurationManager.AppSettings["email_CSDWW"],
                        Password = ConfigurationManager.AppSettings["email_CSD_PasswordWW"]
                    };
                    break;

                default:
                    credential = null;
                    break;
            }
            return credential;
        }

        public static bool SendEmail(string emailSource, Email email)
        {
            var credential = GetEmailCredential(emailSource);
            var emailSender = new EmailSender(credential);
            var lresult = emailSender.Send(email);

            return lresult;
        }


        public static bool SendEmail2(string emailSource, Email email)
        {
            var credential = GetEmailCredential(emailSource);
            var emailSender = new EmailSender(credential);
            var lresult = emailSender.Senddata(email);

            return lresult.Result;
        }

        public static bool SendEmailWw(string emailSource, Email email)
        {
            var credential = GetEmailCredential(emailSource);
            var emailSender = new EmailSender(credential);
            var lresult = emailSender.SendEmailWw(email.receiver, email.subject, email.body, email.Sender);
            //var webmail1 = new Signup_Services.Models.webmail();

            //var lresult = webmail1.webMail(email.receiver, email.subject, email.body, email.Sender);
            return lresult.Result;
            // return lresult.Result;
        }



        static bool ValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        public static bool SendEmailRunbox(string emailSource, Email email)
        {
            var credential = GetEmailCredential(emailSource);
            var emailSender = new EmailSender(credential);

            if (ServicePointManager.SecurityProtocol != SecurityProtocolType.Tls12)
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            }

            ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateCertificate);

            EmailData lresult = new EmailData
            {
                Result = false,
                Message = "Unsent"
            };

            var i = 0;

            while (lresult.Result == false && i < 3)
            {
                lresult = emailSender.SendEmailRunBox(email.receiver, email.subject, email.body, email.Sender, email.senderPassword);

                i++;
                if (lresult.Result == false)
                {
                    Thread.Sleep(5000);
                }
            }

            if (lresult.Result == false)
            {
                var message = "Error Sending Email on Runbox: From This account: " + email.Sender;
                message = message + "</br> Error : " + lresult.Message;

                message = message + "</br>Inner Exception : " + lresult.Excepcion;
                message = message + "</br></br>Data : " + lresult.ExData;
                message = message + ". <br> Receiver: " + email.receiver + "  <br> subject " + email.subject + "  <br> Action Please Check if Runbox have issues";

                SendError("ServiceDesk@heritagesports.com", message, "Check Runbox", 1);
            }

            return lresult.Result;

        }

        public static bool SendEmailWwTest(string emailSource, Email email)
        {
            var credential = GetEmailCredential(emailSource);
            var emailSender = new EmailSender(credential);
            var lresult = emailSender.SendEmailWw(email.receiver, email.subject, email.body, email.Sender);
            //var webmail1 = new Signup_Services.Models.webmail();

            //var lresult = webmail1.webMail(email.receiver, email.subject, email.body, email.Sender);
            return lresult.Result;
            // return lresult.Result;
        }

        public static string GenerateToken(string customerid, string secretWord)
        {
            if (string.IsNullOrEmpty(customerid))
            {
                return "Error";

            }
            var token = GetHashString(customerid, secretWord);

            return token;
        }

        public static byte[] GetHash(string inputString)
        {

            HashAlgorithm algorithm = SHA1.Create();  // SHA1.Create()
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString, string secret)
        {
            var sb = new StringBuilder();
            foreach (var b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }



        public bool sendVerification(string fname, string customerid, string url, string customerEmail, string emailbody, string subject = "", int sportbookid = 1)
        {


            string emailsource;


            var sb = new StringBuilder(emailbody);

            sb.Replace("{CUSTOMERNAME}", fname);
            sb.AppendLine();
            sb.Replace("{CUSTOMERID}", customerid.Trim());
            sb.AppendLine();
            sb.Replace("{URL}", url);
            sb.AppendLine();




            var email = new Email
            {
                subject = subject,
                receiver = customerEmail,
                body = sb.ToString(),
                isHtml = true

            };

            var result = false;

            switch (sportbookid)
            {
                case 1:
                    email.Sender = "newacc@heritagesports.com";
                    email.senderPassword = "newacc123";
                    emailsource = EmailSource.CustomerService;
                    result = SendEmail(emailsource, email);
                    break;
                case 21:
                    email.Sender = "newacc@heritagesports.com";
                    email.senderPassword = "newacc123";
                    emailsource = EmailSource.CustomerService;
                    result = SendEmail(emailsource, email);
                    break;
                case 18:
                    email.Sender = "newacc@heritagesports.com";
                    email.senderPassword = "newacc123";
                    emailsource = EmailSource.CustomerService;
                    result = SendEmail(emailsource, email);
                    break;
                case 31:
                    email.Sender = "noreply@wagerweb.com";
                    email.senderPassword = "Sdf.354.";
                    emailsource = EmailSource.WagerWeb;

                    result = SendEmailWw(emailsource, email);

                    break;
                case 25:
                    email.Sender = ConfigurationManager.AppSettings["SenderBet105"];
                    email.senderPassword = ConfigurationManager.AppSettings["email_CSD_PasswordBet105"];
                    emailsource = EmailSource.Bet105;


                    result = SendEmailRunbox(emailsource, email);

                    break;

                case 26:
                    email.Sender = ConfigurationManager.AppSettings["SenderBetregal"];
                    email.senderPassword = ConfigurationManager.AppSettings["email_CSD_PasswordBetregal"];
                    emailsource = EmailSource.BetRegal;

                    result = SendEmailRunbox(emailsource, email);

                    break;

                default:
                    email.Sender = "newacc@heritagesports.com";
                    email.senderPassword = "newacc123";
                    emailsource = EmailSource.CustomerService;
                    result = SendEmail(emailsource, email);
                    break;
            }


            return result;

        }





        public static bool SendWelcomeEmail2(string fname, string customerid, string password, string customerEmail, string emailbody, string subject = "", int sportbookid = 1)
        {

            var comparetoken = GenerateToken(customerid, "token");
            string emailsource;


            var sb = new StringBuilder(emailbody);

            sb.Replace("{CUSTOMERNAME}", fname);
            sb.AppendLine();
            sb.Replace("{CUSTOMERID}", customerid);
            sb.AppendLine();
            sb.Replace("{PASSWORD}", password);
            sb.AppendLine();

            sb.Replace("{TOKEN}", comparetoken);
            sb.AppendLine();



            var email = new Email
            {
                subject = subject,
                receiver = customerEmail,
                body = sb.ToString(),
                isHtml = true

            };

            var result = false;

            switch (sportbookid)
            {
                case 1:
                    email.Sender = "newacc@heritagesports.com";
                    email.senderPassword = "newacc123";
                    emailsource = EmailSource.CustomerService;
                    result = SendEmail2(emailsource, email);
                    break;
                case 3:
                    email.Sender = ConfigurationManager.AppSettings["email_CSDWW"];
                    email.senderPassword = ConfigurationManager.AppSettings["email_CSD_PasswordWW"];
                    emailsource = EmailSource.WagerWeb;

                    result = SendEmailWw(emailsource, email);

                    break;
                case 20:
                    email.Sender = "registration@drbookie.com";
                    email.senderPassword = "newacc123";
                    emailsource = EmailSource.DrBookie;
                    result = SendEmail(emailsource, email);
                    break;
                default:
                    email.Sender = "newacc@heritagesports.com";
                    email.senderPassword = "newacc123";
                    emailsource = EmailSource.CustomerService;
                    result = SendEmail2(emailsource, email);
                    break;
            }




            return result;

        }


        public static bool SendByPassEmail(string requestID, string customerEmail, string emailbody, string subject = "", int sportbookid = 3)
        {


            string emailsource;


            var sb = new StringBuilder(emailbody);

            sb.Replace("{REQUESTID}", requestID);
            sb.AppendLine();
            sb.Replace("{SID}", sportbookid.ToString());
            sb.AppendLine();



            var email = new Email
            {
                subject = subject,
                receiver = customerEmail,
                body = sb.ToString(),
                isHtml = true

            };

            var result = false;

            switch (sportbookid)
            {
                case 1:
                    email.Sender = "newacc@heritagesports.com";
                    email.senderPassword = "newacc123";
                    emailsource = EmailSource.CustomerService;
                    result = SendEmail2(emailsource, email);
                    break;
                case 3:
                    email.Sender = ConfigurationManager.AppSettings["email_CSDWW"];
                    email.senderPassword = ConfigurationManager.AppSettings["email_CSD_PasswordWW"];
                    emailsource = EmailSource.WagerWeb;

                    result = SendEmailWw(emailsource, email);

                    break;

                default:
                    email.Sender = ConfigurationManager.AppSettings["email_CSDWW"];
                    email.senderPassword = ConfigurationManager.AppSettings["email_CSD_PasswordWW"];
                    emailsource = EmailSource.WagerWeb;

                    result = SendEmailWw(emailsource, email);
                    break;
            }




            return result;

        }



        public static bool SendError(string customerEmail, string emailbody, string subject = "", int sportbookid = 1)
        {


            string emailsource;






            var email = new Email
            {
                subject = subject,
                receiver = customerEmail,
                body = emailbody,
                isHtml = true

            };

            var result = false;

            switch (sportbookid)
            {
                case 1:
                    email.Sender = "newacc@heritagesports.com";
                    email.senderPassword = "newacc123";
                    emailsource = EmailSource.CustomerService;
                    result = SendEmail2(emailsource, email);
                    break;
                case 3:
                    email.Sender = ConfigurationManager.AppSettings["email_CSDWW"];
                    email.senderPassword = ConfigurationManager.AppSettings["email_CSD_PasswordWW"];
                    emailsource = EmailSource.WagerWeb;

                    result = SendEmailWw(emailsource, email);

                    break;
                case 31:
                    email.Sender = ConfigurationManager.AppSettings["email_CSDWW"];
                    email.senderPassword = ConfigurationManager.AppSettings["email_CSD_PasswordWW"];
                    emailsource = EmailSource.WagerWeb;

                    result = SendEmailWw(emailsource, email);

                    break;
                default:
                    email.Sender = "newacc@heritagesports.com";
                    email.senderPassword = "newacc123";
                    emailsource = EmailSource.CustomerService;
                    result = SendEmail2(emailsource, email);
                    break;
            }




            return result;

        }

        #endregion "EMAIL SENDER"
    }



    public class JsonNetFormatter : MediaTypeFormatter
    {
        private readonly JsonSerializerSettings _jsonSerializerSettings;
        private string _callbackQueryParameter;

        public JsonNetFormatter(JsonSerializerSettings jsonSerializerSettings)
        {
            _jsonSerializerSettings = jsonSerializerSettings ?? new JsonSerializerSettings();

            // Fill out the mediatype and encoding we support
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
            SupportedEncodings.Add(new UTF8Encoding(false, true));

            //we also support jsonp.
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/javascript"));
        }

        private Encoding Encoding
        {
            get { return SupportedEncodings[0]; }
        }

        public string CallbackQueryParameter
        {
            get { return _callbackQueryParameter ?? "callback"; }
            set { _callbackQueryParameter = value; }
        }

        public override bool CanReadType(Type type)
        {
            return true;
        }

        public override bool CanWriteType(Type type)
        {
            return true;
        }

        public override MediaTypeFormatter GetPerRequestFormatterInstance(Type type,
                                                HttpRequestMessage request,
                                                MediaTypeHeaderValue mediaType)
        {
            var formatter = new JsonNetFormatter(_jsonSerializerSettings)
            {
                JsonpCallbackFunction = GetJsonCallbackFunction(request)
            };

            return formatter;
        }

        private string GetJsonCallbackFunction(HttpRequestMessage request)
        {
            if (request.Method != HttpMethod.Get)
                return null;

            if (request.RequestUri == null)
                return null;

            var query = HttpUtility.ParseQueryString(request.RequestUri.Query);
            var queryVal = query[CallbackQueryParameter];

            if (string.IsNullOrEmpty(queryVal))
                return null;

            return queryVal;
        }



        private string JsonpCallbackFunction { get; set; }



        public override Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            // Create a serializer
            var serializer = JsonSerializer.Create(_jsonSerializerSettings);

            // Create task reading the content
            return Task.Factory.StartNew(() =>
            {
                using (var streamReader = new StreamReader(readStream, SupportedEncodings[0]))
                {
                    using (var jsonTextReader = new JsonTextReader(streamReader))
                    {
                        return serializer.Deserialize(jsonTextReader, type);
                    }
                }
            });
        }


        public override Task WriteToStreamAsync(Type type, object value, Stream writeStream, HttpContent content, TransportContext transportContext)
        {
            var isJsonp = JsonpCallbackFunction != null;

            // Create a serializer
            var serializer = JsonSerializer.Create(_jsonSerializerSettings);

            // Create task writing the serialized content
            return Task.Factory.StartNew(() =>
            {
                using (var jsonTextWriter = new JsonTextWriter(new StreamWriter(writeStream, Encoding)) { CloseOutput = false })
                {
                    if (isJsonp)
                    {
                        jsonTextWriter.WriteRaw(JsonpCallbackFunction + "(");
                        jsonTextWriter.Flush();
                    }

                    serializer.Serialize(jsonTextWriter, value);
                    jsonTextWriter.Flush();

                    if (isJsonp)
                    {
                        jsonTextWriter.WriteRaw(")");
                        jsonTextWriter.Flush();
                    }
                }
            });
        }
    }

}