﻿using System;
using System.Data;

namespace HSIntranetV2.Models
{
    public class RegalModel
    {

        public Int64 RequestId { get; set; }
        public DateTime RequestDate { get; set; }
        public string DateTo { get; set; }
        public string DateFrom { get; set; }
        public string RequestIp { get; set; }
        public string RequestStatusId { get; set; }
        public string Customerid { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string Password { get; set; }
        public string BirthDate { get; set; }
        public string Comment { get; set; }
        public string PromoCode { get; set; }
        public string Reference { get; set; }
        public string GameId { get; set; }


        public class ServiceTokenResponse
        {

            public string Token
            { get; set; }
        }

        public class RegalUsernameResponse
        {

            public string Status
            { get; set; }
            public string Username
            { get; set; }
            public string Password
            { get; set; }
        }


        public class Customer
        {
            public int id { get; set; }
            public string userName { get; set; }
            public string signinName { get; set; }
            public int siteId { get; set; }
            public string siteName { get; set; }
            public string secretQuestion { get; set; }
            public string secretAnswer { get; set; }
            public int externalUid { get; set; }
            public string externalSessionId { get; set; }
            public string externalUsername { get; set; }
        }

        public class RootObject
        {
            public int ResultCode { get; set; }
            public Customer Customer { get; set; }
        }

        internal static DataTable GetSignup(RegalModel cRegal)
        {
            string[,] spParameters = {  { "@DateFrom", cRegal.DateFrom },
                                        { "@DateTo", cRegal.DateTo }
                                     };

            var cDal = new _BLL.cDAL(_BLL.ConnStringRegal, "Usp_GetSignupRequest", spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }


        internal static DataTable GetTransactions(RegalModel cRegal)
        {
            string[,] spParameters = {  {  "@beginDate", cRegal.DateFrom},
                                        { "@endDate", cRegal.DateTo}
                                     };

            var cDal = new _BLL.cDAL(_BLL.ConnStringRegal, "Usp_GetTransactionList", spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }


        internal static DataTable GetTransactionsbyCustomer(RegalModel cRegal)
        {
            string[,] spParameters = {  { "@customerID", cRegal.Customerid },
                                        { "@beginDate", cRegal.DateFrom },
                                        { "@endDate", cRegal.DateTo }
                                     };

            var cDal = new _BLL.cDAL(_BLL.ConnStringRegal, "Usp_GetTransactionListBycustomer", spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }


        internal static DataTable VerifyTransactionsExists(RegalModel cRegal)
        {
            string[,] spParameters = {  { "@customerID", cRegal.Customerid },
                                        { "@CasinoTransactionId", cRegal.Reference },
                                        { "@GameId", cRegal.GameId}
                                     };

            var cDal = new _BLL.cDAL(_BLL.ConnStringRegal, "usp_validateTransaction", spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }

        internal static DataTable RefundTransactions(string customerid, string enteredBy, string txid)
        {
            string[,] spParameters = {  { "@customerID",customerid },
                                        { "@User", enteredBy },
                                        { "@TXID", txid}
                                     };

            var cDal = new _BLL.cDAL(_BLL.ConnStringRegal, "usp_Refund_Transaction", spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }

        internal static DataTable GetSignupByRequestId(string requestid)
        {
            string[,] spParameters = {  { "@RequestId", requestid}
                                     };

            var cDal = new _BLL.cDAL(_BLL.ConnStringRegal, "Usp_GetSignupRequestByRequestID", spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }


        internal static DataTable GetCustomerInfo(string email, string country)
        {
            string[,] spParameters = {  { "@email", email},{ "@country", country}
                                     };
            var cDal = new _BLL.cDAL(_BLL.ConnStringRegal, "usp_GetCustomerID", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }


        internal static DataTable GetCustomerInfo(string customerId)
        {
            string[,] spParameters = {  { "@customerID", customerId}
                                     };

            var cDal = new _BLL.cDAL(_BLL.ConnStringRegal, "usp_GetCustomerInfo", spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }

        internal static DataTable UpdateCustomerInfo(string customerId, string password, string nameLast, string nameFirst, string email, string homePhone, string businessPhone, string address, string city, string state, string zip, string country, string comments)
        {
            string[,] spParameters = {  { "@customerID", customerId},
                                        { "@Password", password },
                                        { "@NameLast", nameLast },
                                        { "@NameFirst", nameFirst },
                                        { "@EMail", email },
                                        { "@HomePhone", homePhone },
                                        { "@BusinessPhone", businessPhone },
                                        { "@Address", address },
                                        { "@City", city },
                                        { "@State", state },
                                        { "@Zip", zip },
                                        { "@Country", country },
                                        { "@Comments", comments }

                                     };

            var cDal = new _BLL.cDAL(_BLL.ConnStringRegal, "uSp_Inet_UpdateDBCustomer_intranet", spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }


    }
}