﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class CancelGOYBModel
    {
        internal static DataTable GetGOYB(string customerID)
        {
            string[,] spParameters = {
                                        { "@GameNum", "1" },
                                        { "@PeriodNumber", "0" },
                                        { "@WagerType", "S" },
                                        { "@CustomerID", customerID }
                                     };


            string storedProcedureName = "uSp_Local_GetOutOfYourBetEligibleWagers_v2";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetDeletedGOYB(string customerID)
        {
            string[,] spParameters = {
                                        { "@prmCustomerID", customerID },
                                     };


            string storedProcedureName = "spGetDeletedBetsByCustomer";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.ConnectionString_GOB_db, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable CancelGOYB(string ticket_number, string wager_number, string CustomerID)
        {
            string[,] spParameters = {
                                        { "@TicketNumber", ticket_number },
                                        { "@WagerNumber", wager_number },
                                        { "@InvokedBy", CustomerID }
                                     };

            string storedProcedureName = "Hs_GetOutOfYourBet";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static void CancelGOYBLog(string id_game, string ticket_number, string wager_number, string to_win_amount, string amount_wagered, string customer_id, string description)
        {
            string[,] spParameters = {
                                        { "@IdGame", id_game },
                                        { "@TicketNumber", ticket_number },
                                        { "@WagerNumer", wager_number },
                                        { "@ToWinAmount", to_win_amount },
                                        { "@WageredAmount", amount_wagered },
                                        { "@CustomerID", customer_id },
                                        { "@Description", description},
                                        { "@Enterby" , "Intranet"}
                                     };

            string storedProcedureName = "spInsertDeletedWager";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.ConnectionString_GOB_db, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);
        }
    }
}