﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HSIntranetV2.Models
{
    public class _BLL
    {

        public static string connString_HR_BankForm = ConfigurationManager.ConnectionStrings["connString_HR_BankForm"].ConnectionString;
        public static string connString_IntranetDB = ConfigurationManager.ConnectionStrings["connString_IntranetDB"].ConnectionString;
        public static string connString_ASIDb = ConfigurationManager.ConnectionStrings["connString_ASIDb"].ConnectionString;
        public static string connString_Arges = ConfigurationManager.ConnectionStrings["connString_Arges"].ConnectionString;
        public static string connString_Betsy = ConfigurationManager.ConnectionStrings["connString_Betsy"].ConnectionString;
        public static string connString_telephony = ConfigurationManager.ConnectionStrings["connString_telephony"].ConnectionString;
        public static string ConnStringDevBetsy = ConfigurationManager.ConnectionStrings["connString_DevBetsy"].ConnectionString;
        public static string ConnStringMarchContest = ConfigurationManager.ConnectionStrings["connString_ProdMarch"].ConnectionString;
        public static string ConnStringMarchContestWw = ConfigurationManager.ConnectionStrings["connString_ProdMarchWW"].ConnectionString;
        public static string ConnStringRegal = ConfigurationManager.ConnectionStrings["Regal_AsiDB"].ConnectionString;
        public static string connString_Hephaestus_ASIDb = ConfigurationManager.ConnectionStrings["connString_Hephaestus_ASIDb"].ConnectionString;
        public static string connString_BitCoin = ConfigurationManager.ConnectionStrings["connString_BitCoin"].ConnectionString;
        public static string connString_InetAccessLog = ConfigurationManager.ConnectionStrings["connString_InetAccessLog"].ConnectionString;
        public static string ConnectionString_CallCenter = ConfigurationManager.ConnectionStrings["ConnectionString_CallCenter"].ConnectionString;
        public static string ConnectionString_GOB_db = ConfigurationManager.ConnectionStrings["ConnectionString_GOB_db"].ConnectionString;
        public static string connString_DGSDATA = ConfigurationManager.ConnectionStrings["connString_DGSDATA"].ConnectionString;
        public static string connString_DGS_AddOns = ConfigurationManager.ConnectionStrings["connString_DGS_AddOns"].ConnectionString;
        public static string Host_dbConnString = ConfigurationManager.ConnectionStrings["Host_dbConnString"].ConnectionString;
        public static string connString_OfficePool = ConfigurationManager.ConnectionStrings["connString_OfficePool"].ConnectionString;
        public static string connString_Contest = ConfigurationManager.ConnectionStrings["connString_Contest"].ConnectionString;
        public static string connString_Asys = ConfigurationManager.ConnectionStrings["connString_Asys"].ConnectionString;
        public static string connString_SAPIntegration = ConfigurationManager.ConnectionStrings["connString_SAPIntegration"].ConnectionString;
        public static string connString_Trad = ConfigurationManager.ConnectionStrings["Trad_dbConnString"].ConnectionString;

        public class cl_LogAction
        {
            public string User { get; set; }
            public string Action { get; set; }
        }

        public class cDAL
        {
            string s_connString;
            string s_spName;
            string[,] s_spParameters;
            cl_LogAction s_logAction;

            // Constructor
            public cDAL()
            {

            }

            // Constructor
            public cDAL(string c_connString, string c_spName, string[,] c_spParameters, int sTimeout = 120)
            {
                this.connString = c_connString;
                this.spName = c_spName;
                this.spParameters = c_spParameters;
                this.Timeout = sTimeout;

            }

            public cDAL(string c_connString, string c_spName, string[,] c_spParameters, cl_LogAction c_logAction, int sTimeout = 120)
            {
                this.connString = c_connString;
                this.spName = c_spName;
                this.spParameters = c_spParameters;
                this.Timeout = sTimeout;
                this.s_logAction = c_logAction;

            }

            public cDAL(string c_connString, string c_spName, int sTimeout = 120)
            {
                this.connString = c_connString;
                this.spName = c_spName;
                this.Timeout = sTimeout;

            }

            public cl_LogAction logAction
            {
                get
                {
                    return s_logAction;
                }
                set
                {
                    s_logAction = value;
                }
            }

            public string connString { get; set; }

            public string spName { get; set; }

            public string[,] spParameters { get; set; }

            public int Timeout { get; set; }
        }

        public class cErrorMsg
        {
            string s_errorMsg;

            // Constructor
            public cErrorMsg()
            {

            }

            // Constructor
            public cErrorMsg(string c_errorMsg)
            {
                this.s_errorMsg = c_errorMsg;

            }

            public string connString
            {
                get
                {
                    return s_errorMsg;
                }
                set
                {
                    s_errorMsg = value;
                }
            }
        }

        public partial class CustomerToken
        {
            [JsonProperty("Customerid")]
            public string Customerid { get; set; }

            [JsonProperty("token")]

            public string token { get; set; }

        }

        internal static ContentResult GetJson(DataTable dt)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            serializer.MaxJsonLength = Int32.MaxValue;

            List<Dictionary<string, object>> rows =
              new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName.Trim(), dr[col]);
                }
                rows.Add(row);
            }

            var json = serializer.Serialize(rows);

            var Result = new ContentResult { ContentEncoding = Encoding.UTF8, Content = json };

            Result.ContentType = "application/json";

            return Result;

        }
        internal static ContentResult GetJsonHTML(DataTable dt)
        {


            List<Dictionary<string, object>> rows =
                new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName.Trim(), dr[col]);
                }
                rows.Add(row);
            }

            var html = JsonConvert.SerializeObject(dt, Formatting.Indented);

            var Result = new ContentResult { ContentEncoding = Encoding.UTF8, Content = html };

            Result.ContentType = "text/html";

            return Result;

        }


        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            var data = new List<T>();
            var temp = typeof(T);

            foreach (DataRow row in dt.Rows)
            {
                T obj = (T)Activator.CreateInstance(temp);
                foreach (DataColumn column in dt.Columns)
                {
                    foreach (PropertyInfo pro in temp.GetProperties())
                    {
                        if (pro.Name == column.ColumnName)
                        {
                            pro.SetValue(obj, row[column.ColumnName], null);
                            break;
                        }
                    }
                }
                data.Add(obj);
            }
            return data;
        }




        public static List<Dictionary<string, object>> ConvertDataTableToListL(DataTable dt)
        {
            List<Dictionary<string, object>> rows =
                new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName.Trim(), dr[col]);
                }
                rows.Add(row);
            }
            return rows;
        }
        public static string ConvertDataTableToHTML(DataTable dt)
        {
            string html = "<table border='1' cellpadding='3' cellspacing='2' >";
            //add header row
            html += "<thead><tr>";
            for (int i = 0; i < dt.Columns.Count; i++)
                html += "<th>" + dt.Columns[i].ColumnName + "</th>";
            html += "</tr></thead>";
            //add rows
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                html += "<tr>";
                for (int j = 0; j < dt.Columns.Count; j++)
                    html += "<td>" + dt.Rows[i][j].ToString() + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            return html;
        }

        internal static ContentResult GetJsonFromList(List<Dictionary<string, object>> rows)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            serializer.MaxJsonLength = Int32.MaxValue;

            var json = serializer.Serialize(rows);

            var Result = new ContentResult { ContentEncoding = Encoding.UTF8, Content = json };

            Result.ContentType = "application/json";

            return Result;

        }

        internal static ContentResult GetJsonFromAgentTreeHierarchy(List<AccountingModel.cl_AgentTreeHierarchy> rows)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            serializer.MaxJsonLength = Int32.MaxValue;

            var json = serializer.Serialize(rows);

            var Result = new ContentResult { ContentEncoding = Encoding.UTF8, Content = json };

            Result.ContentType = "application/json";

            return Result;

        }

        internal static ContentResult GetJsonv2(DataTable dt)
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;



            List<Dictionary<string, object>> rows =
              new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName.Trim(), dr[col]);
                }
                rows.Add(row);
            }

            var json = JsonConvert.SerializeObject(rows);
            //serializer.Serialize(rows);

            var Result = new ContentResult { ContentEncoding = Encoding.UTF8, Content = json };

            Result.ContentType = "application/json";

            return Result;

        }

        internal static DataTable GetCustomersByAgent(string AgentID)
        {
            string[,] spParameters = { { "@AgentID", AgentID } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "spInetGetCustomersByMasterAgent", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetCustomersByAgentWw(string agentId)
        {
            string[,] spParameters = { { "@AgentID", agentId } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "spInetGetCustomersByMasterAgent_HSCustom", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }




        internal static DataTable AddHorsesSite(string id, string SiteID, string provider)
        {
            string[,] spParameters = { { "@id", id }, { "@siteid", SiteID }, { "@provider", provider } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.Host_dbConnString, "uSpPHN_BLR_AddHorsesSIteID", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


        internal static DataTable GetHorsesSiteList(string provider)
        {
            string[,] spParameters = { { "@provider", provider } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.Host_dbConnString, "uspPHN_BLRGetHorsesList", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetEmployeeLinks(string LoginName)
        {
            string[,] spParameters = { { "@LoginName", LoginName } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, "spSec_GetEmployeeLinks", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetEmployeeInfo(string LoginName)
        {
            string[,] spParameters = { { "@LoginName", LoginName } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, "spSec_GetEmployeeInfo", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetEmployeeList(string DepartmentID, string ClerkList = "1", string OnlyActive = "1")
        {
            if (DepartmentID == "ALL") { DepartmentID = null; }

            string[,] spParameters = { { "@DepartmentID", DepartmentID }, { "@OnlyClerkList", ClerkList }, { "@OnlyActive", OnlyActive } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, "USP_GetAllLoginnamesWithId", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        public static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA1.Create();  // SHA1.Create()
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString, string secret)
        {
            var sb = new StringBuilder();
            foreach (var b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        internal static DataTable GetServerList(string serviceId)
        {


            string[,] spParameters = { { "@name", serviceId } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_IntranetDB, "It_GetServiceDetails", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


        internal static DataTable AddServerList(string server, string privateip, string publicIp, string port, string iisname, string url, string frule, string serverbackup, string backupprivateIp, string notes, string user)
        {


            string[,] spParameters = { { "@Server", server }, { "@PrivateIP", privateip }, { "@PublicIP", publicIp }, { "@Port", port },
                                     { "@IISName", iisname }, { "@URL", url }, { "@FRule", frule }, { "@ServerBackup", serverbackup }, { "@BackupPrivateIP", backupprivateIp }
                                     , { "@Notes",notes }, { "@LastChangedBy", user}};

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_IntranetDB, "It_AddServiceDetails", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


        internal static DataTable CloneEmployeeLink(string SourceName, string NewLogin)
        {
            string[,] spParameters = { { "@SourceEmployeeID", SourceName }, { "@NewEmployeeID", NewLogin } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, "spSec_CloneEmployeeLink", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


        internal static DataTable GetCustomersByAgentRegal(string AgentID)
        {
            string[,] spParameters = { { "@AgentID", AgentID } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.ConnStringRegal, "spInetGetCustomersByMasterAgent", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


    }
}