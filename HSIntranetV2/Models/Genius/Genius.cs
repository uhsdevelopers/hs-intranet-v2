﻿using System.Data;

namespace HSIntranetV2.Models.Genius
{
    public class Genius
    {

        public class cl_UserDetails
        {
            public string User { get; set; }
            public string Name { get; set; }
            public string Department { get; set; }
            public string Nickname { get; set; }

            public string PhoneExt { get; set; }
        }

        internal static DataTable GetTeamsData(string storedProcedureName, string companyId)
        {
            string[,] spParameters = { { "@company_id", companyId } };



            _BLL.cDAL cDal = new _BLL.cDAL(_BLL.ConnectionString_CallCenter, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }

        internal static DataTable AddTimeClock(string storedProcedureName, string user, string status, string reason)
        {
            string[,] spParameters = { { "@user", user }, { "@Status", status }, { "@reason", reason } };

            _BLL.cDAL cDal = new _BLL.cDAL(_BLL.ConnectionString_CallCenter, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }
        internal static DataTable GetUserDetails(string storedProcedureName, string user)
        {
            string[,] spParameters = { { "@user", user } };

            _BLL.cDAL cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }



        internal static DataTable GetTodayData(string storedProcedureName, string user)
        {
            string[,] spParameters = { { "@user", user } };

            _BLL.cDAL cDal = new _BLL.cDAL(_BLL.ConnectionString_CallCenter, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }
    }
}