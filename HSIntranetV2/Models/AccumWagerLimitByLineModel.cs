﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class AccumWagerLimitByLineModel
    {
        internal static DataTable GetCustomerAccumWager(string customerID, string lineMultiplier)
        {
            string[,] spParameters = {
                                        { "@CustomerID", customerID },
                                        { "@LineMultiplier", lineMultiplier }
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSp_Intranet_AccumWagerLimitByLine", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}