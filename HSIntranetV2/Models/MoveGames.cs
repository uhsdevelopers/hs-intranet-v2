﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class MoveGamesModel
    {
        internal static DataTable GetSportDetailsList(string sport)
        {
            string[,] spParameters = {
                { "@prmSportType", sport }
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSpPHN_Intranet_GetSportDetails", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetAllSports()
        {

            string[,] spParameters = {
                { "@prmShowAllSports", "1" }
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSpPHN_Srv_GetSports", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetOpenGames(string sport, string sportSubType, string dateFrom, string dateTo, string RotationFrom, string RotationTo)
        {
            string[,] spParameters = {
                { "@prmSportType", sport},
                { "@prmSportSubType", sportSubType },
                { "@prmDateFrom", dateFrom },
                { "@prmDateTo", dateTo },
                { "@prmRotationFrom", RotationFrom },
                { "@prmRotationTo", RotationTo }
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSp_local_GetGamesToBeMoved", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable SetOpenGames(string sport, string sportSubType, string dateFrom, string dateTo, string RotationFrom, string RotationTo, string prmNewSportType, string prmNewSportSubType)
        {
            string[,] spParameters = {
                { "@prmSportType", sport},
                { "@prmSportSubType", sportSubType },
                { "@prmDateFrom", dateFrom },
                { "@prmDateTo", dateTo },
                { "@prmRotationFrom", RotationFrom },
                { "@prmRotationTo", RotationTo },
                { "@prmNewSportType", prmNewSportType },
                { "@prmNewSportSubType", prmNewSportSubType }
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSp_local_SetGamesToBeMoved", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}