﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Northbet
    {

        public static DataTable Northbet_GetVolume(string storedProcedureName, string Agent, string league, string startdate, string enddate)
        {


            string[,] spParameters = { { "@Agent", Agent }, { "@league", league }, { "@StartDate", startdate }, { "@EndDate", enddate } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }
    }
}