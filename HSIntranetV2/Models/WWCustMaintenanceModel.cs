﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class WWCustMaintenanceModel
    {
        internal static DataTable SetQuickLimitByCust(string agentID, string customerID, string quickLimit)
        {

            string[,] spParameters = {
                                        { "@prmAgentID", agentID },
                                        { "@prmCustomerID", customerID },
                                        { "@prmQuickLimit", quickLimit }

                                     };


            string storedProcedureName = "uSp_Intranet_WW_UpdateQuickLimitByCust";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;

        }


        internal static DataTable GetQuickLimitByCust(string agentID, string customerID)
        {
            string[,] spParameters = {
                                        { "@prmAgentID", agentID }   ,
                                        { "@prmCustomerID", customerID }
                                     };


            string storedProcedureName = "uSp_Intranet_WW_UpdateQuickLimitByCust";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}