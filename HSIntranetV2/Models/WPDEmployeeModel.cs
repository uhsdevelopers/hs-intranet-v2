﻿using System;
using System.Data;
using System.Globalization;


namespace HSIntranetV2.Models
{
    public class WpdEmployeeModel
    {
        public enum Departments
        {
            Clerks = 5000,
            WpdManager = 14000,
            ClerksNight = 18000,
            ClerkManager = 21000
        }




        internal static DataTable GetCommentsByEmployee(string employeeName, string commentBy, DateTime dateFrom, DateTime dateTo, string hrrRequestId, string isPositiveComment, string departmentId, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@LoginName", employeeName }, { "@EnteredBy", commentBy } , { "@DateFrom", dateFrom.ToString(CultureInfo.InvariantCulture) }
                                     , { "@DateTo", dateTo.ToString(CultureInfo.InvariantCulture) }
                                     , { "@hrrRequestId", hrrRequestId }
                                     , { "@IsPositive", isPositiveComment }
                                     , { "@DepartmentID", departmentId }};

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }



        internal static DataTable GetCommentsByLog(DateTime dateFrom, DateTime dateTo, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@DateFrom", dateFrom.ToString(CultureInfo.InvariantCulture) }
                                     , { "@DateTo", dateTo.ToString(CultureInfo.InvariantCulture) }
                                     };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable ValidateToken(string employeeName, string token, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@customerid", employeeName }, { "@Token", token } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }




        internal static DataTable GetEmployeeList(string departmentId, string status, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@DepartmentID", departmentId }, { "@status", status } };


            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable HRR_GetRequestType(string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };



            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }



        internal static DataTable AddCommentToEmployee(string employeeName, string comment, string commentBy, string incidentdate, string departmentId, string hrrRequestId, string isPositive, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@LoginName", employeeName }, { "@Comment", comment }, { "@EnteredBy", commentBy }, { "@IncidentDate", incidentdate }, { "@DepartmentID", departmentId }, { "@HRRRequestID", hrrRequestId }, { "@IsPositive", isPositive } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable DeleteComment(string commentId, string commentBy, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@CommentID", commentId }, { "@EnteredBy", commentBy } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable UpdateComment(string commentId, string comment, string commentBy, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@CommentID", commentId }, { "@Comment", comment }, { "@EnteredBy", commentBy } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetCommentbyId(string commentId, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@CommentID", commentId } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static bool HasAccess(string employeeName, string pageLink, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@Loginname", employeeName }, { "@Link", pageLink } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);
            var boolResult = dtResult.Rows[0]["Access"];

            var result = boolResult.ToString() == "1";

            return result;

        }

    }
}

