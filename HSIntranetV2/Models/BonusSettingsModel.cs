﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class BonusSettingsModel
    {
        internal static DataTable GetCustomerInfo(string customerid, int mask, string storedProcedureName)
        {
            string[,] spParameters = {
                                        { "@Customerid", customerid },{"@mask", mask.ToString()}
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable GetCustomerBonus(string customerid, string storedProcedureName)
        {
            string[,] spParameters = {
                                        { "@prmCustomerID", customerid }
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable SetCustomerInfo(string customerid, string BonusExclusion, string UseAGreaterBonus)
        {
            string[,] spParameters = {
                                        { "@prmCustomerID ", customerid },
                                        { "@prmBonusExclusion ", BonusExclusion },
                                        { "@prmUseAGreaterBonus ", UseAGreaterBonus }
                                     };

            string storedProcedureName = "uSp_Intranet_SetBonusExclusion";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


    }
}