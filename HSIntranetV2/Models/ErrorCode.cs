namespace Signup_Services.Models
{
    public class ErrorCode
    {
        public enum ErrorCodes : uint
        {
            Ok = 0,
            Accepted = 202,
            BlankCredentials = 1001,
            InvalidCredentials = 1002,
            InvalidName = 1003,
            DatabaseError = 1004,
            InvalidEmail = 1005,
            InvalidIp = 1006,
            InvalidPhone = 1007,
            EmailinDatabase = 109,
            EmailinSignup = 110,
            EmailinCapture = 111,
            CustomerinCapture = 112,
            PhoneinDatabase = 209,
            PhoneinSignup = 210,
            PhoneinCapture = 211,
            RequestIdinProgress = 311,

        }
    }
}