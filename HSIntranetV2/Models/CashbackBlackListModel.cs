﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class CashbackBlackListModel
    {
        internal static DataTable GetTransAccList(string customerId)
        {
            string[,] spParameters = {
                                        { "@CustomerID ", customerId  }
                                     };


            string storedProcedureName = "uSp_SubStore_GetCashBackBlackList";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable DeleteTransAcc(string customerId)
        {
            string[,] spParameters = {
                                        { "@CustomerID ", customerId  }
                                     };

            string storedProcedureName = "uSp_SubStore_RemoveCashBackBlackList";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable InsertTransAcc(string customerId)
        {
            string[,] spParameters = {
                                        { "@CustomerID ", customerId  }
                                     };

            string storedProcedureName = "[uSp_SubStore_InsertCashBackBlackList]";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}