﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class DepositWithdrawals
    {
        internal static DataTable GetDeposits(string from, string to, string company)
        {
            string[,] spParameters = {
                                        { "@From", from },
                                        { "@To", to },
                                        { "@Company", company },
                                     };

            string storedProcedureName = "spIntra_GetTransactionResume";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetWithdrawals(string from, string to, string company)
        {
            string[,] spParameters = {
                                        { "@dateFrom", from },
                                        { "@dateTo", to },
                                        { "@Company", company },
                                     };


            string storedProcedureName = "uSp_Local_GetWithdrawal";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetWithdrawalsDetails(string from, string to, string company, string paymentBy)
        {
            string[,] spParameters = {
                                        { "@dateFrom", from },
                                        { "@dateTo", to },
                                        { "@strPayment", paymentBy },
                                        { "@Company", company },
                                     };

            string storedProcedureName = "uSp_Local_GetWithdrawalDetails";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetDepositsDetails(string from, string to, string company, string processor, string status)
        {
            string[,] spParameters = {
                                        { "@dateFrom", from },
                                        { "@dateTo", to },
                                        { "@PROCESOR", processor },
                                        { "@Company", company },
                                        { "@Status", status }
                                     };

            string storedProcedureName = "spIntra_GetDepositDetail";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}