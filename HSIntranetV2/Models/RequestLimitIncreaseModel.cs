﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class RequestLimitIncreaseModel
    {
        internal static DataTable GetRequest(string storedProcedureName, string From, string to, string CustomerId, string Status)
        {
            string[,] spParameters = {
                                        { "@DateTimeStart", From },
                                        { "@DatetimeEnd", to},
                                        { "@CustomerID", CustomerId },
                                        { "@Status", Status}
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable UpdateRequest(string storedProcedureName, string Id, string Status, string Comments, string UserAction)
        {
            string[,] spParameters = {
                                        { "@ID", Id },
                                        { "@Status", Status},
                                        { "@Comments", Comments },
                                        { "@UserAction", UserAction}
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}



