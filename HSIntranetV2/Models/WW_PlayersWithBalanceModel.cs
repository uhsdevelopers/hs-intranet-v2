﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class WW_PlayersWithBalanceModel
    {

        internal static DataTable GetBalance(string dateFrom, string agentID)
        {
            string[,] spParameters = {
                                        { "@prmAgentID", agentID },
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmCompany", "wagerweb" }
                                     };


            string storedProcedureName = "uSp_Intranet_GetPlayersWithBalance";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Hephaestus_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

    }
}