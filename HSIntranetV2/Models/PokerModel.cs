﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class PokerModel
    {
        public string customerID { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string tranCode { get; set; }
        public string status { get; set; }
        public string timeZone { get; set; }
        public string summary { get; set; }

        internal static DataTable GetPokerTransactions(PokerModel cPoker)
        {
            string[,] spParameters = {  { "@prmCustomerID", cPoker.customerID },
                                        { "@prmStartDate", cPoker.startDate },
                                        { "@prmEndDate", cPoker.endDate },
                                        { "@prmTranCode", cPoker.tranCode },
                                        { "@prmStatus", cPoker.status },
                                        { "@prmTimeZone", cPoker.timeZone },
                                        { "@prmSummary", cPoker.summary }
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, "spAPIGetAllMessages", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }



        internal static DataSet GetCustomerPassword(string username, string sportbookid)
        {


            string[,] spParameters = {  { "@customerID", username },
                                        { "@sportbookid", sportbookid }

                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "usp_Contest_GetcustomerPasswordv2", spParameters);

            //DataTable dtResult = _DAL.getDataTable(cDAL);


            DataTable dtResult = new DataTable();
            DataSet dsResult = _DAL.getDataSet(cDAL);

            if (dsResult.Tables.Count == 1)
            {
                dtResult = dsResult.Tables[0];
            }
            else
            {
                dtResult = dsResult.Tables[1];
            }

            return dsResult;


            // return dtResult;

        }

        internal static DataTable GetCustomerLimits(string storedProcedureName, string customerid)
        {


            string[,] spParameters = {  { "@customerID", customerid }

                                     };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;





            // return dtResult;

        }
    }
}