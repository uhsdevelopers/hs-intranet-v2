﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class FreeBetsModel
    {
        internal static DataTable GetFreeBets(string CustomerID, string storedProcedureName)
        {
            string[,] spParameters = { { "@prmCustomerID", CustomerID }, { "@prmIsIntranet", "1" } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable EditFreeBets(string prmFreeBetID, string prmAmount, string prmActive, string prmExpirationDate, string prmClerk, string storedProcedureName)
        {
            string[,] spParameters = { { "@prmID", prmFreeBetID }, { "@prmAmount", prmAmount }, { "@prmActive", prmActive }, { "@prmExpirationDate", prmExpirationDate }, { "@prmClerk", prmClerk } };
            if (prmExpirationDate == "")
            {
                string[,] spParameters1 = { { "@prmID", prmFreeBetID }, { "@prmAmount", prmAmount }, { "@prmActive", prmActive }, { "@prmClerk", prmClerk } };
                spParameters = spParameters1;
            }
            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable DeleteFreeBets(string prmFreeBetID, string prmClerk, string storedProcedureName)
        {
            string[,] spParameters = { { "@ID", prmFreeBetID }, { "@EnteredBy", prmClerk } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable GetFreeBetsCombos(string storedProcedureName)
        {
            string[,] spParameters = { };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable AddFreeBet(string CustomerID, string prmFreeBetID, string prmAmount, string prmExpirationDate, string prmClerk, string storedProcedureName)
        {
            string[,] spParameters = { { "@prmCustomerID", CustomerID }, { "@prmFreeBetID", prmFreeBetID }, { "@prmAmount", prmAmount }, { "@prmExpirationDate", prmExpirationDate }, { "@prmClerk", prmClerk } };
            if (prmExpirationDate == "")
            {
                string[,] spParameters1 = { { "@prmCustomerID", CustomerID }, { "@prmFreeBetID", prmFreeBetID }, { "@prmAmount", prmAmount }, { "@prmClerk", prmClerk } };
                spParameters = spParameters1;
            }
            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}