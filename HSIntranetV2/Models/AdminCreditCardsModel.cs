﻿using System;
using System.Data;
using System.Text;

namespace HSIntranetV2.Models
{
    public class AdminCreditCardsModel
    {

        internal static DataTable GetCreditCards(string customerId, string merchantid)
        {
            string[,] spParameters = { { "@CustomerID", customerId }, { "@merchantid", merchantid } };
            string storedProcedureName = "[HSPay_KYC_GetRegisteredCC]";
            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_BitCoin, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable DeleteCreditCards(string MerchantId, string CustomerID, string BIN, string L4, string CardType, string CardExpDate)
        {
            string[,] spParameters = {
                                        { "@MerchantId", MerchantId },
                                        { "@CustomerID", CustomerID },
                                        { "@BIN", BIN },
                                        { "@L4", L4 },
                                        { "@CardType", CardType },
                                        { "@CardExpDate", CardExpDate }
                                     };


            string storedProcedureName = "HSPay_KYC_RemoveCC";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_BitCoin, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable Getmerchantid(string customerId)
        {
            string[,] spParameters = { { "@CustomerID", customerId } };
            string storedProcedureName = "[uSp_Local_GetCustStoreAndLocation]";
            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static string RemoveWhitespace(string input)
        {
            StringBuilder output = new StringBuilder(input.Length);

            for (int index = 0; index < input.Length; index++)
            {
                if (!Char.IsWhiteSpace(input, index))
                {
                    output.Append(input[index]);
                }
            }

            return output.ToString();
        }
    }
}