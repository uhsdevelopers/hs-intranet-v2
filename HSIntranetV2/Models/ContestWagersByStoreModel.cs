﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class ContestWagersByStoreModel
    {
        internal static DataTable GetContestWagersByStore(string dateFrom, string dateTo, string store)
        {

            string[,] spParameters = {
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmDateTo", dateTo },
                                        { "@prmStore", store },
                                     };


            string storedProcedureName = "uSp_Intranet_GetContestWagersByStore";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;

        }
    }
}