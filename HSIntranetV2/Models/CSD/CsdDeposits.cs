﻿using System;
using System.Data;

namespace HSIntranetV2.Models.CSD
{
    public class CsdDeposits
    {
        public int id { get; set; }
        public string Customerid { get; set; }
        public string TransactionID { get; set; }

        public string ClerkId { get; set; }

        public DateTime EnteredDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public string DStatus { get; set; }


        enum DepositStatus : ushort
        {
            Enabled = 1,
            Disabled = 2,
            BadDeposit = 3,
            Fake = 4
        }

        internal static DataTable GetDepositList(string storedProcedureName, CsdDeposits Deposit)
        {


            string[,] spParameters = { { "@CustomerID", Deposit.Customerid }, { "@TransactionID", Deposit.TransactionID }, { "@Clerkid", Deposit.ClerkId } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetSevenDepositList(string storedProcedureName, string startdate)
        {


            string[,] spParameters = { { "@Date", startdate } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable AddDepositException(string storedProcedureName, CsdDeposits Deposit)
        {


            string[,] spParameters = { { "@CustomerID", Deposit.Customerid }, { "@TransactionID", Deposit.TransactionID }, { "@Clerkid", Deposit.ClerkId }, { "@status", Deposit.DStatus } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable ChangeStatusDepositException(string storedProcedureName, string IDDeposit, string status)
        {


            string[,] spParameters = { { "@ID", IDDeposit }, { "@status", status } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetCustomerDepositList(string storedProcedureName, string CustomerId, string startDate)
        {


            string[,] spParameters = { { "@CustomerID", CustomerId }, { "@Date", startDate } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


    }
}