﻿using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;

namespace HSIntranetV2.Models
{
    public class CardValidationService
    {
        public class customer_information
        {
            public string customerId;
            public string first_name;
            public string last_name;
            public string email;
            public string address1;
            public string address2;
            public string city;
            public string province;
            public string postal_code;
            public string country;
            public string phone1;
            public string phone2;

            public string EmailVerified;
            public string PhoneVerified;
            public string FullnameVerified;
            public string AddressVerified;
            public string PhoneBillVerified;
        }

        public class deposit_limits
        {
            public string pay_method_type;
        }

        public class payment_method
        {
            public string BIN;
            public string last_digits;
            public string card_hash;
        }

        public class return_NewTransaction
        {
            public string internal_trans_id;
            public string status;
            public string desc;
            public string rec;
            public string score;
            public string third_party;
            public string bin_information;
            public string reason;
            public string processors;
        }

        public class return_UpdateTransaction
        {
            public string internal_trans_id;
            public string status;
            public string description;
        }

        internal static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        internal static string RemoveWhitespace(string input)
        {
            StringBuilder output = new StringBuilder(input.Length);

            for (int index = 0; index < input.Length; index++)
            {
                if (!Char.IsWhiteSpace(input, index))
                {
                    output.Append(input[index]);
                }
            }

            return output.ToString();
        }

        internal static DataTable SaveCustomerDocument(string customerId, string fileType, string filePath, string filePath2, string filePath3, string description, string storedProcedureName)
        {
            string[,] spParameters = { { "@Customerid", customerId }, { "@FileType", fileType }, { "@Description", description }, { "@FilePath", filePath }, { "@FilePath2", filePath2 }, { "@FilePath3", filePath3 }, { "Status", "0" }, { "@SportbookID", "1" } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetCustomerDocument(string customerId, string DateFrom, string DateTo, string Status, string storedProcedureName, string sportbookId = "1")
        {
            string[,] spParameters = { { "@Customerid", customerId }, { "@DateFrom", DateFrom }, { "@DateTo", DateTo }, { "@Status", Status }, { "@SportbookID", sportbookId } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetCustomerCards(string customerId, string @merchantid, string storedProcedureName)
        {
            string[,] spParameters = { { "@CustomerID", customerId }, { "@merchantid", merchantid } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_BitCoin, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetCustomerCompanyContry(string customerId, string storedProcedureName)
        {
            string[,] spParameters = { { "@CustomerID", customerId } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable AddNewCard(string MerchantID, string customerId, string cardnumber, string typecard, string CardExp, string storedProcedureName)
        {
            string[,] spParameters = { { "@CustomerID", customerId }, { "@CardNumber", cardnumber }, { "@CardType", typecard }, { "@MerchantID", MerchantID }, { "@CardExp", CardExp } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_BitCoin, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable ChangeStatusCCard(string customerId, string bin, string l4, string type, string status, string MerchantId, string Country, string CardExpDate, string statusreason, string storedProcedureName)
        {
            string[,] spParameters = { { "@CustomerID", customerId }, { "@BIN", bin }, { "@l4", l4 }, { "@CardType", type }, { "@Status", status }, { "@Country", Country }, { "@MerchantId", MerchantId }, { "@CardExpDate", CardExpDate }, { "@Comments", statusreason } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_BitCoin, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable setCustomerDocument(string CustomerId, string rowID, string status, string statusreason, string storedProcedureName, string ChangedBy)
        {
            string[,] spParameters = { { "@CustomerID", CustomerId }, { "@rowId", rowID }, { "@Status", status }, { "@prmStatusReason", statusreason }, { "@prmChangedBy", ChangedBy }, };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable segetCustomerinfo(string CustomerId, string storedProcedureName)
        {
            string[,] spParameters = { { "@CustomerID", CustomerId } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetCustomerPInformation(string CustomerId, string storedProcedureName)
        {
            string[,] spParameters = { { "@CustomerID", CustomerId } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable Usp_UpdateCallCode(string CustomerId, string storedProcedureName, string ChangedBy)
        {
            string[,] spParameters = { { "@customerid", CustomerId }, { "@CallCode", "1234" } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable uSp_Local_DocIDInsertDocument(string CustomerId, string FileType, string storedProcedureName, string ChangedBy)
        {
            string[,] spParameters = { { "@CustomerID", CustomerId }, { "@FileType", FileType }, { "@Description", "Manual Validation" }, { "@Status", "1" }, { "@ChangedBy", ChangedBy } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}
