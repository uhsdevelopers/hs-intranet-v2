﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class WW_ProcessingFeesModel
    {

        internal static DataTable Acc_WWProcessingFees(string dateFrom, string dateTo, string agentID)
        {
            string[,] spParameters = {
                                        { "@DateForWholeWeek", dateFrom },
                                        { "@DateTo", dateTo },
                                        { "@agentID", agentID },
                                        { "@company", "wagerweb" }
                                     };


            string storedProcedureName = "dbo.uSp_WWAffiliate_GetFeeDetailByMethodForWW";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

    }
}