﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Dgs
    {

        internal static DataTable GetScoreSetting(string id)
        {


            //string[,] spParameters = {  { "@ID", id } };
            //string storedProcedureName = "[blm].[spGetScoreSetting]";

            //var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            string[,] spParameters = { };

            string storedProcedureName = "[asi].[spScoreGamesGetSetting]";

            var cDal = new _BLL.cDAL(_BLL.connString_DGSDATA, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable SetScoreSetting(string id, string Enabled)
        {


            string[,] spParameters = {  { "@ID", id }, { "@Enabled", Enabled }

                                     };
            //string storedProcedureName = "[blm].[spSetScoreSetting]";

            //var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);


            string storedProcedureName = "[asi].[spScoreGamesSetSetting]";

            var cDal = new _BLL.cDAL(_BLL.connString_DGSDATA, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetScoreNoSent(string id)
        {


            string[,] spParameters = { };
            //string storedProcedureName = "[blm].[spGetScoreNotSent]";

            //var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            string storedProcedureName = "[asi].[spScoreGamesGetScoreNotSent]";

            var cDal = new _BLL.cDAL(_BLL.connString_DGSDATA, storedProcedureName, spParameters);


            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetAgentsBalance(string dateFrom, string agentID)
        {

            string[,] spParameters = {
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmAgentID", agentID }
                                     };


            string storedProcedureName = "AddOn_Accounting_GetAgentsBalance";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_DGS_AddOns, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;

        }

        internal static DataTable ReleaseUser()
        {

            string[,] spParameters = { };


            //string storedProcedureName = "LogOff_DGSusers_License";
            string storedProcedureName = "LogOff_DGSusers_License";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_DGSDATA, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;

        }

        internal static DataTable GetLoggedUsers()
        {

            string[,] spParameters = { };


            //string storedProcedureName = "LogOff_DGSusers_License";
            string storedProcedureName = "uSp_Intranet_GetLoggedUsers";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_DGS_AddOns, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;

        }

        internal static DataTable SendQueue(string id)
        {
            string[,] spParameters = { { "@ID", id } };


            string storedProcedureName = "[asi].[spScoreGamesSendQueue]";

            var cDal = new _BLL.cDAL(_BLL.connString_DGSDATA, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }

        internal static DataTable DGSService(string serviceID, string status)
        {
            string[,] spParameters = {
                                        { "@prmServiceID", serviceID },
                                        { "@prmStatus", status }
                                     };

            string storedProcedureName = "[dbo].[uSp_DGSService]";

            var cDal = new _BLL.cDAL(_BLL.connString_DGS_AddOns, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable LinePubQueue_GetIdList()
        {
            string[,] spParameters = {
                                     };

            const string storedProcedureName = "[dbo].[LinePubQueue_GetIdListALL]";

            var cDal = new _BLL.cDAL(_BLL.connString_DGSDATA, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable LinePubQueue_GetInfo(string IdQueue)
        {
            string[,] spParameters = {
                                        { "@IdQueue", IdQueue }
                                     };

            const string storedProcedureName = "[dbo].[LinePubQueue_GetInfo]";

            var cDal = new _BLL.cDAL(_BLL.connString_DGSDATA, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

    }
}

