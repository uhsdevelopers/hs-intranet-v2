﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Acc_WWComissionsModel
    {

        internal static DataTable GetWWComissions(string dateFrom, string dateTo)
        {
            string[,] spParameters = {
                                        { "@DateForWholeWeek", dateFrom },
                                        { "@DateTo", dateTo }
                                     };


            string storedProcedureName = "uSp_WWAffiliate_GetFeeFigure";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


        internal static DataTable GetNorthBetWinLoss(string dateFrom, string dateTo)
        {
            string[,] spParameters = {
                                        { "@DateStart", dateFrom },
                                        { "@DateEnd", dateTo }
                                     };


            string storedProcedureName = "uSp_Local_Report_NorthBetWinLoss";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


        internal static DataTable GetNorthBetBonusAdjustments(string dateFrom, string dateTo)
        {
            string[,] spParameters = {
                                        { "@DateStart", dateFrom },
                                        { "@DateEnd", dateTo }
                                     };


            string storedProcedureName = "uSp_Local_Report_NorthBetBonusAdjustments";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}