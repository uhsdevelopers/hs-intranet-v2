﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Acc_WWAffiliateProcessFeeModel
    {

        internal static DataTable GetAcc_WWAffiliateProcessFee(string dateFrom)
        {
            string[,] spParameters = { { "@Date", dateFrom } };

            string storedProcedureName = "uSp_WWAffiliate_ProcessFee";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable WWAffiliate_GetMondayToProcess()
        {
            string[,] spParameters = { };

            string storedProcedureName = "uSp_WWAffiliate_GetMondayToProcess";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

    }
}