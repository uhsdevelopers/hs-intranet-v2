﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace HSIntranetV2.Models
{
    public class AccountingModel
    {

        public static DataTable dt = new DataTable();

        #region PHagentHierarchy

        public class cl_AgentTreeHierarchy
        {
            public string text { get; set; }

            public string backColor { get; set; }

            public List<cl_AgentTreeHierarchy> nodes { get; set; }

            // contructor
            public cl_AgentTreeHierarchy(string s_text, string s_backColor, List<cl_AgentTreeHierarchy> l_children)
            {
                text = s_text;
                backColor = s_backColor;
                nodes = l_children;
            }

            public cl_AgentTreeHierarchy(string s_text, string s_backColor)
            {
                text = s_text;
                backColor = s_backColor;
            }
        }

        internal static ContentResult GetPHagentHierarchy(string agentid, string showAll, string betSystem)
        {

            string[,] spParameters = {
                                       { "@prmAgentID", agentid },
                                       { "@prmShowAll", showAll }
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.Host_dbConnString, "uSpAccounting_AgentHierarchy", spParameters);

            if (betSystem == "DGS")
                cDAL.connString = _BLL.connString_DGS_AddOns;

            dt = _DAL.getDataTable(cDAL);

            List<cl_AgentTreeHierarchy> lAgentTreeHierarchy = CreateAgentTreeHierarchyNode();

            var Result = _BLL.GetJsonFromAgentTreeHierarchy(lAgentTreeHierarchy);

            return Result;
        }

        private static List<cl_AgentTreeHierarchy> CreateAgentTreeHierarchyNode()
        {

            /*
                        List<cl_AgentTreeHierarchy> lTreeListTemp = new List<cl_AgentTreeHierarchy>();

                        List<cl_AgentTreeHierarchy> lTreeListTemp2 = new List<cl_AgentTreeHierarchy>();

                        cl_AgentTreeHierarchy clAgentTree = new cl_AgentTreeHierarchy("Agent", lTreeListTemp2);


                        List<cl_AgentTreeHierarchy> lTreeListTemp3 = new List<cl_AgentTreeHierarchy>();

                        cl_AgentTreeHierarchy clAgentTree3 = new cl_AgentTreeHierarchy("SubAgent2", lTreeListTemp2);

                        lTreeListTemp3.Add(clAgentTree3);


                        cl_AgentTreeHierarchy clAgentTree2 = new cl_AgentTreeHierarchy("Agent2", lTreeListTemp3);


                        lTreeListTemp.Add(clAgentTree);
                        lTreeListTemp.Add(clAgentTree2);
            */

            List<cl_AgentTreeHierarchy> lAgentTreeHierarchy = CreateListAgentTreeHierarchy("", 1); //new List<cl_AgentTreeHierarchy>();


            return lAgentTreeHierarchy;
        }

        private static List<cl_AgentTreeHierarchy> CreateListAgentTreeHierarchy(string sAgentID, int iLevel)
        {
            List<cl_AgentTreeHierarchy> lAgentTree = new List<cl_AgentTreeHierarchy>();

            IEnumerable<DataRow> query = from myRow in dt.AsEnumerable() where (myRow.Field<string>("MasterAgentID") == sAgentID || iLevel == 1) && myRow.Field<int>("Level") == iLevel select myRow;

            //var results = from myRow in dt.AsEnumerable() where myRow.Field<string>("MasterAgentID") == sAgentID select myRow;

            // Create a table from the query.
            DataTable boundTable = dt.Clone();

            foreach (var row in query)
                boundTable.ImportRow(row);

            foreach (DataRow dr in boundTable.Rows)
            {

                string sSubAgentID = dr["AgentID"].ToString();
                string sBackColor = dr["backColor"].ToString();


                List<cl_AgentTreeHierarchy> lSubAgentTree = CreateListAgentTreeHierarchy(sSubAgentID, iLevel + 1); //new List<cl_AgentTreeHierarchy>();


                if (lSubAgentTree.Count() == 0)
                {
                    cl_AgentTreeHierarchy clAgentTree = new cl_AgentTreeHierarchy(sSubAgentID, sBackColor);

                    lAgentTree.Add(clAgentTree);

                }
                else
                {

                    cl_AgentTreeHierarchy clAgentTree = new cl_AgentTreeHierarchy(sSubAgentID, sBackColor, lSubAgentTree);

                    lAgentTree.Add(clAgentTree);
                }

            }

            return lAgentTree;

        }


        #endregion

    }
}