﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Acc_HSCommissionDetailsModel
    {

        internal static DataTable GetAcc_GetCommissionDetalisByAgent(string dateFrom, string dateTo, string agentID)
        {

            string[,] spParameters = {
                                        { "@DateForWholeWeek", dateFrom },
                                        { "@DateTo", dateTo },
                                        { "@agentID", agentID },
                                        { "@company", "" }
                                };


            string storedProcedureName = "uSp_HSAffiliate_GetFeeDetailToChargeByMethod";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetAcc_GetFeeDetalisByAgent(string dateFrom, string dateTo, string agentID)
        {

            string[,] spParameters = {
                                        { "@DateForWholeWeek", dateFrom },
                                        { "@DateTo", dateTo },
                                        { "@agentID", agentID },
                                        { "@company", "" }
                                };


            string storedProcedureName = "uSp_HSAffiliate_GetFeeDetailToChargeByMethod";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetAcc_GetAgentsCommissionSummary(string dateFrom, string agentID)
        {
            string[,] spParameters = {
                                        { "@WeekStartingDate", dateFrom },
                                        { "@AgentID", agentID }
                                     };

            string storedProcedureName = "uSp_Intranet_GetAgentsCommissionDetalis";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}