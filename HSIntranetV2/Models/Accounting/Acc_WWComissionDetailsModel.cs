﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Acc_WWComissionDetailsModel
    {

        internal static DataTable GetWWComissions(string dateFrom, string dateTo)
        {
            string[,] spParameters = {
                                        { "@DateForWholeWeek", dateFrom },
                                        { "@DateTo", dateTo }
                                     };


            string storedProcedureName = "uSp_WWAffiliate_GetFeeDetailToChargeByMethod";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

    }
}