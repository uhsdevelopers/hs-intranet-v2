﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Acc_PPHStatementModel
    {

        internal static DataTable GetPPHStatement(string dateFrom, string dateTo, string agentID)
        {
            string[,] spParameters = {
                                        { "@prmAgentID", agentID },
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmDateTo", dateTo }
                                     };


            string storedProcedureName = "uSp_WWAffiliate_PPHStatement";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

    }
}