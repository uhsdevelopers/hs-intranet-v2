﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Acc_HSExcludedTransactionsModel
    {

        internal static DataTable GetHSExludedTransaction(string dateFrom, string dateTo)
        {
            string[,] spParameters = {
                                        { "@DateForWholeWeek", dateFrom },
                                        { "@DateTo", dateTo }
                                     };


            string storedProcedureName = "uSp_HSAffiliate_GetExcludedTransactions";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


    }
}