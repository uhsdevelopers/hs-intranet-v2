﻿using System.Data;
using static HSIntranetV2.Models._BLL;

namespace HSIntranetV2.Models.Accounting.SAP
{

    public class SapLpMaintenanceModel
    {
        internal static DataTable GetTableInfo(string agentId, string storedProcedureName)
        {
            string[,] spParameters = { { "@prmMasterAgentID", agentId } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_SAPIntegration, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable GetTableInfoLinePros(string agentId, string storedProcedureName, string system)
        {
            DataTable dtResult = new DataTable();
            _BLL.cDAL cDAL = new cDAL();

            if (storedProcedureName == "usp_SAP_GetMasterAgent" || storedProcedureName == "usp_SAP_DSR_GetHeaderTemplate")
            {
                string[,] spParameters = { { "@prmMasterAgentID", agentId }, { "@prmSystem", system } };
                cDAL = new _BLL.cDAL(_BLL.connString_SAPIntegration, storedProcedureName, spParameters);

                dtResult = _DAL.getDataTable(cDAL);
            }
            else
            {

                //storedProcedureName += "_LinePros";
                string[,] spParameters = { { "@prmMasterAgentID", agentId } };
                cDAL = new _BLL.cDAL(_BLL.connString_SAPIntegration, storedProcedureName, spParameters);


            }

            dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable UpdateTableInfo(string storedProcedureName, string row, string parameter0, string parameter1, string parameter2, string parameter3, string parameter4, string parameter5, string parameter6, string parameter7)
        {
            string[,] spParameters =
            {
                {"@row",row},
                {"@parameter0", parameter0},
                {"@parameter1", parameter1},
                {"@parameter2", parameter2},
                {"@parameter3",parameter3},
                {"@parameter4",parameter4},
                {"@parameter5",parameter5},
                {"@parameter6",parameter6},
                {"@parameter7",parameter7}
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_SAPIntegration, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable DeleteTableInfo(string storedProcedureName, string row)
        {
            string[,] spParameters =
            {
                {"@row",row}
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_SAPIntegration, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable InsertTableInfo(string storedProcedureName, string parameter0, string parameter1, string parameter2, string parameter3, string parameter4, string parameter5, string parameter6, string parameter7)
        {
            string[,] spParameters =
            {
                {"@parameter0", parameter0},
                {"@parameter1", parameter1},
                {"@parameter2", parameter2},
                {"@parameter3",parameter3},
                {"@parameter4",parameter4},
                {"@parameter5",parameter5},
                {"@parameter6",parameter6},
                {"@parameter7",parameter7}
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_SAPIntegration, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetTranType()
        {
            string[,] spParameters = { };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_SAPIntegration, "[usp_SAP_GetTranType]", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataSet ExportSAP_Report(string storedProcedureName, string MasterAgentId, string dateFrom, string dateTo, string excludeAgentsTrans, string deposits, string ExcludePHNCustomers)
        {

            if (excludeAgentsTrans == "false")
            {
                string[,] spParameters = {
                                        { "@MA", MasterAgentId },
                                        { "@FROM", dateFrom },
                                        { "@TO", dateTo },
                                        { "@withDeposits", deposits },
                                        {"@ExcludePHNCustomers",ExcludePHNCustomers}
                                     };

                _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_SAPIntegration, storedProcedureName, spParameters);

                DataSet dtResult = _DAL.getDataSet(cDAL);

                return dtResult;

            }
            else
            {
                string[,] spParameters = {
                                        { "@MA", MasterAgentId },
                                        { "@FROM", dateFrom },
                                        { "@TO", dateTo },
                                        { "@ExcludeAgentsTrans", excludeAgentsTrans },
                                        { "@withDeposits", deposits },
                                        {"@ExcludePHNCustomers",ExcludePHNCustomers}
                                     };

                _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_SAPIntegration, storedProcedureName, spParameters);

                DataSet dtResult = _DAL.getDataSet(cDAL);

                return dtResult;

            }


        }
        internal static DataSet ExportSAP_Report(string storedProcedureName, string MasterAgentId, string dateFrom, string dateTo, string excludeAgentsTrans)
        {

            if (excludeAgentsTrans == "false")
            {
                string[,] spParameters = {
                                        { "@MA", MasterAgentId },
                                        { "@FROM", dateFrom },
                                        { "@TO", dateTo }
                                     };

                _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_SAPIntegration, storedProcedureName, spParameters);

                DataSet dtResult = _DAL.getDataSet(cDAL);

                return dtResult;

            }
            else
            {
                string[,] spParameters = {
                                        { "@MA", MasterAgentId },
                                        { "@FROM", dateFrom },
                                        { "@TO", dateTo },
                                        { "@ExcludeAgentsTrans", excludeAgentsTrans }
                                     };

                _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_SAPIntegration, storedProcedureName, spParameters);

                DataSet dtResult = _DAL.getDataSet(cDAL);

                return dtResult;

            }


        }

        internal static DataSet ExportDSR_Report(string storedProcedureName, string MasterAgentId, string dateFrom, string dateTo)
        {
            string[,] spParameters = { { "@MA", MasterAgentId }, { "@FROM", dateFrom }, { "@TO", dateTo } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_SAPIntegration, storedProcedureName, spParameters);

            DataSet dtResult = _DAL.getDataSet(cDAL);

            return dtResult;
        }

    }
}