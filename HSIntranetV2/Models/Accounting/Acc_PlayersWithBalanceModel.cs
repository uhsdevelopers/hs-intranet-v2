﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Acc_PlayersWithBalanceModel
    {

        internal static DataTable GetBalance(string dateFrom, string agentID, string storedProcedureName, string report, string withZeroBalance)
        {
            string[,] spParameters = {
                                        { "@prmAgentID", agentID },
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmReportType", report},
                                        { "@prmWithZeroBalance", withZeroBalance}
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

    }
}