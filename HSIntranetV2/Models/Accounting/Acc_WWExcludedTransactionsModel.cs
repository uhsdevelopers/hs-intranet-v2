﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Acc_WWExcludedTransactionsModel
    {

        internal static DataTable GetWWExludedTransaction(string dateFrom, string dateTo)
        {
            string[,] spParameters = {
                                        { "@DateForWholeWeek", dateFrom },
                                        { "@DateTo", dateTo }
                                     };


            string storedProcedureName = "uSp_WWAffiliate_GetExcludedTransactions";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable GetAgentComisionExludedTransaction(string dateFrom, string dateTo, string Agent, string sp)
        {
            string[,] spParameters = {
                                        { "@DateForWholeWeek", dateFrom },
                                        { "@DateTo", dateTo },
                                        {"@AgentID", Agent}
                                     };


            string storedProcedureName = sp;

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

    }
}