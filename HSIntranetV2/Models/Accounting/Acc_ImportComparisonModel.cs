﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Acc_ImportComparisonModel
    {
        internal static DataTable GetASIimportTables()
        {
            string[,] spParameters = {
                                     };


            string storedProcedureName = "HS_Intranet_ASIimport_Comparison_GetTables";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_IntranetDB, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetComparison(string table1, string table2, string deleteBackup)
        {
            string[,] spParameters = {
                                         {"@prmTable1", table1},
                                         {"@prmTable2", table2},
                                         {"@prmDeleteBackup", deleteBackup}
                                     };


            string storedProcedureName = "HS_Intranet_ASIimport_Comparison";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_IntranetDB, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}