﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Acc_HSAffiliateProcessFeeModel
    {

        internal static DataTable GetAcc_HSAffiliateProcessFee(string dateFrom)
        {
            string[,] spParameters = { { "@Date", dateFrom } };

            string storedProcedureName = "uSp_HSAffiliate_ProcessFee";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable HSAffiliate_GetMondayToProcess()
        {
            string[,] spParameters = { };

            string storedProcedureName = "uSp_HSAffiliate_GetMondayToProcess";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


    }
}