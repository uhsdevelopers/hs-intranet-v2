﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Acc_WWProcessingFeesModel
    {

        internal static DataTable Acc_WWProcessingFees(string dateFrom, string agentID)
        {
            string[,] spParameters = {
                                        { "@DateForWholeWeek", dateFrom },
                                        { "@agentID", agentID }
                                     };


            string storedProcedureName = "uSp_AgentComision_Union_GetFeeToChargeByMethod";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable Acc_WWProcessingFeesDatails(string dateFrom, string dateTo, string agentID)
        {
            string[,] spParameters = {
                                        { "@DateForWholeWeek", dateFrom },
                                        { "@DateTo", dateTo },
                                        { "@agentID", agentID }
                                     };


            string storedProcedureName = "uSp_AgentComision_Union_GetFeeDetailToChargeByMethod";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

    }
}