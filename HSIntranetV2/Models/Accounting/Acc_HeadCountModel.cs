﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Acc_HeadCountModel
    {
        internal static DataTable GetHeadCount(string dateFrom, string dateTo, string agentID)
        {
            string[,] spParameters = {
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmDateTo", dateTo },
                                        { "@prmAgentID", agentID },
                                     };


            string storedProcedureName = "uSp_Intranet_GetHeadCountByAgent";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


    }
}