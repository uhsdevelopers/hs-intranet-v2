﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Acc_DGSAgentChargeModel
    {
        internal static System.Data.DataTable GetDGSAgentCharge(string dateFrom, string dateTo, string agentID)
        {

            string[,] spParameters = {
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmDateTo", dateTo },
                                        { "@prmAgentID", agentID },
                                     };


            string storedProcedureName = "AddOn_AgentCharge";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_DGS_AddOns, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}