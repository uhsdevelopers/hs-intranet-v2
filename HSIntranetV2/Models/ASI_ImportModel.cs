﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class ASI_ImportModel
    {

        internal static DataTable GetASIfigures(string startDate, string agentID, string makeBackup, string storedProcedureName)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };
            string[,] spParameters = {
                                        { "@prmStartDate", startDate },
                                        { "@prmAgentID", agentID },
                                        { "@prmMakeBackup", makeBackup }
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_IntranetDB, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;

        }

        internal static DataTable GetASIdetails(string startDate, string description, string agentID, string storedProcedureName)
        {
            string[,] spParameters = {
                                        { "@prmStartDate", startDate },
                                        { "@prmDescription", description },
                                        { "@prmAgentID", agentID }
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_IntranetDB, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


        internal static DataTable GetASIexcelInfo(string storedProcedureName)
        {
            string[,] spParameters = {
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_IntranetDB, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}