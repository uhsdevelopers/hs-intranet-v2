﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class NewGraderModel
    {
        internal static DataTable GetSportDetailsList(string sport)
        {
            string[,] spParameters = {
                { "@prmSportType", sport }
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSpPHN_Intranet_GetSportDetails", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetAllSports()
        {

            string[,] spParameters = {
                { "@prmShowAllSports", "1" }
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSpPHN_Srv_GetSports", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetOpenGames(string sport, string sportSubType)
        {

            string query = "select GameNum,GameDateTime, WagerCutoff,Status, Team1ID,Team1RotNum, Team2ID, Team2RotNum,Team1FinalScore,Team2FinalScore, FinalWinnerID, ListedPitcher1, ListedPitcher2, GradeDateTime  from tbGame where SportType='" + sport + "' and SportSubType='" + sportSubType + "' and Status in ('I','O','H') ORDER BY SportType, CASE ScheduleDate WHEN '1971-01-01 00:00:00.000' THEN '01/01/2050' ELSE ScheduleDate END, Team1RotNum, SportSubType,GameNum DESC";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, query);

            DataTable dtResult = _DAL.getDataTableQstring(cDAL);

            return dtResult;
        }

        internal static DataTable GetGradedGames(string sport, string sportSubType, string days)
        {

            string query = "select GameNum,GameDateTime, WagerCutoff,Status, Team1ID,Team1RotNum, Team2ID, Team2RotNum,Team1FinalScore,Team2FinalScore, FinalWinnerID, ListedPitcher1, ListedPitcher2, GradeDateTime from tbGame where SportType='" + sport + "' and SportSubType='" + sportSubType + "' and Status in ('C') and GameDateTime > = GETDATE()-" + days + " ORDER BY SportType, CASE ScheduleDate WHEN '1971-01-01 00:00:00.000' THEN '01/01/2050' ELSE ScheduleDate END, Team1RotNum, SportSubType,GameNum DESC";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, query);

            DataTable dtResult = _DAL.getDataTableQstring(cDAL);

            return dtResult;
        }

        internal static DataTable Getperiod(string sport, string sportSubType)
        {

            string query = "select * from tbGamePeriod where SportType='" + sport + "' and SportSubType='" + sportSubType + "'";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, query);

            DataTable dtResult = _DAL.getDataTableQstring(cDAL);

            return dtResult;
        }
        internal static DataTable GetGraderResults(string gameNum, string PeriodNumber)
        {

            string[,] spParameters = {
                { "@GameNum", gameNum },
                {"@PeriodNumber", PeriodNumber}
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "[dbo].[spGradingGetWagersAffected]", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GradingMain(string gameNum, string periodNum, string comments, string gameCancelledFlag, string team1Score, string team2Score, string eopTeam1Score, string eopTeam2Score, string startingPitcher1, string startingPitcher2, string gradeSpreadReqFlag, string gradeMoneyLineReqFlag, string gradeTtlPtsReqFlag, string cancelSpreadFlag, string cancelMoneyLineFlag, string cancelTtlPtsFlag, string dailyFigureDate, string requestedBy = "TestPage", string InetTarget = "", string Store = "")
        {

            string[,] spParameters = {
                { "@gameNum", gameNum },{ "@periodNum", periodNum },
                { "@comments", comments },{ "@gameCancelledFlag", gameCancelledFlag },
                { "@team1Score", team1Score },{ "@team2Score", team2Score },
                { "@eopTeam1Score", eopTeam1Score },{ "@eopTeam2Score", eopTeam2Score },
                { "@startingPitcher1", startingPitcher1 },{ "@startingPitcher2", startingPitcher2 },
                { "@gradeSpreadReqFlag", gradeSpreadReqFlag },{ "@gradeMoneyLineReqFlag", gradeMoneyLineReqFlag },
                { "@gradeTtlPtsReqFlag", gradeTtlPtsReqFlag },{ "@cancelSpreadFlag", cancelSpreadFlag },
                { "@cancelMoneyLineFlag", cancelMoneyLineFlag },{ "@cancelTtlPtsFlag", cancelTtlPtsFlag },
                { "@dailyFigureDate", dailyFigureDate },{ "@requestedBy", "NewGRader" },
                { "@InetTarget", "" },{ "@Store", "" }
            };

            _BLL.cl_LogAction logAction = new _BLL.cl_LogAction();
            logAction.User = "Intranet";
            logAction.Action = "GradingMain";


            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "[dbo].[spGradingMain]", spParameters, logAction);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable GetPointsByPeriod(string gameNum, string PeriodNumber)
        {

            string[,] spParameters = {
                { "@GameNum", gameNum },
                {"@CurrentPerdioNum", PeriodNumber}
            };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "[dbo].[spGradingGetTeamScorePeriod]", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}