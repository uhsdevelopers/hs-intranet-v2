﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class LineManagerModel
    {
        internal static DataTable GetDGSreleaseGrader()
        {

            string[,] spParameters = { };

            string storedProcedureName = "uSp_Intranet_GetDGSreleaseGrader";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_DGS_AddOns, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;

        }

        internal static DataTable GetImporterLog(string teamRot1)
        {

            string[,] spParameters = { { "@TeamRot1", teamRot1 } };

            string storedProcedureName = "uSp_GameImportedBy";

            _BLL.cDAL cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable FixASIBalance()
        {

            string[,] spParameters = { };

            string storedProcedureName = "uSp_Local_FixBeforeZeroBalance";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;

        }

        internal static DataTable FixDGSBalance()
        {

            string[,] spParameters = { };

            string storedProcedureName = "uSp_SynchAll";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_DGS_AddOns, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}