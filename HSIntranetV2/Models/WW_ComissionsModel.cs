﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class WW_ComissionsModel
    {

        internal static DataTable GetWWComissions(string dateFrom, string dateTo, string agentID)
        {
            string[,] spParameters = {
                                        { "@DateForWholeWeek", dateFrom },
                                        { "@DateTo", dateTo },
                                        { "@agentID", agentID },
                                        { "@company", "wagerweb" }

                                     };


            string storedProcedureName = "uSp_WWAffiliate_GetFeeFigure";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

    }
}