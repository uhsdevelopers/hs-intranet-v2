﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class OfficePoolModel
    {
        public DataTable IsCustomerAbleToPlay(string storedProcedureName, string customerid, string poolid = "5")
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@CustomerID", customerid },
                                      { "@PoolId", poolid }};

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        public DataTable GetPoolListOpen(string storedProcedureName, string poolid = "5")
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@PoolId", poolid } };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        public DataTable GetPoolListOpenintra(string storedProcedureName, string poolid, string PoolGameId)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@PoolId", poolid }, { "@PoolGameId", PoolGameId } };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        public DataTable GetCustomerList(string storedProcedureName, string poolGameid = "160")
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@PoolId", poolGameid } };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        public DataTable GetPoolTournamentTeams(string storedProcedureName, string poolGameid = "161")
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@PoolGameId", poolGameid } };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        public DataTable GradeTournamentPosition(string storedProcedureName, string poolGameid, string Position, string TeamId, string user)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@PoolGameId", poolGameid }, { "@TournamentPositionId", Position }, { "@TeamId", TeamId }, { "@LastChangeBy", user } };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        public DataTable GetWinnerList(string storedProcedureName, string poolGameid, string poolsquareid)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@PoolGameId", poolGameid }, { "@PoolSquareId", poolsquareid } };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        public DataTable GetWinnersByPeriodsPlusReverse(string storedProcedureName, string poolsquareid)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@ParentPoolSquareId", poolsquareid } };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }



        public DataTable GetActivePoolList(string storedProcedureName, string PoolId, string CustomerID)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = { { "@PoolId", PoolId }, { "@CustomerID", CustomerID } };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        public DataTable AddToInclusionList(string storedProcedureName, string customerid, string user, string poolid = "5", string InclusionTypeId = "1")
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = {
                                      { "@PoolId", poolid },
                                      { "@InclusionId", customerid },
                                      { "@InclusionTypeId", InclusionTypeId },
                                      { "@LastChangeBy", user }
                                      };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        public DataTable AddToExclusionList(string storedProcedureName, string customerid, string user, string poolid = "5", string InclusionTypeId = "1")
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = {
                                      { "@PoolId", poolid },
                                      { "@ExclusionId", customerid },
                                      { "@ExclusionTypeId", InclusionTypeId },
                                      { "@LastChangeBy", user }
                                      };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        public DataTable GetTeamstoGrade(string storedProcedureName, string PoolGameId)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = {
                                      { "@PoolGameId", PoolGameId }
                                      };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        public DataTable AddGameScoresByPeriod(string storedProcedureName, string PoolGameId, string GamePeriodId, string Team1Score, string Team2Score, string LastChangeBy)
        {
            //string[,] spParameters = { { "prmSpreadDifference", spreadDifference }, { "prmTotalDifference", totalDifference } };

            string[,] spParameters = {
                                      { "@PoolGameId", PoolGameId },
                                      { "@GamePeriodId", GamePeriodId },
                                      { "@Team1Score", Team1Score },
                                      { "@Team2Score", Team2Score },
                                      { "@LastChangeBy", LastChangeBy }
                                      };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        public DataTable CreatePoolGame(string storedProcedureName, string poolId, string cutOffDate, string sportTypeId, string sportsubTypeId, string team1Id, string team2Id)
        {


            string[,] spParameters = {
                                      { "@PoolId", poolId },
                                      { "@CutOffDate", cutOffDate },
                                      { "@SportTypeId", sportTypeId },
                                      { "@SportsubTypeId", sportsubTypeId },
                                      { "@Team1Id", team1Id },
                                      { "@Team2Id", team2Id }
                                      };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        public DataTable CreateGamesTournamentTeams(string storedProcedureName, string NewPool, string BasePool)
        {


            string[,] spParameters = {
                                      { "@PoolGameId", NewPool },
                                      { "@sourcePoolGameId", BasePool }
                                      };

            var cDal = new _BLL.cDAL(_BLL.connString_OfficePool, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


    }
}