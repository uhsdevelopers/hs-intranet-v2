﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class WW_WLReportModel
    {


        internal static DataTable GetWLSummary(string AgentID, string DateFrom, string DateTo, string company)
        {
            string[,] spParameters = {
                                        { "@prmAgentID ", AgentID },
                                        { "@prmDateFrom", DateFrom },
                                        { "@prmDateTo ", DateTo },
                                        { "@prmCompany ", company }
                                     };
            string storedProcedureName = "uSp_WWAffiliate_WLSummary";
            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable GetWLFigureDetail(string AgentID, string DateFrom, string DateTo, string company, string reportType)
        {
            string storedProcedureName = "";
            string[,] spParameters = {
                                        { "@prmAgentID ", AgentID },
                                        { "@prmDateFrom", DateFrom },
                                        { "@prmDateTo", DateTo },
                                        { "@prmCompany ", company }
                                     };
            if (reportType == "Win/Lost")
                storedProcedureName = "uSp_WWAffiliate_WLFigureDetail";
            else if (reportType == "Adjustment")
                storedProcedureName = "uSp_WWAffiliate_WLAdjustmentDetail";
            else
                storedProcedureName = "uSp_WWAffiliate_WLLiveDealerDetail";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

    }
}