﻿namespace Signup_Services.Models
{
    public class EmailSource
    {
        public const string CustomerService = "CSD";
        public const string Intranet = "INT";
        public const string ClientServices = "CSV";
        public const string WageringMenu = "WAG";
        public const string WagerWeb = "WAW";
        public const string DrBookie = "DB";
        public const string Bet105 = "B105";
        public const string BetRegal = "BetRegal";
        public const string BetRegalMX = "BetRegalMX";
        public const string BigBet = "BigBet";
        public const string BetRegalNet = "BetRegalNet";
    }
}