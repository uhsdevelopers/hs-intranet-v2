﻿using System.Configuration;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace HSIntranetV2.Models
{
    public class Casino
    {

        public static string GiveOCRBonus(string userid, int bonusID, int rounds, string games, string bankid)
        {

            var hash = GenerateMd5Hash(userid + rounds + games + bonusID);
            var OCRBonusURL = ConfigurationManager.AppSettings["OCRBonusURL"];

            var url = OCRBonusURL;
            url = url + bankid;
            url = url + "&userId=" + userid;
            url = url + "&rounds=" + rounds;
            url = url + "&games=" + games;
            url = url + "&extBonusId=" + bonusID;
            url = url + "&hash=" + hash;
            url = url + "&format=json";


            var webReq = (HttpWebRequest)WebRequest.Create(url);

            webReq.Method = WebRequestMethods.Http.Get;
            webReq.Accept = "application/json; charset=utf-8";
            webReq.ContentType = "application/json; charset=utf-8";
            var webResp = (HttpWebResponse)webReq.GetResponse();

            var answer = webResp.GetResponseStream();
            var _Answer = new StreamReader(answer);
            string postData = _Answer.ReadToEnd();

            return postData;
        }


        private static string GenerateMd5Hash(string requestParm)
        {
            var passKey = ConfigurationManager.AppSettings["PASS_KEY"];
            string source = requestParm + passKey;

            using (MD5 md5Hash = MD5.Create())
            {
                string hash = GetMd5Hash(md5Hash, source);

                return hash;

            }


        }


        private static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }
    }
}
