﻿using System;
using System.Text;


namespace Signup_Services.Models
{
    [Serializable]
    public class Email
    {

        public string body;


        public string subject;


        public string Sender;

        [NonSerialized()]
        public string senderPassword;


        public string receiver;


        public bool isHtml;



        public Email()
        {
            this.isHtml = false;
            //this.useDefaultSmtp = true;
            //this.useDefaultSmtpCredentials = true;
        }



        public void SendGenericEmail(string toEmail, string subject, string body)
        {

            var emailbody = "{DATE-TIME}" + " " + body;

            var sb = new StringBuilder(emailbody);

            sb.Replace("{DATE-TIME}", DateTime.Now.ToString());
            sb.AppendLine();

            var email = new Email
            {
                //subject = "Authorize Login Attempt",
                subject = subject,
                receiver = toEmail,
                body = sb.ToString(),
                isHtml = true
            };

            Signup.SendEmail("CSD", email);

        }



    }
}