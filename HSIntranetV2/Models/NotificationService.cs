﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace HSIntranetV2.Models
{
    public class NotificationService
    {
        static readonly String Smsurl = System.Configuration.ConfigurationManager.AppSettings["SmsURL"];
        static readonly Uri BaseUri = new Uri(Smsurl);
        static Uri _myUri;


        public class SmsResults
        {
            public string Status { get; set; }
            public string Error { get; set; }
            public string SmslogId { get; set; }
            public string Queue { get; set; }
            public string To { get; set; }
        }

        private static bool BypassAllCertificateStuff(object sender, X509Certificate cert, X509Chain chain, System.Net.Security.SslPolicyErrors error)
        {
            return true;
        }



        internal static string SendSms(string user, string token, string recipient, string message)
        {
            var messageto = message.Replace("ñ", "%C3%B1");


            var parameters = new StringBuilder();
            parameters.Append("?app=webservices&u=");
            parameters.Append(user);
            parameters.Append("&h=");
            parameters.Append(token);
            parameters.Append("&ta=pv&to=");
            parameters.Append(recipient);
            parameters.Append("&msg=");
            parameters.Append(messageto);
            parameters.Append("&format=json");

            _myUri = new Uri(BaseUri, parameters.ToString());

            //_myUri = new Uri(BaseUri, "?employeeName=&commentBy=&dateFrom=01%2F01%2F2014&dateTo=04%2F05%2F2014&hrrRequestId=&isPositiveComment=2&departmentId=5000");

            using (var client = new WebClient())
            {


                string result;
                try
                {
                    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                    result = client.DownloadString(_myUri.ToString());
                }
                catch (WebException e)
                {

                    throw e;
                }
                var output = JsonConvert.SerializeObject(result);

                var smsResultNotification = JsonConvert.DeserializeObject<SmsResults>(output);

                return smsResultNotification.Status;
            }



        }
    }
}
