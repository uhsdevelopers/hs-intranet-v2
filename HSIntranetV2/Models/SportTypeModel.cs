﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class SportTypeModel
    {

        public class SportType
        {
            public string SportSubTypeCopy { get; set; }
            public string SportTypeNew { get; set; }
            public string SportSubTypeNew { get; set; }
            public string PeriodType { get; set; }
            public string DrawFlag { get; set; }

        }

        internal static System.Data.DataTable GetProdSportTypes()
        {
            string[,] spParameters = { };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSp_InstantAction_GetSportTypes", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetHoldSportTypes()
        {
            string[,] spParameters = { };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSp_Intranet_GetHoldSportTypes", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable SetHoldSportType(string sportType, string sportSubType)
        {
            string[,] spParameters = {
                                         { "sportType", sportType },
                                         { "sportSubType", sportSubType }
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSp_Intranet_InsertHoldSportType", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable SetProdSportType(string sportType, string sportSubType)
        {
            string[,] spParameters = {
                                         { "sportType", sportType },
                                         { "sportSubType", sportSubType }
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSp_Intranet_InsertProdSportType", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }



        internal static DataTable InsertNewSport(SportType c_sportType)
        {
            string[,] spParameters = {
                                         { "SportTypeNew", c_sportType.SportTypeNew },
                                         { "SportSubTypeNew", c_sportType.SportSubTypeNew },
                                         { "SportSubTypeCopy", c_sportType.SportSubTypeCopy },
                                         { "PeriodType", c_sportType.PeriodType },
                                         { "DrawFlag", c_sportType.DrawFlag }
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, "uSp_Intranet_InsertNewSportType", spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}