﻿using System;
using System.Data;

namespace HSIntranetV2.Models
{
    public class WW_CasinoTransactionsModel
    {

        internal static DataTable GetWWCasinoTransactions(string dateFrom, string dateTo, string masterAgentID, string customerID, string isNonPosted)
        {

            _BLL.cDAL cDAL = new _BLL.cDAL();
            dateTo = (DateTime.Parse(dateTo).AddDays(1)).ToString();

            string storedProcedureName = "spArptPostedCasinoTransactions";

            string[,] spParameters = {
                                        { "@CustomerID", customerID },
                                        { "@begdate", dateFrom },
                                        { "@enddate", dateTo },
                                        { "@MasterAgentID", masterAgentID }

                                     };

            cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            if (isNonPosted == "true")
            {

                storedProcedureName = "spArptNonPostedCasinoTransactions";

                string[,] spParameters2 = {

                                        { "@CustomerID", customerID },
                                        { "@MasterAgentID", masterAgentID }

                                     };

                cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters2);

            }

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;

        }

    }
}