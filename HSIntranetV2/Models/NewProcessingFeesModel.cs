﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class NewProcessingFeesModel
    {
        internal static DataTable GetNewProcessingFees(string dateFrom, string dateTo, string agentID)
        {
            string[,] spParameters = {
                                        { "@DateForWholeWeek", dateFrom },
                                        { "@DateTo", dateTo },
                                        { "@AgentID", agentID },
                                     };


            string storedProcedureName = "uSp_AgentComision_DWGetFeeDetailToChargeByMethod";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable GetNewProcessingFeesExcluded(string dateFrom, string dateTo, string agentID)
        {
            string[,] spParameters = {
                                        { "@DateForWholeWeek", dateFrom },
                                        { "@DateTo", dateTo },
                                        { "@AgentID", agentID },
                                     };


            string storedProcedureName = "uSp_AgentComision_DWGetExcludedTransactions";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}