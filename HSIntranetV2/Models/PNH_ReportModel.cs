﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class PNH_ReportModel
    {
        internal static DataTable PHN_PlayerCount(string dateFrom, string dateTo, string agentID)
        {
            string[,] spParameters = {
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmDateTo", dateTo },
                                        { "@prmAgentID", agentID },
                                     };


            string storedProcedureName = "uSp_Intranet_GetHeadCountByAgent";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable PHN_Deposit(string dateFrom, string dateTo, string agentID)
        {
            string[,] spParameters = {
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmDateTo", dateTo },
                                        { "@prmAgentID", agentID },
                                     };


            string storedProcedureName = "uSp_Intranet_GetHeadCountByAgent";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable PHN_BalanceReport(string agentID)
        {
            string[,] spParameters = {
                                        { "@prmAgentID", agentID },
                                     };


            string storedProcedureName = "uSp_Intranet_GetHeadCountByAgent";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


        internal static DataTable CancelWager_GetWagers(string storedProcedureName, string dateStart, string customerid, string dateEnd)
        {
            string[,] spParameters = { { "@prmCustomerID", customerid }, { "@prmDateFrom", dateStart }, { "@prmDateTo", dateEnd } };

            var cDal = new _BLL.cDAL(_BLL.Host_dbConnString, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }


        internal static DataTable CancelWager_CancelTicket(string storedProcedureName, string ticket)
        {


            string[] arr_ticket = ticket.Split('-');

            string[,] spParameters = { { "@prmTicketNumber", arr_ticket[0] }, { "@prmWagerNumber", arr_ticket[1] } };

            var cDal = new _BLL.cDAL(_BLL.Host_dbConnString, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }

        internal static DataTable CancelWager_SynchTrans(string storedProcedureName)
        {

            string[,] spParameters = { };

            var cDal = new _BLL.cDAL(_BLL.Host_dbConnString, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }
    }
}