﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class WwConvertionRateModel
    {
        internal static DataSet GetRate(string dateFrom, string dateTo, string agentID)
        {
            string[,] spParameters = {
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmDateTo", dateTo },
                                        { "@prmAgentID", agentID }
                                     };


            const string storedProcedureName = "uSp_Intranet_WW_GetConvertionRate";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = new DataTable();
            DataSet dsResult = _DAL.getDataSet(cDAL);

            dtResult = dsResult.Tables.Count == 1 ? dsResult.Tables[0] : dsResult.Tables[1];

            return dsResult;
        }


        internal static DataTable GetRateV2(string dateFrom, string dateTo, string agentId)
        {
            string[,] spParameters = {
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmDateTo", dateTo },
                                        { "@prmAgentID", agentId }
                                     };


            const string storedProcedureName = "uSp_Intranet_WW_GetConvertionRatev2";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            //DataTable dtResult = new DataTable();
            //DataSet dsResult = _DAL.getDataSet(cDAL);

            var dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


        internal static DataTable GetDeposit(string dateFrom, string dateTo, string agentId)
        {
            string[,] spParameters = {
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmDateTo", dateTo },
                                        { "@prmAgentID", agentId }
                                     };


            const string storedProcedureName = "uSp_Intranet_WW_GetConvertionRatev3";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            //DataTable dtResult = new DataTable();
            //DataSet dsResult = _DAL.getDataSet(cDAL);

            var dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }



        internal static DataTable GetRetention(string storedProcedureName, string dateFrom, string company = "wagerweb")
        {
            string[,] spParameters = {
                                        { "@SignUpDate", dateFrom }
                                     };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;



        }


    }
}