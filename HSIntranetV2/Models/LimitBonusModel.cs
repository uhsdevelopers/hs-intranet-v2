﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class LimitBonusModel
    {
        internal static DataTable GetLimitBonusList(string storedProcedureName)
        {
            string[,] spParameters = { { "@Value", "1" } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
        internal static DataTable SetLimitBonusByCustomer(string CustomerID, string hasBigBonus, string storedProcedureName)
        {
            string[,] spParameters = { { "@Customer", CustomerID }, { "@Value", hasBigBonus } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}