﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class FeedbackModel
    {

        internal static DataTable GetFeedback(string storedProcedureName, string dateStart, string dateEnd)
        {
            string[,] spParameters = { { "@dateStart", dateStart }, { "@dateEnd", dateEnd } };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;

        }

    }
}