﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class RollinIfbetReportModel
    {
        internal static DataTable GetRollinIfbetReport(string dateFrom, string dateTo, string CustomerID)
        {
            string[,] spParameters = {
                                        { "@Date1 ", dateFrom },
                                        { "@Date2 ", dateTo },
                                        { "@CustomerID ", CustomerID  }
                                     };

            string storedProcedureName = "[uSp_local_GetTopRollinIfbet ]";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}

