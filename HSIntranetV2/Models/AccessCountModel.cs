﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class AccessCountModel
    {
        internal static DataTable GetAccessCount(string storedProcedureName, string From, string Limit)
        {
            string[,] spParameters = {
                                        { "@Limit", Limit },
                                        { "@StartDate", From}
                                     };

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


    }
}