﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class BetRegal
    {

        internal static DataTable BetRegal_GetFundedNonFunded(string storedProcedureName, string condition1, string featureId, string startdate, string customerid)
        {


            string[,] spParameters = { { "@Condition", condition1 }, { "@FeatureID", featureId }, { "@StartDate", startdate }, { "@CustomerID", customerid } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }
        internal static DataTable BrGetCasinoPlayers(string storedProcedureName, string agent)
        {


            string[,] spParameters = { { "@Agent", agent } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable BRGetOntarioPlayers(string storedProcedureName)
        {


            string[,] spParameters = { };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }
        internal static DataTable BrGetRevenue(string storedProcedureName, string starDate, string endDate)
        {


            string[,] spParameters = { { "@StartDate", starDate }, { "@EndDate", endDate } };

            var cDal = new _BLL.cDAL(_BLL.Host_dbConnString, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }
        //This operations are for Data migration 
        internal static DataTable BrGetLocalActivy(string storedProcedureName, string customerid)
        {


            string[,] spParameters = { { "@CustomerID", customerid } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable BrGetWagers(string storedProcedureName, string customerid, string operation)
        {


            string[,] spParameters = { { "@CustomerID", customerid }, { "@Kind", operation } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

    }
}