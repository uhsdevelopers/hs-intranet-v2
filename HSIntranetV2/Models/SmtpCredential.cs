﻿using System;


namespace Signup_Services.Models
{
    public class SmtpCredential
    {

        public string Host;

        public string User;

        [NonSerialized()]
        public string Password;
    }
}