﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class InetAccessLogModel
    {
        internal static DataTable GetInetAccessLog(string dateFrom, string dateTo, string accountId, string ipAddress, string relatedAccounts, string company)
        {
            string[,] spParameters = {
                                        { "@begDate", dateFrom },
                                        { "@endDate", dateTo },
                                        { "@loginID", accountId },
                                        { "@IPAddress", ipAddress },
                                        { "@RelatedAccounts", relatedAccounts }
                                     };


            var storedProcedureName = "spcGetInternetAccessLog";
            var cDal = new _BLL.cDAL(_BLL.connString_InetAccessLog, storedProcedureName, spParameters);

            if (company == "DGS")
            {

                string[,] spParametersDgs = {
                                         { "@prmPlayer", accountId },
                                        { "@prmDateFrom", dateFrom },
                                        { "@prmDateTo", dateTo }


                                     };

                storedProcedureName = "AddOn_GetIpByplayer";
                cDal = new _BLL.cDAL(_BLL.connString_DGS_AddOns, storedProcedureName, spParametersDgs);

            }
            else
            {
                if (company == "TRAD")
                {

                    string[,] spParametersTrAd = {
                                        { "@begDate", dateFrom },
                                        { "@endDate", dateTo },
                                        { "@loginID", accountId },
                                        { "@IPAddress", ipAddress }


                                     };

                    storedProcedureName = "spCrInetAccessLogHS";
                    cDal = new _BLL.cDAL(_BLL.connString_Trad, storedProcedureName, spParametersTrAd);

                }
            }




            DataTable dtResult = _DAL.getDataTable(cDal);

            return dtResult;
        }

        internal static DataTable GetRelatedAccountsByIP(string accountId, string ipAddress)
        {
            string[,] spParameters = {

                                        { "@loginID", accountId },
                                        { "@IPAddress", ipAddress }
                                     };


            string storedProcedureName = "spcGetInternetAccessIPSummary";

            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_InetAccessLog, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }




    }
}