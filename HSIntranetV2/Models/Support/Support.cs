﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class Support
    {



        internal static DataTable GetMessageList(string storedProcedureName, string Active)
        {


            string[,] spParameters = { { "active", Active } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetsiteStats(string storedProcedureName, string startdate, string enddate, string domain)
        {


            string[,] spParameters = { { "@dateinit", startdate }, { "@dateend", enddate }, { "@domain", domain } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable CheckLastActivity(string storedProcedureName)
        {


            string[,] spParameters = { };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


        internal static DataTable GetPayoutCode(string storedProcedureName, string startdate)
        {


            string[,] spParameters = { { "@Begin", startdate } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetLateBets(string storedProcedureName, string startdate, string enddate, string @increase)
        {


            string[,] spParameters = { { "@dateFrom", startdate }, { "@dateTo", enddate }, { "@increase", increase } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }


    }
}