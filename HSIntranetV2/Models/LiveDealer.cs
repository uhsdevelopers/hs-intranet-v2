﻿using HSIntranetV2.LiveDealerReference;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace HSIntranetV2.Models
{
    public class LiveDealer
    {


        public class Datum
        {
            public string client { get; set; }
            public string site { get; set; }
            public string currency { get; set; }
            public string externalId { get; set; }
            public int games { get; set; }
            public double wagered { get; set; }
            public double winnings { get; set; }
            public double winLoss { get; set; }
            public double hold { get; set; }
            public double tips { get; set; }
        }

        public class Currency
        {
            public int id { get; set; }
            public string ISO { get; set; }
            public string symbol { get; set; }
            public double exchangeRate { get; set; }
        }

        public class SignupByPass
        {
            public int Id { get; set; }
            public string statusid { get; set; }
            public string Codeid { get; set; }

        }
        public class SignupByPass2
        {

            public string statusid { get; set; }
            public string Codeid { get; set; }

        }

        public class MetaData
        {
            public List<Currency> Currencies { get; set; }
        }

        public class Report
        {
            public List<Datum> data { get; set; }
            public MetaData metaData { get; set; }
        }

        public class Status
        {
            public string code { get; set; }
            public string description { get; set; }
        }

        public class RootObject
        {
            public Report report { get; set; }
            public Status status { get; set; }
        }






        //
        // GET: /LiveDealer/

        //internal  ResponseSearchAgents GetAgent(string token, string id)
        //{
        //    var result = string.Empty;


        //    var ldservice = new LiveDealerservice.BackOfficeAPIClient();

        //     ResponseSearchAgents agents = ldservice.searchAgents(token, id);





        //    return agents;
        //} 

        //internal ResponseAgentPlayers GetPlayerByAgent(string token, string agentid, string signature)
        //{
        //    var result = string.Empty;

        //    var ldservice = new BackOfficeAPIClient();

        //    var listaPlayers = ldservice.agentPlayers(token, agentid, signature);
        //    return listaPlayers;

        //}

        //internal ResponseGameLogTotals GetPlayertotal(string token, string playerid, string signature)
        //{
        //    var result = string.Empty;

        //    var ldservice = new BackOfficeAPIClient();

        //    DateTime stardate = new DateTime(2015, 4, 6, 1, 47, 0);
        //    DateTime enddate = new DateTime(2015, 4, 7, 1, 47, 0);


        //    var gametotal = ldservice.gameLogTotals(token, playerid, signature, stardate, enddate);
        //    return gametotal;

        //}

        //internal ResponseReportFilters GetReport(string token)
        //{
        //    var ldservice = new BackOfficeAPIClient();
        //    ResponseReportFilters result = ldservice.reportFilters(token);

        //    return result;

        //}

        //internal ResponseReportOfReportRowGgrTq_Sk5C80 GetReportData(string token, int clientid, int siteid, int gametype, string stardate, string enddate)
        //{
        //    BackOfficeAPIClient ldservice;
        //    ResponseReportOfReportRowGgrTq_Sk5C80 result;

        //    DateTime stardate = new DateTime(2015, 4, 6, 1, 47, 0);
        //    DateTime enddate = new DateTime(2015, 4, 7, 1, 47, 0);


        //    using (ldservice = new BackOfficeAPIClient())
        //    {
        //        result = ldservice.repGgrData(token, clientid, siteid, gametype, stardate, enddate);
        //    }

        //    using (ResponseRepGgr result = ldservice.repGgrData(token, clientid, siteid, gametype, stardate, enddate))

        //        return result;
        //}



        internal RootObject GetReportData(string token, int clientid, int siteid, int gametype, string stardate, string enddate)
        {

            //BackOfficeAPIClient ldservice;


            //DateTime stardate = new DateTime(2015, 4, 6, 1, 47, 0);
            //DateTime enddate = new DateTime(2015, 4, 7, 1, 47, 0);
            //var task = MakeAsyncRequest(url + key, "text/html");
            //return Content(task.Result);
            new JsonSerializer();
            //var stream = new MemoryStream();

            var url = "http://www.golivedealer.net/gateway/BackOfficeAPI/BackOfficeAPI.svc/reports/ggr?token=";
            url = url + token;
            url = url + "&clientId=" + clientid;
            url = url + "&siteId=" + siteid;
            url = url + "&gameType=" + gametype;
            url = url + "&startDate=" + stardate;
            url = url + "&endDate=" + enddate;
            url = url + "&format=json";


            var webReq = (HttpWebRequest)WebRequest.Create(url);

            webReq.Method = WebRequestMethods.Http.Get;
            webReq.Accept = "application/json; charset=utf-8";

            webReq.ContentType = "application/json; charset=utf-8";

            //WebReq.Headers.Add("Content-Type", "application/json; charset=utf-8");
            var webResp = (HttpWebResponse)webReq.GetResponse();
            //WebResp.ContentType = "application/json; charset=utf-8";
            //WebResp = (HttpWebResponse)WebReq.GetResponse();

            var answer = webResp.GetResponseStream();
            var _Answer = new StreamReader(answer);
            string postData = _Answer.ReadToEnd();




            var jss = new JavaScriptSerializer();
            var user = jss.Deserialize<RootObject>(postData);



            return user;
        }


        internal string GetSevenData(string customerid, string stardate, string enddate)
        {

            new JsonSerializer();

            var username = System.Configuration.ConfigurationManager.AppSettings["SevenLogin"];
            var password = System.Configuration.ConfigurationManager.AppSettings["sevenPassword"];
            string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1")
                                           .GetBytes(username + ":" + password));
            //httpWebRequest.Headers.Add("Authorization", "Basic " + encoded);

            var url = System.Configuration.ConfigurationManager.AppSettings["SevenURL"];
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            url = url + "player=" + customerid + "@heritagesports.com";
            url = url + "&start=" + stardate + " 00:01:01";
            url = url + "&end=" + enddate + " 23:59:01";


            var myWebRequest = WebRequest.Create(url);
            var webReq = (HttpWebRequest)myWebRequest;

            //var webReq = (HttpWebRequest)WebRequest.Create(url);
            webReq.PreAuthenticate = true;
            webReq.Headers.Add("Authorization", "Basic " + encoded);

            webReq.Method = WebRequestMethods.Http.Get;
            webReq.Accept = "application/json; charset=utf-8";


            webReq.ContentType = "application/json; charset=utf-8";


            var webResp = (HttpWebResponse)webReq.GetResponse();

            var answer = webResp.GetResponseStream();
            var _Answer = new StreamReader(answer);
            string postData = _Answer.ReadToEnd();

            var jss = new JavaScriptSerializer();
            return postData;
        }



        internal DataTable GetNonPostedPlayers(string storedProcedureName, string customer, string provider, string company = "Heritage")
        {
            string[,] spParameters = { { "@Customerid", customer }, { "@MasterAgentID", company }, { "@Provider", provider } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal string SendEmailSignup(string requestid, string sportbookid)
        {


            var url = System.Configuration.ConfigurationManager.AppSettings["SentEmail"];
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string responseFromServer = "";

            var myWebRequest = WebRequest.Create(url);
            var webReq = (HttpWebRequest)myWebRequest;
            webReq.Method = WebRequestMethods.Http.Post;
            webReq.Accept = "application/json; charset=utf-8";
            webReq.ContentType = "application/json; charset=utf-8";

            string postDatav1 = "{requestid:'" + requestid + "', sportbookid: '" + sportbookid + "'}";
            byte[] byteArray = Encoding.UTF8.GetBytes(postDatav1);

            // Set the ContentType property of the WebRequest.
            //myWebRequest.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.
            myWebRequest.ContentLength = byteArray.Length;

            // Get the request stream.
            Stream dataStream = myWebRequest.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();

            // Get the response.
            WebResponse response = myWebRequest.GetResponse();

            // Get the stream containing content returned by the server.
            // The using block ensures the stream is automatically closed.
            using (dataStream = response.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.
                if (dataStream != null)
                {
                    var reader = new StreamReader(dataStream);
                    // Read the content.
                    responseFromServer = reader.ReadToEnd();
                }
                else
                {
                    responseFromServer = "Error";
                }
                // Display the content.
                Console.WriteLine(responseFromServer);
            }


            return responseFromServer;
        }


        internal string GetSOData()
        {

            //BackOfficeAPIClient ldservice;


            //DateTime stardate = new DateTime(2015, 4, 6, 1, 47, 0);
            //DateTime enddate = new DateTime(2015, 4, 7, 1, 47, 0);
            //var task = MakeAsyncRequest(url + key, "text/html");
            //return Content(task.Result);
            new JsonSerializer();
            //var stream = new MemoryStream();

            var url = "http://server1.sportsoptions.com/SO/feeds/scoresfull.html";
            //url = url + "&format=json";


            var webReq = (HttpWebRequest)WebRequest.Create(url);

            webReq.Method = WebRequestMethods.Http.Get;
            webReq.Accept = "application/json; charset=utf-8";

            webReq.ContentType = "application/json; charset=utf-8";

            //WebReq.Headers.Add("Content-Type", "application/json; charset=utf-8");
            var webResp = (HttpWebResponse)webReq.GetResponse();
            //WebResp.ContentType = "application/json; charset=utf-8";
            //WebResp = (HttpWebResponse)WebReq.GetResponse();

            var answer = webResp.GetResponseStream();
            var _Answer = new StreamReader(answer);
            string postData = _Answer.ReadToEnd();




            var jss = new JavaScriptSerializer();
            var user = jss.Deserialize<RootObject>(postData);



            return postData;
        }


        public static IPEndPoint BindIpEndPointCallback(ServicePoint servicePoint, IPEndPoint remoteEndPoint, int retryCount)
        {
            Console.WriteLine("BindIPEndpoint called");
            return new IPEndPoint(IPAddress.Any, 5000);

        }

        public static Task<string> MakeAsyncRequest(string url, string contentType)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Accept = "application/json";
            //request.Headers.Add(HttpRequestHeader.ContentType, "application/json; charset=utf-8");
            request.ContentType = contentType;
            request.Method = WebRequestMethods.Http.Get;
            request.Timeout = 20000;
            request.Proxy = null;
            request.ServicePoint.BindIPEndPointDelegate = new BindIPEndPoint(BindIpEndPointCallback);
            request.ContinueTimeout = 10000;


            try
            {
                Task<WebResponse> task = Task.Factory.FromAsync(
                    request.BeginGetResponse,
                    asyncResult => request.EndGetResponse(asyncResult),
                    null);
                return task.ContinueWith(t => ReadStreamFromResponse(t.Result));
            }
            catch (WebException webException)
            {
                return null;
            }





        }

        private static string ReadStreamFromResponse(WebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            using (var sr = new StreamReader(responseStream))
            {
                //Need to return this response 
                string strContent = sr.ReadToEnd();
                return strContent;
            }
        }


        internal String Gettoken(string login, string password)
        {
            var ldservice = new BackOfficeAPIClient();
            var responselogin = ldservice.login(login, password);

            return responselogin.token;
        }


        internal string SentHtmlEmail(string html, string Header, string Subject, string footer, string CasinoDisabled)
        {


            // Create a new email message.
            MailMessage message = new MailMessage();
            var username = System.Configuration.ConfigurationManager.AppSettings["email_CSD"];
            // Set the sender and recipient addresses.
            message.From = new MailAddress(username);
            message.To.Add(new MailAddress("hugo.leiva@heritagesports.com"));


            message.Subject = Subject;
            message.Body = Header + html + CasinoDisabled + footer;
            message.IsBodyHtml = true;

            // Send the email.
            var Host = System.Configuration.ConfigurationManager.AppSettings["email_SmtpClient"];

            var password = System.Configuration.ConfigurationManager.AppSettings["email_CSD_Password"];
            SmtpClient client = new SmtpClient();
            client.Host = Host;
            // Set the SMTP port.
            client.Port = 587;

            // Enable SSL.
            client.EnableSsl = true;

            // Set the credentials.
            client.Credentials = new NetworkCredential(username, password);
            client.Send(message);

            Console.WriteLine("Email sent successfully.");
            return "Sent";
        }

        internal DataTable GenerateCasinoProfit()
        {
            string[,] spParameters = {
                                     };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "spIntra_AddCasinoProfit", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable GenerateCasinoProfit(string startDate, string endDate)
        {
            if (startDate == null) throw new ArgumentNullException("startDate");

            string[,] spParameters = { { "@FechaInicio", startDate },{ "@FechaFinal", endDate }
                                     };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "spIntra_AddCasinoProfitByDates", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }


        internal DataTable GenerateNewsLetterEmail(String state)
        {
            string[,] spParameters = {
                                     };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "spIntra_Newsletter_GetEmail", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }


        internal DataTable UpdateNewsLetterEmail(string email, String state)
        {
            string[,] spParameters = { { "@email", email },{ "@action", state }
                                     };
            var cDal = new _BLL.cDAL(_BLL.connString_Asys, "HS_UpdateEmail", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable GenerateCasinoProfitTotal()
        {
            string[,] spParameters = {
                                     };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "spIntra_CasinoProfitTotal", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable GetCasinoProfit()
        {
            string[,] spParameters = {
                                     };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "spIntra_GetcasinoGeneralProfit", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }
        internal DataTable GetCasinoProfitFinal()
        {
            string[,] spParameters = {
                                     };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "spIntra_GetcasinoProfit", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }



        internal DataTable CasinoBlockUpdate(string customerId, string product, string casinoAccess, string permanent, string user)
        {

            string[,] spParameters = { { "@CustomerID", customerId }, { "@Product ", product }, { "@ActiveState", casinoAccess }, { "@Permanent", permanent }, { "@ChangedBy", user }, { "@AppName", "" }, { "@CustomerActive", "" } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, "spCasinoBlockUpdate ", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable LiveDealerVolumeReport(string agentId, string StartDate, string EndDate)
        {

            string[,] spParameters = { { "@MasterAgent", agentId }, { "@From ", StartDate }, { "@To", EndDate } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, "uSp_Local_LiveDealerVolumeReport ", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }


        internal DataTable GetAccumulatedCustomerRisk(string customerId, string StartDate, string EndDate, int UseLiveDate)
        {

            string[,] spParameters = { { "@CustomerID", customerId }, { "@StartDate ", StartDate }, { "@EndDate", EndDate }, { "@UseLiveDate", UseLiveDate.ToString() } };

            var cDal = new _BLL.cDAL(_BLL.connString_IntranetDB, "spcGetAccumulatedCustomerRiskFinalMoney", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable GetCashRewardCustomerEligibleBets(string customerId, string StartDate, string EndDate, int CashRewardPaymentTypeId)
        {

            string[,] spParameters = { { "@CustomerID", customerId }, { "@StartDate ", StartDate }, { "@EndDate", EndDate }, { "@CashRewardPaymentTypeId", CashRewardPaymentTypeId.ToString() } };

            var cDal = new _BLL.cDAL(_BLL.connString_Asys, "spcGetCashRewardCustomerEligibleBets", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }



        internal DataTable BetRegalCA_GetRevenue(string startDate, string endDate)
        {

            string[,] spParameters = { { "@StartDate ", startDate }, { "@EndDate", endDate } };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, "uSp_BetRegalCA_GetRevenue", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }


        internal DataTable GetSignup(string startDate, string endDate, string sportbookid)
        {

            string[,] spParameters = { { "@StartDate ", startDate }, { "@EndDate", endDate }, { "@SportbookID", sportbookid } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "spIntra_GetSignupsv2", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable DeleteSignRequests(string RequestID, string RequestedBy)
        {

            string[,] spParameters = { { "@RequestID ", RequestID }, { "@RequestedBy", RequestedBy } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "uSp_LOcal_DeleteSignRequests", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable BypassSignup(string requestId, string user)
        {

            string[,] spParameters = { { "@RequestId", requestId }, { "@Clerkid", user } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "Usp_SignupUpdate_Request", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable SignUp_UpdatePhoneEmail(string requestId, string phoneNumber, string email, string PromoEmails)
        {

            string[,] spParameters = { { "@RequestId", requestId }, { "@PhoneNumber", phoneNumber }, { "@Email", email }, { "@PromoEmails", PromoEmails } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "[uSp_SignUp_UpdatePhoneEmail]", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }


        internal DataTable GetLiveDealerData()
        {
            string[,] spParameters = {
                                     };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "Usp_GetLiveDealerData ", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable GetCasinoRebateReport()
        {
            string[,] spParameters = {
            };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "Spintra_GetCasinoRebateReport", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable GetCasinoDisabled()
        {
            string[,] spParameters = {
            };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "Spintra_GetCasinoRebateReportCasinodisabled", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable GetWinLoss()
        {
            string[,] spParameters = {
                                     };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "Usp_GetHLWinLoss ", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable GetBetSoftDetails()
        {
            string[,] spParameters = {
                                     };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "Usp_GetBetSoftDetails ", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable GetNucleusDetails()
        {
            string[,] spParameters = {
                                     };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "Usp_GetNucleusDetails ", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable GetHRLateness()
        {
            string[,] spParameters = {
                                     };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "Usp_GetHRLateness ", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }

        internal DataTable GetCasinoBlock(string product)
        {
            string[,] spParameters = {
                                        { "@Product", product },
                                     };
            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "spCasinoBlockReport", spParameters);
            var dtResult = _DAL.getDataTable(cDal);
            return dtResult;
        }
    }


}



