﻿using System.Data;

namespace HSIntranetV2.Models
{
    public class FeesMaintenanceModel
    {

        internal static DataTable GetFeesMethod(string TranType = "")
        {



            string storedProcedureName = "uSp_AgentFees_GetMethods";

            if (!string.IsNullOrEmpty(TranType))
            {
                string[,] spParameters = { { "@TranType", TranType } };
                _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

                DataTable dtResult = _DAL.getDataTable(cDAL);

                return dtResult;
            }
            else
            {
                string[,] spParameters = { };
                _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

                DataTable dtResult = _DAL.getDataTable(cDAL);

                return dtResult;
            }


        }

        internal static DataTable UpdateFeesMethod(string ID, string TranType, string TranDesc, string Method, string CRMMethod, string Strt, string ENDD, string RTE, string PERCNTAGE, string MIXMODE, string PREAPPROVED)
        {
            string[,] spParameters = {
                                        { "@ID", ID },
                                        { "@TranType", TranType },
                                        { "@TranDesc", TranDesc },
                                        { "@Method", Method },
                                        { "@CRMMethod", CRMMethod },
                                        { "@Strt", Strt },
                                        { "@ENDD", ENDD },
                                        { "@RTE", RTE },
                                        { "@PERCNTAGE", PERCNTAGE },
                                        { "@MIXMODE", MIXMODE },
                                        { "@PREAPPROVED", PREAPPROVED }
                                     };
            string storedProcedureName = "uSp_AgentFees_UpdateMethod";
            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }

        internal static DataTable InsertFeesMethod(string TranType, string TranDesc, string Method, string CRMMethod, string Strt, string ENDD, string RTE, string PERCNTAGE, string MIXMODE, string PREAPPROVED)
        {
            string[,] spParameters = {
                                        { "@TranType", TranType },
                                        { "@TranDesc", TranDesc },
                                        { "@Method", Method },
                                        { "@CRMMethod", CRMMethod },
                                        { "@Strt", Strt },
                                        { "@ENDD", ENDD },
                                        { "@RTE", RTE },
                                        { "@PERCNTAGE", PERCNTAGE },
                                        { "@MIXMODE", MIXMODE },
                                        { "@PREAPPROVED", PREAPPROVED }
                                     };
            string storedProcedureName = "uSp_AgentFees_InsertMethod";
            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }


        internal static DataTable DeleteFees(string ID)
        {
            string[,] spParameters = {
                                        { "@ID", ID },
                                        { "@ActionDelete", "1"}
                                     };
            string storedProcedureName = "uSp_AgentFees_UpdateMethod";
            _BLL.cDAL cDAL = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            DataTable dtResult = _DAL.getDataTable(cDAL);

            return dtResult;
        }
    }
}