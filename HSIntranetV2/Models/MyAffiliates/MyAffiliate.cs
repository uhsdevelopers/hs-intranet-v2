﻿using System.Data;

namespace HSIntranetV2.Models.MyAffiliates
{
    public class MyAffiliate
    {
        public class MyAffASI
        {
            public string ClientId;
            public string Affiliate;

        }




        internal static DataTable GetCustomerLimits(string storedProcedureName, string customerid)
        {


            string[,] spParameters = {  { "@customerID", customerid }

            };

            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;





            // return dtResult;

        }


        public static DataTable uSpGetASILIST(string dateStart, string dateEnd, string company, string Affiliate)
        {



            string[,] spParameters = {  { "@dateStart", dateStart },{ "@dateEnd", dateEnd },{ "@company", company },{ "@Affiliate", Affiliate}

            };



            var cDal = new _BLL.cDAL(_BLL.connString_ASIDb, "uSpGetASILIST", spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;


        }

        public static DataTable GetFreeSpins(string dateStart, string dateEnd)
        {



            string[,] spParameters = {  { "@dateStart", dateStart },{ "@dateEnd", dateEnd }

            };



            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, "spIntra_GetFreeSpins", spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;


        }

    }
}