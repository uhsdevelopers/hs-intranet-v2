﻿function GetDepositsCSD() {

    var Transactionid = document.getElementById('txtTransactionid').value,
        Clerkid = document.getElementById('txtClerkid').value,
        customerId = document.getElementById('txtCustomerid').value;


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (Transactionid === "") {
        Transactionid = "";
    }

    var url;
    var data;

    url = "/CsdDeposit/GetCustomerDepositExceptions",
        data = {
            customerId: customerId,
            TransactionID: Transactionid,
            Clerkid: Clerkid
        };


    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTableCSD($('#AccumulatedSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        loader();
    });
}


function GetDepositsList() {

    var 
        startDate = document.getElementById('Startdate').value,
        customerId = document.getElementById('txtCustomerid').value;


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (customerId === "") {
        customerId = "X";
    }

    var url;
    var data;

    url = "/CsdDeposit/GetCustomerDepositList",
        data = {
            customerId: customerId,
        startDate: startDate
        };


    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#AccumulatedSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        loader();
    });
}



function GetSevenDepositsList() {

    var
        startDate = document.getElementById('Startdate').value;



    var url;
    var data;

    url = "/CsdDeposit/getsevendepositlist",
        data = {
        startdate: startDate
        };


    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#AccumulatedSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        loader();
    });
}


function DeleteDep(id) {


    if (id === "") {
        id = "";

    }

    console.log(id);
    var url;
    var data;
    var status = "2";
    
    url = "/CsdDeposit/ChangeStatusDepositException",
        data = {
        ID: id,
        status: status
        };


    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#AccumulatedSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        loader();
    });
    
}

function AddDep(id) {


    if (id === "") {
        id = "";

    }

    console.log(id);
    var url;
    var data;
    var status = "1";

    url = "/CsdDeposit/ChangeStatusDepositException",
        data = {
            ID: id,
            status: status
        };


    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#AccumulatedSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        loader();
    });

}

function ADDDepositsCSD() {

    var Transactionid = document.getElementById('txtTransactionid').value,
        Clerkid = document.getElementById('clerk').value,
        customerId = document.getElementById('txtCustomerid').value;


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

 

    var url;
    var data;
    var status = 1;

    url = "/CsdDeposit/AddCustomerDepositExceptions",
        data = {
            customerId: customerId,
            TransactionID: Transactionid,
             Clerkid: Clerkid,
             status: status
        };


    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        if (result[0].ErrorMessage == 'Added') {


            var url2;
            var data2;

            url2 = "/CsdDeposit/GetCustomerDepositExceptions",
                data2 = {
                    customerId: customerId,
                    TransactionID: Transactionid,
                    Clerkid: Clerkid
                };


            // start loader
            loader();

            ajaxConnexion(data2, url2, function (result) {
                printDynamicTable($('#AccumulatedSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

                loader();
            });

        }
        printDynamicTable($('#AccumulatedSummary'), result, '1', 'Deposit'); // HS_script.js  function printDynamicTable(table, object)

        loader();
    });
}

function printDynamicTableCSD(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title

    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeaderCSD(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createRowCSD(table, result[i]);
    }
}


function createHeaderCSD(table, row) {

    var newRow = $('<tr></tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });
    $(newRow).append('<th class="' + addClass + '">Remove </th>');
    $(table).children('thead').append(newRow);

}

function createRowCSD(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Status') == 0) {
            if ((value == 1)) {
                value = "Enabled";
            } else {
                if ((value == 2)) {
                    value = "Disabled";
                }
            }
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
        
    });
    if (row.Status == "1") {
        $(newRow).append('<td ><input class="btn btn-danger btn-sm" type="submit" value="Remove" onClick="DeleteDep(' + '' + row.id + ' )"></td>');
    }
    else {
        $(newRow).append('<td ><input class="btn btn-primary btn-sm" type="submit" value="Add" onClick="AddDep(' + '' + row.id + ' )"></td>');
    }
   
    $(newRow).append('</tr>');


    $(table).children('tbody').append(newRow);
}