﻿$(function () {

    $('#btn-submit').click(function () {

        getBalance();
    });

});

function getBalance() {

    var url = "/WW_PlayersWithBalance/GetBalance/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            agentID: $('#txb-agentid').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');

        $('#tbl-result tbody tr:first-child td:first-child').text('Players: ' + String(result.length - 1) );

        // end loader
        loader();
        
    });

}
