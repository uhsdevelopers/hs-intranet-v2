﻿$(function () {
    
    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');

        getRate();
        getFirstDeposit();
    });

});

function getRate() {

    var url = "/Acc_NewSignups/GetRate/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');

        //getFirstDeposit();

        $('#td-signups').text(result.length);

    });

}

function getFirstDeposit() {

    var url = "/Acc_NewSignups/GetFirstDeposit/",
            data = {
                dateFrom: $('#datepickerStart').val(),
                dateTo: $('#datepickerEnd').val(),
                agentID: $('#txb-agentid').val()
            };
        var amount = 0;


    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-firstDeposit'), result, '', '');

        $('#td-firstdeposits').text(result.length);

        $.each(result, function (key, row) {

            amount += row.DepositAmount;

        });

        $('#td-amount').text( formatCurrency(amount) );

        // end loader
        loader();

    });
}

function getNorthbetWinLoss() {
    loader();
    var url = "/Accounting/GetNorthBetWinLoss/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#PokerSummary'), result, '', '');

       

    });

}

function GetNorthBetBonusAdjustments() {
    loader();
    var url = "/Accounting/GetNorthBetBonusAdjustments/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#PokerSummary'), result, '', '');



    });

}


function printDynamicTableLD(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title

    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeaderLD(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createRowLD(table, result[i]);
    }
}

function createRowLD(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Balance') != -1 || key.indexOf('Amount') != -1) {
            if ((/\S/.test(value))) {
                value = formatCurrencyNosymbol(value);
            }
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}

function createHeaderLD(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '" text-align="Center"><span style="margin:auto; display:table;">' + value + '</span></th>');
    });

    $(table).children('thead').append(newRow);

}

function GetLiveDealerVolumeReport() {
    loader();
    var amount = 0;
    var camount = 0;
    var famount = 0;
    var masterAgent = $('#MasterAgent').val();
    var url = "/Accounting/LiveDealerVolumeReport/",
        data = {
            agentId: masterAgent,
            StartDate: $('#datepickerStart').val(),
            EndDate: $('#datepickerEnd').val()
        };

    // start loader


    ajaxConnexion(data, url, function (result) {

        printDynamicTableLD($('#PokerSummary'), result, '', '');

        $.each(result, function (key, row) {

            if (row.Type == "Post Up" & row.Agent != "_*SUBTOTAL*_") {
                amount += row.Amount;
            }
            else
                if (row.Type == "Credit" & row.Agent != "_*SUBTOTAL*_") {
                    camount += row.Amount;
                }


        });
        famount += camount + amount;
        if (camount !== 0) {
            $('#PokerSummary').find('tbody').append("<tr><th colspan='2' class='header'>SubTotal Credit: </td><th id='td-Camount' class='header align-right'></td></tr>");
            $('#td-Camount').text(formatCurrencyNosymbol(camount));
        }
        if (amount !== 0 && camount !== 0) {
            $('#PokerSummary').find('tbody').append("<tr><th colspan='2' class='header'>SubTotal Post Up: </td><th id='td-amount' class='header align-right'></td></tr>");
            $('#td-amount').text(formatCurrencyNosymbol(amount));
        }
       
       
        
        $('#PokerSummary').find('tbody').append("<tr><th colspan='2' class='header'>Total: </td><th id='td-Famount' class='header align-right'></td></tr>");
        $('#td-Famount').text(formatCurrencyNosymbol(famount));

        $('#PokerSummary').tablesorter();
        loader();

    });

}


function formatCurrencyNosymbol(value) {

    if (value == null) {
        value = 0;
    }
    value = parseFloat(value);

    var money = value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

    return  money;
}

function GetLiveDealerVolumeReportExcel() {
    loader();
    var amount = 0;
    var camount = 0;
    var famount = 0;
    var masterAgent = $('#MasterAgent').val();
    var url = "/Accounting/LiveDealerVolumeReport/",
        data = {
            agentId: masterAgent,
            StartDate: $('#datepickerStart').val(),
            EndDate: $('#datepickerEnd').val()
        };

    // start loader

  

    ajaxConnexion(data, url, function (result) {
        
        $.each(result, function (key, row) {

            if (row.Type == "Post Up" & row.Agent != "_*SUBTOTAL*_") {
                amount += row.Amount;
            }
            else
                if (row.Type == "Credit" & row.Agent != "_*SUBTOTAL*_") {
                    camount += row.Amount;
                }


        });
        famount += camount + amount;
        if (amount !== 0 && camount !== 0) {
            result.push({ Type: "Sub Total Credit", Agent: "", Amount: camount });
            result.push({ Type: "Sub Total Post Up", Agent: "", Amount: amount });
        }

        result.push({ Type: "Total", Agent: "----", Amount: famount });

 

        JSONToCSVConvertor(result, "Live Dealer - ", true);
        loader();

    });

}


