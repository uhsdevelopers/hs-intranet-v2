﻿$(function () {
    GetAgents();
    $('#btn-submit').click(function () {

        GetAvoxiReport();
    });
    $('#btn-submit-excel').click(function () {
        var url = "/AvoxiReport/GetAvoxiReport/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txt-agentid').val(),
            department: $('#txt-department').val(),

        };

        // start loader
        loader();

        ajaxConnexion(data, url, function (result) {
            JSONToCSVConvertor(result, "Avoxi Report", true);
            // end loader
            loader();
        });
    });

});

function GetAvoxiReport() {

    var url = "/AvoxiReport/GetAvoxiReport/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txt-agentid').val(),
            department: $('#txt-department').val(),
            
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        $('#tbl-result').empty();
        printDynamicTable($('#tbl-result'), result, '', '');
        // end loader
        loader();

    });

}
function GetAgents() {

    var url = "/AvoxiReport/GetAgents/",
        data = {
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicSelect($($('#txt-agentid')), result, 0, 0);
        // end loader
        loader();

    });

}