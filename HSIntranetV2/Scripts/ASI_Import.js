﻿$(function () {
    $('input[type=radio]').click(function () {
        $('#ASI_Import').toggle();
        $('#Sports_revenue').toggle();
        $('#Horses_revenue').toggle();
        $('#Casino_revenue').toggle();
    });

    $('#getInfoToExcel').click(function () {
        var url = "/ASI_Import/GetExcelInfo",
        data = {
        };

        ajaxConnexion(data, url, function (result) {
            JSONToCSVConvertor(result, "Details", true);
        });

    });

});

function getASI_Import(startDate) {


    var url = "/ASI_Import/GetASIfigures/",
        makeBackup = $('#chkMakeBackup').is(':checked'),
        data = {
            startDate: startDate,
            agentID: $('#txt-agent').val(),
            makeBackup: makeBackup

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#ASI_Import'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        printCategoryTable(result);

        // totals by category
        addTotals();

        bindEvents();

        // end loader
        loader();
    });
}

function bindEvents() {
    $('#ASI_Import tr').click(function () {
        var columnDescription = $(this).find('td')[1],
            description = $(columnDescription).text();

        getDetails(document.getElementById('datepickerStart').value, description);
    });

    $('#Sports_revenue tr').click(function () {
        var columnDescription = $(this).find('td')[1],
            description = $(columnDescription).text();

        getDetails(document.getElementById('datepickerStart').value, description);
    });

    $('#Horses_revenue tr').click(function () {
        var columnDescription = $(this).find('td')[1],
            description = $(columnDescription).text();

        getDetails(document.getElementById('datepickerStart').value, description);
    });

    $('#Casino_revenue tr').click(function () {
        var columnDescription = $(this).find('td')[1],
            description = $(columnDescription).text();

        getDetails(document.getElementById('datepickerStart').value, description);
    });
}

function printCategoryTable(result) {
    
    // clear old data
    $('#Sports_revenue').empty();
    $('#Horses_revenue').empty();
    $('#Casino_revenue').empty();

    // print title
    //$(idTitle).text(title);

    if (result.length == 0) return;

    // table title row
    var keys = ["Cat", "Sports", "Amount"];
    createRow($('#Sports_revenue'), keys);

    keys[1] = "Horses";
    createRow($('#Horses_revenue'), keys);

    keys[1] = "Casino";
    createRow($('#Casino_revenue'), keys);

    for (i = 0, length = result.length; i !== length; i++) {
        switch (result[i].Cat) {
            case "S":
                createRow($('#Sports_revenue'), result[i]);
                break;
            case "H":
                createRow($('#Horses_revenue'), result[i]);
                break;
            case "C":
                createRow($('#Casino_revenue'), result[i]);
                break;

        }
        
    }
}

function getDetails(startDate, description) {
    var url = "/ASI_Import/GetASIdetails/",
        data = {
            startDate: startDate,
            description: description,
            agentID: $('#txt-agent').val()
        };

    // clear old data
    $('#ASI_Details').empty();

    // clear title
    $('#detail-title').text('');

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#ASI_Details'), result, document.getElementById('detail-title'), description); // HS_script.js  function printDynamicTable(table, object)

    });

    showDetails();
}

function addTotals() {

    var total_amount;

    // sports grand total
    total_amount = totalAmount($('#Sports_revenue tr'));

    $('#Sports_revenue').append('<tr><td></td><td><b>Total</b></td><td class="align-right">' + formatCurrency(total_amount) + '</td></tr>');

    // horses grand total
    total_amount = totalAmount($('#Horses_revenue tr'));

    $('#Horses_revenue').append('<tr><td></td><td><b>Total</b></td><td class="align-right">' + formatCurrency(total_amount) + '</td></tr>');

    // casino grand total
    total_amount = totalAmount($('#Casino_revenue tr'));

    $('#Casino_revenue').append('<tr><td></td><td><b>Total</b></td><td class="align-right">' + formatCurrency(total_amount) + '</td></tr>');

}

function totalAmount(rows) {
    
    var amount = 0,
        text = '';

    $.each(rows, function (key, value) {
        text = $(value).find('td:last').text().replace(/\$|,/g, '');
        if (!isNaN(text)) {
            amount += +text;
        }

    });

    return amount;
}