﻿$(function () {
    
    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');
        $('#tbl-result-details, #span-headerDetails').empty();

        if ($('#WL_show_details').is(':checked')) {
            GetWLDetails('');
        } else {
            GetWLSummary();
        }
    });

    $('#tbl-result').on('click', '.moreDetails', function () {
        $('.moreDetails').removeClass('special-tr');
        $(this).addClass('special-tr');
        $('#span-totalPlayers').text('0');

        GetWLDetails(this);
    });

});

function GetWLSummary() {

    var url = "/WW_WLReport/GetWLSummaryWW/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTableWW_WLReport($('#tbl-result'), result, '', '');

        if (result.length == 0) 
            $('.span-totalPlayers').text('Total: 0 Players');
        else
            $('.span-totalPlayers').text(($('#tbl-result tr').length - 1) + ' Players');

        // end loader
        loader();

    });

}
function GetWLDetails($this)
{

    var url = "/WW_WLReport/GetWLFigureDetail/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: ($this == '') ? $('#txb-agentid').val() : $($this).data('singleagent'),
            reportType: ($this == '') ? 'Adjustment' : $($this).data('report')
};
    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTableWW_Details($('#tbl-result-details'), result, '', '', data.reportType);

        if (result.length == 0)
            $('.span-totalPlayers').text('Total: 0 Players');
        else
            $('.span-totalPlayers').text(($('#tbl-result-details tr').length - 1) + ' Players');

        // end loader
        loader();

    });

}
function printDynamicTableWW_WLReport(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    $(table).children('thead').append('<tr><td colspan="7" align="center">W/L Summary Report</td></tr>');
    createHeader(table, keys);

    var totalWinLost = 0,
        totalAgentAdjustments = 0,
        totalLiveDealer = 0;
    for (i = 0, length = result.length; i !== length; i++) {
        createRowWW_WLReport(table, result[i]);
        totalWinLost = parseFloat(result[i]['Win/Lost'] + totalWinLost);
        totalAgentAdjustments = parseFloat(result[i]['Adjustment'] + totalAgentAdjustments);
        totalLiveDealer = parseFloat(result[i]['Live Dealer'] + totalLiveDealer);
    }
    $(table).append('<tr class="special-tr"><td>Total</td><td></td><td></td><td></td><td class="align-right">' + formatCurrency(totalWinLost) + '</td><td class="align-right">' + formatCurrency(totalAgentAdjustments) + '</td><td class="align-right">' + formatCurrency(totalLiveDealer) + '</td></tr>');
}
function printDynamicTableWW_Details(table, result, idTitle, title, reportType) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row 
    var colspan = (reportType == 'Win/Lost') ? '10' : '11';

    $(table).children('thead').append('<tr><td colspan="'+colspan+'" align="center">' + reportType + ' Details Report</td></tr>');
    var keys = Object.keys(result[0]);
    createHeader(table, keys);
   
    for (i = 0, length = result.length; i !== length; i++) {
        createRowWW_WLReport(table, result[i]);
        
    }
}

function createRowWW_WLReport(table, row) {

    var newRow = $('<tr>'),
        addClass = '',
        data_customer = '';

    $.each(row, function (key, value) {
        var tdClass = '',
        addClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value).split(' ')[0];
            }

        } else if (key.indexOf('Adjustment') != -1 || key.indexOf('Live Dealer') != -1 || key.indexOf('Win/Lost') != -1) {
            if ((/\S/.test(value))) {
                value = '<a>'+formatCurrency(value)+'</a>';
                
                data_customer = 'data-SingleAgent="' + row['Agent'] + '" data-report="' + key + '"';
                tdClass += " moreDetails ";
                addClass = 'align-right';
            }
            
        } else if (key.indexOf('%') >= 0) {
            value = value + "%";
            addClass += ' align-right ';
        }
        else if (key.indexOf('Level') >= 0 || key.indexOf('DocumentNumber') >= 0) {
            addClass = 'align-right';
        }
        else if (key.indexOf('Amount') >= 0) {
            addClass = 'align-right';
            value = formatCurrency(value) ;
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '" '+data_customer+'>' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}
