﻿$(function () {
    
    $('#btn-submit').click(function () {
        GetGames();
    });
    $('#btn-submit2').click(function () {
        GetGames();
    });
    $("#selSportTypeList,#selSportSubTypeList,#datepickerStart,#datepickerEnd,.numeric").keypress(function () {
        if (event.which == 13) {
           GetGames(); 
        }  
    });
    $('#btn-submit3').click(function () {
        if (confirm('Are you sure you want to move those games from ' + $('#selSportTypeList').val() + ' | ' + $('#selSportSubTypeList').val() + ' to ' + $('#selSportTypeList2').val() + ' | ' + $('#selSportSubTypeList2').val() + '?')) {
            SetGames();
        }
    });
    getAllSportsList();
    $('#selSportTypeList').change(function () {
        getSportDetailsList($('#selSportTypeList').val(), $('#selSportSubTypeList'));
    });
    $('#selSportTypeList2').change(function () {
        getSportDetailsList($('#selSportTypeList2').val(), $('#selSportSubTypeList2'));
    });
   
});
 

function GetGames() {
    $('#tbl-result').empty();
    var url = "/MoveGames/GetGames/",
        data = {
            sport: $('#selSportTypeList').val(),
            sportSubType: $('#selSportSubTypeList').val(),
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            RotationFrom: $('.RotationFrom').val(),
            RotationTo: $('.RotationTo').val()
        };
    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        if (result.length == 0) {
            $('._title').text("No games found");
            loader();
            return;
        }
        
        printGradeTable($('#tbl-result'), result, '', '');
        $('._title').text("Game list to be moved");
        // end loader
        loader();
    });
}

function SetGames() {
    $('#tbl-result').empty();
    var url = "/MoveGames/SetGames/",
        data = {
            sport: $('#selSportTypeList').val(),
            sportSubType: $('#selSportSubTypeList').val(),
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            RotationFrom: $('.RotationFrom').val(),
            RotationTo: $('.RotationTo').val(),
            NewSportType: $('#selSportTypeList2').val(),
            NewSportSubType: $('#selSportSubTypeList2').val(),
        };
    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        if (result.length == 0) {
            $('._title').text("No games found");
            loader();
            return;
        }
        
        printGradeTable($('#tbl-result'), result, '', '');
        $('._title').text("Games moved successful");
        // end loader
        loader();
    });
}

function getSportDetailsList(sport,select) {
    select.empty();
    var url = "/MoveGames/GetSports",
        data = {
            sport: sport
        }

    ajaxConnexion( data, url, function (result) {

            printDynamicSelect(select, result, 1, 1, false); // Intranet.js  function printDynamicSelect(table, object, value column number, text column number, duplicates)

    });

}

function getAllSportsList() {

    var url = "/MoveGames/GetAllSports",
        data = {
        }

    ajaxConnexion( data, url, function (result) {

        printDynamicSelect($('.selSportTypeList'), result, 0, 0, false); // Intranet.js  function printDynamicSelect(table, object, value column number, text column number, duplicates)

    });

}


function printGradeTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createGradeHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createGradeRow(table, result[i]);
    }
}

function createGradeHeader(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });

    $(table).children('thead').append(newRow);

}

function createGradeRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('GameNum') != -1 ) {
            
            value = "<a class='GameNum' data-score1='" + row["Team1FinalScore"] + "' data-score2='" + row["Team2FinalScore"] + "' data-gamedate='" + formatDate(row["GameDateTime"]) + "' data-pitcher1='" + row["ListedPitcher1"] + "' data-pitcher2='" + row["ListedPitcher2"] + "' data-teamrot1='" + row["Team1RotNum"] + "' data-teamRot2='" + row["Team2RotNum"] + "' data-teamname1='" + row["Team1ID"] + "' data-teamname2='" + row["Team2ID"] + "' data-sport='" + $('#selSportTypeList').val() + "' data-subsport='" + $('#selSportSubTypeList').val() + "'>" + value + "</a>";
            
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}