﻿$(function () {
    
    $('#btn-submit').click(function () {
        CancelWager_GetWagers();
    });

    $('#btn-synch').click(function () {
        CancelWager_SynchTrans();
    });
    

    $(document).keypress(function (event) {
        if (event.which == 13) {
            CancelWager_GetWagers();
        }  
    });
});


function CancelWager_SynchTrans() {

    var url = "/Wagering/CancelWager_SynchTrans/",
        data = {
           
        };

    $('#msgResult').text('');

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        $('#msgResult').text('Synchronization was made. Refresh the transactions');
        // end loader
        loader();

    });

}

function CancelWager_GetWagers() {

    var url = "/Wagering/CancelWager_GetWagers/",
        data = {
            customerid: $('#customerID').val(),
            dateStart: $('#datepickerStart').val(),
            dateEnd: $('#datepickerEnd').val()
            
        };

    $('#msgResult').text('');

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTableCancelWager($('#tbl-result'), result, '', '');
        // end loader
        loader();

    });

}


function printDynamicTableCancelWager(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title

    $(idTitle).text(title);

    if (result.length == 0) {
        $('#msgResult').text('No records found.');
        return;
    }

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeaderCancelWager(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createRowCancelWager(table, result[i]);
    }

    $('.chk_cancel').click(function () {
        
        var r = confirm("Are you sure?");
        if (r == true) {
            CancelWager_CancelTicket(this);
        }

    });

}

function CancelWager_CancelTicket(ticket) {

    

    var url = "/Wagering/CancelWager_CancelTicket/",
        data = {
            ticket: $(ticket).attr('id')

        };


    ajaxConnexion(data, url, function (result) {

      

    });
}


function createHeaderCancelWager(table, row) {

    var newRow = $('<tr><th>Cancel</th></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });

    $(table).children('thead').append(newRow);

}

function createRowCancelWager(table, row) {

    var newRow = $('<tr><td><input type="checkbox" id=' + row.TicketNumber + "-" + row.WagerNumber + ' class="chk_cancel" ></td>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Amount') != -1 || key.indexOf('Balance') != -1 || key.indexOf('Fee') != -1 || key.indexOf('Volume') != -1 || key.indexOf('volumen') != -1) {
            if ((/\S/.test(value))) {
                value = formatCurrency(value);
            }
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}