﻿$(function () {

});

function getASI_Import(startDate) {

    // start loader
    loader();

    Transfer_Totals(startDate)

    ASI_Import(startDate);
}

function Transfer_Totals(startDate) {
    var url = "/InternalTransfers/GetTransferTotals/",
        data = { startDate: startDate };

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#Transfer_Totals'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
    });
}


function ASI_Import(startDate) {
    var url = "/InternalTransfers/GetASIfigures/",
        data = { startDate: startDate };

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#ASI_Import'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        bindEvents();

        // end loader
        loader();
    });
}

function bindEvents() {
    $('#ASI_Import tr').click(function () {
        var columnDescription = $(this).find('td')[0],
            description = $(columnDescription).text();

        getDetails(document.getElementById('datepickerStart').value, description);
    });
}


function getDetails(startDate, description) {
    var url = "/InternalTransfers/GetASIdetails/",
        data = {
            startDate: startDate,
            description: description
        };

    // clear old data
    $('#ASI_Details').empty();

    // clear title
    $('#detail-title').text('');

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#ASI_Details'), result, document.getElementById('detail-title'), description); // HS_script.js  function printDynamicTable(table, object)

    });

    showDetails();
}
