﻿$(function () {

    $('#btn-submit').click(function () {

        getWWComissions();
    });

});

function getWWComissions() {

    var url = "/Acc_WWComissions/GetWWComissions/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');

        if (result.length == 0)
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        // end loader
        loader();

    });

}