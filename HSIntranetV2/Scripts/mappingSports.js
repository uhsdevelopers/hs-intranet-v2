﻿$(function () {

    getIdSport(function (callback) {

        getLeagues(function (callback) {

            getGameTypes(function (callback) {

                getSportsMapping('');

            });

        });

    });

    $('#btn_lookup').click(function () {
        getSportsMapping($('#txt_lookup').val());
    });

    $('input[name="show_mapping"]').click(function () {

        var showAll = $(this).val();

        $('td:first-child:not(.not_mapped)').closest('tr').toggle();

    });

    $('#tbl-result').on('click', '.save_mapping', function () {

        $(this).closest('tr').find('span.league').text($(this).closest('tr').find('td:eq(3) .selLeagues option:selected').text());
        $(this).closest('tr').find('span.gameType').text($(this).closest('tr').find('td:eq(4) .selGameTypes option:selected').text());
        $(this).closest('tr').find('span.idSport').text($(this).closest('tr').find('td:eq(5) .selIdSport option:selected').text());

        saveSportMapping(this);

        afterClicked(this);
    });

    $('#tbl-result').on('click', '.edit_mapping', function () {

        $(this).closest('td').find('input[type="button"], .boxclose').toggle();

        var tdLeague = $(this).closest('tr').find('td:eq(3)'),
            tdGameType = $(this).closest('tr').find('td:eq(4)'),
            tdIdSport = $(this).closest('tr').find('td:eq(5)');

        $(tdLeague).find('span.league').hide();
        $(tdGameType).find('span.gameType').hide();

        $('#hide_select .selLeagues').clone().appendTo(tdLeague);
        $('#hide_select .selGameTypes').clone().appendTo(tdGameType);


        // add option if it is not a league
        $(tdLeague).find('select').prepend('<option value="0" selected="selected">' + $(tdLeague).find('span.league').text().toUpperCase() + '</option>')

        // select value
        $(tdLeague).find('select option').filter(function () {
            return ($(this).text() == $(tdLeague).find('span.league').text()); //To select Blue
        }).prop('selected', true);

        $(tdGameType).find('select option').filter(function () {
            return ($(this).text() == $(tdGameType).find('span.gameType').text()); //To select Blue
        }).prop('selected', true);

        // id sport

        if ($(this).closest('tr').find('.not_mapped').length != 0) {

            $(tdIdSport).find('span.idSport').hide();
            $('#hide_select .selIdSport').clone().appendTo(tdIdSport);

            $(tdIdSport).find('select option').filter(function () {
                return ($(this).text() == $(tdIdSport).find('span.idSport').text()); //To select Blue
            }).prop('selected', true);
        }

    });

    $('#tbl-result').on('click', '.boxclose', function () {

        afterClicked(this);

    });


});

function afterClicked(btnClicked) {

    $(btnClicked).closest('td').find('input[type="button"], .boxclose').toggle();

    var tdLeague = $(btnClicked).closest('tr').find('td:eq(3)'),
        tdGameType = $(btnClicked).closest('tr').find('td:eq(4)'),
        tdIdSport = $(btnClicked).closest('tr').find('td:eq(5)');

    $(tdLeague).find('span.league').show();
    $(tdGameType).find('span.gameType').show();
    $(tdIdSport).find('span.idSport').show();

    $(tdLeague).find('.selLeagues').remove();
    $(tdGameType).find('.selGameTypes').remove();
    $(tdIdSport).find('.selIdSport').remove();

}

function saveSportMapping(btnSave) {

    var url = "/ASItoDGSleagueMapping/SaveSportMapping/",
        row = $(btnSave).closest('tr');
    data = {
        id: $(row).find('td:eq(0)').text(),
        sportType: $(row).find('td:eq(1)').text(),
        sportSubType: $(row).find('td:eq(2)').text(),
        idLeague: $(row).find('td:eq(3) .selLeagues').val(),
        league: $(row).find('td:eq(3) .selLeagues option:selected').text(),
        idGameType: $(row).find('td:eq(4) .selGameTypes').val(),
        idSport: $(row).find('td:eq(5) .selIdSport').val()

    };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {


        // end loader
        loader();

    });

}

function getIdSport(callback) {

    var url = "/ASItoDGSleagueMapping/GetIdSport/",
        data = {
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicSelect($('#hide_select .selIdSport'), result, 0, 0, false); // Intranet.js  function printDynamicSelect(table, object, value column number, text column number, duplicates)

        // end loader
        loader();

    });

    callback();
}

function getGameTypes(callback) {

    var url = "/ASItoDGSleagueMapping/GetGameTypes/",
        data = {
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicSelect($('#hide_select .selGameTypes'), result, 0, 2, false); // Intranet.js  function printDynamicSelect(table, object, value column number, text column number, duplicates)

        // end loader
        loader();

    });

    callback();
}

function getLeagues(callback) {

    var url = "/ASItoDGSleagueMapping/GetLeagues/",
        data = {
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicSelect($('#hide_select .selLeagues'), result, 0, 1, false); // Intranet.js  function printDynamicSelect(table, object, value column number, text column number, duplicates)

        // end loader
        loader();

    });

    callback();

}

function getSportsMapping(lookup) {

    var url = "/ASItoDGSleagueMapping/GetSportsMapping/",
        data = {
            lookup: lookup
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printSportsMappingTable($('#tbl-result'), result, '', '');

        // end loader
        loader();

    });

}

function printSportsMappingTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeader(table, keys);

    // action column
    $(table).find('thead tr').append('<th>Action</th>');

    // columns
    for (i = 0, length = result.length; i !== length; i++) {
        createSportsMappingRow(table, result[i]);
    }

    if ($('input[name="show_mapping"]:checked').val() == '0') {
        $('td:first-child:not(.not_mapped)').closest('tr').hide();
    }

}

function createSportsMappingRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {

        var tdClass = '';

        if (key.indexOf('IdLeagueMapping') >= 0) {
            if (value == 0) tdClass = 'not_mapped';

        } else if (key.indexOf('League') >= 0) {

            $(newRow).append('<td class=""><span class="league">' + value + '</span></td>');
            return;

        } else if (key.indexOf('GameType') >= 0) {

            $(newRow).append('<td class=""><span class="gameType">' + value + '</span></td>');
            return;

        } else if (key.indexOf('IdSport') >= 0) {

            $(newRow).append('<td class=""><span class="idSport">' + value + '</span></td>');
            return;
        }


        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('<td class="action"><input type="button" class="edit_mapping" value="Edit"><input type="button" class="save_mapping" value="Save"><span class="boxclose"></span></td></tr>');

    $(table).children('tbody').append(newRow);
}