﻿var json = '[' +
          '{' +
            '"text": "Parent 1", ' +
            '"nodes": [' +
              '{' +
                '"text": "Child 1",' +
                '"nodes": [' +
                  '{' +
                    '"text": "Grandchild 1"' +
                  '},' +
                  '{' +
                    '"text": "Grandchild 2", "nodes": [{"text": "Prueba1"},{"text": "Prueba2"}]' +
                  '}' +
                ']' +
              '},' +
              '{' +
                '"text": "Child 2"' +
              '}' +
            ']' +
          '},' +
          '{' +
            '"text": "Parent 2", "nodes": []' +
          '},' +
          '{' +
            '"text": "Parent 3"' +
          '},' +
          '{' +
            '"text": "Parent 4"' +
          '},' +
          '{' +
            '"text": "Parent 5"' +
          '}' +
        ']';

var jsonAgentHierarcy;

$(function () {

    $('.uppercase').keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });

});

function initTree() {

    var initSelectableTree = function () {
        return $('#treeview-selectable').treeview({
            data: jsonAgentHierarcy,
            multiSelect: $('#chk-select-multi').is(':checked'),
            onNodeSelected: function (event, node) {
                $('#selectable-output').prepend('<p>' + node.text + ' was selected</p>');
            },
            onNodeUnselected: function (event, node) {
                $('#selectable-output').prepend('<p>' + node.text + ' was unselected</p>');
            }
        });
    };
    var $selectableTree = initSelectableTree();

    var findSelectableNodes = function () {
        return $selectableTree.treeview('search', [$('#input-select-node').val(), { ignoreCase: false, exactMatch: false }]);
    };
    var selectableNodes = findSelectableNodes();

    $('#chk-select-multi:checkbox').on('change', function () {
        console.log('multi-select change');
        $selectableTree = initSelectableTree();
        selectableNodes = findSelectableNodes();
    });

    // Select/unselect/toggle nodes
    $('#input-select-node').on('keyup', function (e) {
        selectableNodes = findSelectableNodes();
        $('.select-node').prop('disabled', !(selectableNodes.length >= 1));
    });

    $('#btn-select-node.select-node').on('click', function (e) {
        $selectableTree.treeview('selectNode', [selectableNodes, { silent: $('#chk-select-silent').is(':checked') }]);
    });

    $('#btn-unselect-node.select-node').on('click', function (e) {
        $selectableTree.treeview('unselectNode', [selectableNodes, { silent: $('#chk-select-silent').is(':checked') }]);
    });

    $('#btn-toggle-selected.select-node').on('click', function (e) {
        $selectableTree.treeview('toggleNodeSelected', [selectableNodes, { silent: $('#chk-select-silent').is(':checked') }]);
    });

    // red background on no relevant agents
    $('.list-group-item').each(function (index) {
        //console.log(index + ": " + $(this).text());
        if ($(this).text().indexOf('*') > -1)
        {
            var textAgent = $(this).text();

            //$(this).addClass('noRelevant');
            //$(this).text(textAgent.replace('-.-', ''));
        }
    });

}

function GetPHagentHierarchy() {

    var url = "/Accounting/GetPHagentHierarchy/",
        data = {
            agentId: $('#agentId').val(),
            showAll: $('#chk-showAll').prop('checked'),
            betSystem: $('input:radio[name=system]:checked').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        jsonAgentHierarcy = result;

        initTree();

        // end loader
        loader();

    });

}



