﻿$(function () {

    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');

        GetOpenGames();
    });

    // no action

    $('._cancelSpread').change(function () {

        if ($(this).is(':checked')) {

            $('._gradeSpread').prop('checked', true);
            $('._gradeSpread').prop("disabled", true);
        }
        else {
            $('._gradeSpread').prop("disabled", false);
        }

    });

    $('._cancelMoneyLine').change(function () {

        if ($(this).is(':checked')) {

            $('._gradeMoneyLine').prop('checked', true);
            $('._gradeMoneyLine').prop("disabled", true);
        }
        else {
            $('._gradeMoneyLine').prop("disabled", false);
        }

    });

    $('._cancelTtlPts').change(function () {

        if ($(this).is(':checked')) {

            $('._gradeTtlPts').prop('checked', true);
            $('._gradeTtlPts').prop("disabled", true);
        }
        else {
            $('._gradeTtlPts').prop("disabled", false);
        }

    });

    //

    $('.hideTable').on('click', function () {
        $('#final-result2').toggle();
    });
    $('#btn-submit2').click(function () {

        $('#span-totalPlayers').text('0');

        GetGradedGames();
    });
    getAllSportsList();
    $('#selSportTypeList').change(function () {
        $('#selSportSubTypeList').empty();
        getSportDetailsList($('#selSportTypeList').val());
    });
    $('#period').change(function () {
        GetPointsByPeriod($('#period').val(), $('.GameNum-selected').text());
        $('.team1ScoreEop,.team1ScorePts,.team2ScoreEop,.team2ScorePts').val('');
    });
    $('.team1ScoreEop').keyup(function () {
        $('.team1ScorePts').val(parseInt($('.team1ScoreEop').val() - $('.team1ScorePts').data('pts')));
    });
    $('.team2ScoreEop').keyup(function () {
        $('.team2ScorePts').val(parseInt($('.team2ScoreEop').val() - $('.team2ScorePts').data('pts')));
    });
    $('#tbl-result').on('click', '.GameNum', function () {
        loader();
        window.scrollTo(0, 0);
        Getperiod($(this).data('sport'), $(this).data('subsport'));
        $('.teamName1').text($(this).data('teamname1'));
        $('.teamName2').text($(this).data('teamname2'));
        $('.teamRot1').text($(this).data('teamrot1'));
        $('.teamRot2').text($(this).data('teamrot2'));
        $('._pitcherName1').text(($(this).data('pitcher1') == null) ? '----' : $(this).data('pitcher1'));
        $('._pitcherName2').text(($(this).data('pitcher2') == null) ? '----' : $(this).data('pitcher2'));
        $('._dateGameTime').text($(this).data('gamedate').split(' ')[0]);
        $('.GameNum-selected').text($(this).text());
        $('._sub-sport').text($(this).data('subsport'));
        $('._-sport').text($(this).data('sport'));
        $('.team1ScoreEop,.team1ScorePts').val(($(this).data('score1') == null) ? '' : $(this).data('score1'));
        $('.team2ScoreEop,.team2ScorePts').val(($(this).data('score2') == null) ? '' : $(this).data('score2'));
        $('.team1ScorePts,.team2ScorePts').val('');
        $('.team1ScorePts,.team2ScorePts').data('pts', 0);
        $('.grader-box').show();
        $('.GameNum-selected').focus();
        if ($(this).data('pitcher1') == null) {
            $('._pitcher1,._pitcher2').prop('disabled', true);
        } else {
            $('._pitcher1,._pitcher2').prop('disabled', false);
        }
        GetPointsByPeriod($('#period').val(), $('.GameNum-selected').text());
        loader();

        $('._comments').val('');


    });
    $('._cancelGame').on('click', function () {
        if ($('._cancelGame').prop('checked')) {
            $('._gradeSpread,._gradeMoneyLine,._gradeTtlPts').prop('checked', true).prop('disabled', true);
            $('._cancelSpread,._cancelMoneyLine,._cancelTtlPts').prop('checked', true).prop('disabled', true);
        } else {
            $('._gradeSpread,._gradeMoneyLine,._gradeTtlPts').prop('checked', true).prop('disabled', false);
            $('._cancelSpread,._cancelMoneyLine,._cancelTtlPts').prop('checked', false).prop('disabled', false);
        }
    });
    $('#GradingMain').on('click', function () {

        $('#final-result ,#final-result2').empty();
        var gameNum = $('.GameNum-selected').text(),
            periodNum = $('#period option:selected').val(),
            comments = $('._comments').val(),
            gameCancelledFlag = ($('._cancelGame').prop('checked')) ? 'Y' : 'N',
            team1Score = $('.team1ScorePts').val(),
            team2Score = $('.team2ScorePts').val(),
            eopTeam1Score = $('.team1ScoreEop').val(),
            eopTeam2Score = $('.team2ScoreEop').val(),
            startingPitcher1 = ($('._pitcher1').prop('checked')) ? $('._pitcherName1').text() : '',
            startingPitcher2 = ($('._pitcher2').prop('checked')) ? $('._pitcherName2').text() : '',
            gradeSpreadReqFlag = ($('._gradeSpread').prop('checked')) ? 'Y' : 'N',
            gradeMoneyLineReqFlag = ($('._gradeMoneyLine').prop('checked')) ? 'Y' : 'N',
            gradeTtlPtsReqFlag = ($('._gradeTtlPts').prop('checked')) ? 'Y' : 'N',
            cancelSpreadFlag = ($('._cancelSpread').prop('checked')) ? 'Y' : 'N',
            cancelMoneyLineFlag = ($('._cancelMoneyLine').prop('checked')) ? 'Y' : 'N',
            cancelTtlPtsFlag = ($('._cancelTtlPts').prop('checked')) ? 'Y' : 'N',
            dailyFigureDate = $('#datepickerStart').val();
        url = "/NewGraderPage/GradingMain/",
         data = {
             gameNum: gameNum,
             periodNum: periodNum,
             comments: comments,
             gameCancelledFlag: gameCancelledFlag,
             team1Score: team1Score,
             team2Score: team2Score,
             eopTeam1Score: eopTeam1Score,
             eopTeam2Score: eopTeam2Score,
             startingPitcher1: startingPitcher1,
             startingPitcher2: startingPitcher2,
             gradeSpreadReqFlag: gradeSpreadReqFlag,
             gradeMoneyLineReqFlag: gradeMoneyLineReqFlag,
             gradeTtlPtsReqFlag: gradeTtlPtsReqFlag,
             cancelSpreadFlag: cancelSpreadFlag,
             cancelMoneyLineFlag: cancelMoneyLineFlag,
             cancelTtlPtsFlag: cancelTtlPtsFlag,
             dailyFigureDate: dailyFigureDate
         };

        if (data.periodNum == "0" || data.periodNum == "1") {
            if (data.team1Score != data.eopTeam1Score || data.team2Score != data.eopTeam2Score) {
                alert("An error has occurred. Refresh page a try again.");
                return;
            }
        }

        // start loader
        loader();
        ajaxConnexion(data, url, function (result) {
            printDynamicTable($('#final-result'), result);
            //GetGraderResults(gameNum, periodNum);
            loader();
        });

        // default values
        $('._gradeSpread').prop('checked', true);
        $('._gradeMoneyLine').prop('checked', true);
        $('._gradeTtlPts').prop('checked', true);
        $('._cancelSpread').prop('checked', false);
        $('._cancelMoneyLine').prop('checked', false);
        $('._cancelTtlPts').prop('checked', false);

        $('._gradeSpread').prop("disabled", false);
        $('._gradeMoneyLine').prop("disabled", false);
        $('._gradeTtlPts').prop("disabled", false);

        $('._comments').val('');


    });
});

function GetGraderResults(gameNum, periodNumber) {

    var url = "/NewGraderPage/GetGraderResults/",
        data = {
            gameNum: gameNum,
            PeriodNumber: periodNumber
        };

    ajaxConnexion(data, url, function (result) {
        $('.hideTable').show();
        printGradeTable($('#final-result2'), result, '', '');
        // end loader
        loader();

    });
}

function GetPointsByPeriod(periodNumber, gameNum) {
    $('#GradingMain').show();
    var url = "/NewGraderPage/GetPointsByPeriod/",
        data = {
            gameNum: gameNum,
            PeriodNumber: periodNumber
        };
    ajaxConnexion(data, url, function (result) {
        if (result[0].Team1Score == '-1') {
            alert('Required period needs to be graded first');
            $('#GradingMain').hide();
        }

        if (result[0].cTeam1Score != null && result[0].Team1Score != -1) {
            $('.team1ScorePts').data('pts', result[0].cTeam1Score - result[0].Team1Score);
            $('.team2ScorePts').data('pts', result[0].cTeam2Score - result[0].Team2Score);
        } else {
            $('.team1ScorePts').data('pts', result[0].Team1Score);
            $('.team2ScorePts').data('pts', result[0].Team2Score);
        }

        $('.team1ScorePts').val(result[0].Team1Score);
        $('.team2ScorePts').val(result[0].Team2Score);
        $('.team1ScoreEop').val(result[0].cTeam1Score);
        $('.team2ScoreEop').val(result[0].cTeam2Score);
        var cpitcher1 = (result[0].cPitcher1 == 0) ? false : true,
            cpitcher2 = (result[0].cPitcher2 == 0) ? false : true;
        $('._pitcher1').prop('checked', cpitcher1);
        $('._pitcher2').prop('checked', cpitcher2);
    });
}

function GetOpenGames() {
    $('#final-result,#final-result2').empty();
    $('.teamName1').text('test1');
    $('.teamName2').text('test2');
    $('.teamRot1,.teamRot2').text(000);
    $('._pitcherName1,._pitcherName2').text('----');
    $('._dateGameTime').text('mm/dd/yyyy');
    $('.GameNum-selected').text('12345678900');
    $('._sub-sport').text('Sub-Sport ');
    $('._-sport').text('Sport');
    $('team1ScoreEop,team2ScoreEop').data('pts', 0);
    $('.team1ScorePts,.team2ScorePts,team1ScoreEop,team2ScoreEop').val('');
    $('._gradeSpread,._gradeMoneyLine,._gradeTtlPts').prop('checked', true);
    $('._pitcher1,._pitcher2,._cancelGame,._cancelSpread,._cancelMoneyLine,._cancelTtlPts').prop('checked', false);
    var url = "/NewGraderPage/GetOpenGames/",
        data = {
            sport: $('#selSportTypeList').val(),
            sportSubType: $('#selSportSubTypeList').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printGradeTable($('#tbl-result'), result, '', '');

        if (result.length == 0)
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        // end loader
        loader();

    });

}

function GetGradedGames() {
    $('#final-result,#final-result2').empty();
    $('.teamName1').text('test1');
    $('.teamName2').text('test2');
    $('.teamRot1,.teamRot2').text(000);
    $('._pitcherName1,._pitcherName2').text('----');
    $('._dateGameTime').text('mm/dd/yyyy');
    $('.GameNum-selected').text('12345678900');
    $('._sub-sport').text('Sub-Sport ');
    $('._-sport').text('Sport');
    $('team1ScoreEop,team2ScoreEop').data('pts', 0);
    $('.team1ScorePts,.team2ScorePts,team1ScoreEop,team2ScoreEop').val('');
    $('._gradeSpread,._gradeMoneyLine,._gradeTtlPts').prop('checked', true);
    $('._pitcher1,._pitcher2,._cancelGame,._cancelSpread,._cancelMoneyLine,._cancelTtlPts').prop('checked', false);
    var url = "/NewGraderPage/GetGradedGames/",
        data = {
            sport: $('#selSportTypeList').val(),
            sportSubType: $('#selSportSubTypeList').val(),
            days: $('#days').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printGradeTable($('#tbl-result'), result, '', '');

        if (result.length == 0)
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        // end loader
        loader();

    });

}

function getSportDetailsList(sport) {

    var url = "/NewGraderPage/GetSports",
        data = {
            sport: sport
        }

    ajaxConnexion(data, url, function (result) {

        printDynamicSelect($('#selSportSubTypeList'), result, 1, 1, false); // Intranet.js  function printDynamicSelect(table, object, value column number, text column number, duplicates)

    });

}

function Getperiod(sport, sportSubType) {
    $('#period').empty();
    var url = "/NewGraderPage/Getperiod",
        data = {
            sport: sport,
            sportSubType: sportSubType
        }

    ajaxConnexion(data, url, function (result) {

        printDynamicSelect($('#period'), result, 2, 3, false); // Intranet.js  function printDynamicSelect(table, object, value column number, text column number, duplicates)

    });

}

function getAllSportsList() {

    var url = "/NewGraderPage/GetAllSports",
        data = {
        }

    ajaxConnexion(data, url, function (result) {

        printDynamicSelect($('#selSportTypeList'), result, 0, 0, false); // Intranet.js  function printDynamicSelect(table, object, value column number, text column number, duplicates)

    });

}

function printGradeTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createGradeHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createGradeRow(table, result[i]);
    }
}

function createGradeHeader(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });

    $(table).children('thead').append(newRow);

}

function createGradeRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('GameNum') != -1) {

            value = "<a class='GameNum' data-score1='" + row["Team1FinalScore"] + "' data-score2='" + row["Team2FinalScore"] + "' data-gamedate='" + formatDate(row["GameDateTime"]) + "' data-pitcher1='" + row["ListedPitcher1"] + "' data-pitcher2='" + row["ListedPitcher2"] + "' data-teamrot1='" + row["Team1RotNum"] + "' data-teamRot2='" + row["Team2RotNum"] + "' data-teamname1='" + row["Team1ID"] + "' data-teamname2='" + row["Team2ID"] + "' data-sport='" + $('#selSportTypeList').val() + "' data-subsport='" + $('#selSportSubTypeList').val() + "'>" + value + "</a>";

            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}