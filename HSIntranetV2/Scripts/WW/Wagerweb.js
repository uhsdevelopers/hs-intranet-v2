﻿$(function () {


});


function GetcustomerReport(){
    var url;
    var data;

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;
    customerId = document.getElementById('txtCustomerid').value;



    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    url = "/ww/GetWagerWebCustReport",
        data = {
            dateFrom: startDate,
            dateTo: endDate,
            Customerid: customerId
        };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#AccumulatedSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        loader();
    });
}
function ReferAFriendList() {



    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;
    customerId = document.getElementById('txtCustomerid').value;



    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    var url;
    var data;

    url = "/ww/GetReferFriend",
        data = {
            Referred: customerId,
            dateFrom: startDate,
            dateTo: endDate
        };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTableLD($('#AccumulatedSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}



function printDynamicTableRF(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createRowRF(table, result[i]);
    }
}

function createRowRF(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        

        var tdClass = '';
        if (key == 'ID') {
            value = '<a href="#" class="summaryIp" data-ip="' + value + '" data-customer="' + row.ID + '">' + value + '</a>';
        }
        else if (isNaN(value) && key == 'DateCreated') {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        }

        if (key === 'CustomerID' && value === '-') {
        value = '<input type="text" id="CID" name="CID">';
        }
       
        if (key === 'Contacted') {

            if (value === false) {
                value = '<input type="checkbox" id="contacted" name="contacted">';
            } else {
                value = '<input type="checkbox" id="contacted" name="contacted" checked>';
            }
            
        }
        if (key === 'ContactedBy' && value === '-') {
            value = '<input type="text" id="Clerk" name="Clerk"> <input type="submit" value="Update" class="btn btn-danger"/>';
        }
        

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');

      

    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}



function printDynamicTableLD(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title

    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeaderLD(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createRowLD(table, result[i]);
    }
}

function createRowLD(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value) && key == 'LastDatePlayed') {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Balance') != -1 || key.indexOf('Amount') != -1) {
            if ((/\S/.test(value))) {
                value = formatCurrencyNosymbol(value);
            }
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}

function createHeaderLD(table, row) {

    var newRow = $('<tr></tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '" text-align="Center"><span style="margin:auto; display:table;">' + value + '</span></th>');
    });

    $(table).children('thead').append(newRow);

}


function WWExportReport() {
    var url;
    var data;

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;
    customerId = document.getElementById('txtCustomerid').value;



    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    url = "/ww/GetWagerWebCustReport",
        data = {
                dateFrom: startDate,
                dateTo: endDate,
                Customerid: customerId
        };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        JSONToCSVConvertor(result, "Details", true);

        loader();
    });
}


