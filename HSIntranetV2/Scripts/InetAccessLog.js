﻿$(function () {
    
    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');

        GetInetAccessLog();
    });
    $("#tbl-result2").dialog({ autoOpen: false });

    $('#tbl-result').on('click', '.summaryIp', function () {
        GetRelatedAccountsByIP($(this).data('customer'),$(this).data('ip'));
    });


});

function GetInetAccessLog() {

    var startDate = $('#datepickerStart').val(),
      endDate = $('#datepickerEnd').val();
    var company;
 
        company = checkedRadioBtn('TType2');
 
        

    var url = "/InetAccessLog/GetInetAccessLog/",
        data = {
            dateFrom: startDate + ' 12:00 AM',
            dateTo: endDate + ' 11:59 PM',
            accountId: $('#Customer').val(),
            ipAddress: $('#Address').val(),
            RelatedAccounts: ($('#Related').is(':checked')) ? '1' : '0',
            Company: company
    };
    if ($('#Customer').val() == '' && $('#Address').val() == '') {
        alert("Customer or IP Can not be blank")
        return false;
    }
       

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
loader();
        printDynamicTableInet($('#tbl-result'), result, '', '');
        
        if (result.length == 0) 
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        // end loader
       

    });

}

function GetRelatedAccountsByIP(accountId, ipAddress) {

    var url = "/InetAccessLog/GetRelatedAccountsByIP/",
        data = {
            accountId: accountId,
            ipAddress: ipAddress
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

       

        if (result.length != 0) {
            printDynamicTableInet($('#tbl-result2'), result, '', '');
            $("#tbl-result2").dialog("open");
            $("#tbl-result2").dialog({ width: 'auto' });
        }
        
        // end loader
        loader();

    });

}

function printDynamicTableInet(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeaderInet(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createRowInet(table, result[i]);
    }
}

function createHeaderInet(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        if (value != 'CounterCorrelated')
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });

    $(table).children('thead').append(newRow);

}

function createRowInet(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        if (key == 'CounterCorrelated')
            return false;

        var tdClass = '';
        if (key == 'IPAddress' && row.CounterCorrelated > 0 && value != 'HSTW') {
            value = '<a href="#" class="summaryIp" data-ip="' + value + '" data-customer="'+row.LoginID+ '">' + value + '</a>';
        }
        else if (isNaN(value) && key == 'AccessDateTime') {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        }  

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}