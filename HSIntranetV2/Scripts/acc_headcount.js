﻿$(function () {
    
    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');
        $('#span-totalCC').text('0');
        $('#span-totalInternet').text('0');

        getHeadCount();
    });

});

function getHeadCount() {

    var url = "/Acc_HeadCount/GetHeadCount/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()

        },
        totalCC = 0
        totalInternet = 0;

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');

        if (result.length == 0) 
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        // end loader
        loader();

        $('.table_Result > tbody tr').each(function () {

            if ($(this).find('td:eq(2)').text() == "1") {
                totalCC = totalCC + 1;
            }
            else {
                totalInternet = totalInternet + 1;
            }
            
        });

        $('#span-totalCC').text(totalCC);
        $('#span-totalInternet').text(totalInternet);

    });

}