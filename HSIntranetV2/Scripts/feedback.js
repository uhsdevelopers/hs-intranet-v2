﻿$(function () {
    $('#datepickerStart').val(dateNow());
    $('#datepickerEnd').val(dateNow());
    getASI_Import($('#datepickerStart').val(), $('#datepickerEnd').val());
    $('#btnSubmit').on('click', function () {
        getASI_Import($('#datepickerStart').val(), $('#datepickerEnd').val());
    });
});

function getASI_Import(dateStart,dateEnd) {
    var url = "/Feedback/GetMobileRatings/",
        data = { dateStart: dateStart, dateEnd: dateEnd + " 23:59:59" };
    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDinamicFeedbackTable($('#feedback'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        // end loader
        loader();
    });
}


function printDinamicFeedbackTable(table, result) {

    $(table).show();

    // clear old data
    $(table).empty();

    if (result.length == 0) return;

    // table title row
    var keys = Object.keys(result[0]);
    createRow(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createFeedbackRow(table, result[i]);
    }
}
function createFeedbackRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
       
            if (key.indexOf('Date') >= 0) {
                value = formatDate(value);
            }
            
            if (key.indexOf('Rate') >= 0) {
            switch (value) {
                case 1:
                    tdClass = "rate_one";
                    break;
                case 2:
                    tdClass = "rate_two";
                    break;
                case 3:
                    tdClass = "rate_three";
                    break;
                case 4:
                    tdClass = "rate_four";
                    break;
                case 5:
                    tdClass = "rate_five";
                    break;
            }
            value = '<div class="stars '+tdClass+'"></div>';
            tdClass = '';
        }else if (key.indexOf('Customer') >= 0) {          
            value = ' <div class="customer_id">'+value+'</div>';
        }
       
        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).append(newRow);
}

