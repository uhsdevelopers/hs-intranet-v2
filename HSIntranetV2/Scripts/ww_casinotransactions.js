﻿$(function () {

    $('#btn-submit').click(function () {

        getCasinoTransactions();

    });

    $('#input_nonposted').click(function () {

        if ($(this).is(':checked')) {
            $('#datepickerStart').prop('disabled', true);
            $('#datepickerEnd').prop('disabled', true);
        }
        else {
            $('#datepickerStart').prop('disabled', false);
            $('#datepickerEnd').prop('disabled', false);
        }

    });

});

function getCasinoTransactions() {

    var url = "/WW_CasinoTransactions/GetWWCasinoTransactions/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            masterAgentID: $('#txb_agentid').val(),
            customerID: $('#txb_customerID').val(),
            isNonPosted: $('#input_nonposted').is(':checked')

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');

        // end loader
        loader();

    });

}