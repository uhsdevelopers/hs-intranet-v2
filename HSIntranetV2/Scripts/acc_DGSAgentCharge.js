﻿$(function () {
    
    $('#btn-submit').click(function () {

       

        getDGSAgentCharge();
    });

});

function getDGSAgentCharge() {

    var url = "/Acc_DGSAgentCharge/GetDGSAgentCharge/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()

        }
    ;

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');

        
        // end loader
        loader();

        
    });

}