﻿$(function () {

    $('#btn-submit').click(function () {

        getWWComissions();
    });

});

function getWWComissions() {

    var url = "/WW_ComissionDetails/GetWWComissions/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');

        if (result.length == 0)
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        summary(result);

        // end loader
        loader();

    });

}

function summary(result) {

    var bonusCredit = 0,
        bonusDebit = 0,
        creditAdj = 0,
        debitAdj = 0,
        deposit = 0,
        feesCredit = 0,
        withdrawal = 0,
        total = 0;


    var arr_cols = $('#tbl-result tr td:nth-child(4)');

    $.each(result, function (key, row) {

        switch (row.TranType) {
            
            case 'Promo Credit (Cash Bonus)':
                bonusCredit += row.Fee;
                break;
            case 'Promotional Credit':
                bonusCredit += row.Fee;
                break;
            case 'Promo Debit (Cash Bonus)':
                bonusDebit += row.Fee;
                break;
            case 'Promotional Debit':
                bonusDebit += row.Fee;
                break;
            case 'Credit Adjustment':
                creditAdj += row.Fee;
                break;
            case 'Debit Adjustment':
                debitAdj += row.Fee;
                break;
            case 'Deposit':
                deposit += row.Fee;
                break;
            case 'Fees Credit':
                feesCredit += row.Fee;
                break;
            case 'Withdrawal':
                withdrawal += row.Fee;
                break;

        }

        total += row.Fee;

    });

    $('#B').text(bonusCredit.toFixed(2));
    $('#N').text(bonusDebit.toFixed(2));
    $('#C').text(creditAdj.toFixed(2));
    $('#D').text(debitAdj.toFixed(2));
    $('#E').text(deposit.toFixed(2));
    $('#F').text(feesCredit.toFixed(2));
    $('#I').text(withdrawal.toFixed(2));
    $('#Total').text(total.toFixed(2));



}