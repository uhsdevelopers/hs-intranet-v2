$(function(){
	if(sessionStorage.weekNumber == "" || sessionStorage.weekNumber == null || sessionStorage.weekNumber == undefined){
		window.location = "SelectWeek.asp";
		return;
	}
	$.get('http://192.168.101.173:8080/Survivor/GamesByweek?weekid=' + sessionStorage.weekNumber, function (contest) {
		var $currentWeek = contest;
		$('.week-number').text($currentWeek.Name);
		for (var i = 0; i < $currentWeek.length; i++) {
		    var game = $currentWeek[i],
			str = '',
			WeekID = game.WeekID,
			team1_id = game.TeamID1,
			team2_id = game.TeamID2,
			team1_name = game.TeamName1.split(' ')[game.TeamName1.split(' ').length-1],
			team1_city = game.TeamName1.replace(team1_name,''),
			team2_name = game.TeamName2.split(' ')[game.TeamName2.split(' ').length-1],
			team2_city = game.TeamName2.replace(team2_name,'');	
			str += '<div class="game">';
			
			str += '<div class="left" data-week="'+WeekID+'" data-teamid="'+team1_id+'">';
			str += '<table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td width="60">';
			str += '<div class="logo '+team1_name.replace('49','').toLowerCase()+'"></div>';
			str += '</td><td>';
			str += '<div class="name">'+team1_city+'<br><b>'+team1_name+'</b></div>';
			str += '</td></tr></table></div>';

			str += '<div class="right" data-week="'+WeekID+'" data-teamid="'+team2_id+'">';
			str += '<table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td>';
			str += '<div class="name">'+team2_city+'<br> <b>'+team2_name+'</b></div>';
			str += '</td>';
			str += '<td width="60">';
			str += '<div class="logo '+team2_name.replace('49','').toLowerCase()+'"></div>';
			str += '</td></tr></table></div>';

			str += '</div>';	
			$('.main_content').append(str);
		}
		$('.loader_container').hide();
	});


	$('.main_content').on('click', '.left,.right', function () {
	    $('.loader_container').show();
	    var $this = $(this),
	        $loseTeam = '';
	    if ($this.hasClass('left')) {
	        $loseTeam = $this.siblings('.right');
	        $loseTeam.removeClass('selected').removeClass('hidden');
	        $this.removeClass('selected').removeClass('hidden');
	        $this.addClass('selected');
	        $loseTeam.addClass('hidden');
	    } else {
	        $loseTeam = $this.siblings('.left');
	        $loseTeam.removeClass('selected').removeClass('hidden');
	        $this.removeClass('selected').removeClass('hidden');
	        $this.addClass('selected');
	        $loseTeam.addClass('hidden');
	    }
	    //alert($this.data('teamid') + ' -- ' + $loseTeam.data('teamid'));
	    submitPick($this.data('teamid'), $loseTeam.data('teamid'));
	});

});

function submitPick(teamId1, teamId2) {
	
	$.get('http://192.168.101.173:8080/Survivor/GradeByWeek?weekid=' + sessionStorage.weekNumber + '&teamid=' + teamId1 + '&status=true', function (result) {
		
	});
	$.get('http://192.168.101.173:8080/Survivor/GradeByWeek?weekid=' + sessionStorage.weekNumber + '&teamid=' + teamId2 + '&status=false', function (result) {
        $('.loader_container').hide();
	});
	
}
