﻿var Gl_result;

$(function () {

    getASIimportTables();

    $('#btn_Submit').click(function () {
        getComparison();
    });

    $('#btn_Delete').click(function () {

        $("#dialog-confirm").dialog("open");

    });

    $('#getInfoToExcel').click(function () {

        JSONToCSVConvertor(Gl_result, "Details", true);
    });

    $("#dialog-confirm").dialog({
        resizable: false,
        height: 200,
        modal: true,
        autoOpen: false,
        buttons: {
            "Delete this backup": function () {
                $(this).dialog("close");
                deleteBackup();
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });



});

function getASIimportTables() {

    $('#fade').show();
    $('#img-loader').show();
    var url = "/Acc_ImportComparison/GetASIimportTables/",
        data = {};

    ajaxConnexion(data, url, function (result) {

        $('.ddl_importTable').empty();
        printDynamicSelect($('.ddl_importTable'), result, 0, 1, true);
        
        $('#fade').hide();
        $('#img-loader').hide();

    });
}

function getComparison() {


    var url = "/Acc_ImportComparison/GetComparison/",
        data = {
            table1: $('#ddl_importTable1').val(),
            table2: $('#ddl_importTable2').val(),
            deleteBackup: 0
        };

    $('#message').text('');

    ajaxConnexion(data, url, function (result) {

        if (result.length == 0) $('#message').text('No differences found.');

        printDynamicTable($('#tbl_table1'), result, '', '');

        Gl_result = result;

    });
}

function deleteBackup() {


  
    var url = "/Acc_ImportComparison/GetComparison/",
       data = {
           table1: $('#ddl_importTable3').val(),
           table2: '',
           deleteBackup: 1
       };

    ajaxConnexion(data, url, function (result) {
        if (result[0].Error == 1) {
            $('.ddl_importTable').empty();
            $('#message2').text('Backup deleted.');
            getASIimportTables();
           
        } else {
            printDynamicTable($('#tbl_table2'), result, '', '');
        }



    });

}
