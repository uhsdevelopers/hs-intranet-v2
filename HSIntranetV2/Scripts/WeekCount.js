﻿$(document).ready(function () {
    moment.locale('en', {
        week: { dow: 1 } // Monday is the first day of the week
    });

    $("#week1").empty();
    $("#week2").empty();
    $("#week1").hide();
    $("#week2").hide();
    $("#ASILBL").hide();
    $("#DGSLBL").hide();
    $("#AllLBL").hide();
    
    //Initialize the datePicker(I have taken format as mm-dd-yyyy, you can     //have your owh)
    $("#weeklyDatePicker").datetimepicker({
        format: 'MM-DD-YYYY'
    });

    $("#weeklyDatePicker2").datetimepicker({
        format: 'MM-DD-YYYY'
    });
    //Get the value of Start and End of Week
    $('#weeklyDatePicker').on('dp.change', function (e) {
        var value = $("#weeklyDatePicker").val();
        var firstDate = moment(value, "MM-DD-YYYY").day(1).format("MM-DD-YYYY");
        var lastDate = moment(value, "MM-DD-YYYY").day(7).format("MM-DD-YYYY");
        $("#weeklyDatePicker").val(firstDate + " - " + lastDate);
    });
    
    $('#weeklyDatePicker2').on('dp.change', function (e) {
        var value = $("#weeklyDatePicker2").val();
        var firstDate = moment(value, "MM-DD-YYYY").day(1).format("MM-DD-YYYY");
        var lastDate = moment(value, "MM-DD-YYYY").day(7).format("MM-DD-YYYY");
        $("#weeklyDatePicker2").val(firstDate + " - " + lastDate);
    });
});

var active1 = 0;
var active2 = 0;
var playercount1 = 0;
var playercount2 = 0;
var postedticket1 = 0;
var postedticket2 = 0;
var volumen1 = 0;
var volumen2 = 0;
function getWeekData() {
    var value = $("#weeklyDatePicker").val();
    var value2 = $("#weeklyDatePicker2").val();
    var startDate = moment(value, "MM-DD-YYYY").day(1).format("MM-DD-YYYY"),
        endDate = moment(value, "MM-DD-YYYY").day(7).format("MM-DD-YYYY"),
        
    company = checkedRadioBtn('TType');

    var startDate2 = moment(value2, "MM-DD-YYYY").day(1).format("MM-DD-YYYY"),
        endDate2 = moment(value2, "MM-DD-YYYY").day(7).format("MM-DD-YYYY");

 
    var url;
    var data;
    var url2;
    var data2;
    var url3;
    var data3;
    var url4;
    var data4;

    if (startDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();
  

 

    url = "/Wagering/GetweekCount",
       data = {
           dateStart: startDate,
           dateEnd: endDate,
           company: company
       };



    url2 = "/Wagering/GetweekCount",
       data2 = {
           dateStart: startDate2,
           dateEnd: endDate2,
           company: company
       };


   

    url3 = "/Wagering/GetweekCountDaily",
       data3 = {
           dateStart: startDate,
           dateEnd: endDate,
           company: company
       };
    
   

    url4 = "/Wagering/GetweekCountDaily",
       data4 = {
           dateStart: startDate2,
           dateEnd: endDate2,
           company: company
       };

    // start loader
    loader();
    
    $("#week1").show();
    $("#week2").show();

    ajaxConnexion(data, url, function (result1) {
        
        active1 = result1[0]["ActivePlayers"];
        playercount1 = result1[0]["PlayerCounterWithTickets"];
        postedticket1 = result1[0]["PostedTickets"];
        volumen1 = result1[0]["volumen"];
        printDynamicTable($('#PokerSummary'), result1, '', ''); // HS_script.js  function printDinamicTable(table, object)
        // end loader
        $('<p>Week from' + value + '</p>').appendTo('#week1');
    });
    


    
    ajaxConnexion(data2, url2, function (result) {
        printDynamicTable($('#TransferSummary'), result, '', ''); // HS_script.js  function printDinamicTable(table, object)
        $('<p> Week From' + value2 + '</p>').appendTo('#week2');
        
      
        active2 = result[0]["ActivePlayers"];
        playercount2 = result[0]["PlayerCounterWithTickets"];
        postedticket2 = result[0]["PostedTickets"];
        volumen2 = result[0]["volumen"];
    
        
    });
    
    ajaxConnexion(data3, url3, function (result3) {

        printDynamicTable($('#DailySummary1'), result3, '', ''); // HS_script.js  function printDinamicTable(table, object)
        // end loader

    });
    
    ajaxConnexion(data4, url4, function (result4) {

        printDynamicTable($('#DailySummary2'), result4, '', ''); // HS_script.js  function printDinamicTable(table, object)
        // end loader
        
    });
    
    $(document).ajaxStop(function () {
        getChart(active1, active2);
        getPlayerCount(playercount1, playercount2);
        getPostedticket(postedticket1, postedticket2);
        getVolumen(volumen1, volumen2);
        loader();
    });



}

function getBookPlayerData() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;

    if (startDate == '') {
        alert('Please select at least one filter.');
        return;
    }
    var d = -1;
    var url = "/wagering/GetBookPlayerCounter/",
        data = {
            week: d,
            dateStart: startDate + ' 12:00 AM',
            dateEnd: endDate + ' 23:59 PM',

        };

    // start loader
    loader();
    $("#AllLBL").show();

    document.getElementById('ASILBLt').innerHTML = "From: " + startDate + " To: " + endDate + " 23:59 PM";
    document.getElementById('DGSLBLt').innerHTML = "From: " + startDate + " To: " + endDate + " 23:59 PM";

    ajaxConnexion(data, url, function (result) {
        $("#ASILBL").show();
        printDynamicTable($('#tbl-result'), result, '', '');
        $('#tbl-result').tablesorter();
        // end loader
        

    });
    
    var urlDgs = "/wagering/GetBookPlayerCounter/",
    dataDgs = {
        week: d,
        dateStart: startDate + ' 12:00 AM',
        dateEnd: endDate + ' 23:59 PM',
        company:2,

    };

    // start loader
  

    ajaxConnexion(dataDgs, urlDgs, function (result) {

        printDynamicTable($('#tbl-result2'), result, '', '');
        $('#tbl-result').tablesorter();
        // end loader
        loader();
        $("#DGSLBL").show();
    });

}




function getBookPlayerDataW(d) {

   // var d = document.getElementById("select_id").value;

    //d.onchange();
    
    var startDate ;
    var endDate ;

    var text ="";
    
    switch (d) {
        case 0:
            // code block
            text = "This Week";
            break;
        case 1:
            // code block
            text = "Last Week";
            break;
        case 2:
            // code block
            text = "Last Two Week";
            break;
        default:
            // code block
    }

   
  
   
    var curr = new Date; // get current date
    var first = (curr.getDate() - curr.getDay())+1; // First day is the day of the month - the day of the week
    var last = first + 6; // last day is the first day + 6

    var firstday = new Date(curr.setDate(first)).toUTCString();
    var lastday = new Date(curr.setDate(last)).toUTCString();
    startDate = moment(firstday).format('MM/DD/YYYY');
    endDate = moment(firstday).format('MM/DD/YYYY');

    loader();
    $("#AllLBL").show();
    $("#tbl-result").empty();
    $("#tbl-result2").empty();

    //alert(startDate + endDate);

    var url = "/wagering/GetBookPlayerCounter/",
        data = {
            week:d,
            dateStart: startDate,
            dateEnd: endDate

        };

    // start loader


    ajaxConnexion(data, url, function (result) {
        $("#ASILBL").show();
        document.getElementById('ASILBLt').innerHTML = text;
        document.getElementById('DGSLBLt').innerHTML = text;
        
        printDynamicTable($('#tbl-result'), result, '', '');
        $('#tbl-result').tablesorter();
        // end loader
        loader();
        

    });
    
    var urlDgs = "/wagering/GetBookPlayerCounter/",
   dataDgs = {
       week: d,
       dateStart: startDate + ' 12:00 AM',
       dateEnd: endDate + ' 23:59 PM',
       company: 2,

   };

    // start loader
   

    ajaxConnexion(dataDgs, urlDgs, function (result) {

        printDynamicTable($('#tbl-result2'), result, '', '');
        $('#tbl-result2').tablesorter();
        // end loader
       
        $("#DGSLBL").show();
    });
    
}

function getPhoneCountW(d,txtAgent) {

    // var d = document.getElementById("select_id").value;

    //d.onchange();

    var startDate;
    var endDate;

    var text = "";

    switch (d) {
        case 0:
            // code block
            text = "This Week";
            break;
        case 1:
            // code block
            text = "Last Week";
            break;
        case 2:
            // code block
            text = "Last Two Week";
            break;
        default:
        // code block
    }




    var curr = new Date; // get current date
    var first = (curr.getDate() - curr.getDay()) + 1; // First day is the day of the month - the day of the week
    var last = first + 6; // last day is the first day + 6

    var firstday = new Date(curr.setDate(first)).toUTCString();
    var lastday = new Date(curr.setDate(last)).toUTCString();
    startDate = moment(firstday).format('MM/DD/YYYY');
    endDate = moment(firstday).format('MM/DD/YYYY');

    loader();
    $("#AllLBL").show();
    $("#tbl-result").empty();
    $("#tbl-result2").empty();

    //alert(startDate + endDate);

    

    var urlDgs = "/wagering/GetPhoneCounter/",
        dataDgs = {
            week: d,
            dateStart: startDate + ' 12:00 AM',
            dateEnd: endDate + ' 23:59 PM',
            Agent: txtAgent,

        };

    // start loader


    ajaxConnexion(dataDgs, urlDgs, function (result) {

        printDynamicTable($('#tbl-result2'), result, '', '');
        $('#tbl-result2').tablesorter();
        // end loader

        $("#DGSLBL").show();
        loader();
    });

}

function getPhoneCountD(txtAgent) {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;

    if (startDate == '') {
        alert('Please select at least one filter.');
        return;
    }
    var d = -1;


    // start loader
    loader();
    $("#AllLBL").show();

 
    document.getElementById('DGSLBLt').innerHTML = "From: " + startDate + " To: " + endDate + " 23:59 PM";




    var urlDgs = "/wagering/GetPhoneCounter/",
        dataDgs = {
            week: d,
            dateStart: startDate + ' 12:00 AM',
            dateEnd: endDate + ' 23:59 PM',
            Agent: txtAgent,

        };

    // start loader


    ajaxConnexion(dataDgs, urlDgs, function (result) {

        printDynamicTable($('#tbl-result2'), result, '', '');
        $('#tbl-result').tablesorter();
        // end loader
        loader();
        $("#DGSLBL").show();
    });

}