﻿$(function () {


});

function getData() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

        url = "/LiveDealer/FillReports",
           data = {
               startDate: startDate+' 12:00 AM' ,
               endDate: endDate + ' 11:59 PM'
           };
 


    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        alert(result);



        // end loader
        loader();
    });
}


function getDataDaily() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/FillReportsph",
       data = {
           startDate: startDate + ' 12:00 AM',
           endDate: endDate + ' 11:59 PM'
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
        loader();
    });
}

function getCasinoData() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetCasinoData",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}

function getCasinoDatabyDate() {


    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetCasinoDatabyDate",
             data = {
                 startDate: startDate + ' 12:00 AM',
                 endDate: endDate + ' 11:59 PM'
             };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}

function getCasinoProfitCSD() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/CSD/GetcasinoGeneralProfit",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)


        $('#CasinoSummary').tablesorter();


        // end loader
        loader();
    });
}

function getCasinototalCSD() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/CSD/GetcasinoGeneralFinal",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)


        $('#CasinoSummary').tablesorter();


        // end loader
        loader();
    });
}

    function getCasinoProfitCSDExcel() {


        $("#TransferSummary").hide();
       

        var url;
        var data;

        url = "/CSD/GetcasinoGeneralProfit",
           data = {
           };



        // start loader
        loader();

        ajaxConnexion(data, url, function (result) {
            JSONToCSVConvertor(result, "CasinoDetails", true);


            // end loader
            loader();
        });
    }

    function getCasinoTotalCSDExcel() {


        $("#TransferSummary").hide();


        var url;
        var data;

        url = "/CSD/GetcasinoGeneralFinal",
           data = {
           };



        // start loader
        loader();

        ajaxConnexion(data, url, function (result) {
            JSONToCSVConvertor(result, "CasinoDetails", true);


            // end loader
            loader();
        });
    }

function getLiveDealerExcel() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;
    var file;
    url = "/LiveDealer/GetLiveDealerExcel",
       data = {
       };

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#LDSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
    });
}

function getBetsoftExcel() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;
    var file;
    url = "/LiveDealer/GetBetsoftExcel",
       data = {
       };

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#LDSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
    });
}

function getNucleusExcel() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;
    var file;
    url = "/LiveDealer/GetNucleusExcel",
       data = {
       };

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#LDSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
    });
}

function getLiveDealer() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetLiveDealerData",
       data = {
       };



   

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#LDSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        
    });
}

function GetCasinoRebateReport() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetCasinoRebateReport",
        data = {
        };


    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#DVCasinoRebateReport'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)


    });
}
function GetCasinoDisabled() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetCasinoDisabledReport",
        data = {
        };


    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#DVGetCasinoDisabled'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)


    });
}



function getLiveDealerDaily() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetLiveDealerData",
       data = {
       };





    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#LDSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader

    });
}


function getCasinoProfitData() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetCasinoProfitTotal",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoProfitSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}


function getCasinoBlock() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/CSD/GetCasinoBlock",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoProfitSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        $('#CasinoProfitSummary').tablesorter();



        // end loader
        loader();
    });
}


function getAccumulated() {


    var startDate = document.getElementById('Startdate').value,
      endDate = document.getElementById('Enddate').value,
      customerId =document.getElementById('txtCustomerid').value;


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    var url;
    var data;

    url = "/csd/GetAccumulatedCustomerRisk",
       data = {
           customerId:customerId,
           startDate: startDate ,
           endDate: endDate
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#AccumulatedSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}



function SevenNonPosted() {


    var customerid = document.getElementById('txtCustomerid').value,
      Company = document.getElementById('txtCompany').value,
    provider = document.getElementById('txtProvider').value;


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

  

    var url;
    var data;

    url = "/Seven/GetNonPostedPlayers",
       data = {
           company: Company,
           player: customerid,
           provider: provider
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#AccumulatedSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}

function SevenAdmin() {


    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value,
        customerid = document.getElementById('txtCustomerid').value;
    
    startDate = moment(startDate, "MM-DD-YYYY");
    endDate = moment(endDate, "MM-DD-YYYY");

    var sDate = startDate.format('YYYY-MM-DD');
    var eDate = endDate.format('YYYY-MM-DD');

    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    var url;
    var data;

    url = "/seven/getplayerData",
       data = {
           player: customerid,
           dateStart: sDate,
           dateEnd: eDate
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printSevenTable($('#AccumulatedSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        $('.show-bets').click(function () {

            $(this).closest('tr').next('.tr-bets').toggle();

        });
        // end loader
        loader();
    });
}


function BRGetRevenue() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/CSD/BetRegalCA_GetRevenue",
       data = {
           startDate: startDate + ' 12:00 AM',
           endDate: endDate + ' 11:59 PM'
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}


function printSevenTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title

    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead class="thead-dark"></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createSevenHeader(table, keys);

    var addClass = '';

    for (j = 0, jlength = result.length; j !== jlength; j++) {

        addClass = (addClass == 'tr-light') ? 'tr-dark' : 'tr-light';

        createSevenRow(table, result[j], addClass);
    }
}

function createSevenHeader(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';
    
    $(newRow).append('<th class="' + addClass + '">id</th>');
    $(newRow).append('<th class="' + addClass + '">channel</th>');
    $(newRow).append('<th class="' + addClass + '">payoutAmount</th>');
    $(newRow).append('<th class="' + addClass + '">payoutDate</th>');
    $(newRow).append('<th class="' + addClass + '">product</th>');
    $(newRow).append('<th class="' + addClass + '">status</th>');
    $(newRow).append('<th class="' + addClass + '">payment</th>');
    $(newRow).append('<th class="' + addClass + '">winnings</th>');
    $(newRow).append('<th class="' + addClass + '">Bets</th>');
    
    $(table).children('thead').append(newRow);

}

function createSevenRow(table, row, addClass) {

    var newRow = $('<tr>'),
        addClass2 = '',
        tdClass = '',
        tableBets = '',
        winnings = (row.winnings == undefined) ? "" : row.winnings,
        payoutAmount = (row.payoutAmount == undefined) ? "" : row.payoutAmount,
        payoutDate = (row.payoutDate == undefined) ? "" : row.payoutDate;
    ;

    $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + row.id + '</td>');
    $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + row.channel + '</td>');
    $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + payoutAmount + '</td>');
    $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + payoutDate + '</td>');
    $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + row.product + '</td>');
    $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + row.status + '</td>');
    $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + row.payment + '</td>');
    $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + winnings + '</td>');
    $(newRow).append('<td class="' + addClass + ' ' + tdClass + '"><span class="show-bets">DISPLAY</span></td>');
    
    $(table).children('tbody').append(newRow);

    if (row.bets.length > 0)
    {
        tableBets = '<tr class="tr-bets" style="display:none;"><td class="' + addClass + '">Bets</td><td colspan="8" class="' + addClass + '"><table class="tbl-bets table-sm table-striped table-dark"><thead class="thead-light">';

        tableBets += '<tr>';
        tableBets += '<th class="' + addClass2 + '">eventid</th>';
        tableBets += '<th class="' + addClass2 + '">status</th>';
        tableBets += '<th class="' + addClass2 + '">type</th>';
        tableBets += '<th class="' + addClass2 + '">value</th>';
        tableBets += '<th class="' + addClass2 + '">winnings</th>';
        tableBets += '</tr></thead><tbody>';

        for (i = 0, ilength = row.bets.length; i !== ilength; i++) {

            tableBets += '<tr>';
            tableBets += '<td class="' + addClass2 + ' ' + tdClass + '">' + row.bets[i].eventId + '</td>';
            tableBets += '<td class="' + addClass2 + ' ' + tdClass + '">' + row.bets[i].status + '</td>';
            tableBets += '<td class="' + addClass2 + ' ' + tdClass + '">' + row.bets[i].type + '</td>';
            tableBets += '<td class="' + addClass2 + ' ' + tdClass + '">' + row.bets[i].value + '</td>';
            tableBets += '<td class="' + addClass2 + ' ' + tdClass + '">' + row.bets[i].winnings + '</td>';
            tableBets += '</tr>';

        }

        tableBets += '</tbody></table></td></td></tr>';

        $(table).children('tbody').append(tableBets);

    }


    //$.each(row, function (key, value) {
    //    var tdClass = '';
    //    if (isNaN(value)) {
    //        if (value.indexOf('/Date') >= 0) {
    //            value = formatDate(value);
    //        }

    //    } else if (key.indexOf('Amount') != -1 || key.indexOf('Balance') != -1 || key.indexOf('Fee') != -1 || key.indexOf('Volume') != -1 || key.indexOf('volumen') != -1) {
    //        if ((/\S/.test(value))) {
    //            value = formatCurrency(value);
    //        }
    //        addClass = 'align-right';
    //    } else if (key.indexOf('CommentID') >= 0) {
    //        tdClass = 'cl-CommentID';
    //    }

    //    $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    //});

    //$(newRow).append('</tr>');

    //$(table).children('tbody').append(newRow);
}
