﻿$(function () {
});

function getSignup() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value,
        requestId = document.getElementById('RequestId').value;

    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '' && requestId == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;
    if (requestId != '') {
        url = "/RegalPoker/GetSignupbyRequestid/",
         data = {
             requestId: requestId
         };
    }
    else {
        url = "/RegalPoker/GetSignup/",
           data = {
               startDate: startDate,
               endDate: endDate
           };
    }


    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printSignupTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}

function getTransactions() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value,
        customerid = document.getElementById('customerid').value;

    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';



    if (startDate == '' && customerid == '') {
        alert('Please select at least one filter.');
        return;
    }

    var url;
    var data;

    if (customerid != '') {
        url = "/RegalPoker/GetTransactionsbyCustomerid/",
 data = {
     startDate: startDate,
     endDate: endDate,
     customerid: customerid

 };
    }
    else {
        url = "/RegalPoker/GetTransactions/",
     data = {
         startDate: startDate,
         endDate: endDate

     };
    }



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#TransferSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}



function printSignupTable(table, result, idTitle, title) {

    $(table).show();
    $("#updCus").show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);
    var length;
    if (result.length == 0) return;

    // table title row
    var tdClass = '';
    
    var keys;
    var rid ;
    var requestDate ;
    var cid ;
    var fname ;
    var lname;
    var zip;
    var state;
    var country;
    var email;
    var phone;
    var i;
    
    var newKey = $('<tr><td>Request ID</td><td>Customer ID</td><td> Request Date </td><td>First Name</td><td>Last Name</td><td>Email</td><td>Phone</td><td>Country</td><td>Zip</td><td>Request IP</td><td>ReSent Email</td><td>Activate Account</td></tr>');
    $(table).append(newKey);
    for (i = 0, length = result.length; i !== length; i++) {
        
   
    keys = Object.keys(result[i]);
     rid = result[i].RequestId;
     requestDate = formatDate(result[i].RequestDate);
     cid = result[i].Customerid;
        fname = result[i].Firstname;
        lname = result[i].Lastname;
        zip = result[i].Zip;
        state = result[i].State;
        country = result[i].Country;
        email = result[i].Email;
        phone = result[i].HomePhone;
        
        var newRow1 = $('<tr>'),
        addClass = '';
    $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '">' + rid + '</td>');
        
    if (cid != null) {
        $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '"><a href="#" onClick="getCustomerinfo(this)">' + cid + '</a><input type="hidden" name="customerid" id="customerid" value=' + cid + '><input type="hidden" name="requestid" id="requestid" value=' + rid + '></td>');
    } else {
        $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '"><a href="#" onClick="CreateAccount(this)" id="' + rid +' ">Create Account</a><input type="hidden" name="requestid" id="requestid" value=' + rid + '><input type="hidden" name="loginid" id="loginid" value="Intranet"></td>');
    }
    $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '" nowrap>' + requestDate + '</td>');
   
  
    $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '">' + fname + '</td>');
    $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '">' + lname + '</td>');
    $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '">' + email + '</td>');
    $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '">' + phone + '</td>');
    $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '">' + country + '</td>');
    $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '">' + zip + ' </td>');
    
    $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '">' + result[i].RequestIp + '</td>');
        if (cid != null) {
            $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '"><a href="#" onClick="ResendEmail(this)" id="' + rid + ' ">Sent Email</a><input type="hidden" name="requestid" id="requestid" value=' + rid + '><input type="hidden" name="loginid" id="loginid" value="Intranet"></td>');
            $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '"><a href="#" onClick="ActivateAccount(this)" id="' + cid + ' ">Activate Account</a><input type="hidden" name="requestid" id="requestid" value=' + cid + '><input type="hidden" name="loginid" id="loginid" value="Intranet"></td>');
        }
        else {
            $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '"></td>');
            $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '"></td>');
        }

        $(table).append(newRow1);
}//for
}

function printTable(table, result, idTitle, title) {

    $(table).show();
    $("#updCus").show();
    $("#PokerSummary").hide();
    

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);
    var length;
    if (result.length == 0) return;

    // table title row
    var keys = Object.keys(result[0]);
    var cid = result[0].CustomerID;

    var countries = new Array("Belgium", "Bulgaria", "Canada", "Czech Republic", "Denmark", "Germany", "Estonia", "Ireland", "Greece", "Spain", "France", "Croatia", "Italy", "Cyprus", "Latvia",
        "Lithuania", "Luxembourg", "Hungary", "Malta", "Netherlands", "Austria", "Poland", "Portugal", "Romania", "Slovenia", "Slovakia", "Finland", "Sweden","Costa rica");
    var countriesValue = new Array("Be", "BG", "CA", "CZ", "DK", "DE", "EE", "IE", "EL", "ES", "FR", "HR", "IT", "CY", "LV",
       "LT", "LU", "HU", "MT", "NL", "AT", "PL", "PT", "RO", "SI", "SK", "FI", "SE", "CR");
  

    var tdClass = '';
    var newKey = $('<tr><td>' + keys[0] + '</td><td> ' + keys[1] + '</td><td>Status</td></tr>');
    var newRow1 = $('<tr>'),
        addClass = '';
    $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '">' + cid + '</td>');
    $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '"><textarea name=' + keys[1] + ' id=' + keys[1] + ' rows="1">' + result[0].Password + '</textarea></td>');

    var active = '';
    if (result[0].Active == 'Y') {
        active = 'checked';
    }
    var credit = '';
    if (result[0].CreditAcctFlag == 'Y') {
        credit = 'checked';
    }

    $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '">' + keys[2] + '<input type="radio" name=' + keys[2] + '  ' + active + '> Credit: <input type="radio" name=' + keys[44] + '  ' + credit + '></td>');
   
    $(newRow1).append('</tr>');
    $(table).append(newKey);
    $(table).append(newRow1);
    
    var newKey1 = $('<tr class="titulo"><td>' + keys[3] + '</td><td> ' + keys[4] + '</td><td>' + keys[7] + '</td></tr>');
    var newRow2 = $('<tr>');

    $(newRow2).append('<td class="' + addClass + ' ' + tdClass + '">' + result[0].AvailableBalance + '</td>');
    $(newRow2).append('<td class="' + addClass + ' ' + tdClass + '">' + result[0].CurrentBalance + '</td>');
    $(newRow2).append('<td class="' + addClass + ' ' + tdClass + '">' + result[0].CreditLimit + '</td>');
    $(newRow2).append('</tr>');
    $(table).append(newKey1);
    $(table).append(newRow2);
    
    var newKey11 = $('<tr class="titulo1"><td>' + keys[45] + '</td><td> ' + keys[46] + '</td><td>' + keys[47] + '</td></tr>');
    var newRow21 = $('<tr>');
    $(newRow21).append('<td class="' + addClass + ' ' + tdClass + '">' + result[0].PendingWagerBalance + '</td>');
    $(newRow21).append('<td class="' + addClass + ' ' + tdClass + '">' + result[0].PendingWagerCount + '</td>');
    $(newRow21).append('<td class="' + addClass + ' ' + tdClass + '">' + result[0].FreePlayBalance + '</td>');
    $(newRow21).append('</tr>');
    $(table).append(newKey11);
    $(table).append(newRow21);
    
    var newKey2 = $('<tr class="titulo"><td>' + keys[9] + '</td><td> ' + keys[11] + '</td><td>' + keys[10] + '</td></tr>');
    var newRow3 = $('<tr>');
    $(newRow3).append('<td class="' + addClass + ' ' + tdClass + '"><textarea name=' + keys[9] + ' id=' + keys[9] + ' >' + result[0].NameFirst + '</textarea></td>');
    $(newRow3).append('<td class="' + addClass + ' ' + tdClass + '"><textarea name=' + keys[11] + ' id=' + keys[11] + ' >' + result[0].NameMI + '</textarea></td>');
    $(newRow3).append('<td class="' + addClass + ' ' + tdClass + '"><textarea name=' + keys[10] + ' id=' + keys[10] + ' >' + result[0].NameLast + '</textarea></td>');
    $(newRow3).append('</tr>');
    $(table).append(newKey2);
    $(table).append(newRow3);
    
    var newKey5 = $('<tr class="titulo1"><td>' + keys[17] + '</td><td> ' + keys[18] + '</td><td>' + keys[19] + '</td></tr>');
    var newRow6 = $('<tr>');
    $(newRow6).append('<td class="' + addClass + ' ' + tdClass + '"><textarea name=' + keys[17] + ' id=' + keys[17] + ' >' + result[0].EMail + '</textarea></td>');
    $(newRow6).append('<td class="' + addClass + ' ' + tdClass + '"><textarea name=' + keys[18] + ' id=' + keys[18] + ' >' + result[0].HomePhone + '</textarea></td>');
    $(newRow6).append('<td class="' + addClass + ' ' + tdClass + '"><textarea name=' + keys[19] + ' id=' + keys[19] + ' >' + result[0].BusinessPhone + '</textarea></td>');
    $(newRow6).append('</tr>');
    $(table).append(newKey5);
    $(table).append(newRow6);

    var newKey3 = $('<tr class="titulo"><td>' + keys[12] + '</td><td> ' + keys[13] + '</td><td>' + keys[14] + '</td></tr>');
    var newRow4 = $('<tr>');
    $(newRow4).append('<td class="' + addClass + ' ' + tdClass + '"><textarea name=' + keys[12] + ' id=' + keys[12] + ' >' + result[0].Address + '</textarea></td>');
    $(newRow4).append('<td class="' + addClass + ' ' + tdClass + '"><textarea name=' + keys[13] + ' id=' + keys[13] + ' >' + result[0].City + '</textarea></td>');
    $(newRow4).append('<td class="' + addClass + ' ' + tdClass + '"><textarea name=' + keys[14] + ' id=' + keys[14] + ' >' + result[0].State + '</textarea></td>');
    $(newRow4).append('</tr>');
    $(table).append(newKey3);
    $(table).append(newRow4);


    var newKey4 = $('<tr class="titulo1"><td>' + keys[15] + '</td><td> ' + keys[16] + '</td><td></td></tr>');
    var newRow5 = $('<tr>');
    $(newRow5).append('<td class="' + addClass + ' ' + tdClass + '"><input type="text" name=' + keys[15] + ' id=' + keys[15] + ' value =' + result[0].Zip + '></td>');
   // $(newRow5).append('<td class="' + addClass + ' ' + tdClass + '"><input type="text" name=' + keys[16] + ' id=' + keys[16] + ' value =' + result[0].Country + '>');

    var listc;
    listc = $('<select name=' + keys[16] + '  id=' + keys[16] + '>');
    for (var hi = 0; hi < countries.length; hi++)
        if (countriesValue[hi] == result[0].Country) {
            $(listc).append("<option value=\"" + countriesValue[hi] + "\" selected>" + countries[hi] + "</option>");
        } else {
            $(listc).append("<option value=\"" + countriesValue[hi] + "\">" + countries[hi] + "</option>");
        }

        
    $(listc).append('</select>');
    
    $(newRow5).append(listc);
    $(newRow5).append('</td>');
    $(newRow5).append('<td class="' + addClass + ' ' + tdClass + '"></td>');
    $(newRow5).append('</tr>');
    $(table).append(newKey4);
    $(table).append(newRow5);


    var comments = result[0].Comments;
    var newKey6 = $('<tr class="titulo"><td colspan=3>' + keys[22] + '</td></tr>');
    var newRow7 = $('<tr>');
    $(newRow7).append('<td class="' + addClass + ' ' + tdClass + '" colspan=3><textarea name=' + keys[22] + ' id=' + keys[22] + ' >' + comments + '</textarea></td>');
    $(newRow7).append('</tr>');
    $(newRow7).append('<input type="hidden" name=' + keys[0] + ' value =' + cid + '>');
    $(table).append(newKey6);
    $(table).append(newRow7);
    
    
    //var updCustomer = $('<input  type="button" value="Update Customer" id="updateCustomer1" onClick="updateCustomer()"/>');

    //$("#updCus").append(updCustomer);

}




function updateCustomer() {

    $("#updCus").hide();

    var customerid = $('#customerid').val();
    var password = $('#Password').val();
    var nameFirst = $('#NameFirst').val();
    var nameLast = $('#NameLast').val();
    var eMail = $('#EMail').val();
    var homePhone = $('#HomePhone').val();
    var businessPhone = $('#BusinessPhone').val();
    
    var address = $('#Address').val();
    var city = $('#City').val();
    var state = $('#State').val();
    var zip = $('#Zip').val();
    
    var country = $('#Country').val();
    
    var comments = $('#Comments').val();
    
  
    

    var url;
    var data;

    if (customerid != '') {
        url = "/RegalPoker/UpdateInfobyCustomerid/",
 data = {
         customerId: customerid,
         password: password,
         nameLast: nameLast,
         nameFirst: nameFirst,
         email: eMail,
         homePhone: homePhone,
         businessPhone: businessPhone,
         address: address,
         city: city,
         state: state,
         zip: zip,
         country: country,
         comments: comments
 
        };
    
    
    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#TransferSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        // end loader
        loader();
    });
    }

}

function getCustomerinfo(link_customer)
{

    var customerid = $(link_customer).text();

    if (customerid == '') {
        customerid = $('#customerid').val();
    }
    
    


    if (customerid == '') {
        alert('Please select at least one filter.');
        return;
    }

    var url;
    var data;


    url = "/RegalPoker/GetInfobyCustomerid/",
 data = {
     customerid: customerid

 };


    // start loader
    //loader();

    ajaxConnexion(data, url, function (result) {
        printTable($('#TransferSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
       // loader();
    });
    




}

function CreateAccount(link_customer) {

    var requestid = $(link_customer).attr('id');
    var loginid = $('#loginid').val();

    if (requestid == '') {
        requestid = $('#requestid').val();
    }




    if (requestid == '') {
        alert('Please select at least one filter.');
        return;
    }

    if (loginid == '') {
        loginid = 'Intranet';
    }

    var url;
    var data;


    url = "http://api_signup.regalpoker.com/API/Signup/",
 data = {
     requestid: requestid,
     login: loginid

 };


    ajaxConnexion(data, url, function (result) {

        if (result.Errorcode == 0) {
            $('#span-msg').text(' Account #:' + result.CustomerId);

            alert(' Account Created #:' + result.CustomerId);


        } else {
            $('#span-msg').text('Error');
            alert(' Error ' + result.Description);
        }
           
    });
}

function ResendEmail(link_customer) {

    var requestid = $(link_customer).attr('id');


    if (requestid == '') {
        requestid = $('#requestid').val();
    }

    var url;
    var data;


    url = "http://api_signup.regalpoker.com/API/Customer/",
 data = {
     requestid: requestid,
     sid: 1

 };


    ajaxConnexion(data, url, function (result) {

        if (result.Errorcode == 1) {
            $('#span-msg').text(' Email was Sent:' + result.CustomerId);

            alert(' Email was Sent #:' + result.EmailSent);


        } else {
            $('#span-msg').text('Error');
            alert(' Error ' + result.Description);
        }

    });
}
///Regalpoker/GenerateToken?customerid=123&secretWord=123
function ActivateAccount(link_customer) {
    var customerid = $(link_customer).attr('id');
    if (requestid == '') {
        requestid = $('#requestid').val();
    }
    var url;
    var data;
    var token;
    url = "/Regalpoker/GenerateToken/",
 data = {
     customerid: customerid
 };
    ajaxConnexion(data, url, function (result) {
        //token = result.Token;
        token = result.Token;
        ReActivateAccount(customerid,token);
    });
    
    
}

function ReActivateAccount(customerid,token) {
    
    var urldata;
    url = "http://api_signup.regalpoker.com/Api/Customer",
 urldata = {
     customerid: customerid,
     key: token
 };
    ajaxConnexion(urldata, url, function (result) {
        //token = result.Token;
        if (result.svalid != 0) {
            $('#span-msg').text(' Account #:' + result.CustomerId);

            alert(' Account Activated #:' + result.CustomerId);


        } else {
            $('#span-msg').text('Error');
            alert(' Error ');
        }
    });
}