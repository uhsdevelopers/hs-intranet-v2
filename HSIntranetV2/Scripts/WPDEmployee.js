﻿$(function () {
    

    var res = '@ViewBag.Tab';
    if (res == "true") {
        $('#tabs-2').css("display", "block");
    }

    getWPD_MentorList(13000);
    $('input[type=radio]').click(function () {
        $('#WPD_Comments').toggle();
    });
    
    $("input[type=submit], a, button")
         .button()
         .click(function (event) {
             event.preventDefault();
         });
    
    $('.DepartmentID').change(function () {

        var department = $("input:radio[name='Department']:checked").val();
        var status = $("input:radio[name='StatusId']:checked").val();
        getWPD_EmployeeList(department,status);
    });

    $('.StatusId').change(function () {

        var department = $("input:radio[name='Department']:checked").val();
        var status = $("input:radio[name='StatusId']:checked").val();
        getWPD_EmployeeList(department, status);
    });

    $('#getInfoToExcel').click(function () {

        var employeeId = document.getElementById('EmployeeID').value;
        var supervisorId = document.getElementById('SupervisorID').value;
        var datestart = document.getElementById('datepickerStart').value;
        var dateEnd = document.getElementById('datepickerEnd').value;
        var requestTypeSearch = "";
        var departmentId = $('input[name=Department]:checked').val();
        var isPositive = $("input:radio[name='IsPositive']:checked").val();


        datestart = datestart !== '' ? datestart : '01/01/2014';

        if (dateEnd === '') {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10)
            { dd = '0' + dd }
            if (mm < 10) { mm = '0' + mm }
            today = mm + '/' + dd + '/' + yyyy;
            dateEnd = today;
        }


        var url;
        var data;

        url = "/WPDEmployee/GetCommentsByEmployee/",
        data =
      {
          employeeName: employeeId
          , commentBy: supervisorId
          , dateFrom: datestart
          , dateTo: dateEnd
          , hrrRequestId: requestTypeSearch
          , isPositiveComment: isPositive
          , departmentId: departmentId
      };

        ajaxConnexion(data, url, function (result) {
            JSONToCSVConvertor(result, "Details", true);
        });

    });

   // $("#tabs").tabs();
    $("#tabs").tabs();
    
    GetRequestType();
    
    $('.ui-button-text').click(function ()
    {
        $('#WPD_Comments').empty();
    });

    
    $('#btnSearch').click(function () {

        var employeeId = document.getElementById('EmployeeID').value;
        var supervisorId = document.getElementById('SupervisorID').value;
        var datestart = document.getElementById('datepickerStart').value;
        var dateEnd = document.getElementById('datepickerEnd').value;
        var requestTypeSearch = "";
        var departmentId = $('input[name=Department]:checked').val();
        var isPositive = $("input:radio[name='IsPositive']:checked").val();
    
        
        datestart = datestart !== '' ? datestart : '01/01/2014';
      
        if (dateEnd === '') {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10)
            { dd = '0' + dd}
            if (mm < 10) { mm = '0' + mm }
            today = mm + '/' + dd + '/' + yyyy;
            dateEnd = today;
        }

        getWPD_Comments(employeeId, supervisorId, datestart, dateEnd, requestTypeSearch, isPositive, departmentId);
    });

    $('#btnSearchlog').click(function () {


        var datestart = document.getElementById('datepickerStart').value;
        var dateEnd = document.getElementById('datepickerEnd').value;



        datestart = datestart !== '' ? datestart : '01/01/2014';

        if (dateEnd === '') {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10)
            { dd = '0' + dd }
            if (mm < 10) { mm = '0' + mm }
            today = mm + '/' + dd + '/' + yyyy;
            dateEnd = today;
        }

        getWPD_CommentsLog(datestart, dateEnd);

    });


    $('#btnAdd').click(function () {

        var employeeId = document.getElementById('EmployeeIDWPD').value;
        var comment = document.getElementById('Comment').value;
        var supervisorId = document.getElementById('SupervisorIDComments').value;
        var incidentdate = document.getElementById('Incidentdate').value;
        var departmentId = $('input[name=Department]:checked').val();
        var requestTypeSearch = "";
        var isPositive = $("input:radio[name='AddIsPositive']:checked").val();


        addWPD_Comments(employeeId, comment, supervisorId, incidentdate, departmentId, requestTypeSearch, isPositive);
       // $('#btnSearch').click();
    });


    $('#btnDelete').click(function () {

        var commentId = $('#Comment_Details').find('.cl-CommentID').text();
        //var comments = $('#Comment_Details').find('.cl-CommentID').text();
        var commentBy = $('#Id_User').text();

        delete_Comment(commentId, commentBy);
        //$('#btnSearch').click();
    });
    
    $('#btnUpdate').click(function () {

        var commentId = $('#Comment_Details').find('.cl-CommentID').text();
        var comment = $('#Comment_Details').find('#txtComment').val();
        var commentBy = $('#Id_User').text();
        
        Update_Comment(commentId, comment, commentBy);
        $('#btnSearch').click();

    });

});

function getWPD_Comments(employeeId, supervisorId, startDate, startEnd, hrrRequestId, isPositive, departmentId) {
    var url = "/WPDEmployee/GetCommentsByEmployee/",
        data =
        {
             employeeName: employeeId
            , commentBy: supervisorId
            , dateFrom: startDate
            , dateTo: startEnd
            , hrrRequestId: hrrRequestId
            , isPositiveComment: isPositive
            , departmentId: departmentId
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#WPD_Comments'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        

        bindEvents();
        // end loader
        loader();
      
    });
}

function getWPD_CommentsLog(startDate, startEnd) {
    var url = "/WPDEmployee/GetCommentsByLog/",
        data =
        {
             dateFrom: startDate
            , dateTo: startEnd

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#WPD_Comments'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)



        bindEvents();
        // end loader
        loader();

    });
}


function addWPD_Comments(employeeId, comment, supervisorId, incidentdate, departmentId, hrrRequestId, isPositive) {
    var tag = $("<div></div>"); //This tag will the hold the dialog content.
    var msg = "";
    var url = "/WPDEmployee/AddCommentsToEmployee/",
        data =
        {
            employeeName: employeeId
            , commentBy: supervisorId
            , comment: comment
            , incidentdate: incidentdate
            , departmentId: departmentId
            , hrrRequestId: hrrRequestId
            , isPositive: isPositive
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        //alert();
        if (result[0].ErrorMessage =='sucess') {
            msg = 'Comment Added';
        } else {
            {
                msg = result[0].ErrorMessage;
            }
        }
       // tag.html(msg).dialog({ modal: true }).dialog('open');
        $("#dialog").dialog();
        // clear old data
        alert(msg);
        clearFields();

        // end loader
        loader();

    });
}





function clearFields() {
    $("#EmployeeID").val("");
    $("#Comment").val("");
    $("#RequestType").val("");
    $("#EmployeeIDWPD").val("");
   
    $("#Incidentdate").val("");
    

    
}

function getWPD_MentorList(departmentId) {
    var url = "/WPDEmployee/GetEmployeeList/",
        data =
        {
            departmentId: departmentId
            
            
        };

    // start loader


    ajaxConnexion(data, url, function (result) {
        printMentorList($('#SupervisorID,#SupervisorIDComments'), result, 'LoginName', ''); // HS_script.js  function printDynamicTable(table, object)

       

    });
}

function getWPD_EmployeeList(departmentId,status) {
    var url = "/WPDEmployee/GetEmployeeList/",
        data =
        {
            departmentId: departmentId
            , status: status
        };

    // start loader


    ajaxConnexion(data, url, function (result) {
        printEmployeeList($('#EmployeeID,#EmployeeIDWPD'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)



    });
}

function GetRequestType() {
    var url = "/WPDEmployee/GetRequestType/",
        data =
        {};

    // start loader


    ajaxConnexion(data, url, function (result) {
      
        printDynamicList2($('#RequestType,#RequestTypeSearch'), result, 'name'); // HS_script.js  function printDynamicTable(table, object)
});
}

function GetCurrentUser() {
    var url = "/WPDEmployee/currentuser/",
        data ={};

    ajaxConnexion(data, url, function (result) {

        var user = result.username;

    });
}

function printDynamicList2(id, result) {
    $(id).empty();
    $('#RequestTypeSearch').append('<option value="">All Incidents</option>');
    // add cust list to select
    for (var i = 0, length = result.length; i !== length; i++) {
        $(id).append('<option value=' + result[i].id + '>' + result[i].Name + '</option>');
    }
}

function printMentorList(id, result) {
    $(id).empty();
    $('#SupervisorID').append('<option value="">Any</option>');
    // add cust list to select
    for (var i = 0, length = result.length; i !== length; i++) {
        $(id).append('<option value=' + result[i].LoginName + '>' + result[i].LoginName + '</option>');
    }
}

function printEmployeeList(id, result) {
    $(id).empty();
    $('#EmployeeID').append('<option value="">All</option>');
    // add cust list to select
    for (var i = 0, length = result.length; i !== length; i++) {
        $(id).append('<option value=' + result[i].LoginName + '>' + result[i].LoginName + '</option>');
    }
}



function bindEvents() {
    $('#WPD_Comments tr').on('click',function () {
        var commentId = $(this).find('.cl-CommentID').text();
        var description = "";
        getDetails(commentId, description);
    });

}


function getDetails(commentId, description) {
    var url = "/WPDEmployee/GetCommentById/",
        data = {
            commentId: commentId
        };

    // clear old data
    $('#Comment_Details').empty();

    // clear title
    $('#detail-title').text('');

    ajaxConnexion(data, url, function (result) {
        printDynamicTableDetails($('#Comment_Details'), result, document.getElementById('detail-title'), description); // HS_script.js  function printDynamicTable(table, object)

    });

    showDetails();
}


function delete_Comment(commentId, commentBy) {
    var tag = $("<div></div>"); //This tag will the hold the dialog content.
    var msg = "";
    
    var url = "/WPDEmployee/DeleteComment/",
        data =
        {
            commentId: commentId,
            commentBy: commentBy

        };

    // start loader


    ajaxConnexion(data, url, function (result) {

        if (result[0].ErrorMessage == 'sucess') {
            msg = 'Comment deleted';
        } else {
            {
                msg = result[0].ErrorMessage;
            }
        }
        // tag.html(msg).dialog({ modal: true }).dialog('open');
        alert(msg);
        $('#closeBtn').click();
        
    });
}

function Update_Comment(commentId, txtcomment, commentBy) {
    var tag = $("<div></div>"); //This tag will the hold the dialog content.
    var msg = "";
    var url = "/WPDEmployee/UpdateComment/",
        data =
        {
            commentId: commentId,
            Comment: txtcomment,
            commentBy: commentBy

        };

    // start loader


    ajaxConnexion(data, url, function (result) {

        if (result[0].ErrorMessage == 'sucess') {
            msg = 'Comment updated';
        } else {
            {
                msg = result[0].ErrorMessage;
            }
        }
        tag.html(msg).dialog({ modal: true }).dialog('open');
        $('#closeBtn').click();
 
    });
}



function printDynamicTableDetails(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // table title row
    var keys = Object.keys(result[0]);
    createDetailsRow(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createDetailsRow(table, result[i]);
    }
}


function createDetailsRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (key=='CommentID') {
            tdClass = 'cl-CommentID';
        }
       if (key == 'Comment') {
                value = '<textarea id="txtComment">' + value + '</textarea>';
                tdClass = 'Cl-Comment';
       }

        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }
        }        
        

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).append(newRow);
}
