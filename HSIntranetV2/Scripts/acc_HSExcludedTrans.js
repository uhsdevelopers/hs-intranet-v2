﻿$(function () {

    $('#btn-submit').click(function () {

        getHSComissions();
    });

});

function getHSComissions() {

    var url = "/Acc_HSExcludedTrans/GetHSExludedTransaction/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');

        if (result.length == 0)
            alert('None excluded transaction');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        // end loader
        loader();

    });

}