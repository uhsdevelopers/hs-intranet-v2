﻿

function SentRequest() {

    var 
        user = document.getElementById('txtuser').value,
        customerId = document.getElementById('txtCustomerid').value;


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (customerId === "") {
        customerId = "X";
    }

    var url;
    var data;

    url = "/Acuity/photoIdOnlineVerificationAsync",
        data = {
            customerId: customerId,
            clerk: user
        };


    // start loader
    loader();

    ajaxConnexionAsync(data, url, function (result) {
        printDynamicTable($('#AccumulatedSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        loader();
    });
}









function ajaxConnexionAsync(data, url, callback) {

    var dataType = "json";
    $.ajax({
        url: url, // Replace with your controller and action names
        type: "GET", // Adjust to POST if your action requires data submission
        data: data,
        dataType: dataType, // Or "html" if you're expecting a view
        success: function (data) {
            if (dataType === "json") {
                // Handle JSON data (e.g., update UI elements)
                $("#result").text(data);
                printDynamicTable($('#AccumulatedSummary'), data, '', '');
                loader();
            } else {
                // Handle HTML data (e.g., replace content)
                $("#content").html(data);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.error("AJAX Error:", textStatus, errorThrown);
            // Handle errors appropriately (e.g., display error messages)
        }
    });
}

