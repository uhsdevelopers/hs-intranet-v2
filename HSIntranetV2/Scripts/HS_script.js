﻿var global_location = (document.domain == 'localhost') ? '' : '/IntranetV2/';

$(function () {

    $(document).on('keydown', '.numeric', function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#datepickerStart").datepicker({
        showOn: "button",
        buttonImage: "/Content/Images/calendar.gif",
        buttonImageOnly: true,
        showOtherMonths: true,
        selectOtherMonths: true
    });

    $("#datepickerEnd").datepicker({
        showOn: "button",
        buttonImage: "/Content/Images/calendar.gif",
        buttonImageOnly: true,
        showOtherMonths: true,
        selectOtherMonths: true
    });

    $("#Incidentdate").datepicker({
        showOn: "button",
        buttonImage: "/Content/Images/calendar.gif",
        buttonImageOnly: true,
        showOtherMonths: true,
        selectOtherMonths: true
    });

    $('#fade, #closeBtn').click(function () {
        showDetails();
    });

    var myDate = new Date();
    var prettyDate = (myDate.getMonth() + 1) + '/' + myDate.getDate() + '/' +
            myDate.getFullYear();
    $("#datepickerStart").val(prettyDate);
    $("#datepickerEnd").val(prettyDate);

});

function ajaxConnexion(data, url, callback) {
    $.ajax({
        type: "GET",
        cache: false,
        url: global_location + url,
        data: data,
        success: function (result) {
            if (callback)
                callback(result);
        },
        dataType: 'json',
        error: function (xhr, ajaxOptions, thrownError) {
            //var err = eval("(" + xhr.responseText + ")");
            //alert(err.Message);
            //alert(xhr.status);
            if (thrownError.length > 0 && thrownError != "Not Found") {
                alert(thrownError);

                $('#fade').hide();
                $('#img-loader').hide();
            }

        }
    });
}

function ajaxConnexionNoReturn(data, url, callback) {
    $.ajax({
        type: "GET",
        cache: false,
        url: global_location + url,
        data: data,
        success: function (result) {
            if (callback)
                callback(result);
        },
        dataType: 'json',

    });
}

function printDynamicList(id, result) {
    $(id).empty();
    // add cust list to select
    for (var i = 0, length = result.length; i !== length; i++) {
        $(id).append('<option>' + result[i].LoginName + '</option>');
    }
}


function printDynamicTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title

    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createRow(table, result[i]);
    }
}


function createHeader(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });

    $(table).children('thead').append(newRow);

}

function createRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Amount') != -1 || key.indexOf('Balance') != -1 || key.indexOf('Fee') != -1 || key.indexOf('Volume') != -1 || key.indexOf('volumen') != -1) {
            if ((/\S/.test(value))) {
                value = formatCurrency(value);
            }
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}

function showDetails() {
    $('#div_Details').toggle();
    $('#fade').toggle();

}

function formatDate(value) {

    var newValue = value.replace(/\D/g, '');

    newValue = parseInt(newValue);

    if (!isNaN(newValue)) {

        var date = new Date(newValue);

        date = date.format('mm/dd/yyyy hh:MM:ss TT');

        return date;
    }

    return '';
}

function loader() {
    $('#fade').toggle();
    $('#img-loader').toggle();
}

function formatCurrency(value) {

    if (value == null) {
        value = 0;
    }
    value = parseFloat(value);

    var money = value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

    return '$' + money;
}

function dateNow() {
    var date = new Date(),
        today,
        month1 = (date.getMonth() + 1),
        month,
        getdate = date.getDate();
    if (getdate < "10") {
        today = "0" + getdate;
    } else {
        today = getdate;
    }
    if (month1 < "10") {
        month = "0" + month1;
    } else {
        month = month1;
    }

    return month + "/" + today + "/" + date.getFullYear();
}

function getCustomersByAgent() {
    var url = '/Home/GetCustomersByAgent/',
        data = {};

    ajaxConnexion(data, url, function (result) {
        if (result.length == 0) return;
        custList(result, function () {
            $('#selectCust').chosen();
        });
    });

}

function getCustomersByAgentWW() {
    var url = '/Home/GetCustomersByAgentWW/',
        data = {};

    ajaxConnexion(data, url, function (result) {
        if (result.length == 0) return;
        custList(result, function () {
            $('#selectCust').chosen();
        });
    });

}

function custList(result, callback) {

    // add cust list to select
    for (var i = 0; i < result.length; i++) {
        $('#selectCust').append('<option>' + result[i].CustomerID + '</option>');
    }

    callback();
}

function printDynamicSelect(select, result, valueNum, textNum, duplicates) {

    // duplicates: false=print only distinct results, true=print all the results

    var value,
        text;

    for (var i = 0; i < result.length; i++) {

        if (duplicates || text != result[i][Object.keys(result[i])[textNum]]) {
            $("<option />", {
                value: result[i][Object.keys(result[i])[valueNum]],
                text: result[i][Object.keys(result[i])[textNum]]
            }).appendTo(select);
        }

        value = result[i][Object.keys(result[i])[valueNum]];
        text = result[i][Object.keys(result[i])[textNum]];
    }
}

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    //CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            if (arrData[i][index] == null) {
                arrData[i][index] = '';
            }
            if (isNaN(arrData[i][index])) {
                if (arrData[i][index].indexOf('/Date') >= 0) {
                    arrData[i][index] = formatDate(arrData[i][index]);
                }
            }
            row += '"' + arrData[i][index] + '",';
        }
        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }

    //this trick will generate a temp "a" tag
    var link = document.createElement("a");
    link.id = "lnkDwnldLnk";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);

    var d = new Date();
    d.setDate(d.getDate() - 1);
    var year = d.getFullYear().toString();
    var month = d.getMonth().toString();
    var day = d.getDate().toString();

    var dateName = year.concat("-", month, "-", day);

    var csv = CSV;
    blob = new Blob([csv], { type: 'text/csv' });
    var csvUrl = window.URL.createObjectURL(blob);
    filename = (ReportTitle == undefined || ReportTitle == "") ? 'UserExport.csv' : dateName + ReportTitle + ".csv";
    $("#lnkDwnldLnk")
    .attr({
        'download': filename,
        'href': csvUrl
    });

    $('#lnkDwnldLnk')[0].click();
    document.body.removeChild(link);

    /*

    //this will remove the blank-spaces from the title and replace it with an underscore
    var fileName = ReportTitle.replace(/ /g, "_") + "_Report";

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);

    */
}


function JSONToTXTConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    //CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + '\t';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            if (arrData[i][index] == null) {
                arrData[i][index] = '';
            }
            if (isNaN(arrData[i][index])) {
                if (arrData[i][index].indexOf('/Date') >= 0) {
                    arrData[i][index] = formatDate(arrData[i][index]);
                }
            }
            row += '' + arrData[i][index] + '\t';
        }
        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }

    //this trick will generate a temp "a" tag
    var link = document.createElement("a");
    link.id = "lnkDwnldLnk";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);

    var d = new Date();
    d.setDate(d.getDate() - 1);
    var year = d.getFullYear().toString();
    var month = d.getMonth().toString();
    var day = d.getDate().toString();

    var dateName = year.concat("-", month, "-", day);

    var csv = CSV;
    blob = new Blob([csv], { type: 'text/csv' });
    var csvUrl = window.URL.createObjectURL(blob);
    filename = (ReportTitle == undefined || ReportTitle == "") ? 'UserExport.csv' : dateName + ReportTitle + ".csv";
    if (ReportTitle == "Header" || ReportTitle == "Lines")
        filename = dateName + " " + ReportTitle + ".txt";
    $("#lnkDwnldLnk")
    .attr({
        'download': filename,
        'href': csvUrl
    });

    $('#lnkDwnldLnk')[0].click();
    document.body.removeChild(link);


}


//define radio buttons, each with a common 'name' and distinct 'id'. 
//       eg- <input type="radio" name="storageGroup" id="localStorage">
//           <input type="radio" name="storageGroup" id="sessionStorage">
//param-sGroupName: 'name' of the group. eg- "storageGroup"
//return: 'id' of the checked radioButton. eg- "localStorage"
//return: can be 'undefined'- be sure to check for that
function checkedRadioBtn(sGroupName) {
    var group = document.getElementsByName(sGroupName);

    for (var i = 0; i < group.length; i++) {
        if (group.item(i).checked) {
            return group.item(i).id;
        } else if (group[0].type !== 'radio') {
            //if you find any in the group not a radio button return null
            return null;
        }
    }
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}