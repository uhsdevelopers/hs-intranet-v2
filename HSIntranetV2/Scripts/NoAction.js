﻿$(function () {
    $("#datepickerStart").datepicker({
        showOn: "button",
        buttonImage: global_location + "Content/Images/calendar.gif",
        buttonImageOnly: true
    });

    $("#datepickerEnd").datepicker({
        showOn: "button",
        buttonImage: global_location + "Content/Images/calendar.gif",
        buttonImageOnly: true
    });

    $("#Incidentdate").datepicker({
        showOn: "button",
        buttonImage: global_location + "Content/Images/calendar.gif",
        buttonImageOnly: true
    });

    $("#tabs").tabs();

    $('#fade, #closeBtn').click(function () {
        showDetails();
    });
    
    $('#ui-id-1,#btnGame').click(function () {

        getGame_List(true, "1");
    });
    

    $('#ui-id-3,#btnAllGames').click(function () {

        getGame_List(true,"0");
    });

    $('#ui-id-2,#btnProps').click(function () {

        getGame_List(false, "0");
    });
    
    $('#btnDelete').click(function () {

        var currentTab = $("#tabs .ui-tabs-panel:visible").attr("id");
        var isgame;
        var isfullGame;
        
        switch(currentTab)
        {
            case "tabs-1":
                isfullGame = "0";
                isgame = true;
                break;
            case "tabs-2":
                isfullGame = "0";
                isgame = false;
                break;
            case "tabs-3":
                isfullGame = "1";
                isgame = true;
                break;
            default:
                isfullGame = "0";
                isgame = true;
        }

        GetSelected(isgame);
        getGame_List(isgame, isfullGame);
    });

    $('.ui-tabs-anchor').click(function () {
        $('#No_ActionList').empty();
    });



    $('#No_ActionList').on('click','#selecctall',function (event) {  //on click 
            if (this.checked) { // check select status
                $('.checkbox1').each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"               
                });
            } else {
                $('.checkbox1').each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });

    });




function GetSelected(isgame)
{
    var games = [];
    $('input:checked').each(function () {
        if ($(this).attr("id") != 'selecctall')
        {
        games.push($(this).attr("id"));
         
        }
        
    });
    console.log(games);
    jQuery.each(games, function (i, val) {
        $("#" + val).text("Mine is " + val + ".");

        // Will stop running after "three"
    
       deleteGame(val, isgame);
    });
}

function getGame_List(isGame, isfullGame) {
    var url = "/NoActionGames/GetGameList/",
        data =
        {
            isGame: isGame,
            isfullGame: isfullGame
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function(result) {
        printDynamicTable2($('#No_ActionList'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
        loader();

    });

}

function deleteGame(gamenum, isaGame) {
    var tag = $("<div></div>"); //This tag will the hold the dialog content.
    var msg = "";
    var url = "/NoActionGames/UpdateGame/";
    var data =
    {
        gamenum: gamenum,
        isGame: isaGame
    };

    // start loader
    msg = isaGame ? "Game: " : "Contest: ";
    
    loader();
    ajaxConnexion(data, url, function(result) {
        
        msg = result[0].ErrorMessage == 'Contest updated' ? msg + gamenum + ' was Closed.' : result[0].ErrorMessage;
        tag.html(msg).dialog({ modal: true }).dialog('open');

        loader();

    });
}

function printDynamicTable2(table, result, idTitle, title) {

        $(table).show();
        $('#btnDelete').show();

        // clear old data
        $(table).empty();

        // print title
        $(idTitle).text(title);

        if (result.length == 0) return;

        // table title row
        var keys = Object.keys(result[0]);
       createRowNA(table, keys);

        for (i = 0, length = result.length; i !== length; i++) {
            createRow2(table, result[i]);
        }
    }

function createRowNA(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Amount') != -1) {
            value = formatCurrency(value);
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });
    $(newRow).append('<td class="' + addClass + '">All <input type="checkbox" name="checkAll" id="selecctall" /></td>');
    $(newRow).append('</tr>');

    $(table).append(newRow);
}


    function createRow2(table, row) {

        var newRow = $('<tr>'),
            addClass = '';

        $.each(row, function (key, value) {
            var tdClass = '';
            if (isNaN(value)) {
                if (value.indexOf('/Date') >= 0) {
                    value = formatDate(value);
                }

            } else if (key.indexOf('Amount') != -1) {
                value = formatCurrency(value);
                addClass = 'align-right';
            } else if (key.indexOf('GameNum') >= 0) {
                tdClass = 'cl-CommentID';
            }

            $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
            
        });
        

        $(newRow).append('<td class="' + addClass + ' "><input type="checkbox" name="delete" class="checkbox1" value="' + row.GameNum + '" id="' + row.GameNum + '"><img src="../../Content/Images/EmptyTrash.png" height="16px" width="16px"></td>');
        $(newRow).append('</tr>');

        $(table).append(newRow);
    }
