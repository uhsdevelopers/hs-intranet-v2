﻿$(function () {
    $("#txt-agentid").keypress(function (event) {
        if (event.which == 13) {
            ValidateGOBCustomer();
        }
    });

    $('#btn-submit').click(function () {
        ValidateGOBCustomer();
    });
    $('#btn-submit1').click(function () {
        GetBets();
    });



    $('#tbl-result').on('click', '.updateFees', function () {
        if (confirm('Are you sure you want to cancel the wager?')) {
            CancelBets();
        }
    });


 

    $('#Result').on('click', '.btnCancel', function () {
        if (confirm('Are you sure you want to cancel the wager?')) {
            CancelGOBBet();
        }
    });

});

function ValidateGOBCustomer() {

    var url = "/GOBet/ValidateGOBCustomer/",
        data = {
            customerID: $('#txt-agentid').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
         $('#Result').show();
        if (result[0].RESULT == 1) {
            $('#Result').html('<span>Customer already got out of his bets</span>');
        } else if (result[0].RESULT == 2) {
            $('#Result').html('<span>Promotion doesn’t exist to this customer</span>');
        }
        else if (result[0].RESULT == 3) {
            $('#Result').html('<span>Customer doesn’t have bets available</span>');
        }
        else if (result[0].RESULT == 4) {
            $('#Result').html('<span>Customer has bets available</span></br><input type="button" class="btnCancel" value="Cancel Bet"/>');
        } else {
            $('#Result').html('<span>Internal Server Error</span>');
        }
        // end loader
        loader();

    });

}



function CancelBets() {


    var ticketNumber = $('#ticketNumber').val();
    var wagerNumber = $('#WagerNumber').val();

    var url = "/GOBet/delete_wager/",
        data = {
            customerID: $('#txt-agentid').val(),
            ticketnumber: ticketNumber,
            wagernumber: wagerNumber
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        $('#Result').show();
        printDynamicTableBets($('#tbl-result'), result, '', '');
        if (result[0].RESULT == 1) {
            $('#Result').append('<br/>Bet has been Canceled');
        } else {
            $('#Result').append('<br/>Error while the bet is being cancelled');
        }



        // end loader
        loader();

    });

}






function GetBets() {

    var url = "/GOBet/GetGobBet/",
        data = {
            customerID: $('#txt-agentid').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        $('#Result').show();
        printDynamicTableBets($('#tbl-result'), result, '', '');
        // end loader
        loader();

    });

}



function createtRow1(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Amount') != -1 || key.indexOf('AmountWagered') != -1 || key.indexOf('Fee') != -1) {
            if ((/\S/.test(value))) {
                value = value / 100;
                value = formatCurrency(value);
            }
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        
        if (key == 'TicketNumber' || key == 'WagerNumber' || key == 'PlayNumber' || key == 'AmountWagered' || key == 'Description') {
            $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>'); 
        }
        
    });
    $(newRow).append('<th><input type="submit" id="btn-cancel" name="btn-cancel" class="updateFees" data-rowid="' + row.TicketNumber + '" Value="Cancel" /> ');
    $(newRow).append('<input type="hidden" name="ticketNumber" id="ticketNumber" Value="' + row.TicketNumber + '" />');
    $(newRow).append('<input type="hidden" name="CustomerID" id="CustomerID" Value="' + row.CustomerID + '" />');
    $(newRow).append('<input type="hidden" name="WagerNumber" id="WagerNumber" Value="' + row.WagerNumber + '" /> </th>');
    $(newRow).append('</tr><tr>');
    
    $(newRow).append('</tr>');
    $(table).children('tbody').append(newRow);
}

function printDynamicTableBets(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys1 = ["TicketNumber", "WagerNumber", "PlayNumber","Wager Amount", "Description"];
   // var keys = Object.keys(result[0]);
    createHeader(table, keys1);

    
    for (i = 0, length = result.length; i !== length; i++) {
        createtRow1(table, result[i]);

    }
   // $(table).append('<tr class="special-tr"><td>Total</td><td></td><td></td><td></td><td class="align-right">' + result[i]['TicketNumber'] + '</td><td class="align-right">' + result[i]['WagerNumber'] + '</td><td class="align-right">' + result[i]['AmountWagered'] + '</td></tr>');

}



function createHeader(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });

    $(table).children('thead').append(newRow);

}








function CancelGOBBet() {

    var url = "/GOBet/CancelGOBBet/",
        data = {
            CustomerID: $('#txt-agentid').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        if (result[0].RESULT == 1) {
            $('#Result').append('<br/>Bet has been Canceled');
        } else {
            $('#Result').append('<br/>Error while the bet is being cancelled');
        }
        // end loader
        loader();

    });

}