﻿$(function () {
    
    $('#btn-submit').click(function () {
        GetDeletedGOYB();
    });
    $('#tbl-result').on('click', '.cancel-btn', function () {
        var $this = $(this),
            id_game = $this.data('gamenum'),
            ticket_number = $this.data('ticketnumber'),
            wager_number = $this.data('wagernumber'),
            to_win_amount = $this.data('towinamount'),
            amount_wagered = $this.data('amountwagered'),
            customer_id = $this.data('customerid'),
            description = $this.data('description');
        CancelGOYB(id_game, ticket_number, wager_number, to_win_amount, amount_wagered, customer_id, description)
    });

});

function GetGOYB() {

    var url = "/CancelGOYB/GetGOYB/",
        data = {
            customerID: $('#customerId').val()
        };
    // start loader
    loader();
    ajaxConnexion(data, url, function (result) {
        $('.description').text('');
        if (result.length == 0) {
            $('.tittle').text('Customer doesn’t have eligible bets');
        } else {
            $('.tittle').text('Eligible Bets');
            printAvailableTable($('#tbl-result'), result, '', '');
        }
        
        // end loader
        loader();
    });
}

function GetDeletedGOYB() {

    var url = "/CancelGOYB/GetDeletedGOYB/",
        data = {
            customerID: $('#customerId').val()
        };
    // start loader
    loader();
    ajaxConnexion(data, url, function (result) {
        if (result.length == 0)
            GetGOYB();
        else
            $('.description').text('Customer already got out of his bets in this promo.');
            $('.tittle').text('Canceled Bet');
            printDynamicTable($('#tbl-result'), result, '', '');
        // end loader
        loader();
    });
}

function CancelGOYB(id_game, ticket_number, wager_number, to_win_amount, amount_wagered, customer_id, description) {
    var url = "/CancelGOYB/CancelGOYB/",
        data = {
            id_game: id_game,
            ticket_number: ticket_number,
            wager_number: wager_number,
            to_win_amount: to_win_amount,
            amount_wagered: amount_wagered,
            customer_id: customer_id,
            description: description
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        GetDeletedGOYB();
        loader();

    });

}

function printAvailableTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createAvailableHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createAvailableRow(table, result[i]);
    }
}

function createAvailableHeader(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        if (value != 'WagerNumber' && value != 'RoundRobinLink' && value != 'FreePlayFlag' && value != 'ARLink' && value != 'ARBirdcageLink' && value != 'BoxLink' && value != 'CustomerID' && value != 'RifBackwardTicketNumber' && value != 'RifBackwardWagerNumber' && value != 'RifWinOnlyFlag' && value != 'PlayNumber' ) {
            $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
            if (value == 'GameNum') {
                $(newRow).append('<th>Action</th>');
            }
        }
    });
   
    $(table).children('thead').append(newRow);

}

function createAvailableRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (key != 'WagerNumber' && key != 'RoundRobinLink' && key != 'FreePlayFlag' && key != 'ARLink' && key != 'ARBirdcageLink' && key != 'BoxLink' && key != 'CustomerID' && key != 'RifBackwardTicketNumber' && key != 'RifBackwardWagerNumber' && key != 'RifWinOnlyFlag' && key != 'PlayNumber' ) {
            if (isNaN(value)) {
                if (value.indexOf('/Date') >= 0) {
                    value = formatDate(value);
                }

            } else if (key.indexOf('Amount') != -1 || key.indexOf('Balance') != -1 || key.indexOf('Fee') != -1) {
                if ((/\S/.test(value))) {
                    value = formatCurrency(value/100);
                }
                addClass = 'align-right';
            } else if (key.indexOf('CommentID') >= 0) {
                tdClass = 'cl-CommentID';
            }
            if (key == 'TicketNumber') {
                value = value + '-' + row.WagerNumber;
            }
            $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
            if (key == 'GameNum')
            {
                $(newRow).append('<td class="' + addClass + ' ' + tdClass + '"><input type="button" value="Cancel" class="cancel-btn" data-customerid="' + row.CustomerID + '" data-gamenum="' + row.GameNum + '" data-wagernumber="' + row.WagerNumber + '" data-amountwagered="' + row.AmountWagered + '" data-towinamount="' + row.ToWinAmount + '" data-description="' + row.Description + '" data-ticketnumber="' + row.TicketNumber + '"/></td>');
            }

            
        }
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}

