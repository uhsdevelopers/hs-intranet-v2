﻿$(function () {
    
    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');

        GetAccumWagerLimitByLine();
    });

    $('#btn-saveLineMultiplier').click(function () {

        SetAccumWagerLimitByLine();
    });

    
   

});

function SetAccumWagerLimitByLine() {

    var url = "/AccumWagerLimitByLine/SetCustomerAccumWager/",
        data = {

            customerID: $('#Customer').val(),
            lineMultiplier: $('#LineMultiplier').val()

        };
    if ($('#Customer').val() == '')
        return false;

    $('#LineMultiplier').val('');

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        loader();

        $('#LineMultiplier').val(result[0].AccumWagerLimitByLineMultiplier);
        
        alert('Line Multiplier has been saved.');


    });

}

function GetAccumWagerLimitByLine() {

    var url = "/AccumWagerLimitByLine/GetCustomerAccumWager/",
        data = {
            
            customerID: $('#Customer').val()
          
    };
    if($('#Customer').val() == '' )
        return false;

    $('#LineMultiplier').val('');

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        loader();
        //printDynamicTableAccum($('#tbl-result'), result, '', '');
        $('#LineMultiplier').val(result[0].AccumWagerLimitByLineMultiplier);
        
        $('#span-totalPlayers').text(result.length);

        // end loader
       

    });

}

function printDynamicTableAccum(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeaderInet(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createRowAccum(table, result[i]);
    }
}

function createHeaderInet(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        if (value != 'CounterCorrelated')
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });

    $(table).children('thead').append(newRow);

}

function createRowAccum(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        if (key == 'CounterCorrelated')
            return false;

        var tdClass = '';
        if (key == 'IPAddress' && row.CounterCorrelated > 0 && value != 'HSTW') {
            value = '<a href="#" class="summaryIp" data-ip="' + value + '" data-customer="'+row.LoginID+ '">' + value + '</a>';
        }
        else if (isNaN(value) && key == 'AccessDateTime') {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        }  

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}