﻿$(function () {

    $('#btn-submit').click(function () {

        getPPHStatement();
    });

});

function getPPHStatement() {

    var url = "/Acc_PPHStatement/GetPPHStatement/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');

        // end loader
        loader();

    });

}