﻿$(function () {
    WWAffiliate_GetMondayToProcess();
    $('#btn-submit').click(function () {
        
        if (confirm("It will charge for the week previous! Are you sure?"))
        {
            getAcc_WWAffiliateProcessFee();
        }
        
    });

});

function getAcc_WWAffiliateProcessFee() {

    var url = "/Acc_WWAffiliateProcessFee/GetAcc_WWAffiliateProcessFee/",
        data = {
            datetocharge: $('#datetocharge').val()
        };
    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        $('#errorMessage').text(result[0].RESULT);

        loader();

    });

}
function WWAffiliate_GetMondayToProcess() {

    var url = "/Acc_WWAffiliateProcessFee/WWAffiliate_GetMondayToProcess/",
        data = {};

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        var fecha = formatDate(result[0].MondayToProcess).split(' ')[0];
        fecha = new Date(fecha);
        fecha.setDate(fecha.getDate() + 6);
        $('#errordate').text('It will charge the week from ' + formatDate(result[0].MondayToProcess).split(' ')[0] + " to " + fecha.format('mm/dd/yyyy'));
        $('#datetocharge').val(formatDate(result[0].MondayToProcess));
        loader();

    });

}


 function sumaFecha(d, fecha) {
    var Fecha = new Date();
    var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() + 1) + "/" + Fecha.getFullYear());
    var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
    var aFecha = sFecha.split(sep);
    var fecha = aFecha[2] + '/' + aFecha[1] + '/' + aFecha[0];
    fecha = new Date(fecha);
    fecha.setDate(fecha.getDate() + parseInt(d));
    var anno = fecha.getFullYear();
    var mes = fecha.getMonth() + 1;
    var dia = fecha.getDate();
    mes = (mes < 10) ? ("0" + mes) : mes;
    dia = (dia < 10) ? ("0" + dia) : dia;
    var fechaFinal = dia + sep + mes + sep + anno;
    return (fechaFinal);
}