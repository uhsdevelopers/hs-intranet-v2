﻿$(function () {
    
    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0'); 
    });
    GetFeesMethod(null);

    $('.table_Result').on('change paste keyup', ':input', function() {
        $(this).closest('tr').find('.updateFees').prop("disabled", false);
        $(this).closest('tr').find('.updateFees').css('background-color', 'rgb(106, 152, 250)');
    });

    $('#getInfoToExcel').on('click', function () {
     
        id = $('#TranType').val()

        var url = "/FeesMaintenance/GetFeesMethod",
            data = {
                TranType: id
            };

        // start loader
        

        ajaxConnexion(data, url, function (result) {
            JSONToCSVConvertor(result, "Fees Methods", true);
        });
        
    });

    $('.table_Result').on('click', '.deleteFees', function () {

        var r = confirm("You are deleting FEE ID: " + $(this).data('rowid'));
        if (r == true) {
            DeleteFees(this);
        } else {
            
        }
        
    });

    $('.table_Result').on('click', '.updateFees', function () {

        var url = "/FeesMaintenance/UpdateFeesMethod",
            $this = $(this),
            $parent = $this.closest('tr'),
            id = $this.data('rowid'),
            data = {
                ID: id,
                TranType: $parent.find('.TranDesc option:selected').val(),
                TranDesc: $parent.find('.TranDesc option:selected').text(),
                Method: $parent.find('.Method').val(),
                CRMMethod: $parent.find('.CRMMethod').val(),
                Strt: $parent.find('.Strt').val(),
                ENDD: $parent.find('.ENDD').val(),
                RTE: $parent.find('.RTE').val(),
                PERCNTAGE: $parent.find('.PERCNTAGE').val(),
                MIXMODE: ($parent.find('.true_MIXMODE').is(':checked')) ? 'true' : 'false',
                PREAPPROVED: ($parent.find('.true_PREAPPROVED').is(':checked')) ? 'true' : 'false'
            };


        ajaxConnexion(data, url, function (result) {
            GetFeesMethod(id);
        });

    });

    $('.table_Result').on('click', '.newFees', function () {

        var url = "/FeesMaintenance/InsertFeesMethod",
            $this = $(this),
            $parent = $this.closest('tr'),
            data = {
                TranType: $parent.find('.TranDesc option:selected').val(),
                TranDesc: $parent.find('.TranDesc option:selected').text(),
                Method: $parent.find('.Method').val(),
                CRMMethod: $parent.find('.CRMMethod').val(),
                Strt: $parent.find('.Strt').val(),
                ENDD: $parent.find('.ENDD').val(),
                RTE: $parent.find('.RTE').val(),
                PERCNTAGE: $parent.find('.PERCNTAGE').val(),
                MIXMODE: ($parent.find('.true_MIXMODE').is(':checked')) ? 'true' : 'false',
                PREAPPROVED: ($parent.find('.true_PREAPPROVED').is(':checked')) ? 'true' : 'false'
            };


        ajaxConnexion(data, url, function (result) {
            if (result[0].ErrorNumber != undefined && result[0].ErrorNumber != 0)
                alert(result[0].ErrorMessage);
            else
                GetFeesMethod(0);
        });
    });

});

function DeleteFees(row) {

    var url = "/FeesMaintenance/DeleteFees",
            id = $(row).data('rowid'),
            data = {
                ID: id
            };

    ajaxConnexion(data, url, function (result) {
        GetFeesMethod(id);
    });

}

function GetFeesMethod() {

    id = $('#TranType').val()

    var url = "/FeesMaintenance/GetFeesMethod",
        data = {
            TranType: id
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printFeesTable($('#tbl-result'), result, '', '');

        if (result.length == 0) 
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        // end loader
        loader();
        if (id != null && id != 0 && id != '') {
            $('#row_' + id).closest('tr').css('background-color', 'rgb(194, 249, 190)');
            $('#row_' + id).closest('tr').find('.updateFees').focus();
        } else if (id != null && id === 0) {
            id = result.length+1;
            $('#row_' + id).closest('tr').css('background-color', 'rgb(194, 249, 190)');
            $('#row_' + id).closest('tr').find('.TranDesc').focus();
        }


    });

}

function printFeesTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createFeesHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createFeesRow(table, result[i],i);
    }
}

function createFeesHeader(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        if (value == 'TranType') {
            $(newRow).append('<th style="display:none">' + value + '</th>');
        } else if (value == 'TranDesc') {
            $(newRow).append('<th class="' + addClass + '">TranType</th>');
        }
        else {
            $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
        }
        
    });
    $(newRow).append('<th>Action</th>');
    $(newRow).append('<th>Delete</th>');

    $(table).children('thead').append(newRow);

}

function createFeesRow(table, row,i) {

    var newRow = $('<tr>'),
        addClass = '';
    if (i == 0) {
        $(newRow).append('<td>--</td><td style="display:none"><input class="TranType" size="3"/></td><td><select class="TranDesc"><option value="B">Promotional Credit</option><option value="N">Promotional Debit</option><option value="C">Credit Adjustment</option><option value="D">Debit Adjustment</option><option value="E">Deposit</option><option value="I">Withdrawal</option><option value="F">Fees Credit</option><option value="H">Fees Debit</option><option value="T">Transfer Credit</option><option value="U">Transfer Debit</option><option value="Z">Zero Balance</option></select></td><td><input class="Method"/></td><td><input class="CRMMethod" style="width: 400px"/></td><td><input class="Strt" style="width: 50px"/></td><td><input class="ENDD" style="width: 100px"/></td><td><input class="RTE" style="width: 50px"/></td><td><input class="PERCNTAGE" style="width: 50px"/></td><td><input type="radio" value="true" name="MIXMODE" class="true_MIXMODE"/>True <input type="radio" value="false" name="MIXMODE" class="false_MIXMODE"/>False</td><td><input type="radio" value="true" name="PREAPPROVED" class="true_PREAPPROVED"/>True <br><input type="radio" value="false" name="PREAPPROVED" class="false_PREAPPROVED"/>False</td><td><input type="button" class="newFees" Value="New"/> </td><td></td>');
        $(newRow).append('</tr>');
        $(table).children('tbody').append(newRow);
        newRow = $('<tr>');
    }
   
    $.each(row, function (key, value) {
        if (value == null) {
            value = '';
        }
        var tdClass = '';
        if (key.indexOf('Amount') != -1 || key.indexOf('Balance') != -1 || key.indexOf('Fee') != -1) {
            if ((/\S/.test(value))) {
                value ='<input class="'+key+'" value="'+ formatCurrency(value)+'"/>';
            }
            addClass = 'align-right';
        }
        else if (key == "ID") {
            value = '<span id="row_'+value+'">'+value+'</span>';

        }
        else if (key.indexOf('MIXMODE') != -1 || key.indexOf('PREAPPROVED') != -1) {
            var is_true = (value == true) ? "checked" : "";
            var is_false = (value == false) ? "checked" : "";
            value = '<input type="radio" name="' + i + key + '" class="true_' + key + '" value="true" ' + is_true + '/>True<br> <input type="radio" name="' + i + key + '" class="false_' + key + '" value="false"' + is_false + '/>False';

        } else if (key=='TranType') {
            value = '<input class="' + key + '" value="' + value + '" size="3"/>';
        }
        else if (key == 'TranDesc') {
            if (value == 'Credit Adjustment') {
                value = '<select class="TranDesc"><option value="B">Promotional Credit</option><option value="N">Promotional Debit</option><option value="C" selected>Credit Adjustment</option><option value="D">Debit Adjustment</option><option value="E">Deposit</option><option value="I">Withdrawal</option><option value="F">Fees Credit</option><option value="H">Fees Debit</option><option value="T">Transfer Credit</option><option value="U">Transfer Debit</option><option value="Z">Zero Balance</option></select>';
            }else if (value == 'Debit Adjustment') {
                value = '<select class="TranDesc"><option value="B">Promotional Credit</option><option value="N">Promotional Debit</option><option value="C">Credit Adjustment</option><option value="D" selected>Debit Adjustment</option><option value="E">Deposit</option><option value="I">Withdrawal</option><option value="F">Fees Credit</option><option value="H">Fees Debit</option><option value="T">Transfer Credit</option><option value="U">Transfer Debit</option><option value="Z">Zero Balance</option></select>';
            }else if (value == 'Deposit') {
                value = '<select class="TranDesc"><option value="B">Promotional Credit</option><option value="N">Promotional Debit</option><option value="C">Credit Adjustment</option><option value="D">Debit Adjustment</option><option value="E" selected>Deposit</option><option value="I">Withdrawal</option><option value="F">Fees Credit</option><option value="H">Fees Debit</option><option value="T">Transfer Credit</option><option value="U">Transfer Debit</option><option value="Z">Zero Balance</option></select>';
            } else if (value == 'Withdrawal') {
                value = '<select class="TranDesc"><option value="B">Promotional Credit</option><option value="N">Promotional Debit</option><option value="C">Credit Adjustment</option><option value="D">Debit Adjustment</option><option value="E">Deposit</option><option value="I" selected>Withdrawal</option><option value="F">Fees Credit</option><option value="H">Fees Debit</option><option value="T">Transfer Credit</option><option value="U">Transfer Debit</option><option value="Z">Zero Balance</option></select>';
            } else if (value == 'Fees Debit') {
                value = '<select class="TranDesc"><option value="B">Promotional Credit</option><option value="N">Promotional Debit</option><option value="C">Credit Adjustment</option><option value="D">Debit Adjustment</option><option value="E">Deposit</option><option value="I">Withdrawal</option><option value="F">Fees Credit</option><option value="H" selected>Fees Debit</option><option value="T">Transfer Credit</option><option value="U">Transfer Debit</option><option value="Z">Zero Balance</option></select>';
            } else if (value == 'Transfer Credit') {
                value = '<select class="TranDesc"><option value="B">Promotional Credit</option><option value="N">Promotional Debit</option><option value="C">Credit Adjustment</option><option value="D">Debit Adjustment</option><option value="E">Deposit</option><option value="I">Withdrawal</option><option value="F">Fees Credit</option><option value="H">Fees Debit</option><option value="T" selected>Transfer Credit</option><option value="U">Transfer Debit</option><option value="Z">Zero Balance</option></select>';
            } else if (value == 'Promotional Credit') {
                value = '<select class="TranDesc"><option value="B" selected>Promotional Credit</option><option value="N">Promotional Debit</option><option value="C">Credit Adjustment</option><option value="D">Debit Adjustment</option><option value="E">Deposit</option><option value="I">Withdrawal</option><option value="F">Fees Credit</option><option value="H">Fees Debit</option><option value="T">Transfer Credit</option><option value="U">Transfer Debit</option><option value="Z">Zero Balance</option></select>';
            } else if (value == 'Promotional Debit') {
                value = '<select class="TranDesc"><option value="B">Promotional Credit</option><option value="N" selected>Promotional Debit</option><option value="C">Credit Adjustment</option><option value="D">Debit Adjustment</option><option value="E">Deposit</option><option value="I">Withdrawal</option><option value="F">Fees Credit</option><option value="H">Fees Debit</option><option value="T">Transfer Credit</option><option value="U">Transfer Debit</option><option value="Z">Zero Balance</option></select>';
            } else if (value == 'Fees Credit') {
                value = '<select class="TranDesc"><option value="B">Promotional Credit</option><option value="N">Promotional Debit</option><option value="C">Credit Adjustment</option><option value="D">Debit Adjustment</option><option value="E">Deposit</option><option value="I">Withdrawal</option><option value="F" selected>Fees Credit</option><option value="H">Fees Debit</option><option value="T">Transfer Credit</option><option value="U">Transfer Debit</option><option value="Z">Zero Balance</option></select>';
            } else if (value == 'Zero Balance') {
                value = '<select class="TranDesc"><option value="B">Promotional Credit</option><option value="N">Promotional Debit</option><option value="C">Credit Adjustment</option><option value="D">Debit Adjustment</option><option value="E">Deposit</option><option value="I">Withdrawal</option><option value="F">Fees Credit</option><option value="H">Fees Debit</option><option value="T">Transfer Credit</option><option value="U">Transfer Debit</option><option value="Z" selected>Zero Balance</option></select>';
            } else if (value == 'Transfer Debit') {
                value = '<select class="TranDesc"><option value="B">Promotional Credit</option><option value="N">Promotional Debit</option><option value="C">Credit Adjustment</option><option value="D">Debit Adjustment</option><option value="E">Deposit</option><option value="I">Withdrawal</option><option value="F">Fees Credit</option><option value="H">Fees Debit</option><option value="T">Transfer Credit</option><option value="U" selected>Transfer Debit</option><option value="Z">Zero Balance</option></select>';

            }
            else {
                value = '<select class="TranDesc"><option value="B">Promotional Credit</option><option value="N">Promotional Debit</option><option value="C">Credit Adjustment</option><option value="D">Debit Adjustment</option><option value="E">Deposit</option><option value="I">Withdrawal</option><option value="F">Fees Credit</option><option value="H">Fees Debit</option><option value="T">Transfer Credit</option><option value="U">Transfer Debit</option><option value="Z">Zero Balance</option></select>';
            }



        } else if (key == 'CRMMethod') {
            value = '<input class="' + key + '" value="' + value + '" style="width: 400px"/>';
        }
        else if (key == 'Strt' || key == 'RTE' || key == 'PERCNTAGE') {
            value = '<input class="' + key + '" value="' + value + '" style="width: 50px"/>';
        } else if (key == 'ENDD' ) {
            value = '<input class="' + key + '" value="' + value + '" style="width: 100px"/>';
        } else {
            value = '<input class="' + key + '" value="' + value + '"/>';
        }

        if (key == 'TranType') {
            $(newRow).append('<td style="display:none">' + value + '</td>');
        } else {
            $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
        }

        
    });
    $(newRow).append('<td><input type="button" class="updateFees" data-rowid="' + row.ID + '" Value="Update" disabled/> </td>');
    $(newRow).append('<td><input type="button" class="deleteFees" data-rowid="' + row.ID + '" Value="Delete" /> </td>');
    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}
