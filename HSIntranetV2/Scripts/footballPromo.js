﻿$(function () {
    getPromo();
});

function getPromo() {

    var customerID = document.getElementById('customerId').value;
    var freebetAmount = document.getElementById('freebetAmount').value;

    //if (customerID == '') {
    //    alert('Please select at least one filter.');
    //    return;
    //}

    // summary
    var url = "/FootballPromo/GetResult/",
        data = {
            customerID: customerID,
            freebetAmount: freebetAmount
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-return'), result, '', '');
        //printPromo(result);

        // end loader
        loader();
    });

}


function printPromo(result) {

    var gameNum = 0,
        id = "",
        table1,
        table2,
        gradedTime,
        divContainer;

    // clear old data
    $('#div_Result').empty();

    if (result.length == 0) return;

    for (i = 0, length = result.length; i !== length; i++) {

        if (result[i].GameNum != gameNum) {
            gameNum = result[i].GameNum;
            if (i != 0) {
                // add table to div Result
                $(divContainer).append(table1).append(table2);
                $('#div_Result').append(title).append(divContainer);
            }

            title = $('<h3>' + gameNum + ' ' + result[i].SportDescription + '. ' + result[i].Team1ID + ' vs ' + result[i].Team2ID + ' for ' + result[i].PeriodDesc + '</h3>');
            divContainer = $('<div class="clear"></div>');

            table1 = $('<table class="table_Result float-left"><tr><th>GradedTime</th><th>' + result[i].Team1ID + '</th><th>' + result[i].Team2ID + '</th></tr></table>');
            table2 = $('<table class="table_Result"><tr><th>Ticket #</th><th>Risk</th><th>Outcome</th></tr></table>');
        }

        if (gradedTime != result[i].GradedTime) {
            gradedTime = result[i].GradedTime;
            $(table1).append('<tr><td>' + formatDate(gradedTime) + '</td><td>' + result[i].Team1Score + '</td><td>' + result[i].Team2Score + '</td></tr>');
        }

        $(table2).append('<tr><td>' + result[i].TicketNumber + '</td><td>' + result[i].AmountWagered + '</td><td>' + result[i].Outcome + '</td></tr>');
        
    }

    // add last table to div Result
    $(divContainer).append(table1).append(table2);
    $('#div_Result').append(title).append(divContainer);
}
