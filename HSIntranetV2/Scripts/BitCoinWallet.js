﻿$(function () {
    
    $(function () {
      
      
        $(document).keyup(function (e) {
            if (e.keyCode == 13) {
                $('#getCardValidation').click();
            }
        });
        $('#GetBitCoinWallet').click(function () {
            GetBitCoinWallet();
        });
        $('#tbl-result').on('change', '._status', function () {
            UpdateBitCoinWallet($($(this).closest('td')).data('id'), $(this).val());
        });

    });

});

function GetBitCoinWallet() {

    var startDate = document.getElementById('datepickerStart').value,
    endDate = document.getElementById('datepickerEnd').value,
    site = checkedRadioBtn('TType');

    var url = "/BitCoinWallet/GetBitCoinWallet/",
        data = {
            startDate: startDate,
            endDate: endDate,
            status: $('#status').val(),
            SportsbookID: $('#site').val()

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDinamicWalletTable($('#tbl-result'), result, '', '');

        if (result.length == 0) 
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        // end loader
        loader();

    });

}

function UpdateBitCoinWallet(id,status) {

    var url = "/BitCoinWallet/UpdateBitCoinWallet/",
        data = {
            id: id,
            userInCharge: $('#clerk').val(),
            status: status
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        loader();
        GetBitCoinWallet();
    });

}

function createWalletRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                if (value == "null" || value == "NULL" || value == 0) {
                    value = '---';
                } else {
                    value = formatDate(value);
                }
            }
        } 
        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
        if (key == 'DateLastChange') {
            if (row.Status == 'P') {
                $(newRow).append('<td data-id="' +row.ID+ '" class="' + addClass + ' ' + tdClass + '"> <select class="_status"><option value="P" selected="selected">Pending</option><option value="I">InProgress</option><option value="A">Approved</option></select>');
            }else if (row.Status == 'I') {
                $(newRow).append('<td data-id="' +row.ID+ '" class="' + addClass + ' ' + tdClass + '"> <select class="_status"><option value="P">Pending</option><option value="I" selected="selected">InProgress</option><option value="A">Approved</option></select>');
            } else if (row.Status == 'A') {
                $(newRow).append('<td data-id="' + row.ID + '" class="' + addClass + ' ' + tdClass + '"> <select class="_status"><option value="P">Pending</option><option value="I">InProgress</option><option value="A" selected="selected">Approved</option></select>');
            }
        }
    });

    $(newRow).append('</tr>');

    $(table).append(newRow);
}

function printDinamicWalletTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) {
        $('#displayreport').append("<h3>No results found</h3>");
        return;
    }

    // table title row
    var keys = Object.keys(result[0]);
    createRow(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createWalletRow(table, result[i]);
    }
}

