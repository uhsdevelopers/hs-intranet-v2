﻿

function getSMSLIST() {


    var IsActive = $('.messageCheckbox:checked').val();
    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/support/GetSMSMessageList",
        data = {
            IsActive: IsActive
        };

    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#SMSSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        loader();
    });
}