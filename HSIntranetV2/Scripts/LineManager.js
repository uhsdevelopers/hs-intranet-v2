﻿$(function () {

    $('#btn_asicarryover').click(function () {

        fixASIBalance();

    });

    $('#btn_dgsstuck').click(function () {

        getDGSreleaseGrader();

    });

    $('#btn_dgssynch').click(function () {

        fixDGSBalance();

    });

});

function fixASIBalance() {

    var url = "/LineManager/FixASIBalance/",
        data = {
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        // end loader
        loader();

        if (result.length == 0) return;

        var releaseResult = result[0].Result + " account(s) fixed.";

        $('#result_asicarryover').text(releaseResult);

       

    });

}

function getDGSreleaseGrader() {

    var url = "/LineManager/GetDGSreleaseGrader/",
        data = {
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

       

        if (result.length == 0) return;

        printDynamicTable($('#tbl-result-dgsGrader'), result, '', '');

        // end loader
        loader();

    });

}


function getImporterLogs() {

    var teamRot1 = $('#RotNumber').val();

    var url = "/LineManager/GetImporterLog/",
        data = {
            teamRot1: teamRot1
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {



        if (result.length == 0) return;

        printDynamicTable($('#ImporterSummary'), result, '', '');


    });

}



function fixDGSBalance() {

    var url = "/LineManager/FixDGSBalance/",
        data = {
        };


    var releaseResult = "This will take up to one minute to complete.";

    $('#result_dgssynch').text(releaseResult);

    ajaxConnexion(data, url, function (result) {

        $('#result_dgssynch').text('Done.');

    });

}