﻿$(function () {
        
    $('#btn-submit').click(function () {

        getContestWagersByStore();

    });

    $('#getInfoToExcel').click(function () {

        var startDate = document.getElementById('datepickerStart').value,
          endDate = document.getElementById('datepickerEnd').value,
          store = checkedRadioBtn('WType');

        if (startDate == '') {
            alert('Please select at least one filter.');
            return;
        }

        var url;
        var data;

        url = "/ContestWagersByStore/GetContestWagersByStore",
    data = {
        dateFrom: startDate,
        dateTo: endDate,
        store: store

    };

        ajaxConnexion(data, url, function (result) {
            JSONToCSVConvertor(result, "Details", true);
        });

    });

});


function getContestWagersByStore() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value,
        store = checkedRadioBtn('WType');


    if (startDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    var url;
    var data;

    url = "/ContestWagersByStore/GetContestWagersByStore",
    data = {
        dateFrom: startDate,
        dateTo: endDate,
        store: store

    };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#tbl-result'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        $('#tbl-result').tablesorter();

        // end loader
        loader();
    });
}