﻿$(function () {
    $('#save').on('click', function () {
        InsertTableInfo();
    });
    hrGet();
    $('#getInfoToExcel').on('click', function() {
        hrExtpor();
    });
});
    function hrGet() {
        var url = "/HR_BankForm/GetForm/",
            data = {
               
            };
        loader();
        ajaxConnexion(data, url, function (result) {
            if (!result.length) {
                loader();
                return;
            }
            printDynamicTable($('#tbl-result'), result, '', '');
            loader();
        });
    }

    function hrExtpor() {
        var url = "/HR_BankForm/GetForm/",
            data = {

            };
        loader();
        ajaxConnexion(data, url, function (result) {
            if (!result.length) {
                loader();
                return;
            }
            for (var x = 0; result.length > x; x++) {
                result[x].ExpirationIDDate = formatDate(result[x].ExpirationIDDate);
            }
            JSONToCSVConvertor(result, "BRC List", true);
            loader();
        });
    }

function InsertTableInfo() {
    var filds = $('.employee-form').find('input:not([type="submit"])'),
        data = {},
        url = "/HR_BankForm/InsertForm/";
    for (var i = 0; filds.length > i; i++) {
        data[$(filds[i]).attr('id')] = $(filds[i]).val();
    }
    ajaxConnexion(data, url, function (result) {
        if (result.length != 0) {
            if (result[0].error == "0") {
                filds.val('');
                alert("Your Information has been Saved");
            }
            else {
                alert(result[0].error);
            }
        }
    });
};

