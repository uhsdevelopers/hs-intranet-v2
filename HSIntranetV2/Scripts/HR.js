﻿$(function () {


});

function getData() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/FillReports",
       data = {
           startDate: startDate + ' 12:00 AM',
           endDate: endDate + ' 11:59 PM'
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        alert(result);



        // end loader
        loader();
    });
}


function getDataDaily() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/FillReportsph",
       data = {
           startDate: startDate + ' 12:00 AM',
           endDate: endDate + ' 11:59 PM'
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}

function getCasinoData() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetCasinoData",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}

function getCasinoProfitCSD() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/CSD/GetcasinoGeneralProfit",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)


        $('#CasinoSummary').tablesorter();


        // end loader
        loader();
    });
}

function getCasinototalCSD() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/CSD/GetcasinoGeneralFinal",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)


        $('#CasinoSummary').tablesorter();


        // end loader
        loader();
    });
}

function getCasinoProfitCSDExcel() {


    $("#TransferSummary").hide();


    var url;
    var data;

    url = "/CSD/GetcasinoGeneralProfit",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        JSONToCSVConvertor(result, "CasinoDetails", true);


        // end loader
        loader();
    });
}

function getCasinoTotalCSDExcel() {


    $("#TransferSummary").hide();


    var url;
    var data;

    url = "/CSD/GetcasinoGeneralFinal",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        JSONToCSVConvertor(result, "CasinoDetails", true);


        // end loader
        loader();
    });
}

function GetHRExcel() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;
    var file;

    // file = document.getElementById('file').value;

    url = "/HR/GetHRExcel",
       data = {
       };





    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#LDSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)


        //alert(JSON.stringify(result));




        // end loader

    });
}


function getLiveDealer() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetLiveDealerData",
       data = {
       };





    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#LDSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader

    });
}


function getLiveDealerDaily() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetLiveDealerData",
       data = {
       };





    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#LDSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader

    });
}


function getCasinoProfitData() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetCasinoProfitTotal",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoProfitSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}


function getCasinoBlock() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/CSD/GetCasinoBlock",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoProfitSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        $('#CasinoProfitSummary').tablesorter();



        // end loader
        loader();
    });
}


function getAccumulated() {


    var startDate = document.getElementById('Startdate').value,
      endDate = document.getElementById('Enddate').value,
      customerId = document.getElementById('txtCustomerid').value;


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    var url;
    var data;

    url = "/csd/GetAccumulatedCustomerRisk",
       data = {
           customerId: customerId,
           startDate: startDate,
           endDate: endDate
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#AccumulatedSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}