﻿$(document).ready(function () {

    var status = "";
    var Reason = "";
    var custid = $(custId).val();
    $('mySelect').hide();
    $("#Pause").click(function () {
        status = "Pause";
        $("#mySelect").show();
    });

    $("#Login").click(function () {

        status = "Login";
        Reason = "Login";
        
        SaveStatus(status, Reason, custid);
        $(this).attr('disabled', true);
        $('#Logout').removeAttr('disabled');
        GetTodayData(custid);
    });

    $("#Logout").click(function () {
        status = "Logout";
        Reason = "Logout";
        SaveStatus(status, Reason, custid);
        $('#Login').removeAttr('disabled');
        $(this).attr('disabled', true);
        GetTodayData(custid);
    });

    $("#Reason").change(function () {

        status = "Pause";
        Reason = $(this).val();
        SaveStatus(status, Reason, custid);
        alert("You have selected " + Reason + ".");
        GetTodayData(custid);
    });

    $('#AgentStats-submit').click(function () {
        GetAgentStats();
    });

    $('#TeamStats-submit').click(function () {
        GetTeamStats();
    });

    $('#ActiveCallas-submit').click(function () {
        GetTeamsActiveCalls();
    });

   

});


function GetTodayData(user) {


    var url = "/Genius/GetTodayData/",
        data = {
            user: user
        };
    loader();
    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#UserSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
        loader();
    });
    $("#UserData").show();
}


function SaveStatus(status,reason, custid) {


    var url = "/Genius/SaveStatus/",
        data = {
            status: status,
            reason: reason,
            usr: custid
        };
    loader();
    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#tbl-result'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
        loader();
    });
}

function GetUserDetails(custid) {


    var url = "/Genius/GetUserDetails/",
        data = {
            user: custid
        };
  
    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#tbl-result'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
       
    });
}

function GetAgentStats() {
    var url = "/Genius/GetTeamsAgentStats/",
        data = {
            companyid: "1"
        };
    loader();
    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#tbl-result'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
        loader();
    });
}

function GetTeamStats() {
    var url = "/Genius/GetTeamsStats/",
        data = {
            companyid: "1"
        };
    loader();
    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#tbl-result'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
        loader();
    });
}

function GetTeamsActiveCalls() {
    var url = "/Genius/GetTeamsActiveCalls/",
        data = {
            companyid: "1"
        };
    loader();
    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#tbl-result'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
        loader();
    });
}

