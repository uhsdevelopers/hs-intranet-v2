﻿$(function () {
    $('#CustomerId').on('keyup', function (e) {
        if (e.which == 13) {
            var cust = $('#CustomerId').val();
            GetCustomerInfo(cust);
            GetCustomerBonus(cust);
        }
    });
    $('#btn-submit').click(function () {
        var cust = $('#CustomerId').val();
        GetCustomerInfo(cust);
        GetCustomerBonus(cust);
    });
    $('#tbl-result').on('click', '#btn-update', function () {
        SetCustomerInfo();
    });
});

function GetCustomerInfo(cust) {

    var url = "/BonusSettings/GetCustomerInfo/",
        data = {
            Customerid: cust

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicCustomerTable($('#tbl-result'), result, '', '');

        if (result.length == 0) 
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        // end loader
        loader();

    });

}

function GetCustomerBonus(cust) {

    var url = "/BonusSettings/GetCustomerBonus/",
        data = {
            Customerid: cust

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result-bonus'), result, '', '');

        if (result.length == 0)
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        // end loader
        loader();

    });

}

function SetCustomerInfo() {

    var url = "/BonusSettings/SetCustomerInfo/",
        cust = $('.CustomerID').text();
        data = {
            Customerid: cust,
            BonusExclusion: $('.BonusExclusion').prop('checked'),
            UseAGreaterBonus: $('.UseAGreaterBonus').prop('checked')
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        GetCustomerInfo(cust);

        loader();

    });

}
function printDynamicCustomerTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createCustomerHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createCustomerRow(table, result[i]);
    }
}
function createCustomerHeader(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        if (value == "CustomerID" || value == "ApplyAllElegibleDeposit" || value == "BonusExclusion" || value == "UseAGreaterBonus")
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });
    $(newRow).append('<th class="' + addClass + '">Action</th>');
    $(table).children('thead').append(newRow);

}

function createCustomerRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (key == "CustomerID" ) {  
            addClass = 'align-right';
            $(newRow).append('<td class="' + key + '">' + value + '</td>');
        } else if (key == "BonusExclusion" || key == "UseAGreaterBonus") {
            $(newRow).append('<td><input type="checkbox" class="' + key + '"' + ((value == true) ? "checked" : "") + '></td>');
        } else if (key == "ApplyAllElegibleDeposit") {
            $(newRow).append('<td><input type="checkbox" disabled class="' + key + '"' + ((value == true) ? "checked" : "") + '></td>');
        }  


    });
  $(newRow).append('<td><input name="submit" type="submit" id="btn-update" value="Save" class="green submit" style="float:right;margin:5px; width:82px;"></td>');
    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}
