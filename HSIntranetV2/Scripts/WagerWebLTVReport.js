﻿$(function () {
    
    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');

        GetLTVReport();
    });

});

function GetLTVReport() {

    var url = "/WagerWebLTVReport/GetLTVReport/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val(),
            CustomerID: $('#txb-customerdi').val()

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printGetLTVReportTable($('#tbl-result'), result, '', '');

        if (result.length == 0) 
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        // end loader
        loader();

    });

}


function printGetLTVReportTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createGetLTVReportHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createGetLTVReportRow(table, result[i]);
    }
}

function createGetLTVReportHeader(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });

    $(table).children('thead').append(newRow);

}

function createGetLTVReportRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Amount') != -1 || key.indexOf('Balance') != -1 || key.indexOf('Deposit') != -1) {
            if ((/\S/.test(value))) {
                value = formatCurrency(value);
            }
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + ((value==null)?'':value) + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}
