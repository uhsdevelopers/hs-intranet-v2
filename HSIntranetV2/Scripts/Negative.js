﻿



function getNegative() {


    var site = checkedRadioBtn('TType');


    var customerid = document.getElementById('txtCustomerid').value;



    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/Wagering/GetNegativeBalances",
       data = {
           customerid: customerid,
           isCredit: "N",
           siteId: site

       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDinamicTable(table, object)





        // end loader
        loader();
    });
}

function getAverage() {


    var site = checkedRadioBtn('TType');


    var customerid = document.getElementById('txtCustomerid').value;
    var startDate, endDate, valueFrom, valueTo, sportSubtype;
    startDate = document.getElementById('datepickerStart').value;
    endDate = document.getElementById('datepickerEnd').value;
    valueFrom = document.getElementById('txtvalueFrom').value;
    valueTo = document.getElementById('txtvalueTo').value;
    sportSubtype = document.getElementById('selSportSubTypeList').value;

    //GetAverageBalances(string masterAgent, string startDate, string endDate, string valueFrom, string valueTo, string sportSubtype =null)

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    
    url = "/Wagering/GetWagerAmountByRange",
       data = {
           masterAgent: site,
           startDate: startDate,
           endDate: endDate,
           valueFrom: valueFrom,
           valueTo: valueTo,
           customerid:customerid,
           sportSubtype: sportSubtype
           

       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDinamicTable(table, object)





        // end loader
        loader();
    });
}


function getRotnumReport() {


  


    var rotnum = document.getElementById('rotnum').value;
    var startDate, endDate, status;
    startDate = document.getElementById('datepickerStart').value;
    endDate = document.getElementById('datepickerEnd').value;

    status =   checkedRadioBtn('TType');

    //GetAverageBalances(string masterAgent, string startDate, string endDate, string valueFrom, string valueTo, string sportSubtype =null)

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;


    url = "/Wagering/GetGameRotationReport",
       data = {
           rotnum: rotnum,
           status: status,
           dateStart: startDate + ' 12:00 AM',
    dateEnd: endDate + ' 23:59 PM',


       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDinamicTable(table, object)





        // end loader
        loader();
    });
}

function getSportDetailsList(sport, select) {
    select.empty();
    var url = "/Wagering/GetSportssubtype",
        data = {
            sport: sport
        };

    ajaxConnexion(data, url, function (result) {

        printDynamicSelect(select, result, 1, 1, false); // Intranet.js  function printDynamicSelect(table, object, value column number, text column number, duplicates)

    });

}

function getAllSportsList() {

    var url = "/Wagering/GetSports",
        data = {
            
        };

    ajaxConnexion(data, url, function (result) {

        printDynamicSelect($('.selSportTypeList'), result, 0, 0, false); // Intranet.js  function printDynamicSelect(table, object, value column number, text column number, duplicates)

    });

}




function getCasinoData() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetCasinoData",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}

function getCasinoProfitData() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetCasinoProfitTotal",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoProfitSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}


function printDynamicTableHL(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title

    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeaderHL(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createRowHL(table, result[i]);
    }
}


function createHeaderHL(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });

    $(table).children('thead').append(newRow);

}

function createRowHL(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Amount') != -1 || key.indexOf('Balance') != -1 || key.indexOf('Fee') != -1 || key.indexOf('Volume') != -1 || key.indexOf('volumen') != -1) {
            if ((/\S/.test(value))) {
                value = formatCurrency(value);
            }
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }
        if (key !='Description') {
            $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
        } else {
            $(newRow).append('</tr><tr><td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
        }
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}