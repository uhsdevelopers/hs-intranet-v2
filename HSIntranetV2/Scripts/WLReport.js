﻿$(function () {
    
    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');

        GetWLSummary();
    });

});

function GetWLSummary() {

    var url = "/WW_WLReport/GetWLSummary/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTableWW_WLReport($('#tbl-result'), result, '', '');

        if (result.length == 0) 
            $('.span-totalPlayers').text('Total: 0 Players');
        else
            $('.span-totalPlayers').text(($('#tbl-result tr').length - 1) + ' Players');

        // end loader
        loader();

    });

}
function printDynamicTableWW_WLReport(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeader(table, keys);

    var totalWinLost = 0,
        totalAgentAdjustments = 0,
            totalLiveDealer = 0;
    for (i = 0, length = result.length; i !== length; i++) {
        createRowWW_WLReport(table, result[i]);
        totalWinLost = parseFloat(result[i]['Win/Lost'] +totalWinLost) ;
        totalAgentAdjustments = parseFloat(result[i]['Adjustment']+totalAgentAdjustments);
        totalLiveDealer = parseFloat(result[i]['Live Dealer']+totalLiveDealer);
    }
    $(table).append('<tr class="special-tr"><td>Total</td><td></td><td></td><td></td><td class="align-right">' + formatCurrency(totalWinLost) + '</td><td class="align-right">' + formatCurrency(totalAgentAdjustments) + '</td><td class="align-right">' + formatCurrency(totalLiveDealer) + '</td></tr>');

}

function createRowWW_WLReport(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Adjustment') != -1 || key.indexOf('Live Dealer') != -1 || key.indexOf('Win/Lost') != -1) {
            if ((/\S/.test(value))) {
                value = formatCurrency(value);
            }
            addClass = 'align-right';
        } else if (key.indexOf('%') >= 0) {
            value = value + "%";
            addClass = 'align-right';
        }
        else if (key.indexOf('Level') >= 0) {
            addClass = 'align-right';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}
