function ajaxConnexion(data, url, callback) {
    $.ajax({
        type: "GET",
        url:  url,
        data: data,
        success: function (result) {
            if (callback)
                callback(result);
        },
        dataType: 'json'
    });
}





function printBitCoinTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) {
        $('#displayreport').append("<h3>No results found</h3>");
        return;
    }

    // table title row
    var keys = Object.keys(result[0]);
    createRow(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createBitCoinRow(table, result[i]);
    }
}

function createBitCoinRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                if (value == "null" || value == "NULL" || value == 0) {
                    value = '---';
                } else {
                    value = formatDate(value);
                }
            }

        } else if (key.indexOf('Amount') != -1 || key.indexOf('ExchangeRate') != -1) {
            if (row.Status == 'P') {
                value = '---';
            } else if ((row.Gateway == 'BTC' || row.Gateway == 'BTC1') && key=='Amount') {
                value = formatCurrencyBTC(value);
            } else {
                value = formatCurrency(value);
            }
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        } else if (key.indexOf('ExchangeRate') != -1) {
            if (row.Status == 'P') {
                value = '---';
            }     
        }
        else if (key.indexOf('Confirmed') != -1) {
            if (value == 0 && row.Status == 'P') {
                value = '<input type="button" data-confirmedby="' + $('#ConfirmedBy').val() + '" data-paymentid="' + row.PaymentID + '" value="Confirm" class="BTC_Confirm"/>';
            }else if (value == 1 && row.Status == 'P') {
                value = 'YES';
            } else {
                value = '---';
            }
        } else if (key.indexOf('ConfirmedDate') != -1) {
            if (value == "null" || value == "NULL" || value == 0) {
                value = '---';
            }
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).append(newRow);
}

function printDinamicTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) {
        $('#displayreport').append("<h3>No results found</h3>");
        return;
    }

    // table title row
    var keys = Object.keys(result[0]);
    createRow(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createRow(table, result[i]);
    }
}

function createRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Amount') != -1) {
            value = formatCurrency(value);
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).append(newRow);
}

function formatCurrency(value) {

    if (value == null) {
        value = 0;
    }

    var money = value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

    return '$' + money;
}

function formatCurrencyBTC(value) {

    if (value == null) {
        value = 0;
    }

    var money = value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

    return  money + ' BTC';
}

function formatDate(value) {

    var newValue = value.replace(/\D/g, '');

    newValue = parseInt(newValue);

    if (!isNaN(newValue)) {

        var date = new Date(newValue);

        date = date.format('mm/dd/yyyy hh:MM:ss TT');

        return date;
    }
    return '';
}

function dateNow() {
    var date = new Date(),
        today,
        month1 = (date.getMonth() + 1),
        month,
        getdate = date.getDate();
    if (getdate < "10") {
        today = "0" + getdate;
    } else {
        today = getdate;
    }
    if (month1 > "12") {
        month = "0" + month1;
    } else {
         month = month1;
    }
    return month + "/" + today + "/" + date.getFullYear();
}

function isNumeric( obj ) {
    return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
}

function hidden_gif() {
    var $gif=$('#hidden_gif'),
        display = ($gif.css('display') == 'none') ? 'block' : 'none';
    $gif.css('display', display);
}

function ResetSettings() {
    var url = "/Reward/ResetSettings",
        $saveMessage = $('#saveMessage'),
        $errorMessage = $('#errorMessage');
    hidden_gif();
    $.get(url , function (result) {
        if (result == "OK") {
            $saveMessage.text("Was reset successfully").show();

        } else {
            $errorMessage.text(result).show();

        }

        // end loader
        hidden_gif();
    });
}

function getmessages() {
    var url = "/Notification/GetNotificationMessage/",
        $startDate = $("#datepicker").val(),
        $endDate = $("#datepicker1").val(),
        $CustomerId = $('#CustomerId').val(),
        $Method = $('#Method').val(),
        $displayreport = $('#displayreport'),
    data = { startDate: $startDate, endDate: $endDate, customerId: $CustomerId, method: $Method };

    // start loader
    hidden_gif();

    ajaxConnexion(data, url, function (result) {
        printDinamicTable($displayreport, result, '', ''); // HS_script.js  function printDinamicTable(table, object)

        // end loader
        hidden_gif();
    });
}

function SaveReward() {
    var url = "/Reward/SaveRewardIntranet/",
        $CustomerId = $('#CustomerId').val(),
        $rewardId = $('#rewardId').val(),
        $clerk = $('#clerk').val(),
        $saveMessage = $('#saveMessage'),
        $errorMessage = $('#errorMessage'),
    data = "?customerId=" + $CustomerId + "&rewardId=" + $rewardId + "&user=" + $clerk;

    // start loader
    hidden_gif();
    $('.msg').hide();
    $.get(url+data, function (result) {
        if (result == "Y") {
            $saveMessage.text($CustomerId + " - '" + $("#rewardId option:selected").text() + "' - Saved").show();

        } else {
            $errorMessage.text(result).show();

        }

        // end loader
        hidden_gif();
    });
}

function getRewards() {
    var url = "/Reward/GetRewardLog/",
        $startDate = $("#datepicker").val(),
        $endDate = $("#datepicker1").val(),
        $CustomerId = $('#CustomerId').val(),
        $last = $('input[name=last]:checked', '#tb-form-index').val(),
        $displayreport = $('#displayreport'),
        data = { startDate: $startDate, endDate: $endDate, customerId: $CustomerId, last: $last };

    // start loader
    hidden_gif();

    ajaxConnexion(data, url, function (result) {
        printDinamicTable($displayreport, result, '', ''); // HS_script.js  function printDinamicTable(table, object)

        // end loader
        hidden_gif();
    });
}

function getBitCoin() {
    var url = "/BitCoin/getBitCoinReport/",
        $startDate = $("#datepicker").val(),
        $endDate = $("#datepicker1").val(),
        $CustomerId = $('#CustomerId').val(),
        $Gateway = $('#Gateway').val(),
        $PaymentDetails = $('#PaymentDetails').val(),
        $displayreport = $('#displayreport'),
        data = { startDate: $startDate, endDate: $endDate, customerId: $CustomerId, gateway: $Gateway, payment: $PaymentDetails };

    // start loader
    hidden_gif();

    ajaxConnexion(data, url, function (result) {
        printBitCoinTable($displayreport, result, '', ''); // HS_script.js  function printDinamicTable(table, object)
        // end loader
        hidden_gif();
    });
}

function getTransactionsReport() {
    var url = "/BitCoin/getTransactionsReport/",
        $startDate = $("#datepicker").val(),
        $endDate = $("#datepicker1").val(),
        $CustomerId = $('#CustomerId').val(),
        $AmountX = $('#AmountX').val(),
        $PaymentDetails = $('#PaymentDetails').val(),
        $displayreport = $('#displayreport'),
        data = { startDate: $startDate, endDate: $endDate, customerId: $CustomerId, AmountX: $AmountX, payment: $PaymentDetails };

    // start loader
    hidden_gif();

    ajaxConnexion(data, url, function (result) {
        printDinamicTable($displayreport, result, '', ''); // HS_script.js  function printDinamicTable(table, object)
        // end loader
        hidden_gif();
    });
}

function getCustomerInfo() {
    var url = "/Notification/GetAccountInfo/",
        $customerId = $('#CustomerId').val(),
        $CustomerInfoDiv = $('.CustomerInfo'),
        $firstName = $('#FirstName'),
        $lastName = $('#LastName'),
        $eMail= $('#Email'),
        $phone = $('#Phone'),
        $country = $('#country'),
        $customer = $('#customer'),
        $errorMessage = $('#errorMessage'),
        data = { customerId: $customerId };

    $errorMessage.hide();
    hidden_gif();
    $CustomerInfoDiv.hide();
    ajaxConnexion(data, url, function (result) {
        if (result.length == 1) {
            var res = result[0];
            $firstName.val(res.NameFirst);
            $lastName.val(res.NameLast);
            $eMail.val(res.Email);
            $phone.val(res.HomePhone);
            $country.val(res.Country);
            $customer.val(res.CustomerID);
            $CustomerInfoDiv.show();
            
        } else {
            $errorMessage.text("Invalid customerId......").show();
        }
        hidden_gif();
    });

}

function getSetting() {
    var url = "/Reward/GetSettingLog/",
        $displayreport = $('#settingsLogs'),
        data = {};

    // start loader
    hidden_gif();

    ajaxConnexion(data, url, function (result) {
        printDinamicTable($displayreport, result, '', ''); // HS_script.js  function printDinamicTable(table, object)

        // end loader
        hidden_gif();
    });
}

function SaveSetting() {
    var url = "/Reward/SaveSetting/",
        $clerk = $('#clerk').val(),
        $startDate = $("#datepicker").val(),
        $endDate = $("#datepicker1").val(),
        $saveMessage = $('#saveMessage'),
        $errorMessage = $('#errorMessage'),
    data = "?startDate=" + $startDate + "&endDate=" + $endDate + "&cleck=" + $clerk;

    // start loader
    hidden_gif();
    $('.msg').hide();
    $.get(url + data, function (result) {
        if (result == "Y") {
            $saveMessage.html("-The Bunus Program in available from <b>" + $startDate + "</b> to <b>" + $endDate + "</b> - ").show();
            getSetting();
        } else {
            $errorMessage.text(result).show();

        }

        // end loader
        hidden_gif();
    });
}

function sendSMS() {
    var url = '/Notification/SendSMS/',
        $customer = $('#customer').val(),
        $Sms = $('#txtSMS'),
        $clerk = $('#clerk').val(),
        $phone = $('#Phone').val(),
        $saveMessage = $('#saveMessage'),
        $errorMessage = $('#errorMessage'),
        data = '?customerID='+ $customer+'&recipient='+$phone+'&message='+$Sms.val()+'&service=Intranet&userFloor'+$clerk;
    $saveMessage.hide();
    $errorMessage.hide();
    hidden_gif();
    $.get(url + data, function (result) {
        if (result == 'OK') {
            $saveMessage.html('SMS was sent successfully').show();
            $Sms.val('');
        } else {
            var error = (result != '') ? result : 'Service Error...';
            $errorMessage.text(error).show();
        }
        hidden_gif();
    });
}

function PayBTC(PaymentID, Amount, ConfirmedBy) {

    if (!isNumeric(Amount)) {
        alert('The amount is incorrect.\n\nPlease use this format "10" or "10.20".');
        return;
    }
    var url = "/BitCoin/PayBTC/",
        $CustomerId = $('#CustomerId').val(),
        $rewardId = $('#rewardId').val(),
        $clerk = $('#clerk').val(),
        $saveMessage = $('#saveMessage'),
        $errorMessage = $('#errorMessage'),
    data = "?PaymentID=" + PaymentID + "&Amount=" + Amount + "&ConfirmedBy=" + ConfirmedBy;

    // start loader
    hidden_gif();
    $.get(url + data, function (result) {
        if (result == "Y") {
            getBitCoin();
        } else {
            alert("Error in database");

        }
        // end loader
        hidden_gif();
    });
}

$(function () {
    var $dateFrom = $("#datepicker"),
        $dateTo = $("#datepicker1"),
        $getMessages = $("#getMessages"),
        $getRewards = $("#getRewards"),
        $SaveReward = $("#SaveReward"),
        $SaveSetting = $('#SaveSettings'),
        $SearchCustomer = $('#SearchCustomer'),
        $SendCustomerSMS = $('#SendCustomerSMS'),
        $getBitCoin = $('#getBitCoin'),
        $ResetSettings = $('#ResetSettings'),
        $getTransactionsReport = $('#getTransactionsReport'),
        $Content =   $("#Content");

    if ($('#settingsLogs').length == 1) {
       getSetting(); 
    }
    $ResetSettings.click(function() {
        var result = confirm("Are you sure you want to allow users to select their Player Program?");
        if (result == true) {
            ResetSettings();
        }  
    });

    $getTransactionsReport.click(function() {
        getTransactionsReport();
    });

    $SearchCustomer.click(function() {
        getCustomerInfo();
    });

    $SendCustomerSMS.click(function () {
        sendSMS();
    });

    $getMessages.click(function() {
        getmessages();
    });

    $SaveReward.click(function () {
        SaveReward();
    });

    $getRewards.click(function () {
        getRewards();
    });

    $SaveSetting.click(function () {
        SaveSetting();
    });

    $getBitCoin.click(function () {
        getBitCoin();
    });

    $dateFrom.val(dateNow()).datepicker();

    $dateTo.val(dateNow()).datepicker();

    $Content.on('click', '.BTC_Confirm', function () {
        var amount = prompt('Are you sure you want to confirm this transaction?\n\nIf so, please add the amount to continue. This is going to update the customer balance, please make sure the amount is correct.\n', '');
        if (amount != null) {
            var $this = $(this),
                PaymentID = $this.data('paymentid'),
                ConfirmedBy = $this.data('confirmedby'),
                Amount = amount;
            PayBTC(PaymentID,Amount, ConfirmedBy);
        }
    });
 
});