﻿$(function () {

    var deleteCC;

    $('#btn-submit').on('click',function () {

         GetCreditCard();
    });
    $('#tbl-result').on('click', '.delete-card', function () {
        //if (confirm('Do you want to delete this credit card?')) {
         //   DeleteCreditCard(this);
        // }
        deleteCC = this;
        $('#DBConfirm').show();
    });
    
    $('#btn-delete').click(function () {

        DeleteCreditCard(deleteCC);

        deleteCC = undefined;
    });

    $('#btn-cancel').click(function () {

        $('#DBConfirm').hide();

        deleteCC = undefined;
    });

    $('#CustomerId').on('keydown', function (e) {
        if (e.which == 13) {
            GetCreditCard();
        }
    });
    
});

function GetCreditCard() {

    var url = "/AdminCreditCards/GetCreditCards",
        data = {
            customerId: $('#CustomerId').val()
};
    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        if (result.length == 0)
            alert("No Credit Cards");

        printCCTable($('#tbl-result'), result, '', '');

        // end loader
        loader();

    });
}

function DeleteCreditCard($this) {

    var url = "/AdminCreditCards/DeleteCreditCards",
        data = {
            CustomerID:$($this).data('customerid'),
            BIN:$($this).data('bin'),
            L4:$($this).data('l4'),
            CardType: $($this).data('cardtype'),
            CardExpDate: $($this).data('cardexpdate')
        };
    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        GetCreditCard();
        // end loader
        loader();

    });
}

function printCCTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createCCHeader(table, keys);
   
    for (i = 0, length = result.length; i !== length; i++) {
        createCCRow(table, result[i], i);
    }
}

function createCCHeader(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });
    $(newRow).append('<th> Action </th>');
    $(table).children('thead').append(newRow);

}
function createCCRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        if (value == null) {
            value = '';
        }
        $(newRow).append('<td>' + value+ '</td>');
    });
    $(newRow).append('<td><input type="button" class="delete-card" data-customerid="' + $('#CustomerId').val()+'" data-BIN="' + row.BIN + '" data-L4="' + row.L4 + '" data-cardExpDate="' + row.ExpDate + '" data-CardType="' + row.CardType + '" Value="Delete"/> </td>');
    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}
