$(function(){
	sessionStorage.removeItem('weekNumber');
	$('.survivor-container').on('click','.week',function(){
		if($(this).data('done') != true && $(this).hasClass('week-highlighted')){
			sessionStorage.weekNumber = $(this).data('weekid');
			window.location = "SurvivorAdmin/Grader";
		}
	}); 
	$.get('http://192.168.101.173:8080/Survivor/WeekList', function (weeklist) {
		$weeks = $('.week');
	    for (var x = 0; weeklist.length > x; x++) {

	        var currentWeek = weeklist[x];

	        if (currentWeek.IsGraded == 1) {
	            winLose = 'win';
	        } else {
	            winLose = 'lose';
	        }

	        if (currentWeek.Status == 2) {
	            $($weeks[x]).addClass('week-active');
	        } else {
	            $($weeks[x]).addClass('week-highlighted');
	        }
	        $($weeks[x]).find('.week-status').addClass(winLose);
	        $($weeks[x]).data('weekid', currentWeek.ID);

	    }
	    $('.loader_container').hide();
	});
});