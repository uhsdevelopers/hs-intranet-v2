﻿$(function () {

    $('#btn-submit').click(function () {

        GetRollinIfbetReport();
    });
    

});

function GetRollinIfbetReport() {

    var url = "/RollinIfbetReport/GetRollinIfbetReport/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            CustomerID: $('#CustomerID').val()
            
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        $('#tbl-result').empty();
        printDynamicTable($('#tbl-result'), result, '', '');
        // end loader
        loader();

    });

}
