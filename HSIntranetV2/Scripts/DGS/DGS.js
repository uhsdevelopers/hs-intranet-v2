﻿$(function () {
    $('#IsEnable').hide();
    $('#btnstatus').hide();
    $('#lblNoGames').hide();
    
    
    $('#btnSendQueue').click(function () {
        SendQueue(0);
    });
    
    $('#btnGetGameInfo').click(function () {
        GetGameInfo(0);
    });
    

    GetScore(1);

    DGSService(1, -1);  //Charting
    DGSService(2, -1); //Publisher

    setInterval(GetScoreNoSent, 10000);
    
});


function SendQueue(id) {

    var r = confirm('Are you sure you want to send all the scores displaying below to the DGS games?');

    if (r != true) return;

    var url;
    var data;

    url = "/DGS/SendQueue",
       data = {
           id: id

       };

    ajaxConnexion(data, url, function (result) {


    });

}


function GetScore(id) {


    var url;
    var data;

    url = "/DGS/GetScoreSetting",
       data = {
           id: id


       };

    loader();

    ajaxConnexion(data, url, function (result) {


        var div = document.getElementById('IsEnable');
        $('#IsEnable').show();
        if (result[0].Enabled == "1") {




            div.innerHTML = 'Running';
            div.className = 'btn btn-success';
        }
        else {
            div.innerHTML = 'Stopped';
            div.className = 'btn btn-danger';
        }

        //printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
        GetScoreNoSent(id);
    });

}

function EnableScore(id,enable) {


    var url;
    var data;

    url = "/DGS/SetScoreSetting",
       data = {
           id: id,
           enabled: enable

       };

    loader();

    ajaxConnexion(data, url, function (result) {

        $('#TransferSummary').show();



        printScoresTable($('#TransferSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        var div = document.getElementById('btnstatus');
        $('#btnstatus').show();
        if (result[0].statusid == "1") {

            if (enable == "1") {
                div.innerHTML = 'The Scores Synchronizer was Started';
                div.className = 'btn btn-success';
            }
            else {
                div.innerHTML = 'The Scores Synchronizer was Stopped';
                div.className = 'btn btn-danger';
            }

        }
        else {

            div.innerHTML = 'An Error Occured';
            div.className = 'btn btn-danger';
        }
        



        // end loader
        loader();
        GetScore(id);
    }
    )
}

function GetScoreNoSent(id) {

   
    var url;
    var data;

    url = "/DGS/getScoreNoSent",
       data = {
           id: id


       };

    $('#lblRefresh').removeClass("lblRefresh").addClass("lblRefresh");

    ajaxConnexion(data, url, function (result) {


        printScoresTable($('#TransferSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        $('#TransferSummary').tablesorter();

        $('.btnSendQueue').click(function () {

            var id = $(this).data("id");

            SendQueue(id);
        });

    });

}


function GetIdList() {


    var url;
    var data;

    url = "/DGS/GetGamesQueue",
       data = {
         
       };

    $('#lblRefresh').removeClass("lblRefresh").addClass("lblRefresh");

    ajaxConnexion(data, url, function (result) {
        
        if (result.length == 0) {
            
            $('#lblNoGames').show();
        }


        printGamesTable($('#GameSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        $('#GameSummary').tablesorter();

        $('.btnGetGameInfo').click(function () {

            var id = $(this).data("id");

            GetGameInfo(id);
        });

    });

}


function GetGameInfo(id) {

    

    var url;
    var data;

    url = "/DGS/GetGamesQueueInfo",
       data = {
           idQueue: id

       };

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#GameSummary2'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

    });

}




/* DGS SERVICE */

function DGSService(serviceID, status) {


    var url;
    var data;

    url = "/DGS/DGSService",
       data = {
           serviceID: serviceID,
           status: status

       };

    loader();

    ajaxConnexion(data, url, function (result) {


        var div;
        
        switch(serviceID) {
            case 1:
                div = document.getElementById('IsEnableCharting');
                break;
            case 2:
                div = document.getElementById('IsEnablePublisher');
                break;
        }

        $(div).show();
        if (result[0].Status == "1") {

            div.innerHTML = 'Running';
            div.className = 'btn btn-success';
        }
        else {
            div.innerHTML = 'Stopped';
            div.className = 'btn btn-danger';
        }


        // end loader
        loader();

    });

}

/* END DGS SERVICE */