﻿function printScoresTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title

    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createScoresHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createScoresRow1(table, result[i]);
    }


}

function printGamesTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title

    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createGamesHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createGamesRow1(table, result[i]);
    }


}

function createScoresHeader(table, row) {

    var newRow = $('<tr></tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });

    $(newRow).append('<th class="' + addClass + '">Send Score</th>');

    $(table).children('thead').append(newRow);

}

function createGamesHeader(table, row) {

    var newRow = $('<tr></tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });

    $(newRow).append('<th class="' + addClass + '">Get games info</th>');

    $(table).children('thead').append(newRow);

}

function createScoresRow1(table, row) {

    var newRow = $('<tr>')
    $idScoreXML = 0,
        addClass = '',
        tdClass = '';

    $.each(row, function (key, value) {


        // idgame
        if (key.indexOf('IdScoreXML') != -1) {
            $idScoreXML = value;
        }

        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Amount') != -1 || key.indexOf('Balance') != -1 || key.indexOf('Fee') != -1 || key.indexOf('Volume') != -1 || key.indexOf('volumen') != -1) {

            addClass = 'align-right';
        }


        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');


    });

    $(newRow).append('<td class="' + addClass + ' ' + tdClass + '"><input type="button" data-id="' + $idScoreXML + '" class="btn btnSendQueue" value="Send Score" ></td>');

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}


function createGamesRow1(table, row) {

    var newRow = $('<tr>')
    $idScoreXML = 0,
        addClass = '',
        tdClass = '';

    $.each(row, function (key, value) {


        // idgame
        if (key.indexOf('IdQueue') != -1) {
            $idScoreXML = value;
        }

        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Amount') != -1 || key.indexOf('Balance') != -1 || key.indexOf('Fee') != -1 || key.indexOf('Volume') != -1 || key.indexOf('volumen') != -1) {

            addClass = 'align-right';
        }


        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');


    });

    $(newRow).append('<td class="' + addClass + ' ' + tdClass + '"><input type="button" data-id="' + $idScoreXML + '" class="btn btnGetGameInfo" value="Get info" ></td>');

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}