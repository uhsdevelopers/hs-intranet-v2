﻿$(function () {
    
    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');

        GetTransAccList();
    });
    $('#tbl-result').on('click', '.btn-delete', function() {
        var url = "/CashbackBlackList/DeleteTransAcc/",
            customerId= $(this).data('customer'),
            data = {
                customerId: customerId
            };
        // start loader
        loader();

        ajaxConnexion(data, url, function (result) {
            loader();
            GetTransAccList();
        });
    });
    $('#tbl-result').on('click', '.btn-add', function () {
        var url = "/CashbackBlackList/InsertTransAcc/",
            customerId = $(this).data('customer'),
            data = {
                customerId: customerId
            };
        // start loader
        loader();
        if (confirm('Customer is not in blacklist. Want to add him? ')) {
            ajaxConnexion(data, url, function (result) {
                loader();
                GetTransAccList();
            });
        }
        
    });
});

function GetTransAccList() {
    $('#tbl-result').empty();
    var url = "/CashbackBlackList/GetTransAccList/",
        data = {
            customerId: $('#customerId').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        if (result.length == 0) {
            $('#tbl-result').html('<center><span style="margin: 10px 10px;padding: 10px;">Customer is not in black list.</span><br><input style="margin: 10px;"type="button" class="btn-add" data-customer="' + $('#customerId').val() + '" value="Add"></center>');
        } else {
            printtDynamicTable($('#tbl-result'), result, '', '');
        }
        // end loader
        loader();
    });

}

function printtDynamicTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createtHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createtRow(table, result[i]);
    }
}

function createtHeader(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
        if (value == 'LastDateTimeChange') {
            $(newRow).append('<td class="">Action</td>');
        }
    });

    $(table).children('thead').append(newRow);

}

function createtRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Amount') != -1 || key.indexOf('Balance') != -1 || key.indexOf('Fee') != -1) {
            if ((/\S/.test(value))) {
                value = formatCurrency(value);
            }
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
        if (key== 'LastDateTimeChange') {
            $(newRow).append('<td class=""><input type="button" data-customer="'+row.CustomerID+'" class="btn-delete" value="Delete"></td>');
        }
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}