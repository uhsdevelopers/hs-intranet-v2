﻿
$(function () {
    $('#btnSubmit').click(function () {
        GetTransactions();
        $('#tbl-Detail').empty();
        $('#tbl-Result').empty();
        $('#td-total').empty();
        $('#td-accepted').empty();
        $('#td-declined').empty();
        $('#tbl-TotalDeposit').hide();
        $('#tbl-TotalWithdrawal').hide();
    });

    
});

function GetTransactions() {

    var isDeposit = ($('#rad-deposit').is(':checked')) ? true : false,
        url = (isDeposit) ? "/DepositWithdrawals/GetDeposits/" : "/DepositWithdrawals/GetWithdrawals/",
        data = {
            from: $('#datepickerStart').val(),
            to: $('#datepickerEnd').val(),
            company: $('#sel-company').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-Result'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        // totals by category
        //addTotals();

        if (isDeposit) {
            $('#tbl-Result tr:not(:first) td:nth-child(2)').click(function () {
                GetDepositsDetails(this, 'A');
            });

            $('#tbl-Result tr:not(:first) td:nth-child(3)').click(function () {
                GetDepositsDetails(this, 'Y');
            });

            $('#tbl-Result tr:not(:first) td:nth-child(4)').click(function () {
                GetDepositsDetails(this, 'N');
            });

            TotalDeposit();
        }
        else {
            $('#tbl-Result tr:not(:first)').click(function () {
                GetWithdrawalsDetails(this);
            });

            TotalWithdrawal();
        }

        $('#tbl-Result tr:not(:first) td:nth-child(n+2)').addClass('isLink');

        // end loader
        loader();
    });

}

function TotalWithdrawal() {

    var totalAmount = 0,
        totalAccepted = 0;

    $('#tbl-Result tr:not(:first) td:nth-child(2)').each(function (i, td) {
        totalAmount += parseFloat($(td).text().replace('$', '').replace(',', ''));
    });

    $('#td-totalWithdrawal').text(formatCurrency(totalAmount));

    $('#tbl-Result tr:not(:first) td:nth-child(3)').each(function (i, td) {
        totalAccepted += parseFloat($(td).text().replace('$', '').replace(',', ''));
    });

    $('#td-trans').text(totalAccepted);

    $('#tbl-TotalWithdrawal').show();
}

function TotalDeposit() {

    var totalAmount = 0,
        totalAccepted = 0,
        totalDeclined = 0;

    $('#tbl-Result tr:not(:first) td:nth-child(2)').each(function(i, td) {
        totalAmount += parseFloat($(td).text().replace('$', '').replace(',', ''));
    });

    $('#td-totalDeposit').text(formatCurrency(totalAmount));

    $('#tbl-Result tr:not(:first) td:nth-child(3)').each(function (i, td) {
        totalAccepted += parseFloat($(td).text().replace('$', '').replace(',', ''));
    });

    $('#td-accepted').text(totalAccepted);

    $('#tbl-Result tr:not(:first) td:nth-child(4)').each(function (i, td) {
        totalDeclined += parseFloat($(td).text().replace('$', '').replace(',', ''));
    });

    $('#td-declined').text(totalDeclined);

    $('#tbl-TotalDeposit').show();
}

function GetDepositsDetails(cell, status) {

    var url = "/DepositWithdrawals/GetDepositsDetails/",
        data = {
            from: $('#datepickerStart').val(),
            to: $('#datepickerEnd').val(),
            company: $('#sel-company').val(),
            processor: $(cell).parent('tr').find('td:first').text(),
            status: status
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-Detail'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        // end loader
        loader();
    });

}

function GetWithdrawalsDetails(row) {

    var url = "/DepositWithdrawals/GetWithdrawalsDetails/",
        data = {
            from: $('#datepickerStart').val(),
            to: $('#datepickerEnd').val(),
            company: $('#sel-company').val(),
            paymentBy: $(row).find('td:first').text()

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-Detail'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        // end loader
        loader();
    });

}