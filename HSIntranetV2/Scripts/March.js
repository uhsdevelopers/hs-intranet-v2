﻿$(function () {
});

function GetCustomer() {

   

    var sportbookid = checkedRadioBtn('TType');

    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (sportbookid == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/March/Getcustomer",
       data = {
           sportbookid: sportbookid
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}


function GetAllGames() {



    var sportbookid = checkedRadioBtn('TType2');

    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (sportbookid == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/March/GetAllGames",
       data = {
           sportbookid: sportbookid
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}


function GetAllTeams() {



    var sportbookid = checkedRadioBtn('TType2');

    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (sportbookid == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/March/GetallTeams",
       data = {
           sportbookid: sportbookid
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable1($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}



function GetLeaderboard() {



    var sportbookid = checkedRadioBtn('TType3');

    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (sportbookid == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/March/GetLeaderBoard",
       data = {
           sportbookid: sportbookid
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}


function getCasinoData() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetCasinoData",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}


function getLiveDealer() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetLiveDealerData",
       data = {
       };





    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#LDSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader

    });
}

function getCasinoProfitData() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetCasinoProfitTotal",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoProfitSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}

function printDynamicTable1(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createRow2(table, result[i]);
    }
}

function createRow2(table, row) {

    var newRow = $('<tr id="' + row.ContestGameID + '">'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
  

        } else if (key.indexOf('Amount') != -1) {
            value = formatCurrency(value);
            addClass = 'align-right';
        } else if (key.indexOf('GameNum') >= 0) {
            tdClass = 'cl-CommentID';
        }
        if (key.indexOf('Team1ID') >= 0 ) {
            $(newRow).append('<td class="' + addClass + '  "><input type="textbox" name="team1" class="checkbox1" value="' + value + '" id="' + row.TeamRot1 + '"></td>');
            var Team1 = $('#' + row.TeamRot1 + '').val();
        }
        else{
            if (key.indexOf('Team2ID') >= 0) {
                $(newRow).append('<td class="' + addClass + '  "><input type="textbox" name="team2" class="checkbox1" value="' + value + '" id="' + row.TeamRot11 + '"></td>');
                var Team2 = $('#' + row.TeamRot11 + '').val();
            }
            else {
                $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
            }
        }
        
    });
  
    
    $(newRow).append('<td class="' + addClass + ' ">');
    $(newRow).append('<input type="button" name="update"  value="submit" onClick="UpdateTeams(' + row.ContestGameID + ',' + row.TeamRot1 + ',' + row.TeamRot11 + ')" id="btn' + row.ContestGameID + '">');

    $(newRow).append('</td></tr>');

    $(table).append(newRow);
}


function UpdateTeams(GameID, RecId1, RecId2 ) {

    var Team1ID = $('#' + RecId1).val();
    var Team2ID = $('#' + RecId2).val();

    var sportbookid = checkedRadioBtn('TType2');

    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (sportbookid == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/March/UpdateTeams",
       data = {
           GameID: GameID,
           Team1ID: Team1ID,
           Team2ID: Team2ID,
           RecId1: RecId1,
           RecId2:RecId2,
           sportbookid: sportbookid
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
       // printDynamicTable1($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
        if (result[0].result == "1") {
            //alert("Updated");
            //$('#' + GameID).hide();
            if (GameID % 2 == 0) {
                $('#' + GameID).css('background', '#afebf0');
            } else {
                $('#' + GameID).css('background', '#d8f5cb');
            }
            
            $('#btn' + GameID).val('Updated');
        } else {
            alert(result[0]);
        }




        // end loader
        loader();
    });
}