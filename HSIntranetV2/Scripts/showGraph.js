function getChart(week1, week2) {
	var chart = new CanvasJS.Chart("chartContainer",
	{
		title:{
		fontFamily: "Monda",
		fontSize: 22,
		fontColor: "#000",
		fontWeight: "bold",
		text: "Active Players",
		},
		height: 200,
		legend:{
			horizontalAlign: "center",
	  		fontFamily: "Monda",
	  		fontSize: 14,
	  		fontColor: "#000",
	  		itemWidth: 200
	 	},
	 	toolTip:{
	 		borderThickness: 2
	 	},
	    animationEnabled: true,
	    backgroundColor: "transparent",
		theme: "theme1",
		data: [{        
		    type: "column",
			indexLabelFontSize:12,
			showInLegend: false,
			legendMarkerType: "square",
			toolTipContent: "{_name}: {y} ", 	
			dataPoints: [
				{ y: week1, _name: "Week 1", label: "Week 1" },
				{ y: week2, _name: "Week 2", name: "Week 2: {y} ", label: "Week 2" }
			]
		}]
	});
	chart.render();
}

function getPlayerCount(week1, week2) {
    var chart = new CanvasJS.Chart("chartContainer2",
	{
	    title: {
	        fontFamily: "Monda",
	        fontSize: 22,
	        fontColor: "#000",
	        fontWeight: "bold",
	        text: "Players Count",
	    },
	    height: 200,
	    legend: {
	        horizontalAlign: "center",
	        fontFamily: "Monda",
	        fontSize: 14,
	        fontColor: "#000",
	        itemWidth: 200
	    },
	    toolTip: {
	        borderThickness: 2
	    },
	    animationEnabled: true,
	    backgroundColor: "transparent",
	    theme: "theme2",
	    data: [{
	        type: "column",
	        indexLabelFontSize: 12,
	        showInLegend: false,
	        legendMarkerType: "square",
	        toolTipContent: "{_name}: {y} ",
	        dataPoints: [
			{ y: week1, _name: "Week 1", label: "Week 1" },
				{ y: week2, _name: "Week 2", name: "Week 2: {y} ", label: "Week 2" }
	        ]
	    }]
	});
    chart.render();
}
	
function getPostedticket(week1, week2) {
    var chart = new CanvasJS.Chart("chartContainer3",
	{
	    title: {
	        fontFamily: "Monda",
	        fontSize: 22,
	        fontColor: "#000",
	        fontWeight: "bold",
	        text: "Posted Tickets",
	    },
	    height: 200,
	    legend: {
	        horizontalAlign: "center",
	        fontFamily: "Monda",
	        fontSize: 14,
	        fontColor: "#000",
	        itemWidth: 200
	    },
	    toolTip: {
	        borderThickness: 2
	    },
	    animationEnabled: true,
	    backgroundColor: "transparent",
	    theme: "theme1",
	    data: [{
	        type: "column",
	        indexLabelFontSize: 12,
	        showInLegend: false,
	        legendMarkerType: "square",
	        toolTipContent: "{_name}: {y} ",
	        dataPoints: [
				{ y: week1, _name: "Week 1", label: "Week 1" },
				{ y: week2, _name: "Week 2", name: "Week 2: {y} ", label: "Week 2" }
	        ]
	    }]
	});
    chart.render();
}

function getVolumen(week1, week2) {
    var chart = new CanvasJS.Chart("chartContainer4",
	{
	    title: {
	        fontFamily: "Monda",
	        fontSize: 22,
	        fontColor: "#000",
	        fontWeight: "bold",
	        text: "Volumen",
	    },
	    height: 200,
	    legend: {
	        horizontalAlign: "center",
	        fontFamily: "Monda",
	        fontSize: 14,
	        fontColor: "#000",
	        itemWidth: 200
	    },
	    toolTip: {
	        borderThickness: 2
	    },
	    animationEnabled: true,
	    backgroundColor: "transparent",
	    theme: "theme2",
	    data: [{
	        type: "column",
	        indexLabelFontSize: 12,
	        showInLegend: false,
	        legendMarkerType: "square",
	        toolTipContent: "{_name}: {y} ",
	        dataPoints: [
				{ y: week1, _name: "Week 1", label: "Week 1" },
				{ y: week2, _name: "Week 2", name: "Week 2: {y} ", label: "Week 2" }
	        ]
	    }]
	});
    chart.render();
}