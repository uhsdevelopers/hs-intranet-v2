﻿$(function () {
    var $dateFrom = $("#datepicker"),
        $dateTo = $("#datepicker1");
    $dateFrom.val(dateNow()).datepicker();

    $dateTo.val(dateNow()).datepicker();
    $(document).keyup(function (e) {
        if (e.keyCode == 13) {
            $('#getCardValidation').click();
        }
    });
    var $getCardValidation = $('#getCardValidation');
    $getCardValidation.click(function () {
        GetCustomerDocument();
    });
    var ChangedBy = $('#clerk').val();
    $('#displayreport').on('click', '.PhoneVerified', function () {
        setCustomervalidation($(this).data('customer'), 'PhoneVerified', ChangedBy);
    });
    $('#displayreport').on('click', '.FullnameVerified', function () {
        setCustomervalidation($(this).data('customer'), 'FullnameVerified', ChangedBy);
    });
    $('#displayreport').on('click', '.AddressVerified', function () {
        setCustomervalidation($(this).data('customer'), 'AddressVerified', ChangedBy);
    });
});
function GetCustomerDocument() {
    var url = "/CardValidation/GetCustomerInfo/",
        $CustomerId = $('#CustomerId').val(),
        data = { customerId: $CustomerId };

    // start loader
    hidden_gif();

    ajaxConnexion(data, url, function (result) {
        printCustomerDocumentTable($('#displayreport'), result, '', ''); // HS_script.js  function printDinamicTable(table, object)
        // end loader
        hidden_gif();
    });
}
function setCustomervalidation(customerId, typefile, ChangedBy) {
    var url = "/CardValidation/setCustomervalidation/",
        data = {
            customerId: customerId, typefile: typefile, ChangedBy: ChangedBy
        };

    hidden_gif();

    ajaxConnexion(data, url, function (result) {
        hidden_gif();
        GetCustomerDocument();
    });
}

function printCustomerDocumentTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) {
        $('#displayreport').append("<h3>No results found</h3>");
        return;
    }
    // table title row
    var keys = Object.keys(result.customer_information);

    createCustomerDocumentRow(table, result.customer_information);

}

function createCustomerDocumentRow(table, row) {

    var newRow = '',
    customerId = row.customerId,
    PhoneVerified = row.PhoneVerified,
    FullnameVerified = row.FullnameVerified,
    AddressVerified = row.AddressVerified;

    newRow += '<tr><td></td><td>Customer Information</td><td></td>';
    newRow += '<tr><td>CustomerId</td>';
    newRow += '<td>' + customerId + '</td><td></td></tr>';
    newRow += '<tr><td>Fullname</td>';
    newRow += '<td>' + row.first_name + '</td>';
    if (FullnameVerified != "1") {
        newRow += '<td><input type="button" data-customer="' + customerId + '"  value="Verify" class="FullnameVerified"></td></tr>';
    } else {
        newRow += '<td>Verified</td></tr>';
    }
    newRow += '<tr><td>Email</td>';
    newRow += '<td>' + row.email + '</td>';
    newRow += '<td>Verified</td></tr>';
    newRow += '<tr><td>Address</td>';
    newRow += '<td>' + row.address1 + '</td>';
    if (AddressVerified != "1") {
        newRow += '<td><input type="button" data-customer="' + customerId + '"  value="Verify" class="AddressVerified"></td></tr>';
    } else {
        newRow += '<td>Verified</td></tr>';
    }
    newRow += '<tr><td>Phone</td>';
    newRow += '<td>' + row.phone1 + '</td>';
    if (PhoneVerified == "" || PhoneVerified == "0") {
        newRow += '<td><input type="button" data-customer="' + customerId + '" value="Verify" class="PhoneVerified"></td></tr>';
    } else {
        newRow += '<td>Verified</td></tr>';
    }

    newRow += '</tr>';

    $(table).append(newRow);
}