﻿$(function () {
    var $dateFrom = $("#datepicker"),
        $dateTo = $("#datepicker1");
    $dateFrom.val(dateNow()).datepicker();

    $dateTo.val(dateNow()).datepicker();
    $(document).keyup(function (e) {
        if (e.keyCode == 13) {
            $('#getCardValidation').click();
        }
    });
    var $getCardValidation = $('#getCardValidation');
    $getCardValidation.click(function () {
        GetCustomerDocument();
    });

    $('#displayreport').on('click', '.changeStatus', function () {
        var customerId = $(this).data('customer'),
            typefile = $(this).data('typefile'),
            status = ($(this).val() == 'Pass') ? '1' : '2',
            bin = $(this).data('bin'),
            l4 = $(this).data('l4'),
            cardtype = $(this).data('cardtype'),
            ExpDate = $(this).data('expdate'),
            rowID = $(this).data('rowid'),
ChangedBY = $('#clerkId').val(),

            statusreason = $(this).closest('tr').find('.txtAreaReason').val();

        if (statusreason != '' || status == "1") {
                setGetCustomerDocument(customerId, typefile, status, bin, l4, cardtype, ExpDate, rowID, statusreason,ChangedBY);
        } else {
            alert("Please select one reason");
            $(this).closest('tr').find('.txtAreaReason').focus();
        }

    });
    });
    function GetCustomerDocument() {
        var url = global_location + "/CardValidation/GetCustomerDocument/",
        $CustomerId = $('#CustomerId').val(),
        status = $('#status').val(),
        dateFrom = $('#datepicker').val(),
        dateTo = $('#datepicker1').val(),
  sportbookId = $('#company').val(),
            data = { customerId: $CustomerId, DateFrom: dateFrom, DateTo: dateTo, Status: status, SportbookID: sportbookId };

    // start loader
    hidden_gif();

    ajaxConnexion(data, url, function (result) {
        printCustomerDocumentTable($('#displayreport'), result, '', ''); // HS_script.js  function printDinamicTable(table, object)
        // end loader
        hidden_gif();
    });
    }
    function setGetCustomerDocument(customerId, typefile, status, bin, l4, cardtype, ExpDate, rowID, statusreason, ChangedBy) {

        if (ChangedBy == '') {
            alert("Your Session has expired, please login again on intranet");
        }


    var url = "/CardValidation/setCustomerDocument/",
        data = {
                customerId: customerId, typefile: typefile, status: status, bin: bin, l4: l4, cardtype: cardtype, ExpDate: ExpDate, rowID: rowID, statusreason: statusreason, ChangedBy: ChangedBy
        };

    hidden_gif();

    ajaxConnexion(data, url, function (result) {
        hidden_gif();
        GetCustomerDocument();
    });
    }

    function printCustomerDocumentTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) {
        $('#displayreport').append("<h3>No results found</h3>");
        return;
    }
    // table title row
    var keys = Object.keys(result[0]);
    createtestRow(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createCustomerDocumentRow(table, result[i]);
    }
    }
    function createtestRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        if (value != "sportbookID") {
            var tdClass = '';
            if (isNaN(value)) {
                if (value.indexOf('/Date') >= 0) {
                    value = formatDate(value);
                }

            } else if (key.indexOf('Amount') != -1) {
                value = formatCurrency(value);
                addClass = 'align-right';
            } else if (key.indexOf('CommentID') >= 0) {
                tdClass = 'cl-CommentID';
            }

            $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
        }

    });
    $(newRow).append('<td>Verify</td>');
    $(newRow).append('<td>Verify</td>');
    $(newRow).append('</tr>');

    $(table).append(newRow);
    }
    function createCustomerDocumentRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '',
        customerid = '',
        typefile = '',
        bin = '',
        l4 = '',
        cardtype = '',
        ExpDate = '',
        rowId = '';

    $.each(row, function (key, value) {
        if (key != "sportbookID") {
            var tdClass = '';
            if (isNaN(value)) {
                if (value.indexOf('/Date') >= 0) {
                    value = formatDate(value);
                }

            }
            if (key.indexOf('Amount') != -1 || key.indexOf('ExchangeRate') != -1) {
                if (row.Status == 'P') {
                    value = '---';
                } else if (row.Gateway == 'BTC' && key == 'Amount') {
                    value = formatCurrencyBTC(value);
                } else {
                    value = formatCurrency(value);
                }

                addClass = 'align-right';
            } else if (key.indexOf('CommentID') >= 0) {
                tdClass = 'cl-CommentID';
            }
            else if (key.indexOf('FilePath') != -1) {
                value = '<a href="http://cimages.heritagesports.com:8082/' + value.replace('////CCFS/Inetpub/testIMG/', '') + '" target="_blank">' + value.replace('////CCFS/Inetpub/testIMG/', '') + '</a>';
            }
            else if (key == 'Status') {
                if (value == 1) {
                    value = 'Passed';
                } else if (value == 2) {
                    value = 'Fail';
                } else {
                    value = 'Pending';
                }
            } else if (key == 'StatusReason') {
                if (row.Status != 1 && row.Status != 2) {
                    value = '<select class="txtAreaReason">' +
                                '<option value="">--select one reason--</option>' +
                                '<option value="Copies are not clear, please resend information.">Copies are not clear, please resend information</option>' +
                                '<option value="Information is not matching with the information in our database.">Information is not matching with the information in our database.</option>' +
                                '<option value="Invalid or expired ID sent.">Invalid or expired ID sent</option>' +
'<option value="Wrong document sent for this particular step in validation process.">Wrong document sent for this particular step in validation process</option>' +
                                '<option value="Authorization form is not signed. Please resend all 3 documents again.">Authorization form is not signed. Please resend all 3 documents again.</option>' +
                     
                               
                      '<option value="Please indicate which of these phone numbers you would like us to change your phone number to.">Please indicate which of these phone numbers you would like us to change your phone number to.</option>' +
        
                                '<option value="">---------Cards Issues---------</option>' +
                                '<option value="You are missing the front copy of the card, please resend all 3 documents again.">You are missing the front copy of the card, please resend all 3 documents again.</option>' +
                                '<option value="You are missing the back copy of the card, please resend all 3 documents again.">You are missing the back copy of the card, please resend all 3 documents again.</option>' +
                        '<option value="Discover cards are not accepted by our processors.">Discover cards are not accepted by our processors</option>' +
                        
'<option value="Your card was received, but we are still missing the Authorization form signed.">Your card was received, but we are still missing the Authorization form signed</option>' +
    '<option value="Card does not belong to the account holder.">Card does not belong to the account holder</option>' +
                                                            
                        '<option value="We need the front and back of the card, and the authorization form signed. Please resend all 3 documents.">We need the front and back of the card, and the authorization form signed. Please resend all 3 documents.</option>' +
                   '<option value="">---------Utility bill---------</option>' +
 '<option value="Utility bill required.">Utility bill required.</option>' +
                        '<option value="Phone bill required.">Phone bill required</option>' +
                                '<option value="To update your phone number you must first revalidate your account information via e-mail">To update your phone number you must first revalidate your account information via e-mail</option>' +

                        '<option value="Utility bill under your name is required to update your address.">Utility bill under your name is required to update your address.</option>' +
                                     '<option value="">---------Changed---------</option>' +
                        '<option value="Your phone number has already been changed.">Your phone number has already been changed.</option>' +
                        '<option value="Your address has already been changed.">Your address has already been changed.</option>' +
                         
                                '</select>';
                }


            }
            if (key.indexOf('CustomerID') != -1) {
                customerid = value;
            }
            if (key.indexOf('FileType') != -1) {
                typefile = value;
            }
            if (key.indexOf('Description') != -1) {
                if (row.FileType) {
                    var x = value.replace('To validate Credit Card: ', '');
                    bin = x.split(' ')[1];
                    l4 = x.split(' ')[3];
                    cardtype = x.split(' ')[5];
                    ExpDate = x.split(' ')[7];
                    rowId = row.ID;
                    if (cardtype === "American") {
                        cardtype = "American Express";
                        ExpDate = x.split(' ')[8];
                    }

                }

            }

            $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
            if (key.indexOf('DateEntered') != -1) {
                if (row.Status == 0) {
                    $(newRow).append('<td><input type="button" data-customer="' + customerid + '" data-typefile="' + typefile + '"data-bin="' + bin + '" data-l4="' + l4 + '" data-cardtype="' + cardtype + '" data-expdate="' + ExpDate + '" data-rowid="' + rowId + '" value="Pass" class="changeStatus"></td>');
                    $(newRow).append('<td><input type="button" data-customer="' + customerid + '" data-typefile="' + typefile + '"data-bin="' + bin + '" data-l4="' + l4 + '" data-cardtype="' + cardtype + '" data-expdate="' + ExpDate + '" data-rowid="' + rowId + '" value="Fail" class="changeStatus"></td>');
                } else {
                    $(newRow).append('<td></td>');
                    $(newRow).append('<td></td>');
                }
            }
        }
    });

    $(newRow).append('</tr>');

    $(table).append(newRow);
}