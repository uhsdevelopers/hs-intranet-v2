﻿$(function () {

    getAllSportsList();
    $('#selSportTypeList').change(function () {
        getSportDetailsList($('#selSportTypeList').val(), $('#selSportSubTypeList'));
    });


    $('#getInfoToExcel').click(function () {

        var startDate = document.getElementById('datepickerStart').value,
          endDate = document.getElementById('datepickerEnd').value,
          customerid = document.getElementById('txtCustomerid').value,
          ishs = checkedRadioBtn('TType2');


        var url;
        var data;

        url = "/Wagering/GetLiveBettingReport",
           data = {
               dateStart: startDate + ' 12:00 AM',
               dateEnd: endDate + ' 23:59 PM',
               tw: ishs,
               siteId: "Heritage",
               customerid: customerid
           };

        ajaxConnexion(data, url, function (result) {
            JSONToCSVConvertor(result, "Details", true);
        });

    });

    $(document).ready(function() {
        getData();
        getDataLog()
    });


    $('#getInfoToExcelAverage').click(function () {

        var site = checkedRadioBtn('TType');


        var customerid = document.getElementById('txtCustomerid').value;
        var startDate, endDate, valueFrom, valueTo, sportSubtype;
        startDate = document.getElementById('datepickerStart').value;
        endDate = document.getElementById('datepickerEnd').value;
        valueFrom = document.getElementById('txtvalueFrom').value;
        valueTo = document.getElementById('txtvalueTo').value;
        sportSubtype = document.getElementById('selSportSubTypeList').value;



        var url;
        var data;

        url = "/Wagering/GetAverageBalances",
           data = {
               masterAgent: site,
               startDate: startDate,
               endDate: endDate,
               valueFrom: valueFrom,
               valueTo: valueTo,
               customerid: customerid,
               sportSubtype: sportSubtype


           };

        ajaxConnexion(data, url, function (result) {
            JSONToCSVConvertor(result, "Details", true);
        });

    });




});

function getData() {



    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/wagering/Getscoreslines",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}


function getSOData() {


    
    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;


    url = "/wagering/GetSOScores",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary2'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}


function DeleteCustomerScores() {

    var customerid = document.getElementById('txtCustomerid').value;
    var rotnum = document.getElementById('txtRotNum').value;

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;


    url = "/wagering/DeleteCustomerScores",
       data = {
           customerid: customerid,
           rotnum: rotnum
       };


    //loader();

    ajaxConnexion(data, url, function (result) {
        // printDynamicTable($('#PokerSummary2'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
        if (result[0].status == "1")
        {
            alert("Deleted");
            getCustomScore();
        }
        
        //loader();
    });
}

function getCustomScore() {


    var customerid = document.getElementById('txtCustomerid').value;
    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/wagering/GetCustomerScores",
       data = {
           customerid: customerid
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#ScoresSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)






        // end loader
        loader();
    });
}


function getDataLog() {



    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/wagering/GetScoreslog",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#LogSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}


function getAnalysisData() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value,
        customerid = document.getElementById('txtCustomerid').value,
    customerProfile = checkedRadioBtn('TType'),
    store = checkedRadioBtn('WType'),
    bussinesUnit = checkedRadioBtn('BType');


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/DepositBonus/GetDepositAndBonus",
       data = {
           dateStart: startDate,
           dateEnd: endDate,
           customerid: customerid,
           store: store,
           cProfile: customerProfile,
           businessUnit: bussinesUnit,
           timeout: 360
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        $('#PokerSummary').tablesorter();



        // end loader
        loader();
    });
}

function getLiveBets() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value,
        ishs = checkedRadioBtn('TType2');

    // summary

    if (document.getElementById("all").checked == true) {
        startDate = null;
        endDate = null;
    }



    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/Wagering/LiveBets",
       data = {
           dateStart: startDate + ' 12:00 AM',
           dateEnd: endDate + ' 23:59 PM',
           ishs: ishs
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        $('#PokerSummary').tablesorter();



        // end loader
        loader();
    });
}



function getLiveBettingper() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value,
        customerid = document.getElementById('txtCustomerid').value,
        ishs = checkedRadioBtn('TType2');


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/Wagering/GetLiveBettingReport",
       data = {
           dateStart: startDate + ' 12:00 AM',
           dateEnd: endDate + ' 23:59 PM',
           tw: ishs,
           siteId: "Heritage",
           customerid: customerid
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        $('#PokerSummary').tablesorter();



        // end loader
        loader();
    });
}


function getWData() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value,
        customerid = document.getElementById('txtCustomerid').value,
    quantity = document.getElementById('txtquantity').value;



    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/Wagering/GetwagerCount",
       data = {
           customerid: customerid,
           dateStart: startDate + ' 12:00 AM',
           dateEnd: endDate + ' 23:59 PM',
           quantity: quantity
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDinamicTable(table, object)





        // end loader
        loader();
    });
}



function getDepositReport() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value,
        customerid = document.getElementById('txtCustomerid').value,
        method = checkedRadioBtn('MType'),
        site = checkedRadioBtn('TType');


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/Wagering/GetDepositReport",
       data = {
           dateStart: startDate,
           dateEnd: endDate,
           method: method,
           customerid: customerid,
           siteId: site
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}



function getPayoutReport() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value,
        customerid = document.getElementById('txtCustomerid').value,
        method = checkedRadioBtn('MType'),
        site = checkedRadioBtn('TType');


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/Wagering/GetPayoutReport",
       data = {
           dateStart: startDate,
           dateEnd: endDate,
           method: method,
           customerid: customerid,
           siteId: site
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}




function getNegative() {


    var site = checkedRadioBtn('TType');


    var customerid = document.getElementById('txtCustomerid').value;



    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/Wagering/GetNegativeBalances",
       data = {
           customerid: customerid,
           isCredit: "N",
           siteId: site

       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDinamicTable(table, object)





        // end loader
        loader();
    });
}

function getAverage() {


    var site = checkedRadioBtn('TType');


    var customerid = document.getElementById('txtCustomerid').value;
    var startDate, endDate, valueFrom, valueTo, sportSubtype;
    startDate = document.getElementById('datepickerStart').value;
    endDate = document.getElementById('datepickerEnd').value;
    valueFrom = document.getElementById('txtvalueFrom').value;
    valueTo = document.getElementById('txtvalueTo').value;
    sportSubtype = document.getElementById('selSportSubTypeList').value;

    //GetAverageBalances(string masterAgent, string startDate, string endDate, string valueFrom, string valueTo, string sportSubtype =null)

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;


    url = "/Wagering/GetWagerAmountByRange",
       data = {
           masterAgent: site,
           startDate: startDate,
           endDate: endDate,
           valueFrom: valueFrom,
           valueTo: valueTo,
           customerid: customerid,
           sportSubtype: sportSubtype


       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDinamicTable(table, object)





        // end loader
        loader();
    });
}

function getSportDetailsList(sport, select) {
    select.empty();
    var url = "/Wagering/GetSportssubtype",
        data = {
            sport: sport
        };

    ajaxConnexion(data, url, function (result) {

        printDynamicSelect(select, result, 1, 1, false); // Intranet.js  function printDynamicSelect(table, object, value column number, text column number, duplicates)

    });

}

function getAllSportsList() {

    var url = "/Wagering/GetSports",
        data = {

        };

    ajaxConnexion(data, url, function (result) {

        printDynamicSelect($('.selSportTypeList'), result, 0, 0, false); // Intranet.js  function printDynamicSelect(table, object, value column number, text column number, duplicates)

    });

}




function getCasinoData() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetCasinoData",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}

function getCasinoProfitData() {


    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/LiveDealer/GetCasinoProfitTotal",
       data = {
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#CasinoProfitSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





        // end loader
        loader();
    });
}