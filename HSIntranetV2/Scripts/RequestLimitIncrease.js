﻿$(function () {
    
    $('#btn-submit').click(function () {
        GetRequest();
    });

    $('#tbl-result').on('click', '.btn-update', function () {
        UpdateRequest(this);
    });


    $(document).keypress(function (event) {
        if (event.which == 13) {
            GetRequest();
        }  
    });
});

function GetRequest() {

    var url = "/RequestLimitIncrease/GetRequest/",
        data = {
            From: $('#datepickerStart').val(),
            To: $('#datepickerEnd').val(),
            CustomerId: $('#customerId').val(),
            Status: $('#status').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTableRequestLimit($('#tbl-result'), result, '', '');
        // end loader
        loader();

    });

}


function printDynamicTableRequestLimit(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeaderRequestLimit(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createRowRequestLimit(table, result[i]);
    }
}
function createHeaderRequestLimit(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
       
            $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });
    $(newRow).append('<th class="' + addClass + '">Action</th>');
    $(table).children('thead').append(newRow);

}

function createRowRequestLimit(table, row) {

    var newRow = $('<tr>'),
        addClass = '',
        clerk = $('#cleck').val();

    $.each(row, function (key, value) {
        var tdClass = '';
        value = (value == null) ? '' : value;
        if (key == "Status") {
            addClass = 'align-right';
            $(newRow).append('<td>' + CreateStatusSelect(value) + '</td>');
        } else if (key == "Comments") {
            $(newRow).append('<td>' + CreateComments(value, row.Status) + '</td>');
        } else if (key == "DateTimeRequest") {
            $(newRow).append('<td>' + formatDate(value) + '</td>');
        } else {
            $(newRow).append('<td>' + value + '</td>');
        }
    });
    if (row.Status == 'P' || row.Status == 'I') {
        $(newRow).append('<td><input name="submit" type="submit" value="Update" data-row="'+row.ID+'" data-clerk="'+clerk+'" class="btn-update green submit" style="float:right;margin:5px; width:82px;"></td>');
    } else {
        $(newRow).append('<td></td>');
    }
    
    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}

function CreateStatusSelect(status) {
    if (status == 'P') {
        return '<select class="requestStatus"><option value="P" selected>Pending</option><option value="I">In Progress</option><option value="A">Approved</option><option value="D">Declined </option></select>';
    } else if (status == 'I') {
        return '<select class="requestStatus"><option value="P">Pending</option><option value="I" selected>In Progress</option><option value="A">Approved</option><option value="D">Declined </option></select>';
    } else if (status == 'N') {
        return  'N/A';
    } else if (status == 'A') {
        return  'Approved';
    } else {
        return 'Declined';
    }
}

function CreateComments(Comments, status) {
    if (status == 'P' || status == 'I') {
        return '<textarea class="requestComments">' + Comments + '</textarea>';
    } else {
        return Comments;
    }   
}


function UpdateRequest($this) {
    var url = "/RequestLimitIncrease/UpdateRequest/",
    data = {
        Id: $($this).data('row'),
        UserAction: $($this).data('clerk'),
        Status: $($($this).closest('tr').find('.requestStatus')).val(),
        Comments: $($($this).closest('tr').find('.requestComments')).val()
    };
    if ($($($this).closest('tr').find('.requestStatus')).val() == 'D' && $($($this).closest('tr').find('.requestComments')).val() == '') {
        alert('Please insert a Comment');
    } else {
        ajaxConnexion(data, url, function(result) {
            GetRequest();
        });
    }
}