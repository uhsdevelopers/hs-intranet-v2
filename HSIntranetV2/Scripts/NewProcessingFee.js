﻿$(function () {
    
    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');

        GetWWProcessingFees();
    });
    $('#btn-submitExcel').click(function () {

        $('#span-totalPlayers').text('0');

        GetWWProcessingFeesExcel();
    });

});

function GetWWProcessingFees() {

    var url = "/NewProcessingFee/GetNewProcessingFees/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');
        GetNewProcessingFeesExcluded();
        if (result.length == 0) 
            $('.span-totalPlayers').text('Total: 0 Players');
        else
            $('.span-totalPlayers').text('Total: ' + ($('#tbl-result tr').length - 1) + ' Players');

        // end loader
  

    });

}
function GetNewProcessingFeesExcluded() {

    var url = "/NewProcessingFee/GetNewProcessingFeesExcluded/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()
        };

    // start loader


    ajaxConnexion(data, url, function (result) {
        $('.h2_title').show();
        printDynamicTable($('#tbl-result2'), result, '', '');

        if (result.length == 0)
            $('.span-totalPlayers').text('Total: 0 Players');
        else
            $('.span-totalPlayers').text('Total: ' + ($('#tbl-result2 tr').length - 1) + ' Players');

        // end loader
      
        loader();
    });

}

function GetWWProcessingFeesExcel() {

    var url = "/NewProcessingFee/GetNewProcessingFees/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()
        };

  
    ajaxConnexion(data, url, function (result) {
        if (result.length == 0)
            $('.span-totalPlayers').text('Total: 0 Players');
        else
            JSONToCSVConvertor(result, "Processing Fees Report", true);
            
        // end loader
       

    });

    var url = "/NewProcessingFee/GetNewProcessingFeesExcluded/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        if (result.length == 0)
            $('.span-totalPlayers').text('Total: 0 Players');
        else
            JSONToCSVConvertor(result, "Excluded Processing Fees Report", true);

        // end loader
        loader();

    });

}



function printDynamicTableAcc_WWProcessingFees(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeader(table, keys);

    var totalAmount = 0,
        totalFees = 0;
    for (i = 0, length = result.length; i !== length; i++) {
        createRow(table, result[i]);
        totalAmount += result[i]['Amount'];
        totalFees += result[i]['Total Fee'];
    }
    $('.span-totalAmount').text('Total Amount: ' + totalAmount.toFixed(4));
    $('.span-totalFees').text('Total Fees: ' + totalFees.toFixed(4));
}

