﻿$(function () {
    
    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');

        GetWWProcessingFees();
    });

});

function GetWWProcessingFees() {

    var url = "/WW_ProcessingFees/GetWWProcessingFees/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTableAcc_WWProcessingFees($('#tbl-result'), result, '', '');

        if (result.length == 0) 
            $('.span-totalPlayers').text('0 Players');
        else
            $('.span-totalPlayers').text(($('#tbl-result tr').length - 1));

        // end loader
        loader();

    });

}
function printDynamicTableAcc_WWProcessingFees(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeader(table, keys);

    var totalAmount = 0,
        totalFees = 0,
        totalAFFees = 0,
        totalWWfFees = 0,
        totalHSFees = 0;

    for (i = 0, length = result.length; i !== length; i++) {
        createRow(table, result[i]);
        totalAmount += result[i]['Amount'];
        totalFees += result[i]['Total Fee'];
        totalAFFees += result[i]['AF Fee'];
        totalWWfFees += result[i]['WW Fee'];
        totalHSFees += result[i]['HS Fee'];
    }
    $('.span-totalAmount').text(formatCurrency(totalAmount));
    $('.span-totalFees').text(formatCurrency(totalFees));
    $('.span-totalAfFees').text(formatCurrency(totalAFFees));
    $('.span-totalWWFees').text(formatCurrency(totalWWfFees));
    $('.span-totalHsFees').text(formatCurrency(totalHSFees));
}

