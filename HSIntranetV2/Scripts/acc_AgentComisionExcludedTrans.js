﻿$(function () {

    $('#btn-submit').click(function () {
       
        // clear old data
        $('#tbl-result').empty();
        $('#tbl-result2').empty();
        $('#span-totalExcluded').text('Processing...');
        $('#span-totalPreApproved').text('Processing...');

        GetAgentComisionExludedTransaction();
        GetAgentComisionExludedTransaction2();
        $('h2').show();
    });

});

function GetAgentComisionExludedTransaction() {

    var url = "/Acc_WWExcludedTrans/GetAgentComisionExludedTransaction/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            Agent: $('#agent').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTableExcludeTrans($('#tbl-result'), result, '', '');

        if (result.length == 0)
            $('#span-totalExcluded').text('Rows: 0');
        else
            $('#span-totalExcluded').text('Rows: ' + result.length);

        
    });

}
function GetAgentComisionExludedTransaction2() {

    var url = "/Acc_WWExcludedTrans/GetAgentComisionExludedTransactionPre/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            Agent: $('#agent').val()
        };

  

    ajaxConnexion(data, url, function (result) {

        printDynamicTableExcludeTrans($('#tbl-result2'), result, '', '', '');

        if (result.length == 0)
            $('#span-totalPreApproved').text('Rows: 0');
        else
            $('#span-totalPreApproved').text('Rows: ' + result.length);

        // end loader
        loader();

    });

}


function printDynamicTableExcludeTrans(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title

    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeaderExcludeTrans(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createRowExcludeTrans(table, result[i]);
    }
}


function createHeaderExcludeTrans(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });

    $(table).children('thead').append(newRow);

}

function createRowExcludeTrans(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Amount') != -1 ) {
            //if ((/\S/.test(value))) {
            //    value = formatCurrency(value);
            //}
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}