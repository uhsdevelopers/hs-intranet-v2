﻿
$(function () {

    var $menu = $('.menu-info, .menu-deposit'),
        $menuInfo = $('.menu-info'),
        $menuDeposit = $('.menu-deposit'),
        $infoReport = $('.info-report'),
        $depositReport = $('.deposit-report'),
        $selectCust = $('#selectCust'),
        $password = $('#pass'),
        $creditLimit = $('#creditLimit'),
        $tempCreditLimit = $('#tempCreditLimit'),
        $creditLimitDate = $('#datepickerStart'),
        $isActiveCredit = $('#isActive'),
        $SaveInfo = $('#Save-Info'),
        $saveMessageInfo = $('#saveMessage-info'),
        $errorMessageInfo = $('#errorMessage-info'),
        $saveMessageDeposit = $('#saveMessage-deposit'),
        $errorMessageDeposit = $('#errorMessage-deposit'),
        $dfDate = $('#datepickerEnd'),
        $Amount = $('#Amount'),
        $tranComments = $('#tranComments'),
        $SaveDeposit = $('#Save-Deposit');

    $("#main-conteiner-report").tabs();

    $SaveInfo.on('click', function () {
        var data = {
                customerID: $selectCust.val(),
                password: $password.val(),
                creditLimit: $creditLimit.val(),
                TempCreditAdj: $tempCreditLimit.val(),
                TempCreditAdjExpDate: $creditLimitDate.val(),
                Active: ($isActiveCredit.is(":checked")) ? 'Y' : 'N'
            },
            url = 'Shortcut/setCustomerMaintenance/';

        if (data.customerID == 'Select Customer') {

            $errorMessageInfo.text('Please select a customer.');

            $errorMessageInfo.fadeIn("slow", function () {
                $errorMessageInfo.fadeOut(8000);
            });

            return;
        }

        $errorMessageInfo.text('An error has occurred.');

        ajaxConnexion(data, url, function (result) {

            if (result.length > 0) {
                if (result[0].ErrorNumber == 0) {

                    $('#selectCust').val('Select Customer');
                    $('.chosen-single span').text('Select Customer');

                    $saveMessageInfo.fadeIn("slow", function () {
                        $saveMessageInfo.fadeOut(8000);
                    });
                }
                else {
                    $errorMessageInfo.fadeIn("slow", function () {
                        $errorMessageInfo.fadeOut(8000);
                    });
                }
            }
            else {
                $errorMessageInfo.fadeIn("slow", function () {
                    $errorMessageInfo.fadeOut(8000);
                });
            }
        });
    });
    $SaveDeposit.on('click', function () {
        var data = {
                customerID: $selectCust.val(),
                TranCode: $("input[name='radiox']:checked").data('trancode'),
                TranType: $("input[name='radiox']:checked").val(),
                Amount: $Amount.val(),
                Description: $tranComments.val(),
                //DailyFigureDate: $dfDate.val(),
                isFreePlay: ($('input[name="radiox"]:checked').prop('class') == 'FreePlay') ? '1' : '0'
            },
            url = 'Shortcut/setCustomerDeposit/';

        if (data.customerID == 'Select Customer') {

            $errorMessageDeposit.text('Please select a customer.');

            $errorMessageDeposit.fadeIn("slow", function () {
                $errorMessageDeposit.fadeOut(8000);
            });

            return;
        }

        $errorMessageDeposit.text('An error has occurred.');

        ajaxConnexion(data, url, function (result) {

            if (result.length > 0) {
                if (result[0].CurrentBalance != null) {

                    $selectCust.val('Select Customer');
                    $Amount.val('');
                    $tranComments.val('');


                    $('.chosen-single span').text('Select Customer');

                    $saveMessageDeposit.text('New balance of: $' + result[0].CurrentBalance);

                    $saveMessageDeposit.fadeIn("slow", function () {
                        $saveMessageDeposit.fadeOut(8000);
                    });
                }
                else {
                    $errorMessageDeposit.fadeIn("slow", function () {
                        $errorMessageDeposit.fadeOut(8000);
                    });
                }
            }
            else {
                $errorMessageDeposit.fadeIn("slow", function () {
                    $errorMessageDeposit.fadeOut(8000);
                });
            }
        });
    });
    $selectCust.on('change', function () {
        var data = {
            customerId: $selectCust.val()
        },
            url = 'Shortcut/getCustomerMaintenance/';
        $password.val('');
        $creditLimit.val('');
        $tempCreditLimit.val('');
        $creditLimitDate.val('');
        $isActiveCredit.prop("checked", false);
        ajaxConnexion(data, url, function (result) {

            if (result.length > 0) {
                $password.val(result[0].Password);
                $creditLimit.val(result[0].CreditLimit);
                $tempCreditLimit.val(result[0].TempCreditAdj);
                if (result[0].TempCreditAdjExpDate != null) $creditLimitDate.val(formatDate(result[0].TempCreditAdjExpDate).split(' ')[0]);
                if (result[0].Active == "Y") { $isActiveCredit.prop("checked", true);
                } else { $isActiveCredit.prop("checked", false);
                }
            }
        });
    });

    getCustomersByAgent();
    
    function getCustomersByAgent() {
        var url = 'Shortcut/GetCustomersByAgent/',
            data = {};

        ajaxConnexion(data, url, function (result) {
            if (result.length == 0) return;
            custList(result, function () {
                $('#selectCust').chosen();
            });
        });

    }

});
