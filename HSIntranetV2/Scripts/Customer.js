﻿$(function () {

    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');
        $('#span-totalCC').text('0');
        $('#span-totalInternet').text('0');

        getHeadCount();
    });

});


function UpdateNewWeb() {
    var site = checkedRadioBtn('TType');


    var customerid = document.getElementById('txtCustomerid').value;
    var url;
    var data;

    url = "/Customer/UpdateCustomer",
       data = {
           customerid: customerid,
           IsNewWeb: site
       };

    loader();

    ajaxConnexion(data, url, function(result) {

        $('#PokerSummary').show();
        $('#PokerSummary').empty();

        if (result.length == 0) return;
        if (result[0].Column1 == 1) {
            alert("Updated");

            $('#PokerSummary').append('<td > Customer updated </td>');
        } else {
            alert("Error");
            $('#PokerSummary').append('<td > Error updating </td>');
        }

        loader();

    }
    );
}

function UpdateNewFE() {
    var site = checkedRadioBtn('TFE');
    var customerid = document.getElementById('txtCustomeridFE').value;

    var url;
    var data;

    url = "/Customer/UpdateCustomerFe",
       data = {
           customerid: customerid,
           isNewFe: site


       };

    loader();

    ajaxConnexion(data, url, function (result) {

        $('#PokerSummary').show();

        // clear old data
        $('#PokerSummary').empty();

        // print title
        //$(idTitle).text(title);

        if (result.length == 0) return;
        if (result[0].Column1 == 1) {
            alert("Updated");

            $('#PokerSummary').append('<td > Customer updated </td>');
        } else {
            alert("Error");
            $('#PokerSummary').append('<td > Error updating </td>');
        }


        // printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)


        // end loader
        loader();

    }
    );
}


function GetInfo() {
    var customerid = document.getElementById('txtCustomerid').value;
    var url;
    var data;

    url = "/Customer/GetCustomerInformation",
        data = {
            customerid: customerid
        };

    loader();

    ajaxConnexion(data, url, function (result) {

            $('#PokerSummary').show();
            $('#PokerSummary').empty();

        if (result.length == 0) return;
       // printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
        if (result[0].NewSite == "Y") {
                alert("New Site");

                $('#PokerSummary').append('<td > Customer On New Site </td>');
            } else {
                alert("Old Site");
            $('#PokerSummary').append('<td > Customer On Old Site </td>');
            }

            loader();

        }
    );
}


function GetRewardByCustomer() {
    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;
    customerId = document.getElementById('txtCustomerid').value;
    var url;
    var data;

    url = "/csd/GetCashRewardCustomerEligibleBets",
        data = {
        customerid: customerId,
        StartDate: startDate,
        EndDate: endDate
        };

    loader();

    ajaxConnexion(data, url, function (result) {

            $('#PokerSummary').show();
            $('#PokerSummary').empty();

            if (result.length == 0) return;
             printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
            

            loader();

        }
    );
}



    function GetNewName() {
        var wasused = checkedRadioBtn('TType');
        var country = checkedRadioBtn('TCountry');

        var url;
        var data;

        url = "/Customer/GetFakeName",
           data = {
               wasused: wasused,
               country: country


           };

        loader();

        ajaxConnexion(data, url, function (result) {

            $('#PokerSummary').show();

  

            printDynamicTableS($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





            // end loader
            loader();
        }
        )
    }

    function printDynamicTableS(table, result, idTitle, title) {

        $(table).show();

        // clear old data
        $(table).empty();

        // print title
        $(idTitle).text(title);

        if (result.length == 0) return;

        // create table head body
        $(table).append('<thead></thead>');
        $(table).append('<tbody></tbody>');

        // table title row
        var keys = Object.keys(result[0]);
        createRow(table, keys);

        for (i = 0, length = result.length; i !== length; i++) {
            createRow2(table, result[i]);
        }
    }


    function createRow(table, row) {

        var newRow = $('<tr>'),
            addClass = '';

        $.each(row, function (key, value) {
            var tdClass = '';
            if (isNaN(value)) {
                if (value.indexOf('/Date') >= 0) {
                    value = formatDate(value);
                }

            } else if (key.indexOf('Amount') != -1) {
                value = formatCurrency(value);
                addClass = 'align-right';
            } else if (key.indexOf('CommentID') >= 0) {
                tdClass = 'cl-CommentID';
            }

            $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
        });
        $(newRow).append('<td class="' + addClass + '"> </td>');
        $(newRow).append('<td class="' + addClass + '">Mark as Used</td>');
        $(newRow).append('<td class="' + addClass + '"> </td>');
        $(newRow).append('<td class="' + addClass + '"> </td>');
        $(newRow).append('</tr>');

        $(table).append(newRow);
    }

    function createRow2(table, row) {

        var newRow = $('<tr>'),
            addClass = '';

        $.each(row, function (key, value) {
            var tdClass = '';
            if (isNaN(value)) {
                if (value.indexOf('/Date') >= 0) {
                    value = formatDate(value);
                }

            } else if (key.indexOf('Amount') != -1) {
                value = formatCurrency(value);
                addClass = 'align-right';
            } else if (key.indexOf('GameNum') >= 0) {
                tdClass = 'cl-CommentID';
            }

            $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
            

        });

        $(newRow).append('<td class="' + addClass + ' ">');
        $(newRow).append('<input id="checkBox" type="checkbox" name="update">');

        $(newRow).append('<td class="' + addClass + ' ">');
        $(newRow).append('<input type="button" name="update"  value="submit" onClick="UpdateName(' + row.id + ')">');

        $(newRow).append('</td></tr>');

        $(table).append(newRow);
    }


    function UpdateName(ID) {

  

        var wasused = (document.getElementById('checkBox').checked) ? '1' : '0';

        var url;
        var data;

        url = "/Customer/UpdateFakeName",
           data = {
               ID: ID,
               wasused: wasused
           };



        // start loader
        loader();

        ajaxConnexion(data, url, function (result) {
            printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)





            // end loader
            loader();
        });
    }
;


    
    

    