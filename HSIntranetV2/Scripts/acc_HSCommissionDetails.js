﻿$(function () {

    $('#btn-submit').click(function () {

        getHSComissions();
    });

    $('#btn-submit-commSummary').click(function () {

        getAcc_GetAgentsCommissionSummary();

    });

    $('#btn-submit-feeDetails').click(function () {

        getAcc_GetFeeDetalisByAgent();

    });

    var agentID = getParameterByName('agentID');

    if (agentID != null && agentID.length > 0) {

        $('#datepickerStart').val(getParameterByName('dateFrom'));
        $('#datepickerEnd').val('');
        $('#txb-agentid').val(getParameterByName('agentID').replace(' Fee', ''));

        getAcc_GetFeeDetalisByAgent();

    }

});

function getAcc_GetFeeDetalisByAgent() {

    var url = "/Acc_HSCommissionDetails/GetAcc_GetFeeDetalisByAgent/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()
        };
    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');

        if (result.length == 0)
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        summary(result);

        // end loader
        loader();

    });

}

function getAcc_GetAgentsCommissionSummary() {

    var url = "/Acc_HSCommissionDetails/GetAcc_GetAgentsCommissionSummary/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            agentID: $('#txb-agentid').val()
        };
    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printCommDetailsTable($('#tbl-result'), result, '', '');

        loader();

    });

    var originalDate = $('#datepickerStart').val();
    if (originalDate == undefined) return;
    var daysToAdd = 6;
    var newDate = addDaysToDateString(originalDate, daysToAdd);

    $('#from').text(originalDate);
    $('#to').text(newDate);

}
function getAcc_GetCommissionDetalisByAgent() {

    var agentID = getParameterByName('agentID').replace(' Fee', '');
    var dateFrom = getParameterByName('dateFrom');

    var url = "/Acc_HSCommissionDetails/GetAcc_GetCommissionDetalisByAgent/",
        data = {
            dateFrom: dateFrom,
            agentID: agentID

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');

        if (result.length == 0)
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        summary(result);

        // end loader
        loader();

    });

}

function printCommDetailsTable(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        createCommDetailsRow(table, result[i]);
    }
}

function createCommDetailsRow(table, row) {

    var weekDate = $('#datepickerStart').val(),
        isFee = (row.Agent.indexOf('Fee') > -1) ? 'true' : 'false',
        newRow = '';

    if(isFee == 'true')
        newRow = $('<tr><td>' + row.Agent + '</td><td><span style="cursor:pointer;font-weight:bold;" onclick="window.open(\'/Acc_HSCommissionDetails/FeeDetails?dateFrom=' + weekDate + '&agentID=' + row.Agent + '\', \'_blank\')" >' + formatCurrencyNegativeRed(row.Amount) + '</span></td></tr>');
    else if($('#txb-agentid').val().indexOf('*') > -1)
    {
        //newRow = $('<tr><td>' + row.Agent + '</td><td><span style="cursor:pointer" onclick="window.open(\'/Acc_HSCommissionDetails?dateFrom=' + weekDate + '&agentID=' + row.Agent + '\', \'_blank\')" >' + formatCurrency(row.Amount) + '</span></td></tr>');
        newRow = $('<tr><td>' + row.Agent + '</td><td><span >' + formatCurrency(row.Amount) + '</span></td></tr>');
    }
    else {
        if (row.Agent == 'TOTAL     ') {
            newRow = $('<tr style="font-weight:bold;"><td>' + row.Agent + '</td><td>' + row.MAusername + '</td><td style="text-align: right;"><span >' + row.CommissionPercentage + '%</span></td><td style="text-align: right;">' + formatCurrencyNegativeRed(row.Commission) + '</td><td style="text-align: right;"><span style="cursor:pointer;" onclick="window.open(\'/Acc_HSCommissionDetails/FeeDetails?dateFrom=' + weekDate + '&agentID=' + row.Agent + '\', \'_blank\')" >' + formatCurrencyNegativeRed(row.Fees) + '</span></td><td style="text-align: right;">' + formatCurrencyNegativeRed(row.Adjustments) + '</td><td style="text-align: right;font-weight:bold;">' + formatCurrencyNegativeRed(row.Balance) + '</td></tr>');
        } else {
            newRow = $('<tr><td>' + row.Agent + '</td><td>' + row.MAusername + '</td><td style="text-align: right;"><span >' + row.CommissionPercentage + '%</span></td><td style="text-align: right;">' + formatCurrencyNegativeRed(row.Commission) + '</td><td style="text-align: right;"><span style="cursor:pointer;" onclick="window.open(\'/Acc_HSCommissionDetails/FeeDetails?dateFrom=' + weekDate + '&agentID=' + row.Agent + '\', \'_blank\')" >' + formatCurrencyNegativeRed(row.Fees) + '</span></td><td style="text-align: right;">' + formatCurrencyNegativeRed(row.Adjustments) + '</td><td style="text-align: right;font-weight:bold;">' + formatCurrencyNegativeRed(row.Balance) + '</td></tr>');
        }
    }

    $(table).children('tbody').append(newRow);
}

function formatCurrencyNegativeRed(value) {

    if (value == null) {
        value = 0;
    }
    value = parseFloat(value);

    var money = value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').replace('-','');

    var stringReturn = "<span>$" + money + "</span>";

    if (value < 0)
        stringReturn = "<span style='color:red;'>($" + money + ")</span>";

    return stringReturn;
}

function getHSComissions() {

    var url = "/Acc_HSCommissionDetails/GetAcc_GetAgentsCommissionSummary/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');

        if (result.length == 0)
            $('#span-totalPlayers').text('0');
        else
            $('#span-totalPlayers').text($('#tbl-result tr').length - 1);

        summary(result);

        // end loader
        loader();

    });

}

function summary(result) {

    var bonusCredit = 0,
        bonusDebit = 0,
        creditAdj = 0,
        debitAdj = 0,
        deposit = 0,
        feesCredit = 0,
        withdrawal = 0,
        total = 0;


    var arr_cols = $('#tbl-result tr td:nth-child(4)');

    $.each(result, function (key, row) {

        switch (row.TranType) {
            
            case 'Promo Credit (Cash Bonus)':
                bonusCredit += row.Fee;
                break;
            case 'Promotional Credit':
                bonusCredit += row.Fee;
                break;
            case 'Promo Debit (Cash Bonus)':
                bonusDebit += row.Fee;
                break;
            case 'Promotional Debit':
                bonusDebit += row.Fee;
                break;
            case 'Credit Adjustment':
                creditAdj += row.Fee;
                break;
            case 'Debit Adjustment':
                debitAdj += row.Fee;
                break;
            case 'Deposit':
                deposit += row.Fee;
                break;
            case 'Fees Credit':
                feesCredit += row.Fee;
                break;
            case 'Withdrawal':
                withdrawal += row.Fee;
                break;

        }

        total += row.Fee;

    });

    $('#B').text(bonusCredit.toFixed(2));
    $('#N').text(bonusDebit.toFixed(2));
    $('#C').text(creditAdj.toFixed(2));
    $('#D').text(debitAdj.toFixed(2));
    $('#E').text(deposit.toFixed(2));
    $('#F').text(feesCredit.toFixed(2));
    $('#I').text(withdrawal.toFixed(2));
    $('#Total').text(total.toFixed(2));



}