﻿$(function () {
    
    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');

        GetWWProcessingFees();
    });
    $('#btn-submitdetails').click(function () {

        $('#span-totalPlayers').text('0');

        GetWWProcessingFeesDetails();
    });

    $('#btn-submit2').on('click', function () {
        var url = "/Acc_WWProcessingFees/GetWWProcessingFees/",
           data = {
               dateFrom: $('#datepickerStart').val(),
               agentID: $('#txb-agentid').val()

           };

        // start loader
        loader();

        ajaxConnexion(data, url, function (result) {

            JSONToCSVConvertor(result, "Fee To Charge By Method ", true);
            loader();
        });
    });
    $('#btn-submit3').on('click', function () {
        var url = "/Acc_WWProcessingFees/GetWWProcessingFeesDetails/",
             data = {
                 dateFrom: $('#datepickerStart').val(),
                 dateTo: $('#datepickerEnd').val(),
                 agentID: $('#txb-agentid').val()

             };

        // start loader
        loader();

        ajaxConnexion(data, url, function (result) {

            JSONToCSVConvertor(result, "Fee Details To Charge By Method ", true);
            loader();
        });
    });

});

function GetWWProcessingFees() {

    var url = "/Acc_WWProcessingFees/GetWWProcessingFees/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            agentID: $('#txb-agentid').val()

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTableAcc_WWProcessingFees($('#tbl-result'), result, '', '');

        if (result.length == 0) 
            $('.span-totalPlayers').text('Total: 0 Players');
        else
            $('.span-totalPlayers').text('Total: ' + ($('#tbl-result tr').length - 1) + ' Players');

        // end loader
        loader();

    });

}

function GetWWProcessingFeesDetails() {

    var url = "/Acc_WWProcessingFees/GetWWProcessingFeesDetails/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTableAcc_WWProcessingFees($('#tbl-result'), result, '', '');

        if (result.length == 0)
            $('.span-totalPlayers').text('Total: 0 Players');
        else
            $('.span-totalPlayers').text('Total: ' + ($('#tbl-result tr').length - 1) + ' Players');

        // end loader
        loader();

    });

}



function printDynamicTableAcc_WWProcessingFees(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeader(table, keys);

    var totalAmount = 0,
        totalFees = 0;
    for (i = 0, length = result.length; i !== length; i++) {
        createRow(table, result[i]);
        totalAmount += result[i]['Amount'];
        totalFees += result[i]['Total Fee'];
    }
    $('.span-totalAmount').text('Total Amount: ' + totalAmount.toFixed(4));
    $('.span-totalFees').text('Total Fees: ' + totalFees.toFixed(4));
}

