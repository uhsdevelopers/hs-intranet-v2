﻿$(function () {
    
    $('#btn-submit').click(function () {

        $('#span-totalPlayers').text('0');

        getRate();
    });

});

function getRate() {

    var url = "/WW_ConvertionRate/GetRate/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');

        getFirstDeposit();

        $('#td-signups').text(result.length);

    });

}

function getFirstDeposit() {

    var url = "/WW_ConvertionRate/GetFirstDeposit/",
        data = {
        },
        amount = 0;


    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-firstDeposit'), result, '', '');

        $('#td-firstdeposits').text(result.length);

        $.each(result, function (key, row) {

            amount += row.DepositAmount;

        });

        $('#td-amount').text(formatCurrency(amount));

        // end loader
        loader();

    });
}