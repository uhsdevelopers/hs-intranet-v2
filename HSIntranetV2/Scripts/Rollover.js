﻿
$(function () {
      
});

function getRollover() {

    var customerID = document.getElementById('customerId').value;

    if (customerID == '' && startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary
    var url = "/Rollover/GetResult/",
        data = {
            customerID: customerID,
            startDate: $('#dateStart').val(),
            endDate: $('#dateEnd').val(),
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-rollover'), result, '', '');
        //printRollover(result);

        // end loader
        loader();
    });

}

function getRolloverCalculation() {

    var customerID = document.getElementById('customerId').value;

    if (customerID == '' ) {
        alert('Please select at least one filter.');
        return;
    }

    // summary
    //https://servicessta.txnservice.com/api/rollover/GetRollOverCalculationValidation?pCustomerID=
    //var url = "https://servicessta.txnservice.com/api/rollover/GetRollOverCalculationValidation",
    var url = "/Rollover/GetCalculationMv2/",
        data = {
            CustomerID: customerID
        };


    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        var responsetoUI = "[" + result + "]";

        printDynamicTable($('#tbl-rollover'), result, '', '');
        //printRollover(result);

        // end loader
        loader();
    });

}

function SetRollOverPayment() {

    var customerID = document.getElementById('customerId').value;
    var Amount = document.getElementById('Amount').value;

    if (customerID == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary
   // var url = "http://vm-intranet02:8020/api/RollOver/SetRollOverPayment",
    var url = "https://servicessta.txnservice.com/api/rollover/SetRollOverPayment",
   // var url ='/rollover/SetRollOverPayment',
        data = {
            pCustomerID: customerID,
            pAmount: Amount
        };

    // start loader
    loader();


    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-rollover'), result, '', '');
        //printRollover(result);

        // end loader
        loader();
    });

}

function GetCalculationDetails() {

    var customerID = document.getElementById('customerId').value;

    if (customerID == '') {
        alert('Please select at least one filter.');
        return;
    }

    //https://servicessta.txnservice.com/api/rollover/GetRollOverCalculationDetails?pCustomerID=
   //var url = "https://servicessta.txnservice.com/api/rollover/GetRollOverCalculationDetails",
   var url = '/rollover/GetCalculationDetails',
        data = {
            CustomerID: customerID

       };

    

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-rollover'), result, '', '');

        loader();
    });

}
