﻿
$(function () {

    getCustomersByAgentWW();

    $('#btn-save').click(function () {
        setQuickLimit();
    });

    $('#selectCust').change(function () {
        gettQuickLimit();
    });

});

function gettQuickLimit() {

    var url = "/WW_CustMaintenance/GetQuickLimitByCust/",
        data = {
            customerID: $('#selectCust').val()

        };

    ajaxConnexion(data, url, function (result) {

        if (result.length > 0) {

            $('#txb-quicklimit').val(result[0].WagerLimit);
        }
        else {

            $('#errorMessage-info').fadeIn("slow", function () {
                $('#errorMessage-info').fadeOut(8000);
            });

        }

    });

}

function setQuickLimit() {

    var url = "/WW_CustMaintenance/SetQuickLimitByCust/",
        data = {
            customerID: $('#selectCust').val(),
            quickLimit: $('#txb-quicklimit').val()

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        if (result.length > 0) {

            if (result[0].ErrorNumber == 0) {

                $('#selectCust').val('Select Customer');
                $('.chosen-single span').text('Select Customer');
                $('#txb-quicklimit').val('');

                $('#saveMessage-info').fadeIn("slow", function () {
                    $('#saveMessage-info').fadeOut(8000);
                });

            }
            else {

                $('#errorMessage-info').fadeIn("slow", function () {
                    $('#errorMessage-info').fadeOut(8000);
                });

            }

        }
        else {

            $('#errorMessage-info').fadeIn("slow", function () {
                $('#errorMessage-info').fadeOut(8000);
            });

        }

        // end loader
        loader();
       
    });

}


