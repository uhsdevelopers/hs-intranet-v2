﻿$(function () {

    $('#btn-submit').click(function () {

        getBalance();
    });
    $('#DownloadInfoToExcel').on('click', function () {
        var company = $('#company').val();
        var url = "/Acc_PlayersWithBalance/GetBalance/",
            data = {
                dateFrom: $('#datepickerStart').val(),
                agentID: $('#txb-agentid').val(),
                report: $('input[name=radio1]:checked').val(),
                withZeroBalance: $('#chk-showAll').prop('checked'),
                company:company
            };
        // start loader

        loader();
        ajaxConnexion(data, url, function (result) {
            JSONToCSVConvertor(result, "Account Balances - " + $('input[name=radio1]:checked').val(), true);
            loader();
        });

    });
});


function printDynamicTableACC(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeaderACC(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        console.log(result[i]);
        createRow(table, result[i]);
    }
}

function createHeaderACC(table, row) {

    var newRow = $('<tr></tr>'),
    addClass = '';

    $.each(row, function (key, value) {
        $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });

    $(table).children('thead').append(newRow);

}

function precise_round(num, decimals) {
    var sign = num >= 0 ? 1 : -1;
    return (Math.round((num * Math.pow(10, decimals)) + (sign * 0.001)) / Math.pow(10, decimals)).toFixed(decimals);
}

function round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
        return Math.round(value);

    value = +value;
    exp = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
        return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}


function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function createRow(table, row) {

    var newRow = $('<tr>'),
        addClass = '';
    var value2 = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Amount') != -1 || key.indexOf('Fee') != -1) {
            if ((/\S/.test(value))) {
                value = addCommas(value);
            }
            addClass = 'align-right';
        } else if (key.indexOf('CommentID') >= 0) {
            tdClass = 'cl-CommentID';
        }

        if (key.indexOf('AgentID') >= 0) {
            //tdClass = 'data-monetary-amount';
            value = addCommas(value);
        }


        if (key.indexOf('Balance') != -1 || key.indexOf('Makeup') != -1) {
            if (value < 0) {
                addClass = 'data-monetary-amount align-right';
                //value = round(value, 2);
                value = precise_round(value, 2);
                value = addCommas(value);
                value2 = value;
                //value2 = '(' + value + ')';
            }
            else {
                addClass = 'align-right';
                value = precise_round(value, 2);
               // value = round(value, 2);
                value = addCommas(value);
                value2 = value;
            }

            $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value2 + '</td>');
        }
        else {

            $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
        }



    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}



function getBalance() {

    var report = $('input[name=radio1]:checked').val();
    var company = $('#company').val();
    var url = "/Acc_PlayersWithBalance/GetBalance/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            agentID: $('#txb-agentid').val(),
            report: report,
            withZeroBalance: $('#chk-showAll').prop('checked'),
            company:company
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        var total = result.length - 1;
        total = addCommas(total);

        if (report == "Player") {

            $('#tbl-resultA').empty();
            printDynamicTableACC($('#tbl-result'), result, '', '');
            $('#tbl-result tbody tr:first-child td:first-child').text('All Players: ');
            //$('#tbl-result tbody tr:first-child td:nth-child(2)').text('' + String(total));
            $('#tbl-result tbody tr:nth-child(2) td:nth-child(1)').text('Postup Players: ');
            $('#tbl-result tbody tr:nth-child(2) td:nth-child(3)').text('Total Postup: ');
            $('#tbl-result tbody tr:nth-child(3) td:nth-child(1)').text('Credit Players: ');
            $('#tbl-result tbody tr:nth-child(3) td:nth-child(3)').text('Total Credit: ');

        } else {
            $('#tbl-result').empty();
            printDynamicTableACC($('#tbl-resultA'), result, '', '');
            $('#tbl-resultA tbody tr:first-child td:first-child').text('All Agents: ');
            //$('#tbl-resultA tbody tr:first-child td:nth-child(2)').text('' + String(total));
            $('#tbl-resultA tbody tr:nth-child(2) td:nth-child(1)').text('Postup Agents: ');
            $('#tbl-resultA tbody tr:nth-child(2) td:nth-child(3)').text('Total Postup: ');
            $('#tbl-resultA tbody tr:nth-child(3) td:nth-child(1)').text('Credit Agents: ');
            $('#tbl-resultA tbody tr:nth-child(3) td:nth-child(3)').text('Total Credit: ');
        }




        // end loader
        loader();

    });

}

