﻿$(function () {
    GetTableInfo();
    SetSelectMasterAgentsearh();
    $('#tb-form-index').on('change','#tableId,.searchByMaster select',function () {
        GetTableInfo();
    });

    $('#btn-submit').click(function () {
        GetTableInfo();
    });
    $('#btn-Export').on('click', function () {
        ExportSAP();
    });
    $('#tbl-result').on('click', '.btn-update', function () {
        var filds = $(this).closest('tr').find('input:not([type="submit"]),.MasterAgentSelec,.TranTypeSelect,.MethodTypeSelect'),
            data = {},
            url = "/SAP_Maintenance/UpdateTableInfo/";
        data.tableid = $('#tableId').val();
        if ($('#tableId').val() == 'HeaderTemplate' || $('#tableId').val() == 'LinesTemplate') {
            data.row = $(filds[1]).data('row');
        } else {
            data.row = $(filds[0]).data('row');
        }
        for (var i = 0; filds.length > i; i++) {
            if ($(filds[i]).hasClass('MasterAgentSelec') && $(filds[i]).val() == 0) {
                alert('Please select a Master Agent');
                $(filds[i]).focus();
                return false;
            }
            if ($(filds[i]).hasClass('TranTypeSelect') && $(filds[i]).val() == 0) {
                alert('Please select a TranType');
                $(filds[i]).focus();
                return false;
            }
            if ($(filds[i]).hasClass('MethodTypeSelect') && $(filds[i]).val() == 0) {
                alert('Please select a MethodType');
                $(filds[i]).focus();
                return false;
            }

            if ($(filds[i]).is('[type="checkbox"]'))
                data['parameter' + i] = $(filds[i]).is(':checked');
            else
                data['parameter' + i] = $(filds[i]).val();
        }
        ajaxConnexion(data, url, function (result) {
            if (result.length != 0) {
                if (result[0].result == "Y") {
                    $("#saveMessage-Bonus").fadeIn("slow", function () {$("#saveMessage-Bonus").fadeOut(4000);});
                }
                else {
                    $("#errorMessage-Bonus").fadeIn("slow", function () {$("#errorMessage-Bonus").fadeOut(4000);});
                }
            }
            GetTableInfo();
        });
    });


    $('#tbl-result').on('click', '.btn-delete', function () {
        var filds = $(this).closest('tr').find('input:not([type="submit"]),.MasterAgentSelec,.TranTypeSelect,.MethodTypeSelect'),
            data = {},
            url = "/SAP_Maintenance/DeleteTableInfo/";
        data.tableid = $('#tableId').val();
        if ($('#tableId').val() == 'HeaderTemplate' || $('#tableId').val() == 'LinesTemplate') {
            data.row = $(filds[1]).data('row');
        } else {
            data.row = $(filds[0]).data('row');
        }
        if (confirm('Are you sure you want to delete this row?')) {
            ajaxConnexion(data, url, function (result) {
                if (result.length != 0) {
                 if (result[0].result == "Y") {
                    $("#saveMessage-Bonus").fadeIn("slow", function () {$("#saveMessage-Bonus").fadeOut(4000);});
                 }
                 else {
                    $("#errorMessage-Bonus").fadeIn("slow", function () {$("#errorMessage-Bonus").fadeOut(4000);});
                 }
                }
             GetTableInfo();
        });
        }
        
    });
});


function ExportSAP() {
    if ($('.searchByMastertoExport select').val() == "0") {
        alert('Please select a Master Agent');
        return false;
    }
    var data = {
        MasterAgentId: $('.searchByMastertoExport option:selected').text(),
        dateFrom: $('#datepickerStart').val(),
        dateTo: $('#datepickerEnd').val(),
        deposits: $('._deposits').prop('checked'),
        ExcludePHNCustomers: $('._AgentExcluded').prop('checked'),
        excludeAgentsTrans: false // not in use
},
        url = "/SAP_Maintenance/ExportSAP_Report/";
    loader();
    ajaxConnexion(data, url, function (result) {
        if (result.length != 0) {
            var table1 = JSON.parse(result.table1.Content),
                table2 = JSON.parse(result.table2.Content);

            if ($('.searchByMastertoExport option:selected').text() == "HSC PHN" || $('.searchByMastertoExport option:selected').text() == "*HSC GROUP*" || $('.searchByMastertoExport option:selected').text() == "** GLOBAL **") {
                table1.unshift({ "JdtNum": "JDT_NUM", "ReferenceDate": "RefDate", "Memo": "Memo", "Reference": "Ref1", "Reference2": "Ref2", "Reference3": "Ref3", "TransactionCode": "TransCode", "TaxDate": "TaxDate", "DueDate": "AutoVAT", "Series": "Series", "Project": "Project" });
                table2.unshift({ "ParentKey": " JdtNum", "LineNum": "LineNum", "AccountCode": "Account", "Debit": "Debit", "Credit": "Credit", "FCDebit": "FCDebit", "FCCredit": "FCCredit", "FCCurrency": "FCCurrency", "ShortName": "ShortName", "LineMemo": "LineMemo", "Reference1": "Ref1", "Reference2": "Ref2", "AdditionalReference": "Ref3Line", "Project": "Project" });
            }
            else {
                table1.unshift({ "JdtNum": "JDT_NUM", "ReferenceDate": "RefDate", "Memo": "Memo", "Reference": "Ref1", "Reference2": "Ref2", "Reference3": "Ref3", "TransactionCode": "TransCode", "TaxDate": "TaxDate", "DueDate": "AutoVAT", "Series": "Series" });
                table2.unshift({ "ParentKey": " JdtNum", "LineNum": "LineNum", "AccountCode": "Account", "Debit": "Debit", "Credit": "Credit", "FCDebit": "FCDebit", "FCCredit": "FCCredit", "FCCurrency": "FCCurrency", "ShortName": "ShortName", "LineMemo": "LineMemo", "Reference1": "Ref1", "Reference2": "Ref2", "AdditionalReference": "Ref3Line"});
            }
            //JSONToCSVConvertor(JSON.parse(JSON.stringify(table1)), "Header", true);
            //JSONToCSVConvertor(JSON.parse(JSON.stringify(table2)), "Details", true);
            JSONToTXTConvertor(JSON.parse(JSON.stringify(table1)), "Header", true);
            JSONToTXTConvertor(JSON.parse(JSON.stringify(table2)), "Lines", true);
        }
        loader();
    });
};


function InsertTableInfo($this) {
    var filds = $($this).closest('tr').find('input:not([type="submit"]),.MasterAgentSelec,.TranTypeSelect,.MethodTypeSelect'),
        data = {},
        url = "/SAP_Maintenance/InsertTableInfo/";
    data.tableid = $('#tableId').val();
    for (var i = 0; filds.length > i; i++) {
        if ($(filds[i]).hasClass('MasterAgentSelec') && $(filds[i]).val() == 0) {
            alert('Please select a Master Agent');
            $(filds[i]).focus();
            return false;
        }
        if ($(filds[i]).hasClass('TranTypeSelect') && $(filds[i]).val() == 0) {
            alert('Please select a TranType');
            $(filds[i]).focus();
            return false;
        }
        if ($(filds[i]).hasClass('MethodTypeSelect') && $(filds[i]).val() == 0) {
            alert('Please select a MethodType');
            $(filds[i]).focus();
            return false;
        }

        if ($(filds[i]).is('[type="checkbox"]'))
            data['parameter' + i] = $(filds[i]).is(':checked');
        else
            data['parameter' + i] = $(filds[i]).val();

    }
    ajaxConnexion(data, url, function (result) {
        if (result.length != 0) {
            if (result[0].result == "Y") {
                $("#saveMessage-Bonus").fadeIn("slow", function () {$("#saveMessage-Bonus").fadeOut(4000);});
            }
            else {
                $("#errorMessage-Bonus").fadeIn("slow", function () {$("#errorMessage-Bonus").fadeOut(4000);});
            }
        }
        GetTableInfo();
    });
};


function GetTableInfo() {
    var url = "/SAP_Maintenance/GetTableInfo/",
        data = {
            tableid: $('#tableId').val(),
            agentId:($('.searchByMaster select').val()== undefined)?0:$('.searchByMaster select').val()
        };
    loader();
    ajaxConnexion(data, url, function (result) {
        printDynamicSAPTable($('#tbl-result'), result, '', '');
        if (result.length != 0) {
            if ($('#tableId').val() == 'HeaderTemplate' || $('#tableId').val() == 'LinesTemplate') {
                SetSelectMasterAgent();
                SetSelectTranType();
                SetSelectMethodType();
                //$('.searchByMaster').show();
            } else {
                //$('.searchByMaster').hide();
            }
        }
        loader();
    });
}

function SetSelectMasterAgent() {
    $('.MasterAgentSelec').remove();
    var url = "/SAP_Maintenance/GetTableInfo/",
        data = {tableid: 'MasterAgent'};
    loader();
    ajaxConnexion(data, url, function (result) {

        if (result.length != 0) {
            for (var i = 0; $('.MasterAgentSelected').length > i; i++) {
                var selected = (i == 0 ) ? 0 : $($('.MasterAgentSelected')[i]).data('row'),
                    str = $('<select class="MasterAgentSelec"></select>');
                str.append('<option value="0">--Select Master Agent--</option>');
                for (var y = 0; result.length > y; y++) {
                    if (result[y].MasterAgent.indexOf('*') == -1) {
                        str.append('<option value="' + result[y].ID + '" ' + ((result[y].ID == selected) ? "selected" : "") + '>' + result[y].MasterAgent + '</option>');
                    }
                }
                $($('.MasterAgentSelected')[i]).append(str);
            }
        }
        loader();
    });
}

function SetSelectMasterAgentsearh() {
    var url = "/SAP_Maintenance/GetTableInfo/",
        data = { tableid: 'MasterAgent' };
    loader();
    ajaxConnexion(data, url, function (result) {

        if (result.length != 0) {
            var str = $('<select class="searchByMaster"></select>');
                str.append('<option value="0">--All Master Agent--</option>');
                for (var y = 0; result.length > y; y++) {
                    str.append('<option value="' + result[y].ID + '" >' + result[y].MasterAgent + '</option>');
                }
                $('.searchByMaster').append(str);
                $('.searchByMastertoExport').append(str);
            }
        loader();
    });
}

function SetSelectTranType() {
    var url = "/SAP_Maintenance/GetTranType/",
        data = {};
    loader();
    ajaxConnexion(data, url, function (result) {

        if (result.length != 0) {
            for (var i = 0; $('td.TranType').length > i; i++) {
                var selected = (i == 0) ? 0 : $($('.TranType')[i]).data('row'),
                    str = $('<select class="TranTypeSelect"></select>');
                str.append('<option value="0">--Select TranType--</option>');
                for (var y = 0; result.length > y; y++) {
                    str.append('<option value="' + result[y].ID + '" ' + ((result[y].ID == selected) ? "selected" : "") + '>' + result[y].Name + '</option>');
                }
                $($('.TranType')[i]).append(str);
            }
        }
        loader();
    });
}

function SetSelectMethodType() {

    for (var i = 0; $('td.MethodType').length > i; i++) {
        var selected = (i == 0) ? 0 : $($('.MethodType')[i]).data('row'),
                 str = $('<select class="MethodTypeSelect"></select>');
        str.append('<option value="0">--Select MethodType--</option>');
        if (selected == 'C') {
            str.append('<option value="C" selected>Credit</option>');
            str.append('<option value="P">Post-up</option>');
        } else if (selected == 'P') {
            str.append('<option value="C">Credit</option>');
            str.append('<option value="P" selected>Post-up</option>');
        } else {
            str.append('<option value="C">Credit</option>');
            str.append('<option value="P">Post-up</option>');
        }
        $($('.MethodType')[i]).append(str);
    }
}

function printDynamicSAPTable(table, result, idTitle, title) {
    $(table).show();
    // clear old data
    $(table).empty();
    // print title
    $(idTitle).text(title);
    if (result.length == 0) return;
    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');
    // table title row
    var keys = Object.keys(result[0]);
    createSAPHeader(table, keys);

    for (i = 0, length = result.length; i !== length; i++) {
        var isLast = (result.length-1 == i)?true:false;
        createSAPRow(table, result[i], isLast);
    }
}

function createSAPHeader(table, row) {
    var newRow = $('<tr></tr>'),
    addClass = '';
    $.each(row, function (key, value) {
        if (value != "MasterAgentID" && value != "ID") {
            $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
        }  
    });
    $(newRow).append('<th class="' + addClass + '">Action</th>');
    $(table).children('thead').append(newRow);
}

function createSAPRow(table, row,isLast) {
    var newRow = $('<tr>'),
        addClass = '';
    $.each(row, function (key, value) {
        var tdClass = '';
        if (key == "CustomerID" ) {  
            addClass = 'align-right';
            $(newRow).append('<td class="' + key + '">' + value + '</td>');
        } else if (key != "MasterAgentID" && key != "ID" && key != "MasterAgentSelected" && key != 'TranType' && key != 'MethodType' && key != 'Excluded') {
            $(newRow).append('<td><input type="text" data-row="'+row.ID+'" class="' + value + '" value="'+value+'"></td>');
        }
        else if (key == "MasterAgentSelected") {
            $(newRow).append('<td data-row="' + row.MasterAgentID + '" class="MasterAgentSelected"></td>');
        }
        else if (key == "TranType") {
            $(newRow).append('<td data-row="' + row.TranType + '" class="TranType"></td>');
        } else if (key == "MethodType") {
            $(newRow).append('<td data-row="' + row.MethodType + '" class="MethodType"></td>');
        } else if (key == "Excluded") {
            $(newRow).append('<td data-row="' + row.Excluded + '" class="Excluded"><input type="checkbox" ' + ((row.Excluded) ? "checked" : "") + ' class="' + row.Excluded + '" ></td>');
        }

    });
    $(newRow).append('<td><input name="submit" type="submit" value="Delete" class="red submit btn-delete" style="float:right;margin:5px; width:82px;"><input name="submit" type="submit" value="Update" class="green submit btn-update" style="float:right;margin:5px; width:82px;"></td>');
    $(newRow).append('</tr>');
    $(table).children('tbody').append(newRow);
    if (isLast) {
        $(newRow).clone().prependTo($(table));
        $($(table).children('tbody').find('tr')[0]).find('input:not([type="submit"])').val('');
        $($(table).children('tbody').find('tr')[0]).find('.btn-update').val('Add New').addClass('addNew').removeClass('btn-update');
        $($(table).children('tbody').find('tr')[0]).find('.btn-delete').hide();
        $('.addNew').on('click', function () {InsertTableInfo(this);});
    }
}
