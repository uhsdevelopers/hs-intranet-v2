﻿$(function () {

    $('#btn-submit').click(function () {

        getWWComissions();
    });

});

function getWWComissions() {

    var url = "/WW_Comissions/GetWWComissions/",
        data = {
            dateFrom: $('#datepickerStart').val(),
            dateTo: $('#datepickerEnd').val(),
            agentID: $('#txb-agentid').val()

        };

    // start loader
    loader();

    ajaxConnexion(data, url, function(result) {

        printDynamicTableWW_CommissionReport($('#tbl-result'), result, '', '');

        if (result.length == 0) {
            $('#span-totalPlayers').text('0');
            $('.span-totalNetAmount').text(formatCurrency(0));
            $('.span-totalTotalFees').text(formatCurrency(0));
            $('.span-totalCommission').text(formatCurrency(0));
            $('.span-totalAgentFee').text(formatCurrency(0));
        } else {
             $('#span-totalPlayers').text($('#tbl-result tr').length - 1);
        }
           

        // end loader
        loader();

    });

}

function printDynamicTableWW_CommissionReport(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeader(table, keys);

    var totalNetAmount = 0,
        totalTotalFees = 0,
            totalCommission = 0,
            totalAgentFee = 0;
    for (i = 0, length = result.length; i !== length; i++) {
        createRowWW_CommissionReport(table, result[i]);
        totalNetAmount = parseFloat(result[i]['Net Amount'] + totalNetAmount);
        totalTotalFees = parseFloat(result[i]['Total Fees'] + totalTotalFees);
        totalCommission = parseFloat(result[i]['Commission'] + totalCommission);
        totalAgentFee = parseFloat(result[i]['AgentFee'] + totalAgentFee);
    }
    $('.span-totalNetAmount').text(formatCurrency(totalNetAmount));
    $('.span-totalTotalFees').text(formatCurrency(totalTotalFees));
    $('.span-totalCommission').text(formatCurrency(totalCommission));
    $('.span-totalAgentFee').text(formatCurrency(totalAgentFee));
}

function createRowWW_CommissionReport(table, row) {

    var newRow = $('<tr>'),
        addClass = '';

    $.each(row, function (key, value) {
        var tdClass = '';
        if (isNaN(value)) {
            if (value.indexOf('/Date') >= 0) {
                value = formatDate(value);
            }

        } else if (key.indexOf('Balance') != -1 || key.indexOf('Red Figure') != -1 || key.indexOf('Net Amount') != -1 || key.indexOf('Fee') != -1 || key.indexOf('Commission') != -1) {
            if ((/\S/.test(value))) {
                value = formatCurrency(value);
            }
            addClass = 'align-right';
        } else if (key.indexOf('%') >= 0) {
            value = value + "%";
            addClass = 'align-right';
        }

        $(newRow).append('<td class="' + addClass + ' ' + tdClass + '">' + value + '</td>');
    });

    $(newRow).append('</tr>');

    $(table).children('tbody').append(newRow);
}
