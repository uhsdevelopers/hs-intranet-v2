﻿function GetFreeSpin() {
    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;
    var url;
    var data;

    url = "/affiliates/GetFreeSpins",
        data = {
            StartDate: startDate,
            EndDate: endDate
        };

    loader();

    ajaxConnexion(data, url, function (result) {

            $('#PokerSummary').show();
            $('#PokerSummary').empty();

        if (result.length == 0) {
            loader();
            $('#PokerSummary').append('<h3>No data available</h3>');
            return;
        }
            printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)


            loader();

        }
    );
}