﻿$(function () {
    GetLimitBonusList();
    $('#btn-add').on('click', function () {
        SetLimitBonusByCustomer($('#Add_CustomerId').val(),1);
        $('#Add_CustomerId').val('');
    });
    $('#tbl-result').on('click', '.hasBigBonus', function() {
        SetLimitBonusByCustomer($(this).val(),0);
    });
});

function GetLimitBonusList() {
    $('#tbl-result').empty();
    var url = "/LimitBonus/GetLimitBonusList/",
        data = {     
            
        };
    loader();
    ajaxConnexion(data, url, function (result) {
        if (!result.length) {
            loader();
            alert('No customer found');
            return;
        }
        printDynamicTable($('#tbl-result'), result, '', '');
        loader();
    });
}
function SetLimitBonusByCustomer(CustomerID,hasBigBonus) {
    $('.saveMessage, #errorMessage').hide();
    var url = "/LimitBonus/SetLimitBonusByCustomer/",
        data = {     
            CustomerID: CustomerID,
            hasBigBonus:hasBigBonus
        };
    loader();
    ajaxConnexion(data, url, function (result) {
        if (result[0].ErrorNumber == 0) {
            if (hasBigBonus == 0) {$('#removeMessage').show();} else {$('#saveMessage').show();}
            GetLimitBonusList();
            $('#Add_CustomerId').val('');

        } else {
            $('#errorAddMessage').text(result[0].ErrorMessage);
            $('#errorAddMessage').show();
        }
        loader();
    });
}

function createRow(table, row) {
    var newRow = $('<tr>');
    $.each(row, function (key, value) {
        if (key.indexOf('UseAGreaterBonus') != -1 ) {
            value = '<input type="checkbox" value ="'+row.CustomerID+'" class="hasBigBonus" '+((value)?"checked":"")+'/>';   
        }
        $(newRow).append('<td style="text-align: center;">' + value + '</td>');
    });
    $(newRow).append('</tr>');
    $(table).children('tbody').append(newRow);
}

