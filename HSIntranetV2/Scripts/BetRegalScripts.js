﻿

function GetSundayParlay() {
    var dateFrom = document.getElementById('datepickerStart').value;
    var dateEnd = document.getElementById('datepickerEnd').value;
    var minAmount = document.getElementById('MinAmount').value;
    var picks = document.getElementById('MinPicks').value;

    var url;
    var data;

    url = "../Customer/GetSundayParlay",
       data = {
           startDate: dateFrom,
           endDate:dateEnd,
           minAmount: minAmount,
           picks: picks


       };

    loader();

    ajaxConnexion(data, url, function (result) {

        $('#PokerSummary').show();


        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)


        // end loader
        loader();
    }
    );
}

function GetBrEmail() {


    var url;
    var data;
    var Condition = $('#Condition').val(),
        Feature = $('#Feature').val()
    var dateFrom = document.getElementById('datepickerStart').value
    var CustomerId = document.getElementById('CustomerId').value

    url = "../BetRegal/BrGetEmail",
        data = {
        condition: Condition,
        Feature: Feature,
        startdate: dateFrom,
        customerid: CustomerId


        };

    loader();

    ajaxConnexion(data, url, function (result) {

        $('#PokerSummary').show();


        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)


        // end loader
        loader();
    }
    );
}


function GetCasinoPlayers() {


    var url;
    var data;
   
    var Agent = document.getElementById('Agent').value

    url = "../BetRegal/BRGetCasinoPlayers",
        data = {
        Agent: Agent


        };

    loader();

    ajaxConnexion(data, url, function (result) {

        $('#PokerSummary').show();


        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)


        // end loader
        loader();
    }
    );
}
function GetOntarioPlayers() {


    var url;
    var data;

   

    url = "../BetRegal/BRGetOntarioPlayers",
        data = {};

    loader();

    ajaxConnexion(data, url, function (result) {

        $('#PokerSummary').show();


        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        $('#PokerSummary').tablesorter();
        // end loader
        loader();
    }
    );
}

function GetBasicEmail() {


    var url;
    var data;
    var Condition = '0',
        Feature = '0'
    var dateFrom = '1900-01-01'
    var CustomerId = ''

    url = "../BetRegal/BrGetEmail",
        data = {
            condition: Condition,
            Feature: Feature,
            startdate: dateFrom,
            customerid: CustomerId


        };

    loader();

    ajaxConnexion(data, url, function (result) {

        $('#PokerSummary').show();


        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)


        // end loader
        loader();
    }
    );
}


function getAnalysisData() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value,
        customerid = document.getElementById('txtCustomerid').value,
    customerProfile = checkedRadioBtn('TType'),
    store = checkedRadioBtn('WType'),
    bussinesUnit = checkedRadioBtn('BType');


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/DepositBonus/GetDepositAndBonus",
       data = {
           dateStart: startDate,
           dateEnd: endDate,
           customerid: customerid,
           store:store,
           cProfile: customerProfile,
           businessUnit: bussinesUnit,
           timeout:360
       };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        $('#PokerSummary').tablesorter();



        // end loader
        loader();
    });
}

function getrevenue() {

    var startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value;


    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (startDate === "") {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;

    url = "/BetRegal/BRGetRevenue",
        data = {
            dateStart: startDate,
            dateEnd: endDate
        };



    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        $('#PokerSummary').tablesorter();



        // end loader
        loader();
    });
}

