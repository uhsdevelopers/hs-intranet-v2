﻿$(function () {
});

function getPokerTransactions() {

    var customerID = document.getElementById('customerId').value,
        startDate = document.getElementById('datepickerStart').value,
        endDate = document.getElementById('datepickerEnd').value,
        tranCode = document.getElementById('tranCode').value,
        status = document.getElementById('status').value,
        timeZone = document.getElementById('timeZone').value;
        //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';
    
    if (customerID == '' && startDate == '' && endDate == '') {
        alert('Please select at least one filter.');
        return;
    }
        
    // summary
    var url = "/Poker/GetPokerTransactions/",
        data = {
            customerID : customerID,
            startDate : startDate,
            endDate : endDate,
            tranCode : tranCode,
            status : status,
            timeZone : timeZone,
            summary : 'Y'
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

    });

    // details
    var url = "/Poker/GetPokerTransactions/",
    data = {
        customerID: customerID,
        startDate: startDate,
        endDate: endDate,
        tranCode: tranCode,
        status: status,
        timeZone: timeZone,
        summary: 'N'
    };

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerTrans'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)

        // end loader
        loader();
    });
}


function syncpassword() {

    var customerID = document.getElementById('customerId').value

    if (customerID == '' ) {
        alert('Please select at least one filter.');
        return;
    }

    // summary
    var url = "/Poker/ChangePokerPassword/",
        data = {
            customerID: customerID
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
        loader();
    });

}
function getPokerLimits() {

    var customerID = document.getElementById('customerId2').value

    if (customerID == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary
    var url = "/Poker/GetCustomerLimits/",
        data = {
            customerID: customerID
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {
        printDynamicTable($('#PokerTrans'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
        loader();
    });

}


function getCustomerInfobyScreen() {

    var username = document.getElementById('Username').value;

    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';

    if (username == '') {
        alert('Please select at least one filter.');
        return;
    }

    // summary

    $("#TransferSummary").hide();
    $("#updCus").hide();

    var url;
    var data;
    if (username != '')
    {
        url = "/Regalpoker/GetCustomerDetails/",
         data = {
             username: username
         };
        

        loader();

        ajaxConnexion(data, url, function (result) {
            

            if (result.resultCode == 0) {
                printCustomerinfoTable($('#PokerSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
            } else {
                alert("Error");
            }



            // end loader
            loader();
        });

    }


}

function getCustomerInfobyCID() {
    var cid = document.getElementById('customerid').value;
    //summary = (document.getElementById('summary').checked) ? 'Y' : 'N';
    if (cid == '') {
        alert('Please select at least one filter.');
        return;
    }
    // summary
    $("#TransferSummary").hide();
    $("#updCus").hide();
    var url;
    var data;
    if (cid != '') {
        url = "/Regalpoker/GetCustomerDetailsCid",
         data = {
             cid: cid
         };

        loader();
        ajaxConnexion(data, url, function (result) {

            if (result.resultCode == 0) {
                AppendCustomerinfoTable($('#TransferSummary'), result, '', ''); // HS_script.js  function printDynamicTable(table, object)
            } else {
                alert("Error");
            }
            // end loader
            loader();
        });
    }
}

function AppendCustomerinfoTable(table, result, idTitle, title) {
    var tdClass = '';
    var addClass = '';

    var code = result.resultCode;
    var username;
    var sname;
    var siteName;
    username = result.customer.userName;
    sname = result.customer.signinName;
    siteName = result.customer.siteName;

    var newKey10 = $('<tr class="titulo"><td>Username </td><td> Signin Name</td><td>SiteName</td></tr>');
    var newRow10 = $('<tr>');
    $(newRow10).append('<td class="' + addClass + ' ' + tdClass + '">' + username + '</td>');
    $(newRow10).append('<td class="' + addClass + ' ' + tdClass + '">' + sname + '</td>');
    $(newRow10).append('<td class="' + addClass + ' ' + tdClass + '">' + siteName + '</td>');
    $(newRow10).append('</tr>');
    $(table).append(newKey10);
    $(table).append(newRow10);

}


function printCustomerinfoTable(table, result, idTitle, title) {

    $(table).show();
    $("#updCus").show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);
    var length;
    if (result.length == 0) return;

    // table title row
    var tdClass = '';

    var code = result.resultCode;
    var username;
    var referenceid;
    var cid;
    var rid;
    var secretQuestion;
    var secretAnswer;

    username = result.customer.userName;
    cid = result.customer.externalUsername;
    rid = result.customer.id;
    secretQuestion = result.customer.secretQuestion;
    secretAnswer = result.customer.secretAnswer;
    
    
    

    var newKey = $('<tr><td> ID</td><td>Customer ID</td><td> UserName </td><td>Secret Question</td><td>Secret Answer</td></tr>');
    $(table).append(newKey);
  

        var newRow1 = $('<tr>'),
        addClass = '';
        $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '">' + rid + '</td>');

        if (cid != null) {
            $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '"><a href="#" onClick="getCustomerinfo(this)">' + cid + '</a><input type="hidden" name="customerid" id="customerid" value=' + cid + '></td>');
        } else {
            $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '"></td>');
        }
        $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '" nowrap>' + username + '</td>');


    
        $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '">' + secretQuestion + '</td>');
        $(newRow1).append('<td class="' + addClass + ' ' + tdClass + '">' + secretAnswer + '</td>');


        $(table).append(newRow1);
   
}