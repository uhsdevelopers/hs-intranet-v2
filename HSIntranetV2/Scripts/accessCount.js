﻿$(function () {
    
    $('#btn-submit').click(function () {
        getHeadCount();
    });
    $(document).keypress(function (event) {
        if (event.which == 13) {
            getHeadCount();
        }  
    });
});

function getHeadCount() {

    var url = "/AccessCount/GetAccessCount/",
        data = {
            From: $('#datepickerStart').val(),
            Limit: $('#Limit').val()
        };

    // start loader
    loader();

    ajaxConnexion(data, url, function (result) {

        printDynamicTable($('#tbl-result'), result, '', '');
        // end loader
        loader();

    });

}