﻿$(function () {


    $('ul').sortable({
        connectWith: ".sportTypeSortable",
        distance: 15,
        placeholder: "ui-state-highlight",
        cancel: "li.ui-state-disabled"
    }).disableSelection();


    loadLists(function () {
        $('li, #trash').droppable({
            drop: function (event, li) {

                var li_sport = $(li.draggable).html().split('<br>'),
                    sportType = li_sport[0],
                    sportSubType = li_sport[1],
                    origin = event.toElement.parentElement.id,
                    target = event.target.parentElement.id;

                if (origin == target) {
                    return;
                }
                else if (event.target.id == 'trash')
                    li.draggable.remove();
                else if (target == 'list-backup') {
                    SetHoldSportType(sportType, sportSubType);
                }
                else if (target == 'list-sportType') {
                    SetProdSportType(sportType, sportSubType);
                }
                    
            }
        });
    });

    $('#btnSaveAddSport').click(function () {
        InsertNewSport();
    });

    
});

function InsertNewSport() {

    var sportSubTypeCopy = $('#ddlSportSubType').val(),
    sportTypeNew = $('#txbNewSport').val(),
    sportSubTypeNew = $('#txbSportSubType').val(),
    periodType = $('#ddlPeriodType').val(),
    drawFlag = ($('#chkboxDraw').is(':checked')) ? 'Y' : 'N';

    if (sportSubTypeCopy.length == 0 || sportTypeNew.length == 0 || sportSubTypeNew.length == 0) {
        alert("All fields are required.");
        return;
    }

    var url = "/SportType/InsertNewSport",
        data = {
            sportSubTypeCopy: sportSubTypeCopy,
            sportTypeNew: sportTypeNew,
            sportSubTypeNew: sportSubTypeNew,
            periodType: periodType,
            drawFlag: drawFlag
        }

    ajaxConnexionNoReturn(data, url, function (result) {
    });

}

function SetProdSportType(sportType, sportSubType) {

    var url = "/SportType/SetProdSportType/",
        data = {
            sportType: sportType,
            sportSubType: sportSubType
        };

    ajaxConnexion(data, url, function (result) {

    });
}

function SetHoldSportType(sportType, sportSubType) {

    var url = "/SportType/SetHoldSportType/",
        data = {
            sportType: sportType,
            sportSubType: sportSubType
        };

    ajaxConnexion(data, url, function (result) {
        
    });
}

function loadLists(callback) {

    var url = "/SportType/GetProdSportTypes/",
        data = { };

    ajaxConnexion(data, url, function (result) {
        printDinamicUnorderedList($('#list-sportType'), result);

        printDinamicDropDownList($('#ddlSportSubType'), result);

        printDinamicDropDownListSport($('#li-sportType'), result);

        url = "/SportType/GetHoldSportTypes/",
        data = {};

        ajaxConnexion(data, url, function (result) {
            printDinamicUnorderedList($('#list-backup'), result);

            callback();
        });

    });
}

function printDinamicDropDownList(id, result) {
    $(id).empty();

    $(id).append('<option></option>');
    // add list to select
    for (var i = 0, length = result.length; i !== length; i++) {
        $(id).append('<option>' + result[i].SportType + ' / ' + result[i].SportSubType + '</option>');
    }
}

function printDinamicDropDownListSport(id, result) {

    var hold_sport = '';

    $(id).empty();
    // add list to select
    for (var i = 0, length = result.length; i !== length; i++) {

        if (hold_sport != result[i].SportType) {

            hold_sport = result[i].SportType;

            $(id).append('<option>' + hold_sport + '</option>');

        }

    }
}


function printDinamicUnorderedList(id, result) {

    $(id).empty();

    // add sport list
    for (var i = 0, length = result.length; i !== length; i++) {
        $(id).append('<li class="ui-state-default" >' + result[i].SportType + '<br/>' + result[i].SportSubType + '</li>');
    }

}
