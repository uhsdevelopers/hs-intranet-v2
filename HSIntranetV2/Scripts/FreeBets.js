﻿$(function () {
    GetFreeBetsCombos();
    $('#Add_DAte').datepicker();
    $('#btn-submit').click(function() {
        $('#tbl-result').empty();
        GetFreeBets();
        $('#saveMessage, #changeMessage, #errorMessage, .btn-update, #errorAddMessage').hide();
    });
    $(document).on("keydown", function(event) {
        if (event.which == 13) {
            $('#tbl-result').empty();
            GetFreeBets();
            $('#saveMessage, #changeMessage, #errorMessage, .btn-update, #errorAddMessage').hide();
        }
    });
    $('#tbl-result').on('change', '.cl-isActive',function() {
        if (confirm('Are you sure you want to change the status?')) {
            EditFreeBets($(this).closest('tr'));
        } else {
            $(this).prop("checked", !$(this).prop("checked"));
        }
    });
    $('#tbl-result').on('change', '.amount', function () {
        if (confirm('Are you sure you want to change the amount?')) {
            EditFreeBets($(this).closest('tr'));
        } else {
            $(this).val($(this).data("text"));
        }
    });
    $('#tbl-result').on('click', '.TicketReference', function (e) {
        e.preventDefault();
        if (confirm('Are you sure you want to delete the wager and activate the freebet?')) {
            DeleteFreeBets($(this));
        } 
    });
    $('#tbl-result').on('change', '.date', function () {
        if (confirm('Are you sure you want to change the date?')) {
            EditFreeBets($(this).closest('tr'));
        } else {
            $(this).val($(this).data("text"));
        }
    });
    $('#btn-submit-add').on('click', function () {    
        AddFreeBet();
    });
});

function GetFreeBets() {
    var url = "/FreeBets/GetFreeBets/",
        data = {     
            CustomerID: $('#CustomerId').val()
        };
    loader();
    ajaxConnexion(data, url, function (result) {
        if (!result.length) {
            loader();
            alert('No freebets');
            return;
        }
        printDynamicTableFreeBets($('#tbl-result'), result, '', '');
        $('.btn-update').show();
        $('.date').datepicker();
        loader();
    });
}
function AddFreeBet() {
    $('#saveMessage, #changeMessage, #errorMessage, #errorAddMessage').hide();
    var url = "/FreeBets/AddFreeBets/",
        data = {     
            CustomerID: $('#Add_CustomerId').val(), 
            prmFreeBetID:$('.Add_FreebetID').val(), 
            prmAmount:$('#Add_Amount').val(), 
            prmExpirationDate:$('#Add_DAte').val()
        };
    loader();
    ajaxConnexion(data, url, function (result) {
        if (result[0].ErrorNumber == 0) {
            $('#saveMessage').show();
            $('#CustomerId').val($('#Add_CustomerId').val());
            GetFreeBets();
            $('#Add_CustomerId').val('');
            $('#Add_Amount').val('');
            $('#Add_DAte').val('');
        } else {
            $('#errorAddMessage').text(result[0].ErrorMessage);
            $('#errorAddMessage').show();
        }
        loader();
    });
}

function GetFreeBetsCombos() {
    var url = "/FreeBets/GetFreeBetsCombos/",
        data = {};
    loader();
    ajaxConnexion(data, url, function (result) {
        for(var i = 0;i< result.length;i++) {
            var opt = '<option value="' + result[i].ID + '">' + result[i].Name + '</option>';
            $('.Add_FreebetID').append(opt);
        }
        loader();
    });
}

function EditFreeBets($this) {
    $('#saveMessage, #changeMessage, #errorMessage, #errorAddMessage').hide();
    var url = "/FreeBets/EditFreeBets/",
        data = {
            prmFreeBetID: $($this.find('td')[0]).data('freebet'),
            prmAmount: ($this.find('.amount').length == 0) ? $this.find('.cl-amount').text().replace('$', '') : $this.find('.amount').val(),
            prmActive: $this.find('.cl-isActive').prop('checked'),
            prmExpirationDate: ($this.find('.date').length == 0) ? formatDate($this.find('.cl-date').text().replace('$', '')) : $this.find('.date').val()
        };
    loader();
    ajaxConnexion(data, url, function (result) {
        loader();
        if (result[0].ErrorNumber == 0) {
            $('#changeMessage').show();
            GetFreeBets();
        } else {
            $('#errorMessage').text(result[0].ErrorMessage);
            $('#errorMessage').show();
        } 
    });
}

function DeleteFreeBets($this) {
    $('#saveMessage').hide();
    $('#changeMessage').hide();
    $('#errorMessage').hide();
    var url = "/FreeBets/DeleteFreeBets/",
        data = {
            prmFreeBetID: $($this).data('freebet')   
        };
    loader();
    ajaxConnexion(data, url, function (result) {
        loader();
        if (result[0].Result == 0) {
            $('#changeMessage').show();
            GetFreeBets();
        } else {
            $('#errorMessage').text(result[0].Message);
            $('#errorMessage').show();
        }
    });
}

function createHeaderFreeBets(table, row) {
    var newRow = $('<tr></tr>'),
    addClass = '';
    $.each(row, function (key, value) {
        if (value == 'MinTeam') {value = 'Teams';}
        if (value == 'MinSellPoints') {value = 'SellPoints';}
        if (value == 'MinBuyPoints') {value = 'BuyPoints';}
        if (value == 'WagerGroupTypeName' || value == 'WagerItemTypeName') {value = 'WagerType';}
        if (value != 'ID' && value != 'FreeBetCustomerID' && value != 'WagerItemType' && value != 'WagerGroupType' && value != 'MaxTeam' && value != 'MinTeam' && value != 'MinSellPoints' && value != 'MaxSellPoints' && value != 'MinBuyPoints' && value != 'MaxBuyPoints')
            $(newRow).append('<th class="' + addClass + '">' + value + '</th>');
    });
    $(table).children('thead').append(newRow);
}

function printDynamicTableFreeBets(table, result, idTitle, title) {

    $(table).show();

    // clear old data
    $(table).empty();

    // print title
    $(idTitle).text(title);

    if (result.length == 0) return;

    // create table head body
    $(table).append('<thead></thead>');
    $(table).append('<tbody></tbody>');

    // table title row
    var keys = Object.keys(result[0]);
    createHeaderFreeBets(table, keys);
    var id = -1;
    for (i = 0, length = result.length; i !== length; i++) {    
            var newRow = $('<tr>'),
                addClass = '';
            if (id != result[i].FreeBetCustomerID) {
                $.each(result[i], function (key, value) {
                    if (key == 'MaxTeam') {
                        result[i].MaxTeam = (result[i].MaxTeam == result[i].MinTeam) ? ' teams' : '-' + result[i].MaxTeam + ' teams';
                        $(newRow).append('<td nowrap class="' + addClass + ' ' + tdClass + '" data-freebet="' + result[i].FreeBetCustomerID + '">' + result[i].MinTeam + result[i].MaxTeam + '</td>');
                    }
                    if (key == 'MaxSellPoints') {
                        result[i].MaxSellPoints = (result[i].MaxSellPoints == result[i].MinSellPoints) ? ' pts' : '-' + result[i].MaxSellPoints + ' pts';
                        $(newRow).append('<td  class="' + addClass + ' ' + tdClass + '" data-freebet="' + result[i].FreeBetCustomerID + '">' + result[i].MinSellPoints + result[i].MaxSellPoints + '</td>');
                    }
                    if (key == 'MaxBuyPoints') {
                        result[i].MaxBuyPoints = (result[i].MaxBuyPoints == result[i].MinBuyPoints) ? ' pst' : '-' + result[i].MaxBuyPoints + ' pts';
                        $(newRow).append('<td  class="' + addClass + ' ' + tdClass + '" data-freebet="' + result[i].FreeBetCustomerID + '">' + result[i].MinBuyPoints + result[i].MaxBuyPoints + '</td>');
                    }
                    if (key != 'ID' && key != 'FreeBetCustomerID' && key != 'WagerItemType' && key != 'WagerGroupType' && key != 'MaxTeam' && key != 'MinTeam' && key != 'MaxSellPoints' && key != 'MinBuyPoints' && key != 'MaxBuyPoints' && key != 'MinSellPoints') {
                        var tdClass = '';
                        if (key.indexOf('Amount') != -1) {
                            if ((/\S/.test(value))) {
                                value = (result[i].Active) ? '<input type="text" data-text="' + value + '" class="amount numeric" size="4" value="' + value + '">' : formatCurrency(value);
                                tdClass = 'cl-amount';
                            }
                            addClass = 'align-right';
                        } else if (key.indexOf('ExpirationDate') != -1) {
                            value = (value == null) ? '' : value;
                            value = (result[i].Active) ? '<input type="text" size="19" data-text="' + value + '" class="date" value="' + formatDate(value) + '">' : formatDate(value);
                            tdClass = 'cl-date';
                            addClass = 'align-right';
                        } else if (key.indexOf('TicketReference') != -1) {
                            value = (value == null) ? '' : '<a class="TicketReference" href="" data-freebet="' + result[i].FreeBetCustomerID + '" title="Delete wager and active freebet">' + value + '<a/>';
                        } else if (key.indexOf('Active') != -1) {
                            var is_checked = (value) ? 'checked' : '';
                            value = '<input type="checkbox"' + is_checked + ' class="cl-isActive" style="width: 30px;height: 21px;"/>';
                        } else if (key.indexOf('CustomerID') >= 0) {
                            tdClass = 'cl-CommentID';
                        }
                        $(newRow).append('<td  class="' + addClass + ' ' + tdClass + '" data-freebet="' + result[i].FreeBetCustomerID + '">' + value + '</td>');
                    }
                });

                $(newRow).append('</tr>');

                $(table).children('tbody').append(newRow);
            } else {
                var $currentTr = $(table).find('tr')[$(table).find('tr').length - 1];
                if ($($($currentTr).find('td')[4]).text().indexOf(result[i].SportType) == -1) { $($($currentTr).find('td')[4]).append('<p>' + result[i].SportType + '</p>'); }
                if ($($($currentTr).find('td')[5]).text().indexOf(result[i].SportSubType) == -1) {$($($currentTr).find('td')[5]).append('<p>' + result[i].SportSubType + '</p>');}
                if ($($($currentTr).find('td')[7]).text().indexOf(result[i].WagerItemTypeName) == -1) {$($($currentTr).find('td')[7]).append('<p>' + result[i].WagerItemTypeName + '</p>');}
                if ($($($currentTr).find('td')[6]).text().indexOf(result[i].Period) == -1) {$($($currentTr).find('td')[6]).append('<p>' + result[i].Period + '</p>');}
            }
            id = result[i].FreeBetCustomerID;
        }
    }


