﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class BitcoinWalletController : Controller
    {

        public ActionResult Index(string usr, string site = "1")
        {
            ViewData["clerk"] = usr;
            ViewData["site"] = site;
            return View();
        }


        public ActionResult GetBitCoinWallet(string startDate, string enddate, string status, string SportsbookID = "1", string customerid = "")
        {
            const string storedProcedureName = "uSp_Inet_GetBitCoinWalletRequest";
            var dtResult = DepositBonusModel.GetBitCoinWallet(storedProcedureName, startDate, enddate, status, SportsbookID, customerid);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult UpdateBitCoinWallet(string id, string userInCharge, string status)
        {
            const string storedProcedureName = "uSp_Inet_UpdateBitCoinWalletRequest";
            var dtResult = DepositBonusModel.UpdateBitCoinWallet(storedProcedureName, id, userInCharge, status);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

    }
}