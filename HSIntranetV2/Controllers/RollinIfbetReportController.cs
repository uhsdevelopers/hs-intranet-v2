﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class RollinIfbetReportController : Controller
    {
        //
        // GET: /RollinIfbetReportController/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetRollinIfbetReport(string dateFrom, string dateTo, string CustomerID)
        {

            DataTable dtResult = RollinIfbetReportModel.GetRollinIfbetReport(dateFrom, dateTo, CustomerID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
    }
}
