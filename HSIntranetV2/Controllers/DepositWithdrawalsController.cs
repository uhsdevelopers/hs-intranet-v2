﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class DepositWithdrawalsController : Controller
    {
        //
        // GET: /DepositWithdrawals/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetDeposits(string from, string to, string company)
        {

            DataTable dtResult = DepositWithdrawals.GetDeposits(from, to, company);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetWithdrawals(string from, string to, string company)
        {

            DataTable dtResult = DepositWithdrawals.GetWithdrawals(from, to, company);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetWithdrawalsDetails(string from, string to, string company, string paymentBy)
        {

            DataTable dtResult = DepositWithdrawals.GetWithdrawalsDetails(from, to, company, paymentBy);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetDepositsDetails(string from, string to, string company, string processor, string status)
        {

            DataTable dtResult = DepositWithdrawals.GetDepositsDetails(from, to, company, processor, status);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
