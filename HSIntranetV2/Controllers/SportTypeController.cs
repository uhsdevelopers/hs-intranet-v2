﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class SportTypeController : Controller
    {
        //
        // GET: /SportType/

        public ActionResult Index()
        {
            return View();
        }

        // GET: /SportType/GetHoldSportTypes/ 
        public ActionResult GetHoldSportTypes()
        {

            DataTable dtResult = SportTypeModel.GetHoldSportTypes();

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        // GET: /SportType/GetProdSportTypes/ 
        public ActionResult GetProdSportTypes()
        {

            DataTable dtResult = SportTypeModel.GetProdSportTypes();

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        // GET: /SportType/SetHoldSportTypes/ 
        public ActionResult SetHoldSportType(string sportType, string sportSubType)
        {

            DataTable dtResult = SportTypeModel.SetHoldSportType(sportType, sportSubType);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        // GET: /SportType/SetProdSportTypes/ 
        public ActionResult SetProdSportType(string sportType, string sportSubType)
        {

            DataTable dtResult = SportTypeModel.SetProdSportType(sportType, sportSubType);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        // GET: /SportType/InsertNewSport/ 
        public ActionResult InsertNewSport(string sportSubTypeCopy, string sportTypeNew, string sportSubTypeNew, string periodType, string drawFlag)
        {

            SportTypeModel.SportType c_sportType = new SportTypeModel.SportType();
            c_sportType.SportSubTypeCopy = sportSubTypeCopy;
            c_sportType.SportTypeNew = sportTypeNew;
            c_sportType.SportSubTypeNew = sportSubTypeNew;
            c_sportType.PeriodType = periodType;
            c_sportType.DrawFlag = drawFlag;

            var Result = new ContentResult();

            DataTable dtResult = SportTypeModel.InsertNewSport(c_sportType);

            Result = _BLL.GetJson(dtResult);

            return Result;
        }
    }
}
