﻿using HSIntranetV2.Models;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class AccumWagerLimitByLineController : Controller
    {
        //
        // GET: /AccumWagerLimitByLine/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetCustomerAccumWager(string customerID)
        {

            DataTable dtResult = AccumWagerLimitByLineModel.GetCustomerAccumWager(customerID, "-1");
            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult SetCustomerAccumWager(string customerID, string lineMultiplier)
        {

            Regex regex = new Regex(@"^[+-]?(?:\d+(?:,\d{3})?(?:\.\d+)?|\.\d+)$");

            if (!regex.IsMatch(lineMultiplier)) // no match
                lineMultiplier = "";

            lineMultiplier = (lineMultiplier.Length == 0) ? "0" : lineMultiplier;

            DataTable dtResult = AccumWagerLimitByLineModel.GetCustomerAccumWager(customerID, lineMultiplier);
            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}