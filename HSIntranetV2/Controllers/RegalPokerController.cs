﻿using HSIntranetV2.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class RegalPokerController : Controller
    {
        //
        // GET: /RegalPoker/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Transaction()
        {
            return View("Transactions");
        }

        public ActionResult CustomerInfo()
        {
            return View("CustomerInfo");
        }

        // GET: /Poker/GetSignup/ 
        public ActionResult GetSignup(string startDate, string endDate)
        {

            var cRegal = new RegalModel { DateFrom = startDate, DateTo = endDate };

            var dtResult = RegalModel.GetSignup(cRegal);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        //Get Regalpoker/GetCustomerDetails?username=RonZ006
        // to get the Customer info
        public ActionResult GetCustomerDetails(string username)
        {
            var url = "https://service.equitypokernetwork.com/services-player-webapp/player/customerDetails/un/" +
                      username + ".json?api_key=";
            const string key = "ED96C3F2364B4A650914FC87A439B5E7";

            //var task = MakeAsyncRequest(url + key, "text/html");
            //return Content(task.Result);

            string result = null;
            var request = (HttpWebRequest)WebRequest.Create(url + key);
            try
            {
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    if (request.HaveResponse && response != null)
                    {
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            result = reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            result = reader.ReadToEnd();

                        }
                    }
                }
            }
            return Content(result);
        }



        public string GetCustomerDetailsCid(string cid)
        {


            var url = "https://service.equitypokernetwork.com/services-player-webapp/player/customerDetails/ext/" + cid + ".json?api_key=";
            const string key = "ED96C3F2364B4A650914FC87A439B5E7";

            string json;

            using (var webClient = new WebClient())
            {
                json = webClient.DownloadString(url + key);
                // Now parse with JSON.Net
            }




            // RunAsync(url).Wait();

            //var task =MakeAsyncRequest(url + key, "text/html");

            return json;
        }


        public ActionResult GenerateToken(string customerid)
        {
            const string secretWord = "Token";
            var token = _BLL.GetHashString(customerid, secretWord);
            var returntoken = new RegalModel.ServiceTokenResponse { Token = token };
            var result = new ContentResult { Content = "{Token :" + token + ", CustomerId :" + customerid + " }" };

            var retorno = Json(returntoken, JsonRequestBehavior.AllowGet);

            return retorno;
        }




        public static IPEndPoint BindIpEndPointCallback(ServicePoint servicePoint, IPEndPoint remoteEndPoint, int retryCount)
        {
            Console.WriteLine("BindIPEndpoint called");
            return new IPEndPoint(IPAddress.Any, 5000);

        }


        public static Task<string> MakeAsyncRequest(string url, string contentType)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = contentType;
            request.Method = WebRequestMethods.Http.Get;
            request.Timeout = 20000;
            request.Proxy = null;
            request.ServicePoint.BindIPEndPointDelegate = new BindIPEndPoint(BindIpEndPointCallback);
            request.ContinueTimeout = 10000;


            try
            {
                Task<WebResponse> task = Task.Factory.FromAsync(
                    request.BeginGetResponse,
                    asyncResult => request.EndGetResponse(asyncResult),
                    null);
                return task.ContinueWith(t => ReadStreamFromResponse(t.Result));
            }
            catch (WebException webException)
            {
                return null;
            }





        }

        private static string ReadStreamFromResponse(WebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            using (var sr = new StreamReader(responseStream))
            {
                //Need to return this response 
                string strContent = sr.ReadToEnd();
                return strContent;
            }
        }


        public ActionResult GetSignupbyRequestid(string requestId)
        {

            var dtResult = RegalModel.GetSignupByRequestId(requestId);

            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetTransactions(string startDate, string endDate)
        {

            var cRegal = new RegalModel { DateFrom = startDate, DateTo = endDate };

            var dtResult = RegalModel.GetTransactions(cRegal);

            var result = _BLL.GetJson(dtResult);

            return result;
        }




        public ActionResult RefundTransactions(string customerid, string enteredBy, string document)
        {

            var cRegal = new RegalModel { Customerid = customerid, Reference = document, GameId = "0" };
            var result = new ContentResult();
            var dtResult = RegalModel.VerifyTransactionsExists(cRegal);

            var status = dtResult.Rows[0][1];
            var documentid = dtResult.Rows[0][0];

            if (status.ToString() == "0")
            {
                if (documentid.ToString() == document)
                {

                    var dtRefund = RegalModel.RefundTransactions(customerid, enteredBy, document);
                    result = _BLL.GetJson(dtRefund);
                }
            }
            else
            {
                string description;
                switch (status.ToString())
                {
                    case "303":
                        description = "Already Refunded";
                        break;
                    case "302":
                        description = "Incorrect Document Number";
                        break;
                    default:
                        description = "Unexpected error";
                        break;
                }

                result.Content = "{Status :" + status + ", Description :" + description + " }";



            }


            return result;
        }



        public ActionResult GetTransactionsbyCustomerid(string startDate, string endDate, string customerid)
        {

            var cRegal = new RegalModel { DateFrom = startDate, DateTo = endDate, Customerid = customerid };

            var dtResult = RegalModel.GetTransactionsbyCustomer(cRegal);

            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetInfobyCustomerid(string customerid)
        {

            // var cRegal = new RegalModel { Customerid = customerid };

            var dtResult = RegalModel.GetCustomerInfo(customerid);

            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetUsername(string email, string country)
        {

            var dtResult = RegalModel.GetCustomerInfo(email, country);

            var status = dtResult.Rows[0][0];
            var customerid = dtResult.Rows[0][1];
            var result = new ContentResult();
            var responseRegal = new RegalModel.RegalUsernameResponse { Status = "0", Username = string.Empty };

            if (Convert.ToBoolean(status))
            {
                var json = GetCustomerDetailsCid(customerid.ToString());
                var regalobject = JsonConvert.DeserializeObject<RegalModel.RootObject>(json);
                responseRegal.Status = status.ToString();
                responseRegal.Username = regalobject.Customer.userName;

            }


            result.Content = JsonConvert.SerializeObject(responseRegal);
            return result;

        }

        public ActionResult GetUsernameData(string email, string country, string username)
        {

            var dtResult = RegalModel.GetCustomerInfo(email, country);

            var status = dtResult.Rows[0][0];
            var customerid = dtResult.Rows[0][1];
            var passwordinDb = dtResult.Rows[0][2];

            var result = new ContentResult();
            var responseRegal = new RegalModel.RegalUsernameResponse { Status = "0", Username = string.Empty, Password = string.Empty };

            if (Convert.ToBoolean(status))
            {

                try
                {
                    var json = GetCustomerDetailsCid(customerid.ToString());
                    var regalobject = JsonConvert.DeserializeObject<RegalModel.RootObject>(json);
                    responseRegal.Status = status.ToString();
                    responseRegal.Username = regalobject.Customer.userName;
                    if (String.Compare(responseRegal.Username, username, StringComparison.Ordinal) == 0)
                    {
                        responseRegal.Password = passwordinDb.ToString();
                    }
                    else
                    {
                        responseRegal.Status = "0";
                    }
                }
                catch (Exception)
                {

                    responseRegal.Status = "0";
                }



            }


            result.Content = JsonConvert.SerializeObject(responseRegal);
            return result;

        }



        public ActionResult UpdateInfobyCustomerid(string customerId, string password, string nameLast, string nameFirst, string email, string homePhone, string businessPhone, string address, string city, string state, string zip, string country, string comments = "")
        {


            var dtResult = RegalModel.UpdateCustomerInfo(customerId, password, nameLast, nameFirst, email, homePhone,
                businessPhone, address, city, state, zip, country, comments);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}