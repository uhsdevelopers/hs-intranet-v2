﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class HR_BankFormController : Controller
    {
        //
        // GET: /HR_BankForm/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Get()
        {
            return View();
        }
        public ActionResult InsertForm(string ID, string IDType, string FullName, string LastName1, string LastName2, string ExpirationIDDate, string CivilStatus, string HomeAddress, string Location1, string Location2, string Location3, string BirthCountryName, string BirthPlaceName, string Citizenship, string SpouseID, string SpouseName, string ChildrenCount, string PhoneHome, string CellPhone, string Email)
        {
            string storedProcedureName = "uSp_PayrollRequestInsertInfo";

            var dtResult = HR_BankFormModel.InsertForm(ID, IDType, FullName, LastName1, LastName2, ExpirationIDDate, CivilStatus, HomeAddress, Location1, Location2, Location3, BirthCountryName, BirthPlaceName, Citizenship, SpouseID, SpouseName, ChildrenCount, PhoneHome, CellPhone, Email, storedProcedureName);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
        public ActionResult GetForm()
        {
            string storedProcedureName = "uSp_PayrollRequestGetInfo";

            var dtResult = HR_BankFormModel.GetForm(storedProcedureName);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

    }
}
