﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class AdminCreditCardsController : Controller
    {
        //
        // GET: /AdminCreditCards/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetCreditCards(string customerId)
        {
            string merchantid = Getmerchantid(customerId);

            DataTable dtResult = AdminCreditCardsModel.GetCreditCards(customerId, merchantid);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult DeleteCreditCards(string CustomerID, string BIN, string L4, string CardType, string CardExpDate)
        {
            string merchantid = Getmerchantid(CustomerID);
            DataTable dtResult = AdminCreditCardsModel.DeleteCreditCards(merchantid, CustomerID, BIN, L4, CardType, CardExpDate);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public string Getmerchantid(string customerId)
        {
            DataTable dtResult = AdminCreditCardsModel.Getmerchantid(customerId);
            string store = AdminCreditCardsModel.RemoveWhitespace(dtResult.Rows[0]["Store"].ToString());
            if (store == "HERITAGE")
            {
                return "1";
            }
            else if (store == "PPH")
            {
                return "3";
            }
            else if (store == "WAGERWEB")
            {
                return "3";
            }
            else
            {
                return "Error";
            }
        }
    }
}
