﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class Acc_HSAffiliateProcessFeeController : Controller
    {
        //
        // GET: /Acc_HSAffiliateProcessFee/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAcc_HSAffiliateProcessFee(string datetocharge)
        {
            DataTable dtResult = Acc_HSAffiliateProcessFeeModel.GetAcc_HSAffiliateProcessFee(datetocharge);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult HSAffiliate_GetMondayToProcess()
        {
            DataTable dtResult = Acc_HSAffiliateProcessFeeModel.HSAffiliate_GetMondayToProcess();

            var result = _BLL.GetJson(dtResult);

            return result;
        }
    }
}
