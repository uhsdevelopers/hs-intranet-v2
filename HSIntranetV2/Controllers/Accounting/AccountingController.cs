﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class AccountingController : Controller
    {
        //
        // GET: /Acc_WWComissions/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LiveDealer()
        {
            return View();
        }

        public ActionResult PHagentHierarchy()
        {
            return View();
        }

        public ActionResult GetPHagentHierarchy(string agentid, string showAll, string betSystem)
        {

            var result = AccountingModel.GetPHagentHierarchy(agentid, showAll, betSystem);

            return result;
        }

        public ActionResult GetNorthBetWinLoss(string dateFrom, string dateTo)
        {

            DataTable dtResult = Acc_WWComissionsModel.GetNorthBetWinLoss(dateFrom, dateTo);

            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetNorthBetBonusAdjustments(string dateFrom, string dateTo)
        {

            DataTable dtResult = Acc_WWComissionsModel.GetNorthBetBonusAdjustments(dateFrom, dateTo);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult LiveDealerVolumeReport(string agentId, string StartDate, string EndDate)
        {
            var cLiveDealer = new LiveDealer();
            var dtResult = cLiveDealer.LiveDealerVolumeReport(agentId, StartDate, EndDate);
            var result = _BLL.GetJson(dtResult);
            return result;
        }
    }
}
