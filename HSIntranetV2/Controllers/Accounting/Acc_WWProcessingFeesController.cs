﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class Acc_WWProcessingFeesController : Controller
    {
        //
        // GET: /Acc_WWProcessingFees/

        // not in use anymore.
        //public ActionResult WWProcessingFeesView()
        //{
        //    return View();
        //}

        // not in use anymore. Use Acc_HSCommissionDetails/FeeDetails instead
        //public ActionResult WWProcessingFeesDetailsView()
        //{
        //    return View();
        //}

        public ActionResult GetWWProcessingFees(string dateFrom, string agentID)
        {
            DataTable dtResult = Acc_WWProcessingFeesModel.Acc_WWProcessingFees(dateFrom, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult GetWWProcessingFeesDetails(string dateFrom, string dateTo, string agentID)
        {
            DataTable dtResult = Acc_WWProcessingFeesModel.Acc_WWProcessingFeesDatails(dateFrom, dateTo, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
