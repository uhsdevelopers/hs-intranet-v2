﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class Acc_WWExcludedTransController : Controller
    {
        //
        // GET: /Acc_WWExcludedTrans/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AgentComision()
        {
            return View();
        }

        public ActionResult GetWWExludedTransaction(string dateFrom, string dateTo)
        {

            DataTable dtResult = Acc_WWExcludedTransactionsModel.GetWWExludedTransaction(dateFrom, dateTo);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult GetAgentComisionExludedTransaction(string dateFrom, string dateTo, string Agent)
        {

            DataTable dtResult = Acc_WWExcludedTransactionsModel.GetAgentComisionExludedTransaction(dateFrom, dateTo, Agent, "uSp_AgentComision_Union_GetExcludedTransactions");

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult GetAgentComisionExludedTransactionPre(string dateFrom, string dateTo, string Agent)
        {

            DataTable dtResult = Acc_WWExcludedTransactionsModel.GetAgentComisionExludedTransaction(dateFrom, dateTo, Agent, "uSp_AgentComision_Union_GetPreApprovedTransactions");

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
