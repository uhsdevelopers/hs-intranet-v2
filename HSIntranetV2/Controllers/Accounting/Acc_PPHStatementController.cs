﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class Acc_PPHStatementController : Controller
    {
        //
        // GET: /Acc_PPHStatement/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetPPHStatement(string dateFrom, string dateTo, string agentID)
        {

            DataTable dtResult = Acc_PPHStatementModel.GetPPHStatement(dateFrom, dateTo, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
