﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class Acc_DGSAgentChargeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetDGSAgentCharge(string dateFrom, string dateTo, string agentID)
        {

            DataTable dtResult = Acc_DGSAgentChargeModel.GetDGSAgentCharge(dateFrom, dateTo, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}