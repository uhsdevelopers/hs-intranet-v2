﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class Acc_WWComissionDetailsController : Controller
    {
        //
        // GET: /Acc_WWComissionDetails/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetWWComissions(string dateFrom, string dateTo)
        {

            DataTable dtResult = Acc_WWComissionDetailsModel.GetWWComissions(dateFrom, dateTo);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
    }
}
