﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class Acc_NewSignupsController : Controller
    {
        //
        // GET: /Acc_NewSignups/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetRate(string dateFrom, string dateTo, string agentId)
        {



            var dtResult = WwConvertionRateModel.GetRateV2(dateFrom, dateTo, agentId);


            var result = _BLL.GetJson(dtResult);


            /*

                        Session["FirstDeposit"] = null;

                        DataSet dsResult = WwConvertionRateModel.GetRate(dateFrom, dateTo, agentID);
                        DataTable dtResult = new DataTable();


                        dtResult = dsResult.Tables[0];

                        Session["FirstDeposit"] = dsResult.Tables[1];



                        var result = _BLL.GetJson(dtResult);*/

            return result;

        }

        public ActionResult GetFirstDeposit(string dateFrom, string dateTo, string agentId)
        {
            var dtResult = WwConvertionRateModel.GetDeposit(dateFrom, dateTo, agentId);

            var result = _BLL.GetJson(dtResult);

            return result;

        }




    }
}
