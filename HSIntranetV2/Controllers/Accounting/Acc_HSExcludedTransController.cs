﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class Acc_HSExcludedTransController : Controller
    {
        //
        // GET: /Acc_HSExcludedTrans/

        //public ActionResult Index()
        //{
        //    return View();

        //    // not in use. instead: Acc_WWExcludedTrans/AgentComision

        //}
        //public ActionResult AgentComision()
        //{
        //    return View();

        //    // not in use. instead: Acc_WWExcludedTrans/AgentComision
        //}

        public ActionResult GetHSExludedTransaction(string dateFrom, string dateTo)
        {

            DataTable dtResult = Acc_HSExcludedTransactionsModel.GetHSExludedTransaction(dateFrom, dateTo);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
