﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class Acc_WWComissionsController : Controller
    {
        //
        // GET: /Acc_WWComissions/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetWWComissions(string dateFrom, string dateTo)
        {

            DataTable dtResult = Acc_WWComissionsModel.GetWWComissions(dateFrom, dateTo);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
    }
}
