﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class Acc_HSCommissionDetailsController : Controller
    {
        //
        // GET: /Acc_HSCommissionDetails/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CommSummary()
        {
            return View();
        }

        public ActionResult FeeDetails()
        {
            return View();
        }

        public ActionResult GetAcc_GetAgentsCommissionSummary(string dateFrom, string agentID)
        {
            DataTable dtResult = Acc_HSCommissionDetailsModel.GetAcc_GetAgentsCommissionSummary(dateFrom, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetAcc_GetCommissionDetalisByAgent(string dateFrom, string dateTo, string agentID)
        {

            DataTable dtResult = Acc_HSCommissionDetailsModel.GetAcc_GetCommissionDetalisByAgent(dateFrom, dateTo, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetAcc_GetFeeDetalisByAgent(string dateFrom, string dateTo, string agentID)
        {

            DataTable dtResult = Acc_HSCommissionDetailsModel.GetAcc_GetFeeDetalisByAgent(dateFrom, dateTo, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
    }
}
