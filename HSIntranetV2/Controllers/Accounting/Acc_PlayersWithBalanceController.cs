﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class Acc_PlayersWithBalanceController : Controller
    {
        //
        // GET: /Acc_PlayersWithBalance/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetBalance(string dateFrom, string agentID, string report, string withZeroBalance, string company = "ASI" )
        {

            string storedProcedureName = "";

            switch (company)
            {
                case "ASI":
                    storedProcedureName = (report == "Player") ? "[uSp_Intranet_GetPlayersWithBalance]" : "[uSp_Intranet_GetAgentsWithBalance]";
                    break;
                case "LP":
                    storedProcedureName = (report == "Player") ? "[uSp_Intranet_GetPlayersWithBalance_LinePros]" : "[uSp_Intranet_GetAgentsWithBalance_LinePros]";
                    break;
                default:
                    storedProcedureName = (report == "Player") ? "[uSp_Intranet_GetPlayersWithBalance]" : "[uSp_Intranet_GetAgentsWithBalance]";
                    break;
            }

            DataTable dtResult = Acc_PlayersWithBalanceModel.GetBalance(dateFrom, agentID, storedProcedureName, report, withZeroBalance);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
