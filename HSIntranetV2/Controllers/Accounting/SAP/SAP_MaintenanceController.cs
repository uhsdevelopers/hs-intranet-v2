﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;


namespace HSIntranetV2.Controllers
{
    public class SAP_MaintenanceController : Controller
    {
        // GET: /SAP_Maintenance/

        #region Public Methods 
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DSR()
        {
            return View();
        }
        public ActionResult GetTranType()
        {
            var dtResult = SAP_MaintenanceModel.GetTranType();

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
        #endregion 

        #region SAP Maintenance
        public ActionResult GetTableInfo(string tableid, string agentId)
        {
            string storedProcedureName = "usp_SAP_Get" + tableid;

            var dtResult = SAP_MaintenanceModel.GetTableInfo(agentId, storedProcedureName);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
        public ActionResult UpdateTableInfo(string tableid, string row, string parameter0, string parameter1, string parameter2, string parameter3, string parameter4, string parameter5, string parameter6, string parameter7)
        {
            string storedProcedureName = "usp_SAP_Update" + tableid;

            var dtResult = SAP_MaintenanceModel.UpdateTableInfo(storedProcedureName, row, parameter0, parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
        public ActionResult DeleteTableInfo(string tableid, string row)
        {
            string storedProcedureName = "usp_SAP_Delete" + tableid;

            var dtResult = SAP_MaintenanceModel.DeleteTableInfo(storedProcedureName, row);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
        public ActionResult InsertTableInfo(string tableid, string parameter0, string parameter1, string parameter2, string parameter3, string parameter4, string parameter5, string parameter6, string parameter7)
        {
            string storedProcedureName = "usp_SAP_Insert" + tableid;

            var dtResult = SAP_MaintenanceModel.InsertTableInfo(storedProcedureName, parameter0, parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
        public ActionResult ExportSAP_Report(string MasterAgentId, string dateFrom, string dateTo, string excludeAgentsTrans, string deposits, string ExcludePHNCustomers)
        {
            string storedProcedureName = "usp_SAP_GetReportHeader";
            var dtResult = SAP_MaintenanceModel.ExportSAP_Report(storedProcedureName, MasterAgentId, dateFrom, dateTo, excludeAgentsTrans, deposits, ExcludePHNCustomers);
            DataTable dtResult0 = dtResult.Tables[0];
            DataTable dtResult1 = dtResult.Tables[1];
            var Result0 = _BLL.GetJson(dtResult0);
            var Result1 = _BLL.GetJson(dtResult1);
            return Json(new { table1 = Result0, table2 = Result1 }, "text/x-json", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region DSR Maintenance
        public ActionResult GetTableInfoDSR(string tableid, string agentId)
        {
            string storedProcedureName = "usp_SAP_DSR_Get" + tableid;

            var dtResult = SAP_MaintenanceModel.GetTableInfo(agentId, storedProcedureName);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
        public ActionResult UpdateTableInfoDSR(string tableid, string row, string parameter0, string parameter1, string parameter2, string parameter3, string parameter4, string parameter5, string parameter6, string parameter7)
        {
            string storedProcedureName = "usp_SAP_DSR_Update" + tableid;

            var dtResult = SAP_MaintenanceModel.UpdateTableInfo(storedProcedureName, row, parameter0, parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
        public ActionResult DeleteTableInfoDSR(string tableid, string row)
        {
            string storedProcedureName = "usp_SAP_DSR_Delete" + tableid;

            var dtResult = SAP_MaintenanceModel.DeleteTableInfo(storedProcedureName, row);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
        public ActionResult InsertTableInfoDSR(string tableid, string parameter0, string parameter1, string parameter2, string parameter3, string parameter4, string parameter5, string parameter6, string parameter7)
        {
            string storedProcedureName = "usp_SAP_DSR_Insert" + tableid;

            var dtResult = SAP_MaintenanceModel.InsertTableInfo(storedProcedureName, parameter0, parameter1, parameter2, parameter3, parameter4, parameter5, parameter6, parameter7);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
        public ActionResult ExportDSR_Report(string MasterAgentId, string dateFrom, string dateTo)
        {
            string storedProcedureName = "usp_SAP_DSR_GetReportHeader";
            var dtResult = SAP_MaintenanceModel.ExportSAP_Report(storedProcedureName, MasterAgentId, dateFrom, dateTo, "false");

            var Result1 = _BLL.GetJson(dtResult.Tables[0]);
            var Result2 = _BLL.GetJson(dtResult.Tables[1]);
            var Result3 = _BLL.GetJson(dtResult.Tables[2]);
            var result = Json(new
            {
                table1 = Result1,
                table2 = Result2,
                table3 = Result3
            }, "application/json", JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        #endregion

    }

}
