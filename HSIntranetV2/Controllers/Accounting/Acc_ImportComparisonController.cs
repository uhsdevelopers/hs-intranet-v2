﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class Acc_ImportComparisonController : Controller
    {
        //
        // GET: /Acc_ImportComparison/

        public ActionResult Index(string usr)
        {
            ViewData["clerk"] = usr;

            if (usr == "finance" || usr == "hugo.leiva" || usr == "federico.ginnari")
            {
                ViewData["delete"] = "1";
            }
            else
            {
                ViewData["delete"] = "0";
            }
            return View();
        }

        public ActionResult GetASIimportTables()
        {

            DataTable dtResult = Acc_ImportComparisonModel.GetASIimportTables();

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetComparison(string table1, string table2, string deleteBackup)
        {

            DataTable dtResult = Acc_ImportComparisonModel.GetComparison(table1, table2, deleteBackup);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
