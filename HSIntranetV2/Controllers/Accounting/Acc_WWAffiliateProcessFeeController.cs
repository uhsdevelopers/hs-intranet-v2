﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class Acc_WWAffiliateProcessFeeController : Controller
    {
        //
        // GET: /Acc_WWAffiliateProcessFee/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetAcc_WWAffiliateProcessFee(string datetocharge)
        {
            DataTable dtResult = Acc_WWAffiliateProcessFeeModel.GetAcc_WWAffiliateProcessFee(datetocharge);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult WWAffiliate_GetMondayToProcess()
        {
            DataTable dtResult = Acc_WWAffiliateProcessFeeModel.WWAffiliate_GetMondayToProcess();

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
