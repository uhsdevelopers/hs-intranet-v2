﻿using HSIntranetV2.Models;
using System.Web.Mvc;



namespace HSIntranetV2.Controllers
{
    [AllowCrossSiteJson]
    public class OfficePoolController : Controller
    {

        public ActionResult IsCustomerAbleToPlay(string customerid, string poolid = "5")
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "sp_IsCustomerAbleToPlay";

            OfficePoolModel OfficePool = new OfficePoolModel();
            var dtResult = OfficePool.IsCustomerAbleToPlay(storedProcedureName, customerid, poolid);
            var result = _BLL.GetJson(dtResult);
            return result;
        }


        public ActionResult GetPoolListOpen(string poolid = "5")
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "spGetPoolListOpen";

            OfficePoolModel OfficePool = new OfficePoolModel();
            var dtResult = OfficePool.GetPoolListOpen(storedProcedureName, poolid);
            var result = _BLL.GetJson(dtResult);
            return result;
        }


        public ActionResult GetPoolListOpenintra(string poolid = "5", string PoolGameId = "164")
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "spGetPoolListOpenIntra";

            OfficePoolModel OfficePool = new OfficePoolModel();
            var dtResult = OfficePool.GetPoolListOpenintra(storedProcedureName, poolid, PoolGameId);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult GetActivePoolList(string PoolId, string CustomerID)
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "spGetCustomerPoolListClosed";

            OfficePoolModel OfficePool = new OfficePoolModel();
            var dtResult = OfficePool.GetActivePoolList(storedProcedureName, PoolId, CustomerID);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult GetPoolCustomerList(string poolGameid = "160")
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "spGetCustomerList";

            OfficePoolModel OfficePool = new OfficePoolModel();
            var dtResult = OfficePool.GetCustomerList(storedProcedureName, poolGameid);
            var result = _BLL.GetJson(dtResult);
            return result;
        }


        public ActionResult GetPoolTournamentTeams(string poolGameid = "161")
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "spGetGameTournamentTeams";

            OfficePoolModel OfficePool = new OfficePoolModel();
            var dtResult = OfficePool.GetPoolTournamentTeams(storedProcedureName, poolGameid);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult GetPoolWinnerList(string poolGameid, string PoolSquareId)
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "usp_GetWinnerList";

            OfficePoolModel OfficePool = new OfficePoolModel();
            var dtResult = OfficePool.GetWinnerList(storedProcedureName, poolGameid, PoolSquareId);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult GetWinnersByPeriodsPlusReverse(string PoolSquareId)
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "spGetWinnersByPeriodsPlusReverse";

            OfficePoolModel OfficePool = new OfficePoolModel();
            var dtResult = OfficePool.GetWinnersByPeriodsPlusReverse(storedProcedureName, PoolSquareId);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult GradeTournamentPosition(string poolGameid, string Position, string TeamId, string user)
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "spAddGameResultByTournamentPosition";

            OfficePoolModel OfficePool = new OfficePoolModel();
            var dtResult = OfficePool.GradeTournamentPosition(storedProcedureName, poolGameid, Position, TeamId, user);
            var result = _BLL.GetJson(dtResult);
            return result;
        }


        public ActionResult AddToInclusionList(string customerid, string user, string InclusionTypeId = "1", string poolid = "5")
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "spAddToInclusionList";

            OfficePoolModel OfficePool = new OfficePoolModel();
            var dtResult = OfficePool.AddToInclusionList(storedProcedureName, customerid, user, poolid, InclusionTypeId);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult AddToExclusionList(string customerid, string user, string InclusionTypeId = "1", string poolid = "5")
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "spAddToExclusionList";

            OfficePoolModel OfficePool = new OfficePoolModel();
            var dtResult = OfficePool.AddToExclusionList(storedProcedureName, customerid, user, poolid, InclusionTypeId);
            var result = _BLL.GetJson(dtResult);
            return result;
        }


        public ActionResult GetTeamstoGrade(string PoolGameId = "165")
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "usp_PoolSquareGetTeamstoGrade";

            OfficePoolModel OfficePool = new OfficePoolModel();
            var dtResult = OfficePool.GetTeamstoGrade(storedProcedureName, PoolGameId);
            var result = _BLL.GetJson(dtResult);
            return result;
        }


        public ActionResult AddGameScoresByPeriod(string PoolGameId, string Period, string Team1Score, string Team2Score, string LastChangeBy = "intranet")
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "spAddGameScoresByPeriod";

            var officePool = new OfficePoolModel();
            var dtResult = officePool.AddGameScoresByPeriod(storedProcedureName, PoolGameId, Period, Team1Score, Team2Score, LastChangeBy);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult CreatePoolGame(string poolId, string cutOffDate, string sportTypeId, string sportsubTypeId, string team1Id, string team2Id)
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "usp_CreatePoolGame";

            var officePool = new OfficePoolModel();
            var dtResult = officePool.CreatePoolGame(storedProcedureName, poolId, cutOffDate, sportTypeId, sportsubTypeId, team1Id, team2Id);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult CreateGamesTournamentTeams(string NewPool, string BasePool)
        {
            //var cLiveDealer = new OfficePool();

            const string storedProcedureName = "usp_CreateGamesTournamentTeams";

            var officePool = new OfficePoolModel();
            var dtResult = officePool.CreateGamesTournamentTeams(storedProcedureName, NewPool, BasePool);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

    }
}