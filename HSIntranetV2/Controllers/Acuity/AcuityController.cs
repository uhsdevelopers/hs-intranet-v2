﻿using HSIntranetV2.Models;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using static HSIntranetV2.Models.Acuity.Acuity;



namespace HSIntranetV2.Controllers.Acuity
{
    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
            filterContext.RequestContext.HttpContext.Response.AddHeader("Content-Type", "application/json");
            base.OnActionExecuting(filterContext);
        }
    }

    [AllowCrossSiteJson]
    public class AcuityController : Controller
    {
        [AllowCrossSiteJson]
        // GET: Acuity
        public ActionResult Index(string usr)
        {
            ViewData["clerk"] = usr;
            return View();
        }

        public async Task<ActionResult> photoIdOnlineVerificationAsync(string customerid, string clerk)
        {

            var finalresponse = new AcuityResponse();
            var isnewrequest = GetelfieListBycustomer(customerid);
            var signup = new Signup_Services.Models.Signup();

            var AcuityService = new AcuityServiceResponse();

            string replacedString = customerid;

            AcuityPersonal personalinfo = GetCustomerInfo(customerid);

            if (string.IsNullOrEmpty(personalinfo.email))
            {
                finalresponse.status = "-98";
                finalresponse.Description = "Error: Account information is not present in Signup Request (Probably a Manual Account or old account Previous 2014)";
            }
            else
            {



                if (isnewrequest.reference_id == null)
                {

                    AcuityRequest AcuityRequest = new AcuityRequest
                    {
                        country = personalinfo.country,
                        email = personalinfo.email,
                        language = "EN",
                        usernumber = personalinfo.SignupRequest,
                        reference = personalinfo.SignupRequest,
                        selected_service = System.Configuration.ConfigurationManager.AppSettings["AcuitySelectedService"],
                        Merchant = System.Configuration.ConfigurationManager.AppSettings["ATmerchant_id"],
                        Password = System.Configuration.ConfigurationManager.AppSettings["ATpassword"],
                        redirect_url = System.Configuration.ConfigurationManager.AppSettings["redirect_url"]
                    };
                    var strPost = "";
                    try
                    {
                        strPost = await LiveCheckVerification(AcuityRequest);
                        replacedString = strPost.Replace("/", "");
                    }
                    catch (Exception e)
                    {
                        replacedString = "Error";
                    }

                    if (replacedString != "Error")
                    {
                        AcuityService = JsonConvert.DeserializeObject<AcuityServiceResponse>(strPost);


                        if (AcuityService.status == 0)
                        {



                            var added = AddSelfieVerification(customerid, AcuityService.description, AcuityService.reference_id,
                                AcuityService.verification_source, AcuityService.status.ToString(), clerk);

                            if (added)
                            {
                                finalresponse.ReferenceId = AcuityService.reference_id;
                                finalresponse.VerificationSource = AcuityService.verification_source;
                                finalresponse.Description = AcuityService.description;
                                finalresponse.status = AcuityService.status.ToString();
                                isnewrequest.verification_source = AcuityService.verification_source;

                                var suspend = deactivateCustomer(customerid);

                                if (suspend)
                                {
                                    finalresponse.Description += " Suspended";
                                }
                                else
                                {
                                    finalresponse.Description += " Unable suspend";
                                }
                            }
                        }
                        else
                        {
                            if (AcuityService.status == -147)
                            {
                                finalresponse.Description += " Account not Registered on Acuity";
                            }
                            else
                            {
                                finalresponse.Description += " Error" + AcuityService.description;
                            }

                        }
                    }

                    finalresponse.ReferenceId = AcuityService.reference_id;
                    finalresponse.VerificationSource = AcuityService.verification_source;
                    finalresponse.Description += AcuityService.description;
                    finalresponse.status = AcuityService.status.ToString();
                    isnewrequest.verification_source = AcuityService.verification_source;
                }
                else
                {
                    finalresponse.ReferenceId = isnewrequest.reference_id;
                    finalresponse.VerificationSource = isnewrequest.verification_source;
                    finalresponse.Description = isnewrequest.description;
                    finalresponse.status = isnewrequest.status.ToString();
                    isnewrequest.verification_source = AcuityService.verification_source;

                }

                if (finalresponse.status == "0")
                {
                    var AcuitySubject = System.Configuration.ConfigurationManager.AppSettings["AcuitySubject"];
                    var strMyTextFile = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/verification.html");
                    var emailbody = System.IO.File.ReadAllText(strMyTextFile);

                    if (signup.sendVerification(personalinfo.name, customerid, isnewrequest.verification_source,
                            personalinfo.email, emailbody, AcuitySubject))
                    {
                        finalresponse.Description = isnewrequest.description + " Email sent";
                    }
                    else
                    {
                        finalresponse.Description = isnewrequest.description + " Email Error";
                    }
                }

            }
            replacedString = JsonConvert.SerializeObject(finalresponse);



            var responsetoUI = "[" + replacedString + "]";

            var Result = new ContentResult
            {
                ContentEncoding = Encoding.UTF8,
                Content = responsetoUI,
                ContentType = "application/json"
            };

            return Content(responsetoUI);


        }



        public ActionResult GetList(string startDate, string endDate, string customerid = "")
        {

            DataTable dtResult = GetelfieList(startDate, endDate, customerid);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }


        public ActionResult CallbackResult(string AcuityId, string status = "0")
        {

            DataTable dtResult = GetCallbackResult(AcuityId, status);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }


    }
}