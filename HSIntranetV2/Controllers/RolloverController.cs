﻿using HSIntranetV2.Models;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class RolloverController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetCalculation()
        {
            return View();
        }

        public ActionResult GetDetails()
        {
            return View("GetCalculationDetails");
        }

        public ActionResult SetRollover()
        {
            return View();
        }

        public ActionResult GetResult(string customerID, string startDate, string endDate)
        {

            DataTable dtResult = RolloverModal.GetResult(customerID, startDate, endDate);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public async Task<ActionResult> GetCalculationM(string customerid)
        {
            if (customerid is null)
            {
                throw new ArgumentNullException(nameof(customerid));
            }

            //var url = "http://vm-intranet02:8020/api/rollover/GetRollOverCalculationValidation?pCustomerID=";

            var url = "https://servicessta.txnservice.com/api/rollover/GetRollOverCalculationValidation?pCustomerID=";

            //var url = "http://localhost:55374/api/rollover/GetRollOverCalculationValidation?pCustomerID=";

            url = url + customerid;
            var responseBody = "";
            var responsetoUI = "";

            using (var handler = new HttpClientHandler())
            {
                handler.UseDefaultCredentials = true;

                using (var client = new HttpClient(handler))
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    response.Headers.Add("User-Agent", "User-Agent: Other");
                    response.Headers.Add("Accept", "/");

                    response = await client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);

                    response.EnsureSuccessStatusCode();

                    responseBody = await response.Content.ReadAsStringAsync();
                }

                responsetoUI = "[" + responseBody + "]";
            }




            return Content(responsetoUI);
        }

        public async Task<ActionResult> GetCalculationMv2(string customerid)
        {
            var responseBody = "";
            if (customerid is null)
            {
                throw new ArgumentNullException(nameof(customerid));
            }

            var responsetoUI = "";
            var url = "https://servicessta.txnservice.com/api/rollover/GetRollOverCalculationValidation?pCustomerID=";
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml");
            // client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Encoding", "gzip, deflate");
            client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");


            url = url + customerid;

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();


            responseBody = await response.Content.ReadAsStringAsync();

            return Content(responseBody);
        }


        public async Task<ActionResult> GetCalculationDetails(string customerid)
        {
            if (customerid is null)
            {
                throw new ArgumentNullException(nameof(customerid));
            }

            var url = "http://vm-intranet02:8020/api/rollover/GetRollOverCalculationDetails?CustomerID=";

            url = url + customerid;
            HttpClient client = new HttpClient();
            string responseFromServer = await client.GetStringAsync(url);

            //var responsetoUI = "[" + responseFromServer + "]";



            return Content(responseFromServer);
        }


        public async Task<HttpResponseMessage> Validate(string baseUri, HttpContent content)
        {
            HttpClient client = new HttpClient();

            return await client.PostAsync(baseUri, content);
        }


        public async Task<string> SetRollOverPayment(string customerid, string Amount)
        {
            if (customerid is null)
            {
                throw new ArgumentNullException(nameof(customerid));
            }


            var data = new
            {
                pCustomerID = customerid,
                pAmount = Amount
            };

            var datat = JsonConvert.SerializeObject(data);



            var url = "http://vm-intranet02:8020/api/RollOver/SetRollOverPayment";


            HttpClient client = new HttpClient();
            var content = new StringContent(datat, Encoding.UTF8, "application/json");

            var result = await Validate(url, content);
            //var response = await client.PostAsync(url, content);
            var contents = await result.Content.ReadAsStringAsync();
            // var response = await client.PostAsync("/api/server/startup", null);

            var responsetoUI = "[" + contents + "]";

            return responsetoUI;

        }
    }
}