﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class FreeBetsController : Controller
    {
        //
        // GET: /FreeBets/

        public ActionResult Index(string usr)
        {
            Session["clerk"] = usr;
            return View();
        }

        public ActionResult GetFreeBets(string CustomerID)
        {
            string storedProcedureName = "[uSp_Inet_FreeBetGetFreeBets]";
            DataTable dtResult = FreeBetsModel.GetFreeBets(CustomerID, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult EditFreeBets(string prmFreeBetID, string prmAmount, string prmActive, string prmExpirationDate)
        {
            string storedProcedureName = "[uSp_Inet_FreeBetSet]";
            DataTable dtResult = FreeBetsModel.EditFreeBets(prmFreeBetID, prmAmount, prmActive, prmExpirationDate, Session["clerk"].ToString(), storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult DeleteFreeBets(string prmFreeBetID)
        {
            string storedProcedureName = "[uSp_Inet_FreeBetDeleteTicketReference]";
            DataTable dtResult = FreeBetsModel.DeleteFreeBets(prmFreeBetID, Session["clerk"].ToString(), storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult GetFreeBetsCombos()
        {
            string storedProcedureName = "[uSp_Inet_FreeBetGetFreeBet]";
            DataTable dtResult = FreeBetsModel.GetFreeBetsCombos(storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult AddFreeBets(string CustomerID, string prmFreeBetID, string prmAmount, string prmExpirationDate)
        {
            string storedProcedureName = "[uSp_Inet_FreeBetInsert]";
            DataTable dtResult = FreeBetsModel.AddFreeBet(CustomerID, prmFreeBetID, prmAmount, prmExpirationDate + " 23:59:59", Session["clerk"].ToString(), storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
    }
}
