﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class GOBetController : Controller
    {
        //
        // GET: /GOBet/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetBets()
        {
            return View("GetBets");
        }

        public ActionResult ValidateGOBCustomer(string customerID)
        {

            DataTable dtResult = GOBetModel.ValidateGOBCustomer(customerID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult CancelGOBBet(string customerID)
        {

            DataTable dtResult = GOBetModel.CancelGOBBet(customerID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetGobBet(string customerID)
        {

            DataTable dtResult = GOBetModel.GetGobBet(customerID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult delete_wager(string customerID, string ticketnumber, string wagernumber, string gamenum)
        {

            DataTable dtResult = GOBetModel.Hs_GetOutOfYourBet(customerID, ticketnumber, wagernumber, gamenum);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
