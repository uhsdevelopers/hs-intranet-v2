﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class CashbackBlackListController : Controller
    {
        //
        // GET: /CashbackBlackList/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetTransAccList(string customerId)
        {
            DataTable dtResult = CashbackBlackListModel.GetTransAccList(customerId);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult DeleteTransAcc(string customerId)
        {
            DataTable dtResult = CashbackBlackListModel.DeleteTransAcc(customerId);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult InsertTransAcc(string customerId)
        {
            DataTable dtResult = CashbackBlackListModel.InsertTransAcc(customerId);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
