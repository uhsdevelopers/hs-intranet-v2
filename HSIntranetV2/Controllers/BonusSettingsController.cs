﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class BonusSettingsController : Controller
    {
        //
        // GET: /BonusSettings/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetCustomerInfo(string customerid, int mask = 1)
        {
            string storedProcedureName = "uSp_Inet_GetCustomerPInformation";

            var dtResult = BonusSettingsModel.GetCustomerInfo(customerid, mask, storedProcedureName);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult GetCustomerBonus(string customerid)
        {
            string storedProcedureName = "[uSp_Intranet_GetBonusExclusion]";

            var dtResult = BonusSettingsModel.GetCustomerBonus(customerid, storedProcedureName);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult SetCustomerInfo(string customerid, string BonusExclusion, string UseAGreaterBonus)
        {

            var dtResult = BonusSettingsModel.SetCustomerInfo(customerid, BonusExclusion, UseAGreaterBonus);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

    }
}
