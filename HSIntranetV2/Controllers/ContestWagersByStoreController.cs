﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class ContestWagersByStoreController : Controller
    {
        //
        // GET: /ContestWagersByStore/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetContestWagersByStore(string dateFrom, string dateTo, string store)
        {

            DataTable dtResult = ContestWagersByStoreModel.GetContestWagersByStore(dateFrom, dateTo, store);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
