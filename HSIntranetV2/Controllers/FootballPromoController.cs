﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class FootballPromoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetResult(string customerID, string freebetAmount)
        {

            DataTable dtResult = FootballPromoModal.GetResult(customerID, freebetAmount);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
    }
}