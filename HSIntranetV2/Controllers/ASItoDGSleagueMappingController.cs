﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class ASItoDGSleagueMappingController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSportsMapping(string lookup)
        {

            DataTable dtResult = ASItoDGSmappingSportsModel.GetSportsMapping(lookup);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetLeagues()
        {

            DataTable dtResult = ASItoDGSmappingSportsModel.GetLeagues();

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetGameTypes()
        {

            DataTable dtResult = ASItoDGSmappingSportsModel.GetGameTypes();

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetIdSport()
        {

            DataTable dtResult = ASItoDGSmappingSportsModel.GetIdSport();

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult SaveSportMapping(string id, string sportType, string sportSubType, string idLeague, string league, string idGameType, string idSport)
        {

            DataTable dtResult = ASItoDGSmappingSportsModel.SaveSportMapping(id, sportType, sportSubType, idLeague, league, idGameType, idSport);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
    }
}