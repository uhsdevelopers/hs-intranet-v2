﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class CancelGOYBController : Controller
    {
        //
        // GET: /CancelGOYB/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetGOYB(string customerID)
        {

            DataTable dtResult = CancelGOYBModel.GetGOYB(customerID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult GetDeletedGOYB(string customerID)
        {

            DataTable dtResult = CancelGOYBModel.GetDeletedGOYB(customerID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult CancelGOYB(string id_game, string ticket_number, string wager_number, string to_win_amount, string amount_wagered, string customer_id, string description)
        {

            CancelGOYBModel.CancelGOYBLog(id_game, ticket_number, wager_number, to_win_amount, amount_wagered, customer_id, description);
            DataTable dtResult = CancelGOYBModel.CancelGOYB(ticket_number, wager_number, customer_id);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
