﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class PHN_ReportsController : Controller
    {
        //
        // GET: /PHN_Reports/

        public ActionResult PHN_PlayerCountView()
        {
            return View();
        }
        public ActionResult PHN_PlayerCount(string agentID, string dateFrom, string dateTo)
        {
            DataTable dtResult = PNH_ReportModel.PHN_PlayerCount(dateFrom, dateTo, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult PHN_DepositView()
        {
            return View();
        }
        public ActionResult PHN_Deposit(string agentID, string dateFrom, string dateTo)
        {
            DataTable dtResult = PNH_ReportModel.PHN_Deposit(dateFrom, dateTo, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult PHN_BalanceReportView()
        {
            return View();
        }
        public ActionResult PHN_BalanceReport(string agentID)
        {
            DataTable dtResult = PNH_ReportModel.PHN_BalanceReport(agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
