﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class NewGraderPageController : Controller
    {
        //
        // GET: /NewGraderPage/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetSports(string sport)
        {

            var Result = new ContentResult();

            DataTable dtResult = NewGraderModel.GetSportDetailsList(sport);

            Result = _BLL.GetJson(dtResult);

            return Result;

        }

        public ActionResult GetAllSports()
        {
            var Result = new ContentResult();

            DataTable dtResult = NewGraderModel.GetAllSports();

            Result = _BLL.GetJson(dtResult);

            return Result;

        }

        public ActionResult GetOpenGames(string sport, string sportSubType)
        {
            var Result = new ContentResult();

            DataTable dtResult = NewGraderModel.GetOpenGames(sport, sportSubType);

            Result = _BLL.GetJson(dtResult);

            return Result;

        }

        public ActionResult GetGradedGames(string sport, string sportSubType, string days)
        {
            var Result = new ContentResult();

            DataTable dtResult = NewGraderModel.GetGradedGames(sport, sportSubType, days);

            Result = _BLL.GetJson(dtResult);

            return Result;

        }

        public ActionResult Getperiod(string sport, string sportSubType)
        {
            var Result = new ContentResult();

            DataTable dtResult = NewGraderModel.Getperiod(sport, sportSubType);

            Result = _BLL.GetJson(dtResult);

            return Result;

        }

        public ActionResult GradingMain(string gameNum, string periodNum, string comments, string gameCancelledFlag, string team1Score, string team2Score, string eopTeam1Score, string eopTeam2Score, string startingPitcher1, string startingPitcher2, string gradeSpreadReqFlag, string gradeMoneyLineReqFlag, string gradeTtlPtsReqFlag, string cancelSpreadFlag, string cancelMoneyLineFlag, string cancelTtlPtsFlag, string dailyFigureDate, string requestedBy = "TestPage", string InetTarget = "", string Store = "")
        {
            var Result = new ContentResult();

            DataTable dtResult = NewGraderModel.GradingMain(gameNum, periodNum, comments, gameCancelledFlag, team1Score, team2Score, eopTeam1Score, eopTeam2Score, startingPitcher1, startingPitcher2, gradeSpreadReqFlag, gradeMoneyLineReqFlag, gradeTtlPtsReqFlag, cancelSpreadFlag, cancelMoneyLineFlag, cancelTtlPtsFlag, dailyFigureDate, requestedBy, InetTarget, Store);

            Result = _BLL.GetJson(dtResult);

            return Result;

        }

        public ActionResult GetGraderResults(string gameNum, string PeriodNumber)
        {
            var Result = new ContentResult();

            DataTable dtResult = NewGraderModel.GetGraderResults(gameNum, PeriodNumber);

            Result = _BLL.GetJson(dtResult);

            return Result;

        }

        public ActionResult GetPointsByPeriod(string gameNum, string PeriodNumber)
        {
            var Result = new ContentResult();

            DataTable dtResult = NewGraderModel.GetPointsByPeriod(gameNum, PeriodNumber);

            Result = _BLL.GetJson(dtResult);

            return Result;

        }
    }
}
