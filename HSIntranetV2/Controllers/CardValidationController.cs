﻿using HSIntranetV2.Models;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HSIntranetV2.Controllers
{
    [AllowCrossSiteJson]
    public class CardValidationController : Controller
    {
        static HttpPostedFileBase File;
        static HttpPostedFileBase File2;
        static HttpPostedFileBase File3;

        #region impersonateValidUser
        public const int LOGON32_LOGON_INTERACTIVE = 2;
        public const int LOGON32_PROVIDER_DEFAULT = 0;

        WindowsImpersonationContext impersonationContext;
        private bool impersonateValidUser(String userName, String domain, String password)
        {
            WindowsIdentity tempWindowsIdentity;
            IntPtr token = IntPtr.Zero;
            IntPtr tokenDuplicate = IntPtr.Zero;

            if (RevertToSelf())
            {
                if (LogonUserA(userName, domain, password, LOGON32_LOGON_INTERACTIVE,
                    LOGON32_PROVIDER_DEFAULT, ref token) != 0)
                {
                    if (DuplicateToken(token, 2, ref tokenDuplicate) != 0)
                    {
                        tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                        impersonationContext = tempWindowsIdentity.Impersonate();
                        if (impersonationContext != null)
                        {
                            CloseHandle(token);
                            CloseHandle(tokenDuplicate);
                            return true;
                        }
                    }
                }
            }
            if (token != IntPtr.Zero)
                CloseHandle(token);
            if (tokenDuplicate != IntPtr.Zero)
                CloseHandle(tokenDuplicate);
            return false;
        }

        private void undoImpersonation()
        {
            impersonationContext.Undo();
        }

        [DllImport("advapi32.dll")]
        public static extern int LogonUserA(String lpszUserName,
            String lpszDomain,
            String lpszPassword,
            int dwLogonType,
            int dwLogonProvider,
            ref IntPtr phToken);
        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int DuplicateToken(IntPtr hToken,
            int impersonationLevel,
            ref IntPtr hNewToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool RevertToSelf();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern bool CloseHandle(IntPtr handle);
        #endregion

        #region Upload_Imagefile and SaveCustomerDocument
        public ActionResult Upload_Imagefile(string Customer, string type, string description, string bin = "", string l4 = "", string typecard = "", string ExpDate = "", string action = "1")
        {
            var Result = new ContentResult();
            String patchCard = ConfigurationManager.AppSettings["PatchCard"];
            int fileContentLength = Convert.ToInt32(ConfigurationManager.AppSettings["FileContentLength"]);
            String fileContentError = ConfigurationManager.AppSettings["FileContentError"];
            string error = "Y";
            try
            {

                if (impersonateValidUser("hswebstatus", "heritagesports.com", "123456"))
                {
                    var time = DateTime.Now.ToString("yyyy_MM_dd HH'_'mm'_'ss");
                    var path = "";
                    var path2 = "";
                    var path3 = "";
                    File = this.HttpContext.Request.Files.Get("file");
                    File2 = this.HttpContext.Request.Files.Get("file2");
                    File3 = this.HttpContext.Request.Files.Get("file3");
                    if (File.ContentLength == 0 && File2.ContentLength == 0 && File3.ContentLength == 0)
                    {
                        error = "Please attach a file";
                    }
                    else if (File.ContentLength > fileContentLength && File2.ContentLength > fileContentLength && File3.ContentLength > fileContentLength)
                    {
                        error = fileContentError;
                    }
                    else
                    {
                        if (File.ContentLength != 0)
                        {
                            string typefile1 = "Invalid";
                            switch (File.ContentType.ToString())
                            {
                                case "image/jpeg":
                                    typefile1 = ".jpeg";
                                    break;
                                case "image/png":
                                    typefile1 = ".png";
                                    break;
                                case "application/pdf":
                                    typefile1 = ".pdf";
                                    break;
                                default:
                                    typefile1 = "Invalid";
                                    break;
                            }
                            if (typefile1 != "Invalid")
                            {
                                string fileName = "";
                                if (type == "1")
                                {
                                    fileName = "PhotoId_front_" + Customer + typefile1;
                                }
                                else if (type == "2")
                                {
                                    fileName = "UtilityBill_front_" + Customer + typefile1;
                                }
                                else if (type == "3")
                                {
                                    fileName = "Authorization_Form_" + Customer + typefile1;
                                }
                                else if (type == "4")
                                {
                                    fileName = "PhoneBill_front_" + Customer + typefile1;
                                }
                                else
                                {
                                    fileName = "";
                                }

                                path = Path.Combine(string.Format("////{0}/{1}_{2}", patchCard, time, fileName));

                                var data = new byte[File.ContentLength];
                                File.InputStream.Read(data, 0, File.ContentLength);

                                using (var sw = new FileStream(path, FileMode.Create))
                                {
                                    sw.Write(data, 0, data.Length);
                                }

                            }
                            else
                            {
                                error = "Firts file is invalid (only image or pdf)";
                            }
                        }
                        else
                        {
                            error = "Please attach the file in the first field";
                        }
                        if (File2.ContentLength != 0 && error == "Y" && (type == "1" || type == "3"))
                        {
                            string typefile2 = "Invalid";
                            switch (File2.ContentType.ToString())
                            {
                                case "image/jpeg":
                                    typefile2 = ".jpeg";
                                    break;
                                case "image/png":
                                    typefile2 = ".png";
                                    break;
                                case "application/pdf":
                                    typefile2 = ".pdf";
                                    break;
                                default:
                                    typefile2 = "Invalid";
                                    break;
                            }
                            if (typefile2 != "Invalid")
                            {
                                string fileName = (type == "1")
                                    ? "PhotoId_back_" + Customer + typefile2
                                    : "Front_Card_" + Customer + typefile2;
                                path2 = Path.Combine(string.Format("////{0}/{1}_{2}", patchCard, time, fileName));

                                var data = new byte[File2.ContentLength];
                                File2.InputStream.Read(data, 0, File2.ContentLength);

                                using (var sw = new FileStream(path2, FileMode.Create))
                                {
                                    sw.Write(data, 0, data.Length);
                                }

                            }
                            else
                            {
                                error = "The second File is invalid (only image or pdf)";
                            }
                        }
                        if (File3 != null && (File3.ContentLength != 0 && error == "Y" && type == "3"))
                        {
                            string typefile3 = "Invalid";
                            switch (File3.ContentType.ToString())
                            {
                                case "image/jpeg":
                                    typefile3 = ".jpeg";
                                    break;
                                case "image/png":
                                    typefile3 = ".png";
                                    break;
                                case "application/pdf":
                                    typefile3 = ".pdf";
                                    break;
                                default:
                                    typefile3 = "Invalid";
                                    break;
                            }
                            if (typefile3 != "Invalid")
                            {
                                string fileName = "Back_Card_" + Customer + typefile3;
                                path3 = Path.Combine(string.Format("////{0}/{1}_{2}", patchCard, time, fileName));

                                var data = new byte[File3.ContentLength];
                                File3.InputStream.Read(data, 0, File3.ContentLength);

                                using (var sw = new FileStream(path3, FileMode.Create))
                                {
                                    sw.Write(data, 0, data.Length);
                                }
                            }
                            else
                            {
                                error = "The third File is invalid (only image or pdf)";
                            }
                        }
                        if (error == "Y")
                        {
                            SaveCustomerDocument(Customer, path, path2, path3, type, description, action);
                            if (type == "3")
                            {
                                ChangeStatusCCard(Customer, bin, l4, typecard, ExpDate, "U", "");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            var serializer = new JavaScriptSerializer();
            Result = new ContentResult

            {
                ContentEncoding = Encoding.UTF8,

                Content = serializer.Serialize(new
                {
                    error
                }),
                ContentType = "application/json"
            };

            return Result;
        }

        public void SaveCustomerDocument(string customerId, string filePath, string filePath2, string filePath3, string type, string description, string action)
        {
            //if (!_DAL.haspermission())
            //{
            //    return RedirectToAction("Index", "Error");string FileType, string Description, string FilePath,
            //}
            string fileType;
            if (type == "1")
            {
                description = "To validate full name: " + description;
                fileType = "PhotoId";
            }
            else if (type == "3")
            {
                description = "To validate Credit Card: " + description;
                fileType = "Authorization Form";
            }
            else if (type == "4")
            {
                description = "To validate PhoneBill: " + description;
                fileType = "Phone Bill";
            }
            else
            {
                description = (action == "1") ? "To validate address: " + description : "To change address: " + description;
                fileType = "Utility Bill";
            }
            const string storedProcedureName = "[uSp_Local_DocIDInsertDocument]";
            var dtResult = CardValidationService.SaveCustomerDocument(customerId, fileType, filePath, filePath2, filePath3, description, storedProcedureName);

        }
        #endregion

        #region ValidateCreditCard
        public string ValidateCreditCard(string customerid, string customerip, string cardnumber, string expdate, string cardtype, string amount)
        {

            if (string.IsNullOrEmpty(customerip))
            {
                return ("IP is not accepted");
            }
            string merchantid = GetCustomerCompanyContry(customerid, "company");

            if (string.IsNullOrEmpty(merchantid))
            {
                return ("company is not accepted");
            }
            cardnumber = CardValidationService.RemoveWhitespace(cardnumber);

            var regex = new Regex(@"^6(?:011|5[0-9]{2})[0-9]{12}$");
            var match = regex.Match(cardnumber);
            if (match.Success)
            {
                return ("Discover Card are not accepted");
            }
            expdate = CardValidationService.RemoveWhitespace(expdate);
            var customerInformation = new CardValidationService.customer_information();
            const string storedProcedureName = "spBetSvcGetCustomerInfo";
            string status = "", validatereturn = "";
            DataTable dtResult = CardValidationService.segetCustomerinfo(customerid, storedProcedureName);
            foreach (DataRow row in dtResult.Rows)
            {
                customerInformation.first_name = row["NameFirst"].ToString();
                customerInformation.last_name = row["NameLast"].ToString();
                customerInformation.email = row["EMail"].ToString();
                customerInformation.address1 = row["Address"].ToString();
                customerInformation.address2 = row["Address"].ToString();
                customerInformation.city = row["City"].ToString();
                customerInformation.province = row["State"].ToString();
                customerInformation.postal_code = row["Zip"].ToString();
                customerInformation.country = row["Country"].ToString();
                customerInformation.phone1 = row["HomePhone"].ToString();
                customerInformation.phone2 = row["BusinessPhone"].ToString();
            }

            var deposit_limits = new CardValidationService.deposit_limits();
            var payment_method = new CardValidationService.payment_method();
            string time = DateTime.Now.ToString("yyyy-MM-dd HH':'mm':'ss");
            string trans_id = CardValidationService.CalculateMD5Hash(customerid + time);

            deposit_limits.pay_method_type = "CC";
            payment_method.BIN = cardnumber.Substring(0, 6);
            // payment_method.last_digits = cardnumber.Substring(12, 4);
            payment_method.last_digits = cardnumber.Substring((cardnumber.Length - 4), 4);
            payment_method.card_hash = CardValidationService.CalculateMD5Hash(cardnumber);


            //***** Method: New Transaction *****//
            CardValidationService.return_NewTransaction returnNewTransaction = NewTransaction(customerInformation, deposit_limits, payment_method, customerid, customerip, amount, trans_id, time);
            if (float.Parse(returnNewTransaction.score) < 80 && returnNewTransaction.status == "0")
            {
                status = "1";
                validatereturn = AddNewCard(merchantid, customerid, cardnumber, expdate, cardtype);
            }
            else
            {
                status = "5";
                validatereturn = (returnNewTransaction.rec == null) ? "Internal Error" : returnNewTransaction.rec;
            }

            //***** Method: Update Transaction *****//
            var taskupdateTransactionStatus = new Task(() => UpdateTransactionStatus(trans_id, returnNewTransaction.internal_trans_id, status, returnNewTransaction.reason, returnNewTransaction.processors));
            taskupdateTransactionStatus.Start();


            return validatereturn;
        }

        public string AddNewCard(string merchantid, string customerId, string cardnumber, string CardExp, string typecard)
        {
            string error = "", storedProcedureName = "[HSPay_KYC_AddNewCC]";
            DataTable dtResult = CardValidationService.AddNewCard(merchantid, customerId, cardnumber, typecard, CardExp, storedProcedureName);
            var Result = _BLL.GetJson(dtResult);

            try
            {
                if (dtResult.Rows[0]["ResultCode"].ToString() == "1")
                {
                    error = "Y";
                }
                else
                {
                    error = dtResult.Rows[0]["ResultDesc"].ToString();
                }

            }
            catch (Exception e)
            {

                error = "Database Error... " + e.Message;
            }
            return error;
        }

        public string ChangeStatusCCard(string customerId, string bin, string l4, string type, string CardExpDate, string status, string statusreason)
        {
            string error = "", storedProcedureName = "[HSPay_KYC_ChangeStatusCC]";
            string Country = GetCustomerCompanyContry(customerId, "country");
            string MerchantId = GetCustomerCompanyContry(customerId, "company");
            DataTable dtResult = CardValidationService.ChangeStatusCCard(customerId, bin, l4, type, status, MerchantId, Country, CardExpDate, statusreason, storedProcedureName);
            var Result = _BLL.GetJson(dtResult);

            try
            {
                if (dtResult.Rows[0]["ResultCode"].ToString() == "1")
                {
                    error = "Y";
                }
                else
                {
                    error = dtResult.Rows[0]["ResultDesc"].ToString();
                }

            }
            catch (Exception e)
            {
                error = "Database Error... " + e.Message;
            }
            return error;
        }

        public ActionResult GetCustomerCards(string customerId)
        {
            string storedProcedureName = "[HSPay_KYC_GetRegisteredCC]";
            string merchantid = GetCustomerCompanyContry(customerId, "company");
            if (string.IsNullOrEmpty(merchantid))
            {
                return Content("[]");
            }

            DataTable dtResult = CardValidationService.GetCustomerCards(customerId, merchantid, storedProcedureName);
            var Result = _BLL.GetJson(dtResult);
            return Result;
        }

        public string GetCustomerCompanyContry(string customerId, string data)
        {
            const string storedProcedureName = "[uSp_Local_GetCustStoreAndLocation]";
            string return_data = "";
            DataTable dtResult = CardValidationService.GetCustomerCompanyContry(customerId, storedProcedureName);
            try
            {
                if (data == "country")
                {
                    return_data = dtResult.Rows[0]["Country"].ToString();
                }
                else if (data == "company")
                {
                    string store = CardValidationService.RemoveWhitespace(dtResult.Rows[0]["Store"].ToString());
                    switch (store)
                    {
                        case "HERITAGE":
                            return_data = "1";
                            break;
                        case "HSTESTMA":
                            return_data = "1";
                            break;
                        case "PPH":
                            return_data = "2";
                            break;
                        case "WAGERWEB":
                            return_data = "3";
                            break;
                        default:
                            if (string.IsNullOrEmpty(store))
                            {
                                return_data = "0";
                            }
                            break;
                    }
                }
            }
            catch (Exception)
            {
                if (data == "country")
                {
                    return_data = "US";
                }
                else if (data == "company")
                {
                    return_data = "1";
                }
            }

            return return_data;
        }
        #endregion

        #region AT Services
        private CardValidationService.return_NewTransaction NewTransaction(CardValidationService.customer_information customer_information, CardValidationService.deposit_limits deposit_limits, CardValidationService.payment_method payment_method, string customerid, string customerip, string amount, string trans_id, string time)
        {
            var response = new CardValidationService.return_NewTransaction();
            var acuityTecUrl = System.Configuration.ConfigurationManager.AppSettings["AcuityTecURL"];
            var webserviceUrl = acuityTecUrl + "/newtransaction";
            string aTmerchantId = System.Configuration.ConfigurationManager.AppSettings["ATmerchant_id"];
            string aTpassword = System.Configuration.ConfigurationManager.AppSettings["ATpassword"];

            try
            {
                var webRequest = System.Net.WebRequest.Create(webserviceUrl);
                webRequest.Method = "POST";
                webRequest.Timeout = 20000;
                webRequest.ContentType = "application/x-www-form-urlencoded";
                String postData = String.Format("merchant_id=" + aTmerchantId +
                                                "&password=" + aTpassword +
                                                "&user_name=" + customerid +
                                                "&user_number=" + customerid +
                                                "&customer_information[first_name]=" + customer_information.first_name +
                                                "&customer_information[last_name]=" + customer_information.last_name +
                                                "&customer_information[email]=" + customer_information.email +
                                                "&customer_information[address1]=" + customer_information.address1 +
                                                "&customer_information[address2]=" + customer_information.address2 +
                                                "&customer_information[city]=" + customer_information.city +
                                                "&customer_information[province]=" + customer_information.province +
                                                "&customer_information[postal_code]=" + customer_information.postal_code +
                                                "&customer_information[country]=" + customer_information.country +
                                                "&customer_information[phone1]=" + customer_information.phone1 +
                                                "&customer_information[phone2]=" + customer_information.phone2 +
                                                "&first_dep_date=" +
                                                "&first_with_date=" +
                                                "&deposit_limits[pay_method_type]=" + deposit_limits.pay_method_type +
                                                "&trans_id=" + trans_id +
                                                "&payment_method[bin]=" + payment_method.BIN +
                                                "&payment_method[last_digits]=" + payment_method.last_digits +
                                                "&payment_method[card_hash]=" + payment_method.card_hash +
                                                "&amount=" + amount +
                                                "&currency=USD" +
                                                "&time=" + time +
                                                "&status=Undefined" +
                                                "&ip=" + customerip
                                                );
                var byteArray = Encoding.UTF8.GetBytes(postData);
                webRequest.ContentLength = byteArray.Length;
                Stream datastream = webRequest.GetRequestStream();
                datastream.Write(byteArray, 0, byteArray.Length);
                using (var s = webRequest.GetResponse().GetResponseStream())
                {
                    using (var sr = new StreamReader(s))
                    {
                        var newTransaction = sr.ReadToEnd();
                        response = new JavaScriptSerializer().Deserialize<CardValidationService.return_NewTransaction>(newTransaction);
                    }
                }
            }
            catch (WebException ex)
            {
                response.status = "-2";
                response.desc = ex.Message;
            }
            return response;
        }

        private void UpdateTransactionStatus(string trans_id, string internal_trans_id, string status, string reason, string processor)
        {
            var response = new CardValidationService.return_UpdateTransaction();
            var acuityTecUrl = System.Configuration.ConfigurationManager.AppSettings["AcuityTecURL"];
            var webserviceUrl = acuityTecUrl + "/updatetransaction";
            string aTmerchantId = System.Configuration.ConfigurationManager.AppSettings["ATmerchant_id"];
            string aTpassword = System.Configuration.ConfigurationManager.AppSettings["ATpassword"];

            try
            {
                var webRequest = System.Net.WebRequest.Create(webserviceUrl);
                webRequest.Method = "POST";
                webRequest.Timeout = 20000;
                webRequest.ContentType = "application/x-www-form-urlencoded";
                String postData = String.Format("merchant_id=" + aTmerchantId +
                                                "&password=" + aTpassword +
                                                "&trans_id=" + trans_id +
                                                "&internal_trans_id=" + internal_trans_id +
                                                "&status=" + status +
                                                "&reason=" + reason +
                                                "&processor=" + processor
                                                );
                var byteArray = Encoding.UTF8.GetBytes(postData);
                webRequest.ContentLength = byteArray.Length;
                Stream datastream = webRequest.GetRequestStream();
                datastream.Write(byteArray, 0, byteArray.Length);
                using (var s = webRequest.GetResponse().GetResponseStream())
                {
                    using (var sr = new StreamReader(s))
                    {
                        var updateTransaction = sr.ReadToEnd();
                        response = new JavaScriptSerializer().Deserialize<CardValidationService.return_UpdateTransaction>(updateTransaction);
                    }
                }
            }
            catch (WebException ex)
            {
                response.status = "-2";
                response.description = ex.Message;
            }

        }
        #endregion

        #region ********** for test ***********
        public ActionResult GetCustomerDocument(string customerId, string DateFrom, string DateTo, string Status, string sportbookId = "5")
        {
            //if (!_DAL.haspermission())
            //{
            //    return RedirectToAction("Index", "Error");
            //}
            const string storedProcedureName = "[uSp_Local_DocIDGetDocument]";
            DataTable dtResult = CardValidationService.GetCustomerDocument(customerId, DateFrom + " 00:00:00", DateTo + " 23:59:59", Status, storedProcedureName, sportbookId);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult setCustomerDocument(string CustomerId, string typefile, string status, string bin, string l4, string cardtype, string ExpDate, string rowID, string statusreason, string ChangedBy)
        {
            if (string.IsNullOrEmpty(ChangedBy))
            {
                return Content("Error: Your Session Expired!");
            }

            string storedProcedureName = "[uSp_Local_DocIDUpdateFileStatus]";
            statusreason = (status == "1") ? "" : statusreason;
            DataTable dtResult = CardValidationService.setCustomerDocument(CustomerId, rowID, status, statusreason, storedProcedureName, ChangedBy);
            if (typefile == "Authorization Form")
            {
                ChangeStatusCCard(CustomerId, bin, l4, cardtype, ExpDate, (status == "1") ? "V" : "F", statusreason);
            }
            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult test(string clerk, string sid = "1")
        {
            if (string.IsNullOrEmpty(clerk))
            {
                return Content("Error: Your Session has  Expired!");
            }

            ViewBag.clerk = clerk;
            ViewBag.Sportbookid = sid;
            return View();
        }

        public ActionResult PHN()
        {
            return View();
        }

        public ActionResult Onserf()
        {
            return View();
        }

        public ActionResult GetCustomerInfo(string customerId)
        {
            //if (!_DAL.haspermission())
            //{
            //    return RedirectToAction("Index", "Error");
            //}
            var customer_information = new CardValidationService.customer_information();
            var serializer = new JavaScriptSerializer();
            DataTable dtResult = CardValidationService.GetCustomerPInformation(customerId, "[uSp_Inet_GetCustomerPInformation1]");
            foreach (DataRow row in dtResult.Rows)
            {
                customer_information.first_name = row["NameFirst"].ToString() + " " + row["NameLast"].ToString();
                customer_information.customerId = row["CustomerID"].ToString();
                customer_information.email = row["EMail"].ToString();
                customer_information.phone1 = row["HomePhone"].ToString();
                customer_information.address1 = row["Address"].ToString();
                customer_information.EmailVerified = row["EmailVerified"].ToString();
                customer_information.PhoneVerified = row["Codeid"].ToString();
            }
            dtResult = CardValidationService.segetCustomerinfo(customerId, "[uSp_Local_DocIDValidateFileStatus]");
            foreach (DataRow row in dtResult.Rows)
            {
                if (row["filetype"].ToString() == "Utility Bill")
                    customer_information.AddressVerified = row["status"].ToString();

                if (row["filetype"].ToString() == "PhotoId")
                    customer_information.FullnameVerified = row["status"].ToString();
                if (row["filetype"].ToString() == "Phone Bill")
                    customer_information.PhoneBillVerified = row["status"].ToString();
            }

            var Result = new ContentResult

            {

                ContentEncoding = Encoding.UTF8,

                Content = serializer.Serialize(new

                {
                    customer_information
                })

            };

            return Result;
        }

        public ActionResult setCustomervalidation(string customerId, string typefile, string ChangedBy)
        {
            string error = "Y";
            try
            {

                if (typefile == "PhoneVerified")
                {
                    CardValidationService.Usp_UpdateCallCode(customerId, "[Usp_UpdateCallCode]", ChangedBy);
                }
                else if (typefile == "FullnameVerified")
                {
                    CardValidationService.uSp_Local_DocIDInsertDocument(customerId, "PhotoId", "[uSp_Local_DocIDInsertDocument]", ChangedBy);
                }
                else if (typefile == "AddressVerified")
                {
                    CardValidationService.uSp_Local_DocIDInsertDocument(customerId, "Utility Bill", "[uSp_Local_DocIDInsertDocument]", ChangedBy);
                }
                else if (typefile == "PhoneBillVerified")
                {
                    CardValidationService.uSp_Local_DocIDInsertDocument(customerId, "Phone Bill", "[uSp_Local_DocIDInsertDocument]", ChangedBy);
                }
            }
            catch (Exception e)
            {

                error = e.Message;
            }
            var serializer = new JavaScriptSerializer();
            var Result = new ContentResult

            {
                ContentEncoding = Encoding.UTF8,

                Content = serializer.Serialize(new

                {
                    error
                })
            };

            return Result;
        }


        public ActionResult validateCustomerInfo(string usr)
        {
            ViewBag.clerk = usr;
            return View();
        }
        public ActionResult validateCustomerInfoOnSerf()
        {
            return View();
        }


        #endregion
    }
}