﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class MoveGamesController : Controller
    {
        //
        // GET: /MoveGames/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSports(string sport)
        {

            var Result = new ContentResult();

            DataTable dtResult = MoveGamesModel.GetSportDetailsList(sport);

            Result = _BLL.GetJson(dtResult);

            return Result;

        }

        public ActionResult GetAllSports()
        {
            var Result = new ContentResult();

            DataTable dtResult = MoveGamesModel.GetAllSports();

            Result = _BLL.GetJson(dtResult);

            return Result;

        }
        public ActionResult GetGames(string sport, string sportSubType, string dateFrom, string dateTo, string RotationFrom, string RotationTo)
        {
            if (string.IsNullOrEmpty(RotationFrom))
            {
                RotationFrom = null;
            }
            if (string.IsNullOrEmpty(RotationTo))
            {
                RotationTo = null;
            }
            var Result = new ContentResult();

            DataTable dtResult = MoveGamesModel.GetOpenGames(sport, sportSubType, dateFrom, dateTo, RotationFrom, RotationTo);

            Result = _BLL.GetJson(dtResult);

            return Result;

        }
        public ActionResult SetGames(string sport, string sportSubType, string dateFrom, string dateTo, string RotationFrom, string RotationTo, string NewSportType, string NewSportSubType)
        {
            if (string.IsNullOrEmpty(RotationFrom))
            {
                RotationFrom = null;
            }
            if (string.IsNullOrEmpty(RotationTo))
            {
                RotationTo = null;
            }
            var Result = new ContentResult();

            DataTable dtResult = MoveGamesModel.SetOpenGames(sport, sportSubType, dateFrom, dateTo, RotationFrom, RotationTo, NewSportType, NewSportSubType);

            Result = _BLL.GetJson(dtResult);

            return Result;

        }
    }
}
