﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    [AllowCrossSiteJson]
    public class CustomerController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FakeName()
        {
            return View();
        }

        public ActionResult SundayParlay()
        {
            return View();
        }




        public ActionResult UpdateCustomer(string customerid, string IsNewWeb = "Y")
        {
            const string storedProcedureName = "spInetUpdateCustomerMarkedForNewWebSite";
            var dtResult = DepositBonusModel.UpdateCustomer(storedProcedureName, customerid, IsNewWeb);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult UpdateCustomerFe(string customerid, string isNewFe = "Y")
        {
            const string storedProcedureName = "spInetUpdateCustomerMarkedForNewFE";
            var dtResult = DepositBonusModel.UpdateCustomer(storedProcedureName, customerid, isNewFe);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetCustomerInformation(string customerid)
        {
            const string storedProcedureName = "uSp_Inet_GetCustomerPInformation1";
            var dtResult = DepositBonusModel.GetCustomerInformation(storedProcedureName, customerid);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetFakeName(string wasused = "0", string country = "USA")
        {
            const string storedProcedureName = "spIntra_GetFakeName";
            var dtResult = DepositBonusModel.GetFakeName(storedProcedureName, wasused, country);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult GetSundayParlay(string startDate, string endDate, string minAmount = "20", string picks = "2")
        {
            if (string.IsNullOrEmpty(startDate))
            {
                Response.StatusCode = 404;
                return Content("Error");
            }

            const string storedProcedureName = "uSp_BetRegal_GetSundayParlay";
            var dtResult = DepositBonusModel.GetSundayParlay(storedProcedureName, startDate, endDate, minAmount, picks);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult General_BlockCC(string CustomerID, string User, string MerchantID = "3")
        {


            const string storedProcedureName = "DP_BlockCCDepositOptionNoRes";
            var Updated = DepositBonusModel.General_BlockCC(storedProcedureName, MerchantID, CustomerID, User);


            var response = "{\"Updated\":\"true\"}";

            if (!Updated)
            {
                response = "{\"Updated\":\"false\"}";
            }




            return Content(response.ToString()); ;
        }


        public ActionResult UpdateFakeName(string id, string wasused = "0")
        {
            const string storedProcedureName = "spIntra_MarkFakeName";
            var dtResult = DepositBonusModel.UpdateFakeName(storedProcedureName, id, wasused);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }


        public ActionResult UpdateCustomerToken(string CustomerID, string token)
        {
            const string storedProcedureName = "Hs_signup_UpdateDBCustomerToken";
            var dtResult = DepositBonusModel.UpdateDBCustomerToken(storedProcedureName, CustomerID, token);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }


        public ActionResult GiveOCRBonus(string CustomerID, int bonusID)
        {

            int rounds = 5;
            string games = "210|221";
            string bankid = "5339";
            var dtResult = Casino.GiveOCRBonus(CustomerID, bonusID, rounds, games, bankid);


            //var Result = dtResult.ToString();


            return Content(dtResult.ToString()); ;
        }


        public ActionResult GetNewAccountsWW(string startDate)
        {

            if (string.IsNullOrEmpty(startDate))
            {
                Response.StatusCode = 404;
                return Content("Error");
            }
            const string storedProcedureName = "Usp_GetManualAccountsWW";
            var dtResult = DepositBonusModel.GetNewManualAccount(storedProcedureName, startDate);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult GetCustomerByCountry(string country)
        {
            const string storedProcedureName = "spIntra_GetCustomerByCountry";
            var dtResult = DepositBonusModel.GetCustomerByCountry(storedProcedureName, country);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


    }
}