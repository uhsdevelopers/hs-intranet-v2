﻿using HSIntranetV2.Models;
using HSIntranetV2.Models.CSD;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers.CSD
{
    public class CsdDepositController : Controller
    {
        // GET: CsdDeposit
        public ActionResult Index(string usr)
        {
            ViewData["clerk"] = usr;
            return View();
        }

        // GET: CsdDeposit/Details/5
        // GET: CsdDeposit
        public ActionResult Details(string usr)
        {
            ViewData["clerk"] = usr;
            return View();
        }

        // GET: CsdDeposit/Details/5
        // GET: CsdDeposit
        public ActionResult CustomerDeposit(string usr)
        {
            ViewData["clerk"] = usr;
            return View();
        }

        public ActionResult SevenDeposit(string usr)
        {
            ViewData["clerk"] = usr;
            return View();
        }

        public ActionResult GetCustomerDepositExceptions(string CustomerID = "", string TransactionID = "", string Clerkid = "")
        {
            var csdDeposit = new CsdDeposits
            {
                TransactionID = TransactionID,
                Customerid = CustomerID,
                ClerkId = Clerkid
            };


            const string storedProcedureName = "uSp_Intranet_GetCustomerDepositExceptions";
            var dtResult = CsdDeposits.GetDepositList(storedProcedureName, csdDeposit);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }


        public ActionResult AddCustomerDepositExceptions(string CustomerID = "", string TransactionID = "", string Clerkid = "", string status = "1")
        {
            var csdDeposit = new CsdDeposits
            {
                TransactionID = TransactionID,
                Customerid = CustomerID,
                ClerkId = Clerkid,
                DStatus = status
            };


            const string storedProcedureName = "uSp_Intranet_AddCustomerDepositExceptions";
            var dtResult = CsdDeposits.AddDepositException(storedProcedureName, csdDeposit);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }


        public ActionResult ChangeStatusDepositException(string ID, string status = "2")
        {

            if (string.IsNullOrEmpty(ID))
            {
                Response.StatusCode = 404;
                return Content("Error");
            }


            const string storedProcedureName = "uSp_Intranet_RemoveCustomerDepositExceptions";
            var dtResult = CsdDeposits.ChangeStatusDepositException(storedProcedureName, ID, status);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }


        public ActionResult GetCustomerDepositList(string Customerid, string startDate = "2020-01-01")
        {

            if (string.IsNullOrEmpty(Customerid))
            {
                Response.StatusCode = 404;
                return Content("Error");
            }


            //const string storedProcedureName = "uSp_Local_GetLastNDeposit";
            const string storedProcedureName = "uSp_Local_GetLastNDepositAdvanced";
            //uSp_Local_GetLastNDepositAdvanced 
            var dtResult = CsdDeposits.GetCustomerDepositList(storedProcedureName, Customerid, startDate);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetSevenDepositList(string startDate = "2020-12-01")
        {
            //const string storedProcedureName = "uSp_Local_GetLastNDeposit";
            const string storedProcedureName = "uSp_Local_GetLastNDepositSummarized";
            //uSp_Local_GetLastNDepositAdvanced 
            var dtResult = CsdDeposits.GetSevenDepositList(storedProcedureName, startDate);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        // GET: CsdDeposit/Create
        public ActionResult Create(string usr)
        {
            ViewData["clerk"] = usr;
            return View();
        }

        // POST: CsdDeposit/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CsdDeposit/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CsdDeposit/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CsdDeposit/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CsdDeposit/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
