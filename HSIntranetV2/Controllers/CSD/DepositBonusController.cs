﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class DepositBonusController : Controller
    {
        //
        // GET: /DepositBonus/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DepositAndBonus()
        {
            return View("Analysis");
        }

        public ActionResult GetDepositReport(string dateStart, string dateEnd, string customerid = null, string initPercentage = "80", string endPercentage = "110", string sportsbookid = "Heritage", string cProfile = "-110")
        {

            if (string.IsNullOrEmpty(customerid))
            {
                customerid = null;
            }
            const string storedProcedureName = "uSp_Local_AnalysisDepositWagerFreePlay";
            var dtResult = DepositBonusModel.GetReport(storedProcedureName, dateStart, dateEnd, customerid, initPercentage, endPercentage, cProfile);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult GetDepositAndBonus(string dateStart, string dateEnd, string customerid = null, string sportsbookid = "Heritage", string cProfile = null, string store = null, string businessUnit = "HERITAGE", int timeout = 120)
        {

            if (string.IsNullOrEmpty(customerid))
            {
                customerid = null;
            }
            if (store == "All")
            {
                store = null;
            }
            if (cProfile == "All")
            {
                cProfile = null;
            }
            const string storedProcedureName = "uSp_Local_AnalysisDepositAndBonus";
            var dtResult = DepositBonusModel.GetAnalysisDepositAndBonusReport(storedProcedureName, dateStart, dateEnd, customerid, businessUnit, store, cProfile, timeout);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

    }
}