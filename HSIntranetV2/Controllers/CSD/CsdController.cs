﻿using HSIntranetV2.Models;
using System.Web.Mvc;

public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
{
    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
        filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
        filterContext.RequestContext.HttpContext.Response.AddHeader("Content-Type", "application/json");
        base.OnActionExecuting(filterContext);
    }
}

namespace HSIntranetV2.Controllers
{
    [AllowCrossSiteJson]
    public class CsdController : Controller
    {
        public ActionResult CasinoReport()
        {
            return View();
        }

        public ActionResult CasinoReportFinal()
        {
            return View();
        }

        public ActionResult Casinoblock()
        {
            return View();
        }

        public ActionResult AccumulatedRisk()
        {
            return View();
        }

        public ActionResult BrGetRevenue()
        {
            return View();
        }

        public ActionResult CasinoTransactions()
        {
            return View();
        }

        public ActionResult CashRewardCustomerEligibleBets()
        {
            return View();
        }

        public ActionResult GetcasinoGeneralProfit()
        {
            var cLiveDealer = new LiveDealer();
            var dtResult = cLiveDealer.GetCasinoProfit();
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult GetcasinoGeneralFinal()
        {
            var cLiveDealer = new LiveDealer();
            var dtResult = cLiveDealer.GetCasinoProfitFinal();
            var result = _BLL.GetJson(dtResult);
            return result;
        }




        public ActionResult ActivateCustomer(string customerId, string user = "Internet")
        {
            const string storedProcedureName = "uSp_Local_ActivateCustomer ";
            var dtResult = DepositBonusModel.ActivateCustomer(storedProcedureName, customerId, user);


            var result = _BLL.GetJson(dtResult);

            return result;
        }



        public ActionResult CasinoBlockUpdate(string customerId, string product = "CASINO", string casinoAccess = "y", string Permanent = "N", string user = "Internet")
        {
            var cLiveDealer = new LiveDealer();
            var dtResult = cLiveDealer.CasinoBlockUpdate(customerId, product, casinoAccess, Permanent, user);
            var result = _BLL.GetJson(dtResult);
            return result;
        }


        public ActionResult GetcasinoBlock(string product = "Casino")
        {
            var CSD = new LiveDealer();
            var dtResult = CSD.GetCasinoBlock(product);
            var result = _BLL.GetJson(dtResult);
            return result;
        }


        public ActionResult GetAccumulatedCustomerRisk(string customerId, string StartDate, string EndDate, int UseLiveDate = 0)
        {
            if (string.IsNullOrEmpty(StartDate) || string.IsNullOrEmpty(EndDate) || string.IsNullOrEmpty(customerId))
            {
                return Content("{ 'Error':'Invalid parameter' }");
            }

            var CSD = new LiveDealer();
            var dtResult = CSD.GetAccumulatedCustomerRisk(customerId, StartDate, EndDate, UseLiveDate);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult GetCashRewardCustomerEligibleBets(string customerId, string StartDate, string EndDate, int RewardPaymentTypeId = 6)
        {
            if (string.IsNullOrEmpty(StartDate) || string.IsNullOrEmpty(EndDate) || string.IsNullOrEmpty(customerId))
            {
                return Content("{ 'Error':'Invalid parameter' }");
            }

            var CSD = new LiveDealer();
            var dtResult = CSD.GetCashRewardCustomerEligibleBets(customerId, StartDate, EndDate, RewardPaymentTypeId);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult BetRegalCA_GetRevenue(string StartDate, string EndDate)
        {
            if (string.IsNullOrEmpty(StartDate) || string.IsNullOrEmpty(EndDate))
            {
                return Content("{ 'Error':'Invalid parameter' }");
            }

            var csd = new LiveDealer();
            var dtResult = csd.BetRegalCA_GetRevenue(StartDate, EndDate);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult GetSignup(string startDate, string endDate, string sid)
        {
            if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate) || string.IsNullOrEmpty(sid))
            {
                return Content("{ 'Error':'Invalid parameter' }");
            }

            var csd = new LiveDealer();
            var dtResult = csd.GetSignup(startDate, endDate, sid);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult DeleteSignRequests(string RequestID, string RequestedBy)
        {
            if (string.IsNullOrEmpty(RequestID) || string.IsNullOrEmpty(RequestedBy))
            {
                return Content("{ 'Error':'Invalid parameter' }");
            }

            var csd = new LiveDealer();
            var dtResult = csd.DeleteSignRequests(RequestID, RequestedBy);
            var result = _BLL.GetJson(dtResult);
            return result;
        }


        public ActionResult BypassSignup(string requestId, string user)
        {
            if (string.IsNullOrEmpty(requestId) || string.IsNullOrEmpty(user))
            {
                return Content("{ 'Error':'Invalid parameter' }");
            }

            var csd = new LiveDealer();
            var dtResult = csd.BypassSignup(requestId, user);

            var result = _BLL.GetJson(dtResult);
            return result;
            //var signupdata = new LiveDealer.SignupByPass2
            //    {
            //        statusid = dtResult.Rows[0]["statusid"].ToString(),
            //        Codeid = dtResult.Rows[0]["codeid"].ToString()
            //    };

            //var dataString = JsonConvert.SerializeObject(signupdata);

            //return Content(dataString);
        }

        public ActionResult UpdatePdSignup(string requestId, string phoneNumber = "", string email = "", string PromoEmails = "")
        {
            if (string.IsNullOrEmpty(requestId))
            {
                return Content("{ 'Error':'Invalid parameter' }");
            }

            var csd = new LiveDealer();
            var dtResult = csd.SignUp_UpdatePhoneEmail(requestId, phoneNumber, email, PromoEmails);
            var result = _BLL.GetJson(dtResult);
            return result;


        }



    }
}