﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class LimitBonusController : Controller
    {
        //
        // GET: /LimitBonus/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetLimitBonusList()
        {
            string storedProcedureName = "[uSp_Local_GreatBonusGetCustomers]";
            DataTable dtResult = LimitBonusModel.GetLimitBonusList(storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult SetLimitBonusByCustomer(string CustomerID, string hasBigBonus)
        {
            string storedProcedureName = "[uSp_Local_GreatBonusSet]";
            DataTable dtResult = LimitBonusModel.SetLimitBonusByCustomer(CustomerID, hasBigBonus, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
