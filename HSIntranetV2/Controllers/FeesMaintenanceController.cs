﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class FeesMaintenanceController : Controller
    {
        //
        // GET: /FeesMaintenance/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetFeesMethod(string TranType = "")
        {
            DataTable dtResult = FeesMaintenanceModel.GetFeesMethod(TranType);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult UpdateFeesMethod(string ID, string TranType, string TranDesc, string Method, string CRMMethod, string Strt, string ENDD, string RTE, string PERCNTAGE, string MIXMODE, string PREAPPROVED)
        {
            DataTable dtResult = FeesMaintenanceModel.UpdateFeesMethod(ID, TranType.ToUpper(), TranDesc, Method, CRMMethod, Strt, ENDD, RTE, PERCNTAGE, MIXMODE, PREAPPROVED);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult InsertFeesMethod(string TranType, string TranDesc, string Method, string CRMMethod, string Strt, string ENDD, string RTE, string PERCNTAGE, string MIXMODE, string PREAPPROVED)
        {
            DataTable dtResult = FeesMaintenanceModel.InsertFeesMethod(TranType.ToUpper(), TranDesc, Method, CRMMethod, Strt, ENDD, RTE, PERCNTAGE, MIXMODE, PREAPPROVED);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult DeleteFees(string ID)
        {
            DataTable dtResult = FeesMaintenanceModel.DeleteFees(ID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
