﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class NotificationController : Controller
    {
        //
        // GET: /Notification/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult SendMessage(string user, string token, string recipient, string message)
        {
            var result = NotificationService.SendSms(user, token, recipient, message);



            return Content(result);
        }
    }
}
