﻿using HSIntranetV2.Models;
using System;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class PokerController : Controller
    {
        //
        // GET: /Poker/

        public class PokerResponse
        {
            public string ErrorCode { get; set; }
            public string ErrorDescription { get; set; }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Customer()
        {
            return View("Customer");
        }

        // GET: /Poker/GetPokerTransactions/ 
        public ActionResult GetPokerTransactions(string customerID, string startDate, string endDate, string tranCode, string status, string timeZone, string summary)
        {

            PokerModel cPoker = new PokerModel();
            cPoker.customerID = customerID;
            cPoker.startDate = startDate;
            cPoker.endDate = endDate;
            cPoker.tranCode = tranCode;
            cPoker.status = status;
            cPoker.timeZone = timeZone;
            cPoker.summary = summary;

            DataTable dtResult = PokerModel.GetPokerTransactions(cPoker);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        // GET: /Poker/GetPokerTransactions/ 
        public ActionResult GetCustomerLimits(string customerID)
        {

            PokerModel cPoker = new PokerModel();
            cPoker.customerID = customerID;

            const string storedProcedureName = "uSp_Poker_ValidateAccumulatedList";
            DataTable dtResult = PokerModel.GetCustomerLimits(storedProcedureName, cPoker.customerID);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }


        // GET: /Poker/GetPokerTransactions/ 
        public JsonResult ChangePokerPassword(string customerID)
        {


            var idmpoker = new IDMPoker.ExternalIdmWsSoapClient();


            PokerModel cPoker = new PokerModel();
            cPoker.customerID = customerID;

            DataSet dsResult = PokerModel.GetCustomerPassword(customerID, "1");
            DataTable dtResult = new DataTable();

            dtResult = dsResult.Tables[0];

            PokerResponse password2 = new PokerResponse();

            string password3 = dsResult.Tables[0].Rows[0][0].ToString();


            //var Result = _BLL.GetJson(dtResult);

            HSIntranetV2.IDMPoker.cResult Responsepoker1;

            try
            {
                Responsepoker1 = idmpoker.ChangePlayerPassword("1002", "fHKbsmjfWU", customerID, password3);
                password2.ErrorCode = Responsepoker1.error;
                password2.ErrorDescription = Responsepoker1.errordescr;
            }
            catch (Exception e)
            {
                password2.ErrorCode = e.Source;
                password2.ErrorDescription = e.Message;
            }

            //var Responsepoker = new IDMPoker.ChangePlayerPasswordResponse(Responsepoker1);
            //var Result = _BLL.GetJson(Responsepoker);



            var Responsepoker = Json(password2, "application/json", JsonRequestBehavior.AllowGet);


            return Responsepoker;
        }

    }
}
