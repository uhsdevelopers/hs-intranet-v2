﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class LineManagerController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ImporterLog()
        {
            return View();
        }

        public ActionResult GetDGSreleaseGrader()
        {

            DataTable dtResult = LineManagerModel.GetDGSreleaseGrader();

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult FixASIBalance()
        {

            DataTable dtResult = LineManagerModel.FixASIBalance();

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult FixDGSBalance()
        {

            DataTable dtResult = LineManagerModel.FixDGSBalance();

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetImporterLog(string teamRot1)
        {
            if (teamRot1 != null)
            {


                DataTable dtResult = LineManagerModel.GetImporterLog(teamRot1);

                var result = _BLL.GetJson(dtResult);

                return result;
            }

            return Content("RotNumber Required");
        }

    }
}