﻿using HSIntranetV2.Models;
using System;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{

    [AllowCrossSiteJson]
    public class SupportController : Controller
    {
        // GET: Support
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetPCode()
        {
            return View();
        }

        public ActionResult CheckLastTransaction()
        {
            return View();
        }

        public ActionResult sites()
        {
            return View();
        }

        public ActionResult Latebets()
        {
            return View();
        }

        public ActionResult SMSMessageList()
        {
            return View();
        }



        public ActionResult GetSiteStats(string startdate, string enddate = null, string domain = "")
        {
            if (enddate == null)
            {
                enddate = DateTime.UtcNow.ToString();
            }

            const string storedProcedureName = "uSp_GetSiteStatistics";
            var dtResult = Support.GetsiteStats(storedProcedureName, startdate, enddate, domain);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }


        public ActionResult GetSMSMessageList(string IsActive = "1")
        {


            const string storedProcedureName = "sp_GetSMSMessageList";
            var dtResult = Support.GetMessageList(storedProcedureName, IsActive);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult GetCodes(string startdate)
        {


            const string storedProcedureName = "usp_GetPayoutCode";
            var dtResult = Support.GetPayoutCode(storedProcedureName, startdate);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult LastTransaction()
        {


            const string storedProcedureName = "uSp_Replica_CheckLastActivity";
            var dtResult = Support.CheckLastActivity(storedProcedureName);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult GetLateBets(string startDate, string endDate, string increase)
        {


            const string storedProcedureName = "spIntra_GetLateBets";
            var dtResult = Support.GetLateBets(storedProcedureName, startDate, endDate, increase);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
    }
}