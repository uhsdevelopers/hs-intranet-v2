﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class FeedbackController : Controller
    {
        //
        // GET: /Feedback/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetMobileRatings(string dateStart, string dateEnd)
        {
            string storedProcedureName = "sp_IntraGetMobileRatings";
            DataTable dtResult = FeedbackModel.GetFeedback(storedProcedureName, dateStart, dateEnd);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
    }
}
