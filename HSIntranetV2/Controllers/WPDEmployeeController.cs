﻿using HSIntranetV2.Models;
using System;
using System.Data;
using System.Web.Mvc;


namespace HSIntranetV2.Controllers
{
    [AllowCrossSiteJson]
    public class WpdEmployeeController : Controller
    {
        //
        // GET: /WPDEmployee/




        public ActionResult Index(string usr, string token)
        {
            if (usr != null)
            {
                ViewData["clerk"] = usr;
                //var result = ValidateToken(usr, token);

                var hasAccess = ValidateAccess(usr, "http://intranet/tool/WPdComments.asp?1");
                if (hasAccess)
                {
                    ViewBag.Tab = "true";
                    ViewBag.TabText = "<li><a href='#tabs-2'>Add New Comment</a></li>";
                    ViewBag.Update = "<button id='btnUpdate'> Update </button>";

                }
                else
                {
                    ViewBag.Tab = "false";
                }

                var canDelete = ValidateAccess(usr, "http://intranet/tool/WPdComments.asp?2");
                if (canDelete)
                {
                    ViewBag.user = ViewData["clerk"];
                    ViewBag.Delete = "<button id='btnDelete'> delete </button>";
                }


                //           var isvalid = result["isvalid"].ToString();
            }

            return View();
        }

        public ActionResult Log()
        {



            return View("Log");
        }

        public ActionResult Csd(string usr, string token)
        {
            if (usr != null)
            {
                ViewData["clerk"] = usr;
                //var result = ValidateToken(usr, token);

                var hasAccess = ValidateAccess(usr, "http://intranet/tool/WPdComments.asp?1");
                if (hasAccess)
                {
                    ViewBag.Tab = "true";
                    ViewBag.TabText = "<li><a href='#tabs-2'>Add New Comment</a></li>";
                    ViewBag.Update = "<button id='btnUpdate'> Update </button>";

                }
                else
                {
                    ViewBag.Tab = "false";
                }

                var canDelete = ValidateAccess(usr, "http://intranet/tool/WPdComments.asp?2");
                if (canDelete)
                {
                    ViewBag.user = ViewData["clerk"];
                    ViewBag.Delete = "<button id='btnDelete'> delete </button>";
                }


                //           var isvalid = result["isvalid"].ToString();
            }
            return View("Csd");
        }

        public string CurrentUser()
        {
            var username = Environment.UserName;
            ViewData["clerk"] = username;
            return username;

        }



        public bool ValidateAccess(string employeeName, string pageLink)
        {
            const string storedProcedureName = "spSec_ValidatePageAccess";
            var hasAccess = WpdEmployeeModel.HasAccess(employeeName, pageLink, storedProcedureName);
            return hasAccess;
        }


        public ActionResult ValidateToken(string employeeName = "", string token = "")
        {
            const string storedProcedureName = "Hs_VerifyTokenIntranet";
            DataTable dtResult = WpdEmployeeModel.ValidateToken(employeeName, token, storedProcedureName);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        // GET: /WPDEmployee/GetCommentsByEmployee/ 
        public ActionResult GetCommentsByEmployee(string employeeName = "", string commentBy = "", DateTime dateFrom = default(DateTime), DateTime dateTo = default(DateTime), string hrrRequestId = "", string isPositiveComment = "", string departmentId = "")
        {
            const string storedProcedureName = "spIntra_SearchWPDEmployeeComments";
            DataTable dtResult = WpdEmployeeModel.GetCommentsByEmployee(employeeName, commentBy, dateFrom, dateTo, hrrRequestId, isPositiveComment, departmentId, storedProcedureName);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        // GET: /WPDEmployee/GetCommentsByLog/ 
        public ActionResult GetCommentsByLog(DateTime dateFrom = default(DateTime), DateTime dateTo = default(DateTime))
        {
            const string storedProcedureName = "spIntra_SearchWPDEmployeeLog";
            var dtResult = WpdEmployeeModel.GetCommentsByLog(dateFrom, dateTo, storedProcedureName);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        // GET: /WPDEmployee/AddCommentsToEmployee/ 

        public ActionResult AddCommentsToEmployee(string employeeName, string comment, string commentBy, string incidentdate, string departmentId, string hrrRequestId, string isPositive)
        {
            var message = Server.HtmlEncode(comment);
            const string storedProcedureName = "spIntra_AddWPDEmployeeComments";

            var hasAccess = ValidateAccess(commentBy, "http://intranet/tool/WPdComments.asp?1");


            if (!hasAccess)
            {
                return Content("unauthorized user : Contact WPD Manager");
            }
            else
            {
                if (employeeName == null || message == null || commentBy == null || incidentdate == null)
                {
                    var result = new EmptyResult();

                    return result;
                }
                else
                {
                    var dtResult = WpdEmployeeModel.AddCommentToEmployee(employeeName, message, commentBy, incidentdate,
                                                                         departmentId, hrrRequestId, isPositive,
                                                                         storedProcedureName);

                    var result = _BLL.GetJson(dtResult);

                    return result;
                }
            }
        }


        // GET: /WPDEmployee/GetEmployeeList/ 
        public ActionResult GetEmployeeList(string departmentId, string status = "A")
        {
            const string storedProcedureName = "spIntra_GetWPDEmployeeList";
            var dtResult = WpdEmployeeModel.GetEmployeeList(departmentId, status, storedProcedureName);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        // GET: /WPDEmployee/GetRequestType/ 
        public ActionResult GetRequestType()
        {
            const string storedProcedureName = "HRR_GetRequestType";
            var dtResult = WpdEmployeeModel.HRR_GetRequestType(storedProcedureName);


            var result = _BLL.GetJson(dtResult);
            if (result == null) throw new ArgumentNullException("result");

            return result;
        }

        // GET: /WPDEmployee/DeleteComment/ 
        public ActionResult DeleteComment(string commentId, string commentBy)
        {
            const string storedProcedureName = "spIntra_DeleteEmployeeCommentsWPDWithLog";
            var dtResult = WpdEmployeeModel.DeleteComment(commentId, commentBy, storedProcedureName);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        // GET: /WPDEmployee/DeleteComment/ 
        public ActionResult UpdateComment(string commentId, string comment, string commentBy)
        {
            const string storedProcedureName = "spIntra_UpdateWPDEmployeeCommentsWithLog";
            var dtResult = WpdEmployeeModel.UpdateComment(commentId, comment, commentBy, storedProcedureName);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        // GET: /WPDEmployee/GetCommentById/ 
        public ActionResult GetCommentById(string commentId)
        {



            const string storedProcedureName = "spIntra_GetWPDEmployeeComments";
            var dtResult = WpdEmployeeModel.GetCommentbyId(commentId, storedProcedureName);


            var result = _BLL.GetJson(dtResult);

            return result;
        }
    }
}
