﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class NewProcessingFeeController : Controller
    {
        //
        // GET: /NewProcessingFee/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetNewProcessingFees(string dateFrom, string dateTo, string agentID)
        {

            DataTable dtResult = NewProcessingFeesModel.GetNewProcessingFees(dateFrom, dateTo, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetNewProcessingFeesExcluded(string dateFrom, string dateTo, string agentID)
        {

            DataTable dtResult = NewProcessingFeesModel.GetNewProcessingFeesExcluded(dateFrom, dateTo, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
    }
}
