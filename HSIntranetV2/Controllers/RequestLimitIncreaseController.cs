﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class RequestLimitIncreaseController : Controller
    {
        //
        // GET: /RequestLimitIncrease/

        public ActionResult Index(string clerk)
        {
            Session["clerk"] = clerk;
            return View();
        }
        public ActionResult GetRequest(string From, string To, string CustomerId, string Status)
        {
            const string storedProcedureName = "uSp_Local_LimitGetCustomerRequests";
            var dtResult = RequestLimitIncreaseModel.GetRequest(storedProcedureName, From, To, CustomerId, Status);
            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
        public ActionResult UpdateRequest(string Id, string Status, string Comments, string UserAction)
        {
            const string storedProcedureName = "uSp_Local_LimitUpdateCustomerRequest";
            var dtResult = RequestLimitIncreaseModel.UpdateRequest(storedProcedureName, Id, Status, Comments, UserAction);
            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

    }
}
