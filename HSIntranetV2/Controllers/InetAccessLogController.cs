﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class InetAccessLogController : Controller
    {
        //
        // GET: /InetAccessLog/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetInetAccessLog(string dateFrom, string dateTo, string accountId, string ipAddress, string relatedAccounts, string company = "ASI")
        {

            DataTable dtResult = InetAccessLogModel.GetInetAccessLog(dateFrom, dateTo, accountId, ipAddress, relatedAccounts, company);
            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetRelatedAccountsByIP(string accountId, string ipAddress)
        {

            DataTable dtResult = InetAccessLogModel.GetRelatedAccountsByIP(accountId, ipAddress);
            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
