﻿using HSIntranetV2.Models;
using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HSIntranetV2.Controllers
{
    [AllowCrossSiteJson]
    public class HRController : Controller
    {
        //
        // GET: /LiveDealer/
        static HttpPostedFileBase File;
        static HttpPostedFileBase File2;
        static HttpPostedFileBase File3;
        public ActionResult Index()
        {
            return View();
        }






        public ActionResult FillReports(string startDate, string enddate)
        {
            if (startDate == null) throw new ArgumentNullException("startDate");
            if (enddate == null) throw new ArgumentNullException("enddate");
            var ld = new LiveDealer { };




            var login = Properties.Settings.Default.LiveDealerLogin;
            var password = Properties.Settings.Default.LiveDealerPwd;
            var token = ld.Gettoken(login, password);


            /* ReportData
             * Value AllGames = -1
             * Baccarat = 2
             * European Roulete = 4
            */
            var reportdata = ld.GetReportData(token, 14, 28, -1, startDate, enddate);

            if (reportdata.report == null)
            {
                return Content("Error");
            }
            //var reportBaccarat = ld.GetReportData(token, 14, 28, 2, startDate, enddate);
            //var reportEuropean = ld.GetReportData(token, 14, 28, 4, startDate, enddate);



            var completeWl = from reportw in reportdata.report.data
                                 //join r in reportBaccarat.report.data on reportw.externalId equals r.externalId into Ld
                                 //join eu in reportEuropean.report.data on reportw.externalId equals eu.externalId into LdE
                             where reportw.client == "HERITAGE"
                             //from sublist in Ld.DefaultIfEmpty()
                             //from europeanlist in LdE.DefaultIfEmpty()
                             select new
                             {
                                 Cid = reportw.externalId,
                                 winloss = reportw.winLoss,
                                 Baccarat = 0/*(sublist == null ? 0 : sublist.winLoss)*/,
                                 European = 0 /*(europeanlist == null ? 0 : europeanlist.winLoss)*/,
                                 //winlossfinal = (
                                 //reportw.winLoss + (-1 * ((sublist == null ? 0 : sublist.winLoss) + (europeanlist == null ? 0 : europeanlist.winLoss)))
                                 winlossfinal = reportw.winLoss

                             };

            //Create DataTable
            var initialWinLoss = new DataTable("initialWinLoss");
            //Create Column 1: Date
            var dateColumn = new DataColumn { DataType = Type.GetType("System.DateTime"), ColumnName = "Date" };

            //Create Column 2: CustomerId
            var customerIdIdColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Customerid"
            };

            //Create Column 3: CompleteWinLoss
            var totalWinLossColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "CompleteWinLoss"
            };

            //Create Column 4: Baccarat
            var baccarat = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Baccarat"
            };

            //Create Column 5: European Roulette
            var european = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "European"
            };

            //Create Column 6: WinLoss
            var winloss = new DataColumn
            {
                DataType = Type.GetType("System.Int32"),
                ColumnName = "winloss"
            };

            initialWinLoss.Columns.Add(dateColumn);
            initialWinLoss.Columns.Add(customerIdIdColumn);
            initialWinLoss.Columns.Add(totalWinLossColumn);
            initialWinLoss.Columns.Add(baccarat);
            initialWinLoss.Columns.Add(european);
            initialWinLoss.Columns.Add(winloss);

            foreach (var k in completeWl)
            {
                var winlossdataRow = initialWinLoss.NewRow();
                winlossdataRow["Date"] = DateTime.Now.ToString("yyyy-MM-dd");
                winlossdataRow["Customerid"] = k.Cid;
                winlossdataRow["CompleteWinLoss"] = k.winloss;
                winlossdataRow["Baccarat"] = k.Baccarat;
                winlossdataRow["European"] = k.European;
                winlossdataRow["winloss"] = k.winlossfinal;

                initialWinLoss.Rows.Add(winlossdataRow);
            }






            using (var dbConnection = new SqlConnection(_BLL.connString_Betsy))
            {
                dbConnection.Open();
                using (dbConnection)
                {
                    var truncate = new SqlCommand("delete from initialWinLoss", dbConnection);
                    truncate.ExecuteNonQuery();


                    using (var s = new SqlBulkCopy(dbConnection))
                    {
                        s.DestinationTableName = initialWinLoss.TableName;
                        foreach (var column in initialWinLoss.Columns)
                            s.ColumnMappings.Add(column.ToString(), column.ToString());
                        s.WriteToServer(initialWinLoss);
                    }
                }
            }

            var jsonResult = Json(completeWl, "Json");

            //var jsonResult = Json("", "Json");
            string json = new JavaScriptSerializer().Serialize(jsonResult.Data);
            return Content(json);



        }

        public DataTable ImportExceltoDatatable()
        {
            string filepath;
            string path = Server.MapPath("~/App_Data/HRReport.xls");
            //var fileContents = System.IO.File.ReadAllText(Server.MapPath(@"~/App_Data/HSReport.xls"));

            filepath = path;
            // string sqlquery= "Select * From [SheetName$] Where YourCondition";
            const string sqlquery = "Select * From [HRReport$] ";
            var ds = new DataSet();
            string constring = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filepath + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
            var con = new OleDbConnection(constring + "");
            var da = new OleDbDataAdapter(sqlquery, con);
            var dt = new DataTable();
            try
            {
                da.Fill(ds);
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                dt.Columns.Add("Error");
                DataRow _ravi = dt.NewRow();
                _ravi["Error"] = ex.Message;

                dt.Rows.Add(_ravi);
                dt.TableName = "Error";
            }
            return dt;

        }

        public class LiveDealerResult
        {
            public string Result;

        }
        public ActionResult GetHRExcel()
        {


            string path = Server.MapPath("~/App_Data/HRReport.xls");
            // var absolutePath = HttpContext.Server.MapPath(file);
            if (!System.IO.File.Exists(path))
            {
                return Content(path);

            }

            var Test = ImportExceltoDatatable();

            if (Test.TableName == "Error")
            {
                string name = "error";

                var json1 = _BLL.GetJson(Test);

                //var jsonResult = Json(initialWinLoss, "Json");

                //var jsonResult = Json("", "Json");
                // string json = new JavaScriptSerializer().Serialize(jsonResult.Data);
                return json1;
            }
            Test.TableName = "Intra_LatenessBK";

            string result;

            //Create DataTable
            var Intra_Lateness = new DataTable("Intra_LatenessBK");

            var customerIdIdColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "User"
            };

            //Create Column 3: CompleteWinLoss
            var Date1Column = new DataColumn
            {
                DataType = Type.GetType("System.DateTime"),
                ColumnName = "Date1"
            };
            var Date2Column = new DataColumn
            {
                DataType = Type.GetType("System.DateTime"),
                ColumnName = "Date2"
            };
            var Date3Column = new DataColumn
            {
                DataType = Type.GetType("System.DateTime"),
                ColumnName = "Date3"
            };
            var CommentColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Comment"
            };

            Intra_Lateness.Columns.Add(customerIdIdColumn);
            Intra_Lateness.Columns.Add(Date1Column);
            Intra_Lateness.Columns.Add(Date2Column);
            Intra_Lateness.Columns.Add(Date3Column);
            Intra_Lateness.Columns.Add(CommentColumn);


            Intra_Lateness.Merge(Test);




            var connStringBetsy = ConfigurationManager.ConnectionStrings["connString_Betsy"].ConnectionString;
            using (var dbConnection = new SqlConnection(connStringBetsy))
            {
                dbConnection.Open();
                using (dbConnection)
                {
                    var truncate = new SqlCommand("delete from Intra_LatenessBK", dbConnection);
                    truncate.ExecuteNonQuery();


                    using (var s = new SqlBulkCopy(dbConnection))
                    {
                        s.DestinationTableName = Test.TableName;
                        foreach (var column in Test.Columns)
                            s.ColumnMappings.Add(column.ToString(), column.ToString());
                        try
                        {
                            s.WriteToServer(Intra_Lateness);


                            result = "Sucess";


                        }
                        catch (Exception ex)
                        {
                            result = ex.Message.ToString();
                        }
                    }
                }
            }

            if (result != "Sucess")
            {
                return Content("Error");
            }
            var cLiveDealer = new LiveDealer();
            var dtResult = cLiveDealer.GetHRLateness();
            var json = _BLL.GetJson(dtResult);

            //var jsonResult = Json(initialWinLoss, "Json");

            //var jsonResult = Json("", "Json");
            // string json = new JavaScriptSerializer().Serialize(jsonResult.Data);
            return json;

        }






        /*
         Reports with Exclusion
         */


        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            string HRFileLocation = ConfigurationManager.AppSettings["HRFileLocation"];
            // Verify that the user selected a file
            try
            {
                if (file != null && file.ContentLength > 0)
                {
                    // extract only the filename
                    var fileName = Path.GetFileName(file.FileName);
                    // store the file inside ~/App_Data/uploads folder
                    //HRFileLocation
                    var path = Path.Combine(Server.MapPath("~/App_Data/"), fileName);
                    //var path = Path.Combine(HRFileLocation, fileName);
                    //string path = Server.MapPath("~/App_Data/HRReport.xls");

                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    file.SaveAs(path);
                }
                TempData["SuccessMessage"] = "The File was Added Sucesfully";
                return RedirectToAction("Index", "HR");
                //return Content("Addedd Succsfully");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("ReviewErrors", ex.Message);
                //return RedirectToAction("Index");
                return Content(ex.Message);

            }

            // redirect back to the index action to show the form once again
            //return RedirectToAction("Index");
        }



        public ActionResult Upload_Imagefile(string customer, string type, string description, string bin = "", string l4 = "", string typecard = "", string ExpDate = "", string action = "1")
        {
            var Result = new ContentResult();
            string error = "Y";
            try
            {
                if (customer == "Dev")
                {
                    string time = DateTime.Now.ToString("yyyy_MM_dd HH'_'mm'_'ss");
                    var path = "";
                    var path2 = "";
                    var path3 = "";
                    File = this.HttpContext.Request.Files.Get("file");

                    if (File.ContentLength == 0)
                    {
                        error = "Please attach a file";
                    }
                    else if (File.ContentLength > 5000000)
                    {
                        error = "Maximum file size limit: 5MB";
                    }
                    else
                    {
                        if (File.ContentLength != 0)
                        {
                            string typefile1 = "Invalid";
                            switch (File.ContentType.ToString())
                            {
                                case "image/jpeg":
                                    typefile1 = ".jpeg";
                                    break;
                                case "image/png":
                                    typefile1 = ".png";
                                    break;
                                case "application/pdf":
                                    typefile1 = ".pdf";
                                    break;
                                default:
                                    typefile1 = "Invalid";
                                    break;
                            }
                            if (typefile1 != "Invalid")
                            {
                                const string fileName = "HSReport.xls";


                                path = Path.Combine("////VM-INTRANET02/Inetpub/HSIntranetV2/Casino/" + fileName);

                                var data = new byte[File.ContentLength];
                                File.InputStream.Read(data, 0, File.ContentLength);

                                using (var sw = new FileStream(path, FileMode.Create))
                                {
                                    sw.Write(data, 0, data.Length);
                                }

                            }
                            else
                            {
                                error = "Firts file is invalid (only image or pdf)";
                            }
                        }
                        else
                        {
                            error = "Please attach the file in the first field";
                        }

                        if (error == "Y")
                        {
                            //todo

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            var serializer = new JavaScriptSerializer();
            Result = new ContentResult

            {
                ContentEncoding = Encoding.UTF8,

                Content = serializer.Serialize(new
                {
                    error
                })
            };

            Result.ContentType = "application/json";
            return Result;
        }


        public ActionResult GetNewsleterEmail(String state = "2")
        {
            var cLiveDealer = new LiveDealer();
            var dtResult = cLiveDealer.GenerateNewsLetterEmail(state);
            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult updateNewsleterEmail(string email, String state = "1")
        {
            var cLiveDealer = new LiveDealer();
            var dtResult = cLiveDealer.UpdateNewsLetterEmail(email, state);
            var result = _BLL.GetJson(dtResult);
            return result;
        }
    }
}