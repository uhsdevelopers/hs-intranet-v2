﻿using HSIntranetV2.Models;
using System;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HSIntranetV2.Controllers
{


    [AllowCrossSiteJson]
    public class WageringController : Controller
    {
        //
        // GET: /Wagering/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NegativeReport()
        {
            return View("NegativeReport");
        }

        public ActionResult WeekCount()
        {
            return View("WeekCount");
        }

        public ActionResult DepositReport()
        {
            return View("DepositReport");
        }


        public ActionResult PayoutReport()
        {
            return View("PayoutReport");
        }

        public ActionResult LiveReport()
        {
            return View("LiveReport");
        }


        public ActionResult LiveBettingPer()
        {
            return View("LiveBetting");
        }

        public ActionResult AverageReport()
        {
            return View("AverageReport");
        }


        public ActionResult GetScores()
        {
            return View("../scores/scores");
        }

        public ActionResult OpenWagers()
        {
            return View("Openwagers");
        }


        public ActionResult GameRotationReport()
        {
            return View("GameRotationReport");
        }

        public ActionResult BookPlayerCounter()
        {
            return View("BookPlayerCounter");
        }

        public ActionResult PhoneCounter()
        {
            return View();
        }

        public ActionResult CancelWager()
        {
            return View("CancelWager");
        }

        public ActionResult OpenSports()
        {
            return View("OpenSports");
        }

        public ActionResult CancelWager_GetWagers(string customerid, string dateStart, string dateEnd = "")
        {
            const string storedProcedureName = "uSp_Local_CancelWager_GetWagers";
            var dtResult = PNH_ReportModel.CancelWager_GetWagers(storedProcedureName, dateStart, customerid, dateEnd);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult CancelWager_CancelTicket(string ticket)
        {
            const string storedProcedureName = "uSp_Local_CancelWager_CancelTicket";
            var dtResult = PNH_ReportModel.CancelWager_CancelTicket(storedProcedureName, ticket);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult CancelWager_SynchTrans()
        {
            const string storedProcedureName = "uSpHS_SynchArchiveTransactions";
            var dtResult = PNH_ReportModel.CancelWager_SynchTrans(storedProcedureName);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult GetWagerCount(string customerid, string dateStart, string dateEnd = "", string quantity = "1")
        {
            const string storedProcedureName = "usp_intraWagersCount";
            var dtResult = DepositBonusModel.GetWagerCountReport(storedProcedureName, dateStart, customerid, dateEnd, quantity);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }


        public ActionResult GetWeekCount(string dateStart, string dateEnd, string company = "Heritage")
        {
            const string storedProcedureName = "uSp_Local_GetWeeksVolumeResults";
            var dtResult = DepositBonusModel.GetWeekCountReport(storedProcedureName, dateStart, dateEnd, company);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult GetWeekCountDaily(string dateStart, string dateEnd, string company = "Heritage")
        {
            const string storedProcedureName = "uSp_Local_GetWeeksInDaysVolumeResults";
            var dtResult = DepositBonusModel.GetWeekDailyCountReport(storedProcedureName, dateStart, dateEnd, company);


            var result = _BLL.GetJson(dtResult);

            return result;
        }




        public ActionResult GetOpenWagers(string dateStart = "", string site = "Heritage", string dateEnd = "", string customerid = "", bool byDate = false)
        {
            const string storedProcedureName = "uSp_Local_GetOpenWagers";




            var dtResult = DepositBonusModel.GetOpenWagers(storedProcedureName, dateStart, customerid, dateEnd, site, byDate);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }


        public ActionResult GetBetsOnOpenSports()
        {
            const string storedProcedureName = "spIntra_GetOpenSports";




            var dtResult = DepositBonusModel.GetOpenSports(storedProcedureName);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }


        public ActionResult LiveBets(string dateStart, string dateEnd, string ishs = "1")
        {

            if (string.IsNullOrEmpty(dateStart))
            {
                dateStart = null;
                dateEnd = null;
            }

            const string storedProcedureName = "uSp_Local_GetPendingLiveBets";
            var dtResult = DepositBonusModel.GetLiveBets(storedProcedureName, dateStart, dateEnd, ishs);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult DGSLiveBets(string dateStart, string dateEnd, string ishs = "1")
        {

            if (string.IsNullOrEmpty(dateStart))
            {
                dateStart = null;
                dateEnd = null;
            }

            const string storedProcedureName = "spintraGetOpenLive";
            var dtResult = DepositBonusModel.GetDGSLiveBets(storedProcedureName, dateStart, dateEnd, ishs);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetNegativeBalances(string customerid = "", string isCredit = "N", string siteId = "Heritage")
        {
            const string storedProcedureName = "[dbo].[uSp_Intranet_GetNegativeBalances]";
            var dtResult = DepositBonusModel.GetNegativeBalances(storedProcedureName, customerid, isCredit, siteId);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetAverageBalances(string masterAgent, string startDate, string endDate, string valueFrom, string valueTo, string customerid = null, string sportSubtype = null)
        {
            const string storedProcedureName = "[dbo].[uSp_Local_GetSportBookAVGWager]";
            var dtResult = DepositBonusModel.GetAverageBalances(storedProcedureName, masterAgent, startDate, endDate, valueFrom, valueTo, customerid, sportSubtype);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetWagerAmountByRange(string masterAgent, string startDate, string endDate, string valueFrom, string valueTo, string customerid = null, string sportSubtype = null)
        {
            const string storedProcedureName = "[dbo].[uSp_Local_GetSportBookBetweenAmountWager]";
            var dtResult = DepositBonusModel.GetAverageBalances(storedProcedureName, masterAgent, startDate, endDate, valueFrom, valueTo, customerid, sportSubtype);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetSports()
        {
            const string storedProcedureName = "[dbo].[uSp_Local_GetSports]";
            var dtResult = DepositBonusModel.GetAllSports(storedProcedureName);


            var result = _BLL.GetJson(dtResult);

            return result;
        }



        public ActionResult GetSportssubtype(string sport)
        {
            const string storedProcedureName = "[dbo].[uSpPHN_Intranet_GetSportDetails]";
            var dtResult = DepositBonusModel.GetSportSubtype(storedProcedureName, sport);


            var result = _BLL.GetJson(dtResult);

            return result;
        }




        public ActionResult GetScoreslines(string sport)
        {
            const string storedProcedureName = "[dbo].[sp_GetAllscores]";
            var dtResult = DepositBonusModel.GetScores(storedProcedureName, sport);


            var result = _BLL.GetJson(dtResult);

            return result;
        }



        public ActionResult GetCustomerScores(string customerid)
        {
            const string storedProcedureName = "[dbo].[sp_ivrGetCustomersScores]";
            var dtResult = DepositBonusModel.GetCustomerScores(storedProcedureName, customerid);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult DeleteCustomerScores(string customerid, string rotnum)
        {
            const string storedProcedureName = "[dbo].[usp_HS_DeleteGame]";
            var dtResult = DepositBonusModel.DeleteCustomerScores(storedProcedureName, customerid, rotnum);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetScoreslog()
        {
            const string storedProcedureName = "[dbo].[sp_GetscoresLog]";
            var dtResult = DepositBonusModel.GetScores(storedProcedureName);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetSO()
        {
            const string storedProcedureName = "[dbo].[sp_GetscoresLog]";
            var dtResult = DepositBonusModel.GetScores(storedProcedureName);

            var a = new LiveDealer();

            var b = a.GetSOData();
            var result = _BLL.GetJson(dtResult);

            return Content(b);
        }

        public ActionResult GetSOScores()
        {
            const string storedProcedureName = "[dbo].[sp_GetSoscoresLog]";
            var dtResult = DepositBonusModel.GetSOScores(storedProcedureName);

            var a = new LiveDealer();

            // var b = a.GetSOData();
            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetDepositReport(string dateStart, string dateEnd, string method = "BTC", string customerid = "", string siteId = "Heritage")
        {
            const string storedProcedureName = "usp_GetDepositByMethod";
            var dtResult = DepositBonusModel.GetDepositByMethod(storedProcedureName, customerid, method, siteId,
                                                                dateStart, dateEnd);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetPayoutReport(string dateStart, string dateEnd, string method = "Chargeback", string customerid = "", string siteId = "Heritage")
        {
            const string storedProcedureName = "usp_GetPayoutByMethod";
            var dtResult = DepositBonusModel.GetPayoutByMethod(storedProcedureName, customerid, method, siteId,
                                                                dateStart, dateEnd);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetDeletedWagers(string dateStart, string dateEnd, string customerid = "", string clerk = "")
        {
            const string storedProcedureName = "spIntra_SearchDeletedWagers";
            var dtResult = DepositBonusModel.GetDeletedWagers(storedProcedureName, dateStart, customerid, dateEnd, clerk);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetDeletedWagersdetails(string id)
        {
            const string storedProcedureName = "spIntra_SearchDeletedWagersDetails";
            var dtResult = DepositBonusModel.GetDeletedWagersDetails(storedProcedureName, id);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetBet105Customers(string betCustomer = "", string hsCustomer = "")
        {
            const string storedProcedureName = "spSignup_GetBet105Customers";
            var dtResult = DepositBonusModel.GetBet105Customer(storedProcedureName, betCustomer, hsCustomer);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetBet105Email(string betCustomer = "")
        {
            const string storedProcedureName = "spSignup_GetBet105CustomersByEmail";
            var dtResult = DepositBonusModel.GetBet105Email(storedProcedureName, betCustomer);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetBet105PwdLink(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return Content("Error");

            }
            else
            {


                var token = _BLL.GetHashString(cid.ToLower(), "token");
                // var dtResult = DepositBonusModel.GetBet105Email(storedProcedureName, betCustomer);
                // {"firstName":"John", "lastName":"Doe"}
                var serializer = new JavaScriptSerializer();

                serializer.MaxJsonLength = Int32.MaxValue;
                var customerToken = new _BLL.CustomerToken { Customerid = cid, token = token };


                var json = serializer.Serialize(customerToken);

                var Result = new ContentResult
                {
                    ContentEncoding = Encoding.UTF8,
                    Content = json,
                    ContentType = "application/json"
                };

                return Result;

            }



        }

        public ActionResult GetSimilarAccounts(string customer)
        {
            const string storedProcedureName = "uSp_Local_ValidateDuplicatedCustomerInfo";
            var dtResult = DepositBonusModel.GetSimilarAccount(storedProcedureName, customer);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetCashBoost(string customer = "")
        {
            const string storedProcedureName = "spintra_GetCashBoost";
            var dtResult = DepositBonusModel.GetCashBoost(storedProcedureName, customer);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetLiveBettingReport(string dateStart, string dateEnd, string tw = "Live", string siteId = "Heritage", string customerid = "")
        {
            const string storedProcedureName = "uSp_Local_GetLiveBettingCustomerPerf";
            var dtResult = DepositBonusModel.GetLiveBettingReport(storedProcedureName, tw, customerid, siteId,
                                                                dateStart, dateEnd);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetGameRotationReport(string rotnum, string status, string dateStart, string dateEnd)
        {
            const string storedProcedureName = "sp_siteGetGameInformation";
            var dtResult = DepositBonusModel.GetRotationName(storedProcedureName, rotnum, status,
                                                                dateStart, dateEnd);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetCOWRequestByWeek(string dateStart, string StatusID = "0", string Evaluator = "")
        {
            const string storedProcedureName = "spintra_GetCallOfWeekRequestByWeek";
            var dtResult = DepositBonusModel.GetCallOfWeekRequestbyWeek(storedProcedureName, dateStart, StatusID, Evaluator);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetCOWRequestByclerk(string Clerk, string Evaluator)
        {
            const string storedProcedureName = "spintra_GetCallOfWeekRequestByClerk";
            var dtResult = DepositBonusModel.GetCallOfWeekRequestbyClerk(storedProcedureName, Clerk, Evaluator);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetCallOfWeekDetails(string id)
        {
            const string storedProcedureName = "spintra_GetCallOfWeekDetails";
            var dtResult = DepositBonusModel.GetCallOfWeekDetails(storedProcedureName, id);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetCallOfWeekVotesDetails(string id)
        {
            const string storedProcedureName = "spintra_GetCallOfWeekVotesDetails";
            var dtResult = DepositBonusModel.GetCallOfWeekDetails(storedProcedureName, id);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetCallOfWeekvotes(string dateStart)
        {
            const string storedProcedureName = "spintra_GetCallOfWeekVotesByWeek";
            var dtResult = DepositBonusModel.GetCallOfWeekvotes(storedProcedureName, dateStart);


            var result = _BLL.GetJson(dtResult);

            return result;
        }



        public ActionResult GetBookPlayerCounter(string week, string dateStart, string dateEnd, int company = 1)
        {
            string storedProcedureName = "uSp_Local_GetBookPlayerCounter";

            if (company == 2)
            {
                storedProcedureName = "uSp_Intranet_GetPlayerCountByBook";
            }

            var dtResult = DepositBonusModel.GetBookPlayerCounter(storedProcedureName, week, dateStart, dateEnd, company);


            var result = _BLL.GetJson(dtResult);

            return result;
        }
        [OutputCache(Duration = 2000, VaryByParam = "week")]
        public ActionResult GetPhoneCounter(string week, string dateStart, string dateEnd, string Agent = "99W_MASTER")
        {
            string storedProcedureName = "uSp_Intranet_GetPlayerCountByAgent";



            var dtResult = DepositBonusModel.GetBookPhoneCounter(storedProcedureName, week, dateStart, dateEnd, Agent);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult AddCallReview(string clerk, string callid, string vote, string comment)
        {
            const string storedProcedureName = "spIntra_EmployeesAddCallReview2018";
            var dtResult = DepositBonusModel.AddCallReview(storedProcedureName, clerk, callid, vote, comment);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult AddCowRequest(string clerk, string session, string dateStart, string extension, string description)
        {
            const string storedProcedureName = "spintra_AddCallOfWeek";
            var dtResult = DepositBonusModel.AddCallOfWeekRequest(storedProcedureName, clerk, session, dateStart, extension, description);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult updateCowRequest(string id, string session = "", string dateStart = "", string extension = "", string description = "")
        {
            const string storedProcedureName = "spintra_UpdateCallOfWeekRequest";
            var dtResult = DepositBonusModel.UpdateCallOfWeekRequest(storedProcedureName, id, session, dateStart, extension, description);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult updateCowStatus(string id, string status)
        {
            const string storedProcedureName = "spintra_UpdateCallOfWeekStatus";
            var dtResult = DepositBonusModel.UpdateCallOfWeekStatus(storedProcedureName, id, status);


            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult GetSoccerRiskFree(string dateStart, string dateEnd, string league)
        {
            const string storedProcedureName = "spintra_FreeBet";
            var dtResult = RiskFree.GetSoccerRiskFree(storedProcedureName, dateStart, dateEnd, league);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetSoccerRiskFreeDetails(string dateStart, string dateEnd, string league)
        {
            const string storedProcedureName = "spintra_FreeBetDetails";
            var dtResult = RiskFree.GetSoccerRiskFreeDetails(storedProcedureName, dateStart, dateEnd, league);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetSoccerRiskFreeDetailsByCustomer(string dateStart, string dateEnd, string league, string customerid)
        {
            const string storedProcedureName = "spintra_FreeBetDetailsByCustomer";
            var dtResult = RiskFree.GetSoccerRiskFreeDetailsByCustomer(storedProcedureName, dateStart, dateEnd, league, customerid);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}