﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class NorthbetController : Controller
    {
        // GET: Northbet
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetLeagueVolume(string league, string Agent = "NBETMA", string startdate = null, string enddate = null)
        {


            const string storedProcedureName = "uSp_Local_GetLeagueVolume";
            var dtResult = Northbet.Northbet_GetVolume(storedProcedureName, Agent, league, startdate, enddate);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
    }
}