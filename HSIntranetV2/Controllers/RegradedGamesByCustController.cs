﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{

    public class RegradedGamesByCustController : Controller
    {
        //
        // GET: /RegradedGamesByCust/

        public ActionResult Index()
        {
            return View();
        }

        // GET: /RegradedGamesByCust/GetResult/ 
        public ActionResult GetResult(string customerID, string startDate, string endDate)
        {

            RegradedGamesByCustModel cRegraded = new RegradedGamesByCustModel();
            cRegraded.customerID = customerID;
            cRegraded.startDate = startDate;
            cRegraded.endDate = endDate;

            DataTable dtResult = RegradedGamesByCustModel.GetResult(cRegraded);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

    }
}
