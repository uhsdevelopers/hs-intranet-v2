﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class AccessCountController : Controller
    {
        //
        // GET: /AccessCount/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetAccessCount(string From, string Limit)
        {
            string storedProcedureName = "[uSp_Local_InetGetAccessCount]";

            var dtResult = AccessCountModel.GetAccessCount(storedProcedureName, From, Limit);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

    }
}
