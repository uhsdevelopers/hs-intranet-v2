﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers.WagerWeb
{
    [AllowCrossSiteJson]
    public class WWController : Controller
    {
        // GET: WW
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult WagerWebCustReport(string user)
        {
            return View();
        }

        public ActionResult GetReferFriend(string dateFrom, string dateTo, string Referred = "")
        {

            DataTable dtResult = WagerWebLTVReportModel.GetReferAFriendList(dateFrom, dateTo, Referred);

            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult GetWagerWebCustReport(string dateFrom, string dateTo, string Customerid = "")
        {
            if (string.IsNullOrEmpty(dateFrom) || string.IsNullOrEmpty(dateTo))
            {
                return Content("{ 'Error':'Invalid parameter' }");
            }
            DataTable dtResult = WagerWebLTVReportModel.GetWagerWebCustReport(dateFrom, dateTo, Customerid);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult UpdateReferFriend(string id, string customerid, string wascontacted = "0", string Contactedby = "", string CallStatus = "", string comments = "", string bonus = "0")
        {

            DataTable dtResult = WagerWebLTVReportModel.UpdateReferAFriendList(id, customerid, wascontacted, Contactedby, CallStatus, comments);

            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult UpdateReferFriendTX(string id, string bonus = "0")
        {

            DataTable dtResult = WagerWebLTVReportModel.UpdateReferAFriendTx(id, bonus);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}