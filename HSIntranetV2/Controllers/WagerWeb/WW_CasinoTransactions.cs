﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class WW_CasinoTransactionsController : Controller
    {
        // GET: /WW_CasinoTransactions/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetWWCasinoTransactions(string dateFrom, string dateTo, string masterAgentID, string customerID, string isNonPosted)
        {

            DataTable dtResult = WW_CasinoTransactionsModel.GetWWCasinoTransactions(dateFrom, dateTo, masterAgentID, customerID, isNonPosted);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
    }
}