﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class WW_WLReportController : Controller
    {
        //
        // GET: /WW_WLReport /

        public ActionResult IndexWW()
        {
            return View();
        }

        public ActionResult GetWLSummaryWW(string AgentID, string DateFrom, string DateTo)
        {
            DataTable dtResult = WW_WLReportModel.GetWLSummary(AgentID, DateFrom, DateTo, "WagerWeb");

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult GetWLFigureDetail(string AgentID, string DateFrom, string DateTo, string SingleAgent, string reportType)
        {
            DataTable dtResult = WW_WLReportModel.GetWLFigureDetail(AgentID, DateFrom, DateTo, "WagerWeb", reportType);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
