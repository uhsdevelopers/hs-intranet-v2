﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class WagerWebLTVReportController : Controller
    {
        //
        // GET: /Acc_HeadCount/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetLTVReport(string dateFrom, string dateTo, string agentID, string CustomerID)
        {

            DataTable dtResult = WagerWebLTVReportModel.GetLTVReport(dateFrom, dateTo, agentID, CustomerID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
