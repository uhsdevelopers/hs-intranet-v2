﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    [AllowCrossSiteJson]
    public class WW_ConvertionRateController : Controller
    {
        //
        // GET: /WW_ConvertionRate/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetRate(string dateFrom, string dateTo)
        {
            Session["FirstDeposit"] = null;

            DataSet dsResult = WwConvertionRateModel.GetRate(dateFrom, dateTo, "WagerWeb");
            DataTable dtResult = new DataTable();

            dtResult = dsResult.Tables[0];

            Session["FirstDeposit"] = dsResult.Tables[1];

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult GetFirstDeposit()
        {
            DataTable dtResult = new DataTable();

            dtResult = Session["FirstDeposit"] as DataTable;

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult GetRetention(string dateFrom, string company = "wagerweb")
        {
            const string storedProcedureName = "uSp_Local_GetWWCountDeposit";

            var dtResult = WwConvertionRateModel.GetRetention(storedProcedureName, dateFrom, company);


            var result = _BLL.GetJson(dtResult);

            return result;



        }

    }
}
