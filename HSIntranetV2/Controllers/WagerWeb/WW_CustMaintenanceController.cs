﻿using HSIntranetV2.Models;
using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class WW_CustMaintenanceController : Controller
    {
        //
        // GET: /WWCustMaintenance/

        public ActionResult Index()
        {

            Session["AgentID"] = "WagerWeb";

            return View();
        }

        public ActionResult ManualTransaction()
        {

            Session["AgentID"] = "WagerWeb";

            return View();
        }

        public ActionResult SetQuickLimitByCust(string customerID, string quickLimit)
        {
            var dtagentid = WW_CustMaintenanceModel.GetAgentIdByCust(customerID);
            var agentId = "";

            var distinctRows = (from DataRow dRow in dtagentid.Rows
                                select new { col1 = dRow["agentid"] }).Distinct();

            foreach (var row in distinctRows)
            {
                agentId = row.col1.ToString();


            }

            if (string.IsNullOrEmpty(agentId))
            {
                return Content("Invalid AgentId");
            }

            DataTable dtResult = WW_CustMaintenanceModel.SetQuickLimitByCust(agentId, customerID, quickLimit);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetQuickLimitByCust(string customerID)
        {
            var dtagentid = WW_CustMaintenanceModel.GetAgentIdByCust(customerID);
            var agentId = "";

            var distinctRows = (from DataRow dRow in dtagentid.Rows
                                select new { col1 = dRow["agentid"] }).Distinct();

            foreach (var row in distinctRows)
            {
                agentId = row.col1.ToString();


            }

            if (string.IsNullOrEmpty(agentId))
            {
                return Content("Invalid AgentId");
            }


            DataTable dtResult = WW_CustMaintenanceModel.GetQuickLimitByCust(agentId, customerID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        [AllowCrossSiteJson]
        public ActionResult GetCashBackReport(string dateFrom, string dateTo)
        {

            DataTable dtResult = WagerWebLTVReportModel.GetCashBackReport(dateFrom, dateTo);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        [AllowCrossSiteJson]
        public ActionResult GetCashBackReportCid(string customerid, string dateFrom, string dateTo)
        {

            DataTable dtResult = WagerWebLTVReportModel.GetCashBackReportByCustomer(customerid, dateFrom, dateTo);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        [AllowCrossSiteJson]
        public ActionResult GetAvailableCashBackByCustomer(string customerid)
        {

            DataTable dtResult = WagerWebLTVReportModel.GetAvailableCashBackByCustomer(customerid);

            var result = _BLL.GetJson(dtResult);

            return result;
        }



        [AllowCrossSiteJson]
        public ActionResult AddBtcPayment(string customerId, string btcAddress, string btcAmount, string btcHash, double usdAmount,
            string comments, string sid = "3")

        {

            string btcFromAddress = string.Empty;
            string walletId = string.Empty;
            const string currency = "USD";
            const double rate = 0.0;
            string status = string.Empty;

            var result = new ContentResult { ContentEncoding = Encoding.UTF8, Content = string.Empty };

            if (customerId == null)
            {
                return Content("Invalid CustomerId");
            }
            if (btcAddress == null) return Content("Invalid BTCAddress");
            if (btcAmount == null) return Content("Invalid BTC Amount");
            if (btcHash == null) return Content("Invalid BTCHash");

            DataTable dtResult = WagerWebLTVReportModel.AddBtcPayment(customerId, btcAddress, btcAmount, btcHash,
                                                                      usdAmount,
                                                                      currency, rate, status, comments,
                                                                      btcFromAddress, walletId, sid);

            result = _BLL.GetJson(dtResult);

            return result;
        }

        [AllowCrossSiteJson]
        public ActionResult GetBtcPayment(string customerId, string startDate, string endDate, string sportbookid = "3")

        {
            var result = new ContentResult { ContentEncoding = Encoding.UTF8, Content = string.Empty };

            if (customerId == null)
            {
                return Content("Invalid CustomerId");
            }
            if (startDate == null)
            {
                return Content("Invalid date");
            }
            if (endDate == null)
            {
                endDate = DateTime.Now.ToString();
                var dtResult = WagerWebLTVReportModel.GetBtcPayment(customerId, startDate, endDate, sportbookid);

                result = _BLL.GetJson(dtResult);


            }
            return result;
        }

        [AllowCrossSiteJson]
        public ActionResult GetBTCtransactions(string dateFrom, string dateTo, string filterConfirmed)
        {
            var result = new ContentResult { ContentEncoding = Encoding.UTF8, Content = string.Empty };


            if (dateFrom == null)
            {
                return Content("Invalid date");
            }
            if (dateTo == null)
            {
                dateTo = DateTime.Now.ToString();

            }

            var dtResult = WagerWebLTVReportModel.GetBTCtransactions(dateFrom, dateTo, filterConfirmed);

            result = _BLL.GetJson(dtResult);

            return result;
        }
    }
}
