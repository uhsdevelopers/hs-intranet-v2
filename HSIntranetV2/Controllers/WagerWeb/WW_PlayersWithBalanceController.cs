﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class WW_PlayersWithBalanceController : Controller
    {
        //
        // GET: /WW_PlayersWithBalance/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetBalance(string dateFrom, string agentID)
        {

            DataTable dtResult = WW_PlayersWithBalanceModel.GetBalance(dateFrom, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
