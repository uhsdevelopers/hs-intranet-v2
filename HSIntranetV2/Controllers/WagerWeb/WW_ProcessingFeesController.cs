﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class WW_ProcessingFeesController : Controller
    {
        //
        // GET: /WW_ProcessingFees/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetWWProcessingFees(string dateFrom, string dateTo, string agentID)
        {
            DataTable dtResult = WW_ProcessingFeesModel.Acc_WWProcessingFees(dateFrom, dateTo, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
