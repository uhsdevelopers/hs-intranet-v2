﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class WW_ComissionDetailsController : Controller
    {
        //
        // GET: /WW_ComissionDetails/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetWWComissions(string dateFrom, string dateTo, string agentID)
        {

            DataTable dtResult = WW_ComissionDetailsModel.GetWWComissions(dateFrom, dateTo, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
    }
}
