﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class WW_HeadCountController : Controller
    {
        //
        // GET: /WW_HeadCount/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetHeadCount(string dateFrom, string dateTo, string agentID)
        {

            DataTable dtResult = WW_HeadCountModel.GetHeadCount(dateFrom, dateTo, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
