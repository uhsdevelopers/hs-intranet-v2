﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class SevenController : Controller
    {

        public ActionResult Admin()
        {
            return View("Admin");
        }

        public ActionResult NonPosted()
        {
            return View("NonPosted");
        }



        public ActionResult GetPlayerData(string dateStart, string dateEnd, string player)
        {



            var cLiveDealer = new LiveDealer();
            var dtResult = cLiveDealer.GetSevenData(player, dateStart, dateEnd);
            var result = dtResult;
            return Content(result);
        }



        public ActionResult GetNonPostedPlayers(string company, string provider = "", string player = "")
        {
            const string storedProcedureName = "spArptNonPostedCasinoTransactions";
            var cLiveDealer = new LiveDealer();
            var dtResult = cLiveDealer.GetNonPostedPlayers(storedProcedureName, player, provider, company);


            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}