﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class DGSController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult GetScoreSetting(string id = "1")
        {

            DataTable dtResult = Dgs.GetScoreSetting(id);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult SetScoreSetting(string id = "1", string enabled = "1")
        {

            DataTable dtResult = Dgs.SetScoreSetting(id, enabled);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetScoreNoSent(string id = "1")
        {

            DataTable dtResult = Dgs.GetScoreNoSent(id);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult SendQueue(string id)
        {

            DataTable dtResult = Dgs.SendQueue(id);

            var result = _BLL.GetJson(dtResult);

            return result;
        }


        public ActionResult DGS_AgentsBalance()
        {
            return View();
        }
        public ActionResult DGS_Users()
        {
            return View();
        }
        public ActionResult GetAgentsBalance(string dateFrom, string agentID)
        {

            DataTable dtResult = Dgs.GetAgentsBalance(dateFrom, agentID);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult ReleaseUser()
        {

            DataTable dtResult = Dgs.ReleaseUser();

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetLoggedUsers()
        {

            DataTable dtResult = Dgs.GetLoggedUsers();

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult DGSService(string serviceID, string status)
        {

            DataTable dtResult = Dgs.DGSService(serviceID, status);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetGamesQueue()
        {

            DataTable dtResult = Dgs.LinePubQueue_GetIdList();

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult GetGamesQueueInfo(string idQueue)
        {

            DataTable dtResult = Dgs.LinePubQueue_GetInfo(idQueue);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}