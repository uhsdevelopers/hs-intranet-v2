﻿using HSIntranetV2.Models;
using HSIntranetV2.Models.Genius;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.Mvc;
using static HSIntranetV2.Models.Genius.Genius;

namespace HSIntranetV2.Controllers.CallCenter
{
    public class GeniusController : Controller
    {
        // GET: Genius

        public ActionResult TeamsStats()
        {
            ViewData["clerk"] = "intranet";
            return View();
        }

        public ActionResult TeamsActiveCalls()
        {
            ViewData["clerk"] = "intranet";
            return View();
        }

        public ActionResult AgentStats()
        {
            ViewData["clerk"] = "intranet";
            return View();
        }

        public ActionResult Index(string usr, string site = "1")
        {
            if (string.IsNullOrEmpty(usr))
            {
                return Content("Error: Your Session has  Expired!");
            }

            ViewData["clerk"] = usr;
            ViewData["site"] = site;

            var userDetails = new cl_UserDetails();
            userDetails = GetUserDetails(usr);
            if (userDetails != null)
            {
                ViewData["NickName"] = userDetails.Nickname;
                ViewData["PhoneExt"] = userDetails.PhoneExt;
            }
            else
            {
                ViewData["NickName"] = usr;
                ViewData["PhoneExt"] = "NA";
            }


            return View();
        }

        public ActionResult SaveStatus(string status, string reason, string usr)
        {
            var result = "";
            if (status == null) result = "Status null";
            if (reason == null) result = "Reason null";
            if (usr == null) result = "USR null";



            const string storedProcedure = "sp_insertTimeClock";
            var dtResult = Genius.AddTimeClock(storedProcedure, usr, status, reason);

            var result2 = _BLL.GetJson(dtResult);
            return result2;

        }

        public ActionResult GetTeamsStats(string companyid = "1")
        {
            const string storedProcedure = "sp_GetTeamsStats";
            var dtResult = Genius.GetTeamsData(storedProcedure, companyid);

            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult GetTeamsActiveCalls(string companyid = "1")
        {
            const string storedProcedure = "sp_GetTeamsActiveCalls";
            var dtResult = Genius.GetTeamsData(storedProcedure, companyid);

            var result = _BLL.GetJson(dtResult);
            return result;
        }

        public ActionResult GetTeamsAgentStats(string companyid = "1")
        {
            const string storedProcedure = "sp_GetTeamsAgentsStats";
            var dtResult = Genius.GetTeamsData(storedProcedure, companyid);

            var result = _BLL.GetJson(dtResult);
            return result;
        }
        public ActionResult GetTodayData(string user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));


            const string storedProcedure = "sp_GetTodayData";
            var dtResult = Genius.GetTodayData(storedProcedure, user);

            var result = _BLL.GetJson(dtResult);
            return result;
        }


        public cl_UserDetails GetUserDetails(string user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            var userDetails = new cl_UserDetails();

            const string storedProcedure = "sp_GetUserDetails";
            var dtResult = Genius.GetUserDetails(storedProcedure, user);



            foreach (DataRow row in dtResult.Rows)
            {
                userDetails.User = row["Loginname"].ToString();
                userDetails.Nickname = row["NickName"].ToString();
                userDetails.PhoneExt = row["PhoneExt"].ToString();
                // userDetails.Name = row["Employee"].ToString();
                //userDetails.Department = row["Department"].ToString();
            }

            //var result = _BLL.GetJson(dtResult);
            return userDetails;
        }


        public ActionResult GetAgentStatusExcel()
        {
            ContentResult json1 = new ContentResult();
            string path = Server.MapPath("~/App_Data/avoxi/agent-status.xls");
            if (!System.IO.File.Exists(path))
            {
                return Content(path);

            }

            var dt = ImportExceltoDatatable(path);

            if (dt.TableName == "Error")
            {
                // string name = "error";

                json1 = _BLL.GetJson(dt);



            }
            dt.TableName = "TimeClockUser";

            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(dt);
            Response.Write(JSONresult);

            var DtAgentStatus = new DataTable("TimeClockUser");

            var AgentNameColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "AgentName"
            };

            //Create Column 3: CompleteWinLoss



            var StartTimeColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "StartTime"
            };
            var EndTimeColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "EndTime"
            };

            var DurationHoursColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "DurationHours"
            };
            var DurationSColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "DurationS"
            };

            var StatusNameColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Status"
            };
            var ReasonNameColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Reason"
            };

            DtAgentStatus.Columns.Add(AgentNameColumn);
            DtAgentStatus.Columns.Add(StartTimeColumn);
            DtAgentStatus.Columns.Add(EndTimeColumn);
            DtAgentStatus.Columns.Add(DurationHoursColumn);
            DtAgentStatus.Columns.Add(DurationSColumn);
            DtAgentStatus.Columns.Add(StatusNameColumn);
            DtAgentStatus.Columns.Add(ReasonNameColumn);


            string result;

            // DtAgentStatus.Merge(dt, true, MissingSchemaAction.Ignore);

            var connStringBetsy = ConfigurationManager.ConnectionStrings["ConnectionString_CallCenter"].ConnectionString;

            //using (var dbConnection = new SqlConnection(connStringBetsy))
            //{
            //    dbConnection.Open();
            //    using (dbConnection)
            //    {
            //        //var truncate = new SqlCommand("delete from NGPlayers", dbConnection);
            //        // truncate.ExecuteNonQuery();


            //        using (var s = new SqlBulkCopy(dbConnection))
            //        {
            //            s.DestinationTableName = dt.TableName;
            //            foreach (DataColumn col in dt.Columns)
            //            {
            //                s.ColumnMappings.Add(col.ColumnName, col.ColumnName);
            //            }

            //            try
            //            {
            //                s.WriteToServer(DtAgentStatus);
            //                result = "Sucess";
            //            }
            //            catch (Exception ex)
            //            {
            //                result = ex.Message.ToString();
            //            }
            //        }
            //    }
            //}

            foreach (var k in dt.AsEnumerable())
            {
                var winlossdataRow = DtAgentStatus.NewRow();
                winlossdataRow["AgentName"] = k.Table.Columns[0];
                winlossdataRow["StartTime"] = k.Table.Columns[1];
                winlossdataRow["EndTime"] = k.Table.Columns[2];
                winlossdataRow["DurationHours"] = k.Table.Columns[3];
                winlossdataRow["DurationS"] = k.Table.Columns[4];
                winlossdataRow["Status"] = k.Table.Columns[5];
                winlossdataRow["Reason"] = k.Table.Columns[6];


                DtAgentStatus.Rows.Add(winlossdataRow);
            }


            using (var dbConnection = new SqlConnection(_BLL.ConnectionString_CallCenter))
            {
                dbConnection.Open();
                using (dbConnection)
                {
                    var truncate = new SqlCommand("delete from TimeClockUser", dbConnection);
                    truncate.ExecuteNonQuery();


                    using (var s = new SqlBulkCopy(dbConnection))
                    {
                        s.DestinationTableName = DtAgentStatus.TableName;
                        foreach (var column in DtAgentStatus.Columns)
                            s.ColumnMappings.Add(column.ToString(), column.ToString());
                        s.WriteToServer(DtAgentStatus);
                    }
                }
            }

            //if (result != "Sucess")
            //{
            //    return Content("Error");
            //}
            var jsonResult = Json(dt, "Json");

            //var jsonResult = Json("", "Json");
            //string json = new JavaScriptSerializer().Serialize(jsonResult.Data);
            return Content(jsonResult.Data.ToString());
        }

        public ActionResult GetAgentStatusExcelv2()
        {
            ContentResult json1 = new ContentResult();
            string path = Server.MapPath("~/App_Data/avoxi/agent-status.xlsx");
            if (!System.IO.File.Exists(path))
            {
                return Content(path);

            }

            var dt = ImportExceltoDatatable(path);

            if (dt.TableName == "Error")
            {
                // string name = "error";

                json1 = _BLL.GetJson(dt);



            }
            dt.TableName = "TimeClockUser";

            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(dt);
            Response.Write(JSONresult);

            var DtAgentStatus = new DataTable("TimeClockUser");

            var AgentNameColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "AgentName"
            };

            //Create Column 3: CompleteWinLoss



            var StartTimeColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "StartTime"
            };
            var EndTimeColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "EndTime"
            };

            //var DurationHoursColumn = new DataColumn
            //{
            //    DataType = Type.GetType("System.String"),
            //    ColumnName = "DurationHours"
            //};
            var DurationSColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "DurationS"
            };

            var StatusNameColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Status"
            };
            var ReasonNameColumn = new DataColumn
            {
                DataType = Type.GetType("System.String"),
                ColumnName = "Reason"
            };

            DtAgentStatus.Columns.Add(AgentNameColumn);
            DtAgentStatus.Columns.Add(StartTimeColumn);
            DtAgentStatus.Columns.Add(EndTimeColumn);
            // DtAgentStatus.Columns.Add(DurationHoursColumn);
            DtAgentStatus.Columns.Add(DurationSColumn);
            DtAgentStatus.Columns.Add(StatusNameColumn);
            DtAgentStatus.Columns.Add(ReasonNameColumn);


            string result;

            DtAgentStatus.Merge(dt, true, MissingSchemaAction.Ignore);

            var connStringBetsy = ConfigurationManager.ConnectionStrings["ConnectionString_CallCenter"].ConnectionString;

            using (var dbConnection = new SqlConnection(connStringBetsy))
            {
                dbConnection.Open();
                using (dbConnection)
                {
                    var truncate = new SqlCommand("delete from TimeClockUser", dbConnection);
                    truncate.ExecuteNonQuery();


                    using (var s = new SqlBulkCopy(dbConnection))
                    {
                        s.DestinationTableName = dt.TableName;
                        foreach (DataColumn col in dt.Columns)
                        {
                            s.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                        }

                        try
                        {
                            s.WriteToServer(DtAgentStatus);
                            result = "Sucess";
                        }
                        catch (Exception ex)
                        {
                            result = ex.Message.ToString();
                        }
                    }
                }
            }





            if (result != "Sucess")
            {
                return Content("Error");
            }
            var jsonResult = Json(dt, "Json");

            //var jsonResult = Json("", "Json");
            //string json = new JavaScriptSerializer().Serialize(jsonResult.Data);
            return Content(jsonResult.Data.ToString());
        }

        public DataTable ImportExceltoDatatable(string filepath)
        {

            const string sqlquery = "Select * From [agentstatus$] ";
            var ds = new DataSet();
            string constring = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filepath + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
            var con = new OleDbConnection(constring + "");
            var da = new OleDbDataAdapter(sqlquery, con);
            var dt = new DataTable();
            try
            {
                da.Fill(ds);
                dt = ds.Tables[0];

            }
            catch (Exception ex)
            {
                dt.Columns.Add("Error");
                DataRow _ravi = dt.NewRow();
                _ravi["Error"] = ex.Message;

                dt.Rows.Add(_ravi);
                dt.TableName = "Error";
            }
            return dt;

        }



    }
}