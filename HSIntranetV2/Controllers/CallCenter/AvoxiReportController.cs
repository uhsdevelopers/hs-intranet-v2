﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class AvoxiReportController : Controller
    {
        //
        // GET: /Acc_HeadCount/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAvoxiReport(string dateFrom, string dateTo, string agentID, string department)
        {

            DataTable dtResult = AvoxiReportModel.GetAvoxiReport(dateFrom, dateTo, agentID, department);

            var result = _BLL.GetJson(dtResult);

            return result;
        }
        public ActionResult GetAgents()
        {

            DataTable dtResult = AvoxiReportModel.GetAgents();

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
