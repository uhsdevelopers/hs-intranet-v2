﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class InternalTransfersController : Controller
    {
        //
        // GET: /InternalTransfers/

        public ActionResult Index()
        {
            return View();
        }

        // GET: /InternalTransfers/GetASIfigures/ 
        public ActionResult GetASIfigures(string startDate)
        {
            string storedProcedureName = "HS_Intranet_Transfers";
            DataTable dtResult = ASI_ImportModel.GetASIfigures(startDate, "", "0", storedProcedureName);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        // GET: /InternalTransfers/GetASIdetails/ 
        public ActionResult GetASIdetails(string startDate, string description)
        {
            string storedProcedureName = "HS_Intranet_Transfer_details";
            DataTable dtResult = ASI_ImportModel.GetASIdetails(startDate, description, "", storedProcedureName);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        // GET: /InternalTransfers/GetTransferTotals/ 
        public ActionResult GetTransferTotals(string startDate, string description)
        {
            string storedProcedureName = "HS_Intranet_Transfer_Totals";
            DataTable dtResult = ASI_ImportModel.GetASIfigures(startDate, "", "0", storedProcedureName);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

    }
}
