﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace ASIwebReports.Controllers
{
    public class ShortcutController : Controller
    {
        //
        // GET: /Shortcut/

        public ActionResult Index()
        {
            Session["AgentID"] = "XTOPA";

            return View();
        }


        #region Customer Maintenance

        // GET: /Shortcut/getCustomerMaintenance
        public ActionResult GetCustomerMaintenance(string customerId)
        {
            const string agentId = "XTOPA";


            if (customerId == null)
            {
                return Content("Error");
            }
            else
            {
                var dtResult = ShortcutModel.GetCustomerMaintenance(agentId, customerId);

                var result = _BLL.GetJson(dtResult);


                return result;
            }
        }

        // GET: /Shortcut/setCustomerMaintenance
        public ActionResult SetCustomerMaintenance(string customerID, string password, string creditLimit, string TempCreditAdj, string TempCreditAdjExpDate, string Active)
        {
            var result = new ContentResult();
            const string agentId = "XTOPA";

            if (customerID != "Select Customer" && customerID != "")
            {
                var dtResult = ShortcutModel.setCustomerMaintenance(agentId, customerID, password, creditLimit, TempCreditAdj, TempCreditAdjExpDate, Active);

                result = _BLL.GetJson(dtResult);
            }

            return result;
        }

        public ActionResult SetCustomerDeposit(string customerID, string TranCode, string TranType, string Amount, string Description, /*string DailyFigureDate,*/ string isFreePlay)
        {
            var result = new ContentResult();
            const string agentId = "XTOPA";



            if (customerID != "Select Customer" && customerID != "")
            {
                var dtResult = ShortcutModel.setCustomerDeposit(agentId, customerID, TranCode, TranType, Amount, Description, /*DailyFigureDate,*/ isFreePlay);

                result = _BLL.GetJson(dtResult);
            }

            return result;
        }



        public ActionResult GetCustomersByAgent()
        {
            var result = new ContentResult();

            if (Session["AgentID"] != null)
            {

                DataTable dtResult = _BLL.GetCustomersByAgentRegal(Session["AgentID"].ToString());

                result = _BLL.GetJson(dtResult);

            }

            return result;
        }


        #endregion

    }
}
