﻿using HSIntranetV2.Models;
using System.Data;

namespace HSIntranetV2.Controllers
{
    public class RiskFree
    {
        internal static DataTable GetSoccerRiskFree(string storedProcedureName, string dateStart, string dateEnd, string league)
        {

            string[,] spParameters = { { "@StartDate", dateStart }, { "@EndDate", dateEnd }, { "@League", league } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetSoccerRiskFreeDetails(string storedProcedureName, string dateStart, string dateEnd, string league)
        {

            string[,] spParameters = { { "@StartDate", dateStart }, { "@EndDate", dateEnd }, { "@League", league } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }

        internal static DataTable GetSoccerRiskFreeDetailsByCustomer(string storedProcedureName, string dateStart, string dateEnd, string league, string customerid)
        {

            string[,] spParameters = { { "@StartDate", dateStart }, { "@EndDate", dateEnd }, { "@League", league }, { "@CustomerID", customerid } };

            var cDal = new _BLL.cDAL(_BLL.connString_Betsy, storedProcedureName, spParameters);

            var dtResult = _DAL.getDataTable(cDal);

            return dtResult;

        }
    }
}