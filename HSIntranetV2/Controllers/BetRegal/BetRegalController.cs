﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    [AllowCrossSiteJson]
    public class BetRegalController : Controller
    {
        // GET: BetRegal
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BrEmail()
        {
            return View();
        }

        public ActionResult BasicEmail()
        {
            return View("BasicEmailList");
        }

        public ActionResult CasinoPlayers()
        {
            return View("CasinoPlayers");
        }

        public ActionResult OntarioPlayers()
        {
            return View();
        }

        public ActionResult GetRevenue()
        {
            return View();
        }

        public ActionResult BrGetEmail(string condition = "0", string Feature = "0", string startdate = null, string customerid = "")
        {


            const string storedProcedureName = "uSp_BetRegal_GetFundedNonFunded";
            var dtResult = BetRegal.BetRegal_GetFundedNonFunded(storedProcedureName, condition, Feature, startdate, customerid);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        public ActionResult BRGetCasinoPlayers(string Agent = "Betregal")
        {


            const string storedProcedureName = "spIntra_GetcasinoPlayers";
            var dtResult = BetRegal.BrGetCasinoPlayers(storedProcedureName, Agent);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        [OutputCache(Duration = 2000)]
        public ActionResult BRGetOntarioPlayers()
        {


            const string storedProcedureName = "GetBetregalOntarioInfo";
            var dtResult = BetRegal.BRGetOntarioPlayers(storedProcedureName);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        [OutputCache(Duration = 2000)]
        public ActionResult BRGetRevenue(string dateStart, string dateEnd)
        {

            if (string.IsNullOrEmpty(dateStart) || string.IsNullOrEmpty(dateEnd))
            {
                Response.StatusCode = 404;
                return Content("Error");
            }
            const string storedProcedureName = "uSp_BetRegalCA_GetRevenue";
            var dtResult = BetRegal.BrGetRevenue(storedProcedureName, dateStart, dateEnd);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }


        public ActionResult BRGetLocalActivity(string customerid)
        {


            const string storedProcedureName = "uSp_Local_BRActivity";
            var dtResult = BetRegal.BrGetLocalActivy(storedProcedureName, customerid);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
        public ActionResult BRGetWagers(string customerid, string operation)
        {


            const string storedProcedureName = "uSp_Local_BRGetWagers";
            var dtResult = BetRegal.BrGetWagers(storedProcedureName, customerid, operation);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
    }
}