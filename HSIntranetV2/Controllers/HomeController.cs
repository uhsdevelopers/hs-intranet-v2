﻿using HSIntranetV2.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
namespace HSIntranetV2.Controllers
{

    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
            base.OnActionExecuting(filterContext);
        }
    }

    [AllowCrossSiteJson]
    public class HomeController : Controller
    {

        public class Menu
        {
            public string Name { get; set; }
            public string MenuPosition { get; set; }
            public string Description { get; set; }
            public string Link { get; set; }
            public string LinkID { get; set; }
            public int ShowinMenu { get; set; }

        }

        public class Currency
        {
            public int id { get; set; }
            public string ISO { get; set; }
            public string symbol { get; set; }
            public double exchangeRate { get; set; }
        }

        public class MetaData
        {
            public List<Currency> Currencies { get; set; }
        }

        public class Report
        {
            public List<Menu> children { get; set; }
            public string description { get; set; }

        }

        public class Status
        {
            public string code { get; set; }
            public string description { get; set; }
        }

        public class RootObject
        {
            public List<Report> report { get; set; }
            //public Status status { get; set; }
        }

        //
        // GET: /Home/

        public ActionResult Index()
        {

            return View();
        }

        public ActionResult ServerList()
        {

            return View("Server");
        }

        // GET: /Home/GetCustomersByAgent
        public ActionResult GetCustomersByAgent()
        {
            var Result = new ContentResult();

            if (Session["AgentID"] != null)
            {

                DataTable dtResult = _BLL.GetCustomersByAgent(Session["AgentID"].ToString());




                Result = _BLL.GetJson(dtResult);

            }

            return Result;
        }

        // GET: /Home/GetCustomersByAgent
        public ActionResult GetCustomersByAgentww(string agentid = "wagerweb")
        {
            var Result = new ContentResult();



            DataTable dtResult = _BLL.GetCustomersByAgentWw(agentid);




            Result = _BLL.GetJson(dtResult);



            return Result;
        }

        // GET: /Home/GetEmployeeLinks
        public ActionResult GetEmployeeLinks(string LoginName)
        {
            // var Result = new ContentResult();
            List<RootObject> menus = new List<RootObject>();
            var pos = "";

            RootObject Raiz = new RootObject();
            Report r = new Report();

            List<Menu> ListMenu = new List<Menu>();
            List<Report> ListReport = new List<Report>();

            DataTable dtResult = _BLL.GetEmployeeLinks(LoginName);

            foreach (DataRow row in dtResult.Rows)
            {

                Menu menues = new Menu();
                if (string.Equals(pos, row["Name"].ToString()))
                {
                    //Report r = new Report();

                    menues.Description = row[2].ToString();
                    menues.Link = row[3].ToString();
                    menues.LinkID = row[4].ToString();
                    menues.MenuPosition = row[2].ToString();
                    menues.Name = row["Name"].ToString();
                    menues.ShowinMenu = Convert.ToInt32(row[5]);

                    ListMenu.Add(menues);

                }
                else
                {

                    if (pos != "")
                    {
                        r = new Report();

                        r.description = pos;
                        r.children = ListMenu;

                        ListReport.Add(r);

                        ListMenu = new List<Menu>();

                    }

                    menues.Description = row[0].ToString();
                    menues.Link = row[1].ToString();
                    menues.LinkID = row[2].ToString();
                    menues.MenuPosition = row[3].ToString();
                    menues.Name = row["Name"].ToString();

                    ListMenu.Add(menues);

                }

                pos = row["Name"].ToString();
            }


            Raiz.report = ListReport;

            //menus.Add(ListReport);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(Raiz);

            var Result = new ContentResult { ContentEncoding = Encoding.UTF8, Content = json };

            Result.ContentType = "application/json";

            return Result;
        }


        // GET: /Home/GetCustomersByAgent

        public ActionResult AddHorsesSite(string id, string SiteID, string provider)
        {
            var Result = new ContentResult();

            DataTable dtResult = _BLL.AddHorsesSite(id, SiteID, provider);
            Result = _BLL.GetJson(dtResult);



            return Result;
        }

        // GET: /Home/GetCustomersByAgent

        public ActionResult GetHorsesSiteList(string provider)
        {
            var Result = new ContentResult();

            DataTable dtResult = _BLL.GetHorsesSiteList(provider);
            Result = _BLL.GetJson(dtResult);



            return Result;
        }
        // GET: Home/GetEmployeeInfo?LoginName=matt
        public ActionResult GetEmployeeInfo(string LoginName)
        {
            var Result = new ContentResult();



            DataTable dtResult = _BLL.GetEmployeeInfo(LoginName);




            Result = _BLL.GetJson(dtResult);



            return Result;
        }

        // GET: Home/GetEmployeeList
        public ActionResult GetEmployeeList(string DepartmentID = "ALL", string ClerkList = "1", string OnlyActive = "1")
        {
            var Result = new ContentResult();



            DataTable dtResult = _BLL.GetEmployeeList(DepartmentID, ClerkList, OnlyActive);




            Result = _BLL.GetJson(dtResult);



            return Result;
        }

        // GET: /Home/CloneEmployeeLink
        public ActionResult CloneEmployeeLink(string Source, string New)
        {
            var Result = new ContentResult();



            DataTable dtResult = _BLL.CloneEmployeeLink(Source, New);




            Result = _BLL.GetJson(dtResult);



            return Result;
        }

        // GET: Home/GetEmployeeList
        public ActionResult GetServerList(string serviceId = "0")
        {
            var Result = new ContentResult();



            DataTable dtResult = _BLL.GetServerList(serviceId);




            Result = _BLL.GetJson(dtResult);



            return Result;
        }
    }
}
