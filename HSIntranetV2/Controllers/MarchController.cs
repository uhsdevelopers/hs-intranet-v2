﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    [AllowCrossSiteJson]
    public class MarchController : Controller
    {
        //
        // GET: /March/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Teams()
        {
            return View("Teams");
        }

        public ActionResult NFL()
        {
            return View("contest");
        }

        public string Welcome()
        {

            return "This is the Welcome action method... <br>";
        }


        // GET: /MarchController/AddCustomerSelection/ 

        public ActionResult AddCustomerSelection(string customerId, string contestId, string teamId, string intValue)
        {

            const string storedProcedureName = "sp_UpdateContestCustomerSelectionsIntValuesJL";
            if (customerId == null || contestId == null || teamId == null || intValue == null)
            {

                var result = new EmptyResult();

                return result;

            }
            else
            {
                var dtResult = MarchContest.AddCustomerSelection(customerId, contestId, teamId, intValue, storedProcedureName);

                var result = _BLL.GetJson(dtResult);

                return result;
            }
        }


        // GET: /MarchController/DeleteCustomerSelection/ 
        public ActionResult DeleteCustomerSelection(string customerId, string contestId, string teamId)
        {

            const string storedProcedureName = "sp_deleteContestCustomerSelectionsIntValues";
            if (customerId == null || contestId == null || teamId == null)
            {

                var result = new EmptyResult();

                return result;

            }
            else
            {
                var dtResult = MarchContest.DeleteSelection(customerId, contestId, teamId, storedProcedureName);

                var result = _BLL.GetJson(dtResult);

                return result;
            }
        }


        public ActionResult GetCustomer(int sportbookid = 1)
        {

            const string storedProcedureName = "sp_RetrieveAllCustomers";

            var dtResult = MarchContest.GetCustomerList(sportbookid, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult CloseContest(int sportbookid = 1)
        {

            const string storedProcedureName = "spcUpdContestCloseStatus";

            var dtResult = MarchContest.ContestCloseStatus(storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }



        public ActionResult GetAllCustomer(int sportbookid = 1)
        {

            const string storedProcedureName = "sp_RetrieveAllCustomersContest";

            var dtResult = MarchContest.GetAllCustomerList(sportbookid, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }


        public ActionResult GetAllCustomerPoints(int sportbookid = 1)
        {

            const string storedProcedureName = "sp_RetrieveAllCustomersContestPoints";

            var dtResult = MarchContest.GetAllCustomerList(sportbookid, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }


        public ActionResult GetLeaderBoard(int sportbookid = 1)
        {

            const string storedProcedureName = "sp_RetrieveContestFullLeaderBoard2";

            var dtResult = MarchContest.GetAllCustomerList(sportbookid, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }


        public ActionResult GetAllGames(int sportbookid = 1, string contestID = "4", string showGraded = "N")
        {

            const string storedProcedureName = "sp_GetParentGames";

            var dtResult = MarchContest.GetAllGames(sportbookid, contestID, showGraded, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult GradeoneGame(string contestGameID, string winnerID, int sportbookid = 1, string contestID = "4", string showGraded = "N")
        {

            const string storedProcedureName = "sp_GradeContestGame";

            var dtResult = MarchContest.GradeOneGame(sportbookid, contestID, contestGameID, winnerID,
                                                     storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }


        public ActionResult GetallTeams(int sportbookid = 1)
        {

            const string storedProcedureName = "usp_GetTeams";

            var dtResult = MarchContest.GetallTeams(sportbookid, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult UpdateTeams(int GameID, string Team1ID, string Team2ID, string RecId1, string RecId2, int sportbookid = 1)
        {

            const string storedProcedureName = "sp_updateTeams2018";

            var dtResult = MarchContest.UpdateTeams(GameID, Team1ID, Team2ID, RecId1, RecId2, sportbookid, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult GetCustomerBracket(string roundNumber, string customerID, int sportbookid = 1)
        {

            const string storedProcedureName = "sp_RetrieveCustomerBracketTeams";

            var dtResult = MarchContest.GetCustomerBracketTeams(sportbookid, roundNumber, customerID, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }
        /*
         * NFL Pick the pross
         * 
         * */

        public ActionResult GetallNFLTeams(string week, int sportbookid = 1)
        {

            const string storedProcedureName = "Usp_GetNFLGames";

            var dtResult = MarchContest.GetNFLGames(week, sportbookid, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult addNFLContest(string idcontest, string week, string closeDateTime, int sportbookid = 1)
        {

            const string storedProcedureName = "usp_addContest";

            var dtResult = MarchContest.AddNflWeek(idcontest, week, closeDateTime, sportbookid, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }


        public ActionResult addEventCursor(string idcontest, string week, string closeDateTime, int sportbookid = 1)
        {

            //DateTime date = DateTime.ParseExact(closeDateTime, "dd/MM/yyyy", null);

            //DateTime strdate = date.AddHours(18);

            const string storedProcedureName = "USP_AddEvent_Cursor";



            var dtResult = MarchContest.AddEvent_Cursor(idcontest, week, closeDateTime, sportbookid, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }


        public ActionResult AddParticipantinEvent(string idcontest, int sportbookid = 1)
        {

            const string storedProcedureName = "USP_AddParticipantinEvent";

            var dtResult = MarchContest.AddParticipantinEvent(idcontest, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult GetParticipantinEvent(string idcontest, int sportbookid = 1)
        {

            const string storedProcedureName = "USP_GetParticipantinEvent";

            var dtResult = MarchContest.GetParticipantinEvent(idcontest, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult AddContestinEvents(string idcontest)
        {

            const string storedProcedureName = "USP_AddContestinEvents";

            var dtResult = MarchContest.AddContestinEvents(idcontest, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }






        public ActionResult AddLines(string idcontest, string type, int sportbookid = 1)
        {

            const string storedProcedureName = "USP_AddLines";

            var dtResult = MarchContest.AddLinesSpread(idcontest, type, storedProcedureName);

            var result = _BLL.GetJsonv2(dtResult);

            return result;

        }

        public ActionResult AddTotalLines(string idcontest)
        {

            const string storedProcedureName = "USP_AddLinesTotals";

            var dtResult = MarchContest.AddTotalLines(idcontest, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult GetLines(string idcontest)
        {

            const string storedProcedureName = "USP_GetContestEventLines";

            var dtResult = MarchContest.AddTotalLines(idcontest, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }


        public ActionResult InsertEvent(string Team1ID, string Team2ID, string closeDateTime, string week, string type, string idcontest, int sportbookid = 1)
        {

            const string storedProcedureName = "USP_InsertEvent";

            var dtResult = MarchContest.InsertEvent(Team1ID, Team2ID, closeDateTime, week, type, idcontest, sportbookid, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult InsertTotal(string IdEvent, string type)
        {

            const string storedProcedureName = "USP_InsertTotal";

            var dtResult = MarchContest.InsertTotal(IdEvent, type, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }



        public ActionResult CloneEvents(string ParentContestId, string SourceContestId, string sportbookid = "3")
        {

            const string storedProcedureName = "USP_CloneEvents";

            var dtResult = MarchContest.CloneEvents(ParentContestId, SourceContestId, sportbookid, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult DynamicLines(string ContestId)
        {

            const string storedProcedureName = "usp_updateDinamycLines";

            var dtResult = MarchContest.DynamicLines(ContestId, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult GetContestEventList(string idcontest, int sportbookid = 1)
        {

            const string storedProcedureName = "USP_GetContestEventList";

            var dtResult = MarchContest.GetContestEventList(idcontest, sportbookid, storedProcedureName);

            var result = _BLL.GetJsonv2(dtResult);

            return result;

        }



        public ActionResult GetContestinEvents(string idcontest, int sportbookid = 1)
        {

            const string storedProcedureName = "USP_GetContestinEvents";

            var dtResult = MarchContest.GetContestinEvents(idcontest, sportbookid, storedProcedureName);

            var result = _BLL.GetJsonv2(dtResult);

            return result;

        }


        public ActionResult AddLinesTotals(string idcontest, int sportbookid = 1)
        {

            const string storedProcedureName = "USP_AddLinesTotals";

            var dtResult = MarchContest.AddLinesTotals(idcontest, storedProcedureName);

            var result = _BLL.GetJsonv2(dtResult);

            return result;

        }


        public ActionResult GetContestList(string idcontest)
        {

            const string storedProcedureName = "USP_GetContestWeekList";

            var dtResult = MarchContest.GetContestList(idcontest, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;

        }

        public ActionResult GetTeamList(string League)
        {

            const string storedProcedureName = "Usp_GetTeams";

            var dtResult = MarchContest.GetTeamsList(League, storedProcedureName);

            var result = _BLL.GetJsonv2(dtResult);

            return result;

        }

        public ActionResult GetSurvivorWeekList(string ContestID)
        {

            const string storedProcedureName = "SurVAdminGetWeek";

            var dtResult = MarchContest.GetSurvivorWeekList(ContestID, storedProcedureName);

            var result = _BLL.GetJsonv2(dtResult);

            return result;

        }

        public ActionResult GetSurvivorGamesByWeek(string ContestID)
        {

            const string storedProcedureName = "SurVAdminGetGamesByWeek";

            var dtResult = MarchContest.GetSurvivorGamesByWeek(ContestID, storedProcedureName);

            var result = _BLL.GetJsonv2(dtResult);

            return result;

        }



        public ActionResult SurvivorAddweek(string ContestID, string weekid)
        {

            const string storedProcedureName = "SurVAddEvents";

            var dtResult = MarchContest.SurvivorAddweek(ContestID, weekid, storedProcedureName);

            var result = _BLL.GetJsonv2(dtResult);

            return result;

        }

        public ActionResult SurvivorUpdateStatus(string weekid)
        {

            const string storedProcedureName = "SurVChangeStatus";

            var dtResult = MarchContest.SurvivorUpdateStatus(weekid, storedProcedureName);

            var result = _BLL.GetJsonv2(dtResult);

            return result;

        }



    }
}
