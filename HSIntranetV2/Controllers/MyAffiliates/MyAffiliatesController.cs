﻿using HSIntranetV2.Models;
using HSIntranetV2.Models.MyAffiliates;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Linq;




namespace HSIntranetV2.Controllers
{
    [AllowCrossSiteJson]
    public class AffiliatesController : Controller
    {

        public ActionResult GetFreeSpinReport()
        {
            ViewBag.Message = string.Empty;
            return View();
        }


        [AllowCrossSiteJson]
        public async Task<List<MyAffiliate.MyAffASI>> GetCustomersMYAFF(string DateFrom, string DateTo, string Company, string Affiliate)
        {

            var AffUrl = System.Configuration.ConfigurationManager.AppSettings["MyAffNew"];
            var AffUrl2 = System.Configuration.ConfigurationManager.AppSettings["MyAffSearch"];
            var Affuserl = System.Configuration.ConfigurationManager.AppSettings["AffUser"];
            var AffPWD = System.Configuration.ConfigurationManager.AppSettings["AffPwd"];

            string combined = Affuserl + ":" + AffPWD;
            byte[] byteArray2 = Encoding.ASCII.GetBytes(combined);
            string authorizationHeaderValue = "Basic " + Convert.ToBase64String(byteArray2);

            ServicePointManager.Expect100Continue = false;
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls | SecurityProtocolType.Tls13;




            var newTransaction = string.Empty;

            var options = new RestClientOptions(AffUrl)
            {
                Timeout = TimeSpan.FromSeconds(10),
            };
            var client = new RestClient(options);
            var request = new RestRequest(AffUrl2, Method.Post);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Authorization", authorizationHeaderValue);
            request.AddParameter("FROM_DATE", DateFrom);
            request.AddParameter("TO_DATE", DateTo);
            request.AddParameter("SOURCE_GROUP", Company);

            if (Affiliate != "ALL")
            {
                request.AddParameter("AFFILIATE_NAME", Affiliate);
            }


            RestResponse response = await client.ExecuteAsync(request);

            newTransaction = response.Content;


            var result = newTransaction;

            XDocument doc = XDocument.Parse(result);

            var playerInfos = doc.Descendants("PLAYER")
                .Select(player => new MyAffiliate.MyAffASI
                {
                    ClientId = player.Attribute("CLIENT_ID").Value,
                    Affiliate = player.Attribute("AFFILIATE_USERNAME").Value
                })
                .ToList();

            return playerInfos;
        }




        [AllowCrossSiteJson]
        public async Task<ActionResult> SearchCustomer(string DateFrom, string DateTo, string Company, string Affiliate = "ALL")
        {
            // var manager = Globals.Manager;
            //var contentType = manager.FormatContentType("json"); // text/xml
            string callback = HttpContext.Request.QueryString["callback"] ?? HttpContext.Request.Form["callback"];

            var playerInfos = await GetCustomersMYAFF(DateFrom, DateTo, Company, Affiliate);

            // var ASIPlayer = uSpGetASILIST(DateFrom, DateTo, Company, Affiliate);
            List<MyAffiliate.MyAffASI> ASIPlayer = new List<MyAffiliate.MyAffASI>();

            var ASIDT = MyAffiliate.uSpGetASILIST(DateFrom, DateTo, Company, Affiliate);

            foreach (DataRow row in ASIDT.Rows)
            {
                MyAffiliate.MyAffASI player = new MyAffiliate.MyAffASI
                {
                    Affiliate = row["affiliate"].ToString(),
                    ClientId = row["clientid"].ToString()
                };
                ASIPlayer.Add(player);
            }

            var difference1 = playerInfos.Except(ASIPlayer, new ClientComparer()).ToList();

            var difference2 = ASIPlayer.Except(playerInfos, new ClientComparer()).ToList();

            var combinedDifference = difference1.Union(difference2).ToList();



            var replacedString = JsonConvert.SerializeObject(combinedDifference);



            var responsetoUI = "[" + replacedString + "]";

            var Result = new ContentResult
            {
                ContentEncoding = Encoding.UTF8,
                Content = responsetoUI,
                ContentType = "application/json"
            };

            return Content(responsetoUI);



        }


        public ActionResult GetFreeSpins(string startDate, string endDate)
        {
            if (string.IsNullOrEmpty(startDate))
            {
                Response.StatusCode = 404;
                return Content("Error");
            }

            if (string.IsNullOrEmpty(endDate))
            {
                endDate = System.DateTime.Today.AddDays(1).ToString();
            }

            var dtResult = MyAffiliate.GetFreeSpins(startDate, endDate);


            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        private string ContentJSONResult(object obj)
        {
            string jsonString = "" + Newtonsoft.Json.JsonConvert.SerializeObject(obj) + "";
            return jsonString;
        }

        public class ClientComparer : IEqualityComparer<MyAffiliate.MyAffASI>
        {
            public bool Equals(MyAffiliate.MyAffASI x, MyAffiliate.MyAffASI y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (x == null || y == null) return false;
                return x.ClientId == y.ClientId;
            }

            public int GetHashCode(MyAffiliate.MyAffASI obj)
            {
                return obj.ClientId.GetHashCode();
            }
        }




    }
}