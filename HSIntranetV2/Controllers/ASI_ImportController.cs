﻿using HSIntranetV2.Models;
using System.Data;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class ASI_ImportController : Controller
    {
        //
        // GET: /ASI_Import/

        public ActionResult Index()
        {
            return View();
        }

        // GET: /ASI_Import/GetASIfigures/ 
        public ActionResult GetASIfigures(string startDate, string agentID, string makeBackup)
        {
            string storedProcedureName = "HS_Intranet_ASIimport";

            DataTable dtResult = ASI_ImportModel.GetASIfigures(startDate, agentID, makeBackup, storedProcedureName);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        // GET: /ASI_Import/GetASIdetails/ 
        public ActionResult GetASIdetails(string startDate, string description, string agentID)
        {
            string storedProcedureName = "HS_Intranet_ASIdetails";

            DataTable dtResult = ASI_ImportModel.GetASIdetails(startDate, description, agentID, storedProcedureName);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }

        // GET: /ASI_Import/GetASIdetails/ 
        public ActionResult GetExcelInfo()
        {
            string storedProcedureName = "HS_Intranet_ASIimport_ExcelInfo";

            DataTable dtResult = ASI_ImportModel.GetASIexcelInfo(storedProcedureName);

            var Result = _BLL.GetJson(dtResult);

            return Result;
        }
    }
}
