﻿using HSIntranetV2.Models;
using System.Web.Mvc;

namespace HSIntranetV2.Controllers
{
    public class NoActionGamesController : Controller
    {
        //
        // GET: /NoActionGames/

        public ActionResult Index()
        {
            return View();
        }

        public string Welcome()
        {
            return "This is the Welcome action method...";
        }


        public ActionResult GetGameList(bool isGame, string isfullGame)
        {
            string storedProcedureName = isGame ? "spSite_GetGamesWithNoMoneyWagered" : "spSite_GetPropsWithNoMoneyWagered";

            var dtResult = NoActionGames.GetGamesList(storedProcedureName, isfullGame);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult UpdateAllGame(bool isGame)
        {
            var storedProcedureName = isGame ? "spSite_UpdateGamesWithNoMoneyWagered_" : "spSite_UpdatePropsWithNoMoneyWagered_";

            var dtResult = NoActionGames.UpdateAllGames(storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

        public ActionResult UpdateGame(bool isGame, string gamenum)
        {
            const string storedProcedureName = "spSite_UpdateGamesWithNoMoneyWagered";

            var dtResult = NoActionGames.UpdateGames(gamenum, isGame, storedProcedureName);

            var result = _BLL.GetJson(dtResult);

            return result;
        }

    }
}
